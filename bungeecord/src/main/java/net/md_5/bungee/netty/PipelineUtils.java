package net.md_5.bungee.netty;

import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.*;
import io.netty.channel.epoll.Epoll;
import io.netty.channel.epoll.EpollDatagramChannel;
import io.netty.channel.epoll.EpollServerSocketChannel;
import io.netty.channel.epoll.EpollSocketChannel;
import io.netty.channel.socket.nio.NioDatagramChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.util.AttributeKey;
import io.netty.util.internal.PlatformDependent;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.BungeeServerInfo;
import net.md_5.bungee.UserConnection;
import net.md_5.bungee.Util;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ListenerInfo;
import net.md_5.bungee.connection.InitialHandler;
import net.md_5.bungee.protocol.*;

import java.net.InetSocketAddress;
import java.util.concurrent.ThreadFactory;
import java.util.logging.Level;

public class PipelineUtils {
    public static final AttributeKey<ListenerInfo> LISTENER = AttributeKey.valueOf("ListerInfo");
    public static final AttributeKey<UserConnection> USER = AttributeKey.valueOf("User");
    public static final AttributeKey<BungeeServerInfo> TARGET = AttributeKey.valueOf("Target");
    public static final Base BASE = new Base();
    public static final String TIMEOUT_HANDLER = "timeout";
    public static final String PACKET_DECODER = "packet-decoder";
    public static final String PACKET_ENCODER = "packet-encoder";
    public static final String BOSS_HANDLER = "inbound-boss";
    public static final String ENCRYPT_HANDLER = "encrypt";
    public static final String DECRYPT_HANDLER = "decrypt";
    public static final String FRAME_DECODER = "frame-decoder";
    public static final String FRAME_PREPENDER = "frame-prepender";
    public static final String LEGACY_DECODER = "legacy-decoder";
    public static final String LEGACY_KICKER = "legacy-kick";
    private static final KickStringWriter legacyKicker = new KickStringWriter();
    private static final Varint21LengthFieldPrepender framePrepender = new Varint21LengthFieldPrepender();
    private static final int LOW_MARK = Integer.getInteger("net.md_5.bungee.low_mark", 524288);
    private static final int HIGH_MARK = Integer.getInteger("net.md_5.bungee.high_mark", 2097152);
    private static final WriteBufferWaterMark MARK = new WriteBufferWaterMark(LOW_MARK, HIGH_MARK);

    public static final ChannelInitializer<Channel> SERVER_CHILD = new ChannelInitializer<Channel>() {
        protected void initChannel(Channel ch) {
            if ((BungeeCord.getInstance().getConnectionThrottle() != null) && (BungeeCord.getInstance().getConnectionThrottle().throttle(((InetSocketAddress) ch.remoteAddress()).getAddress()))) {
                ch.close();
                return;
            }

            ListenerInfo listener = ch.attr(PipelineUtils.LISTENER).get();

            PipelineUtils.BASE.initChannel(ch);
            ch.pipeline().addBefore("frame-decoder", "legacy-decoder", new LegacyDecoder());
            ch.pipeline().addAfter("frame-decoder", "packet-decoder", new MinecraftDecoder(Protocol.HANDSHAKE, true, ProxyServer.getInstance().getProtocolVersion()));
            ch.pipeline().addAfter("frame-prepender", "packet-encoder", new MinecraftEncoder(Protocol.HANDSHAKE, true, ProxyServer.getInstance().getProtocolVersion()));
            ch.pipeline().addBefore("frame-prepender", "legacy-kick", PipelineUtils.legacyKicker);
            ch.pipeline().get(HandlerBoss.class).setHandler(new InitialHandler(BungeeCord.getInstance(), listener));

            if (listener.isProxyProtocol()) {
                ch.pipeline().addFirst(new io.netty.handler.codec.haproxy.HAProxyMessageDecoder());
            }
        }
    };

    private static boolean epoll;

    static {
        if ((!PlatformDependent.isWindows()) && (Boolean.parseBoolean(System.getProperty("bungee.epoll", "true")))) {
            ProxyServer.getInstance().getLogger().info("Not on Windows, attempting to use enhanced EpollEventLoop");

            if ((epoll = Epoll.isAvailable())) {
                ProxyServer.getInstance().getLogger().info("Epoll is working, utilising it!");
            } else {
                ProxyServer.getInstance().getLogger().log(Level.WARNING, "Epoll is not working, falling back to NIO: {0}", Util.exception(Epoll.unavailabilityCause()));
            }
        }
    }

    public static EventLoopGroup newEventLoopGroup(int threads, ThreadFactory factory) {
        return epoll ? new io.netty.channel.epoll.EpollEventLoopGroup(threads, factory) : new io.netty.channel.nio.NioEventLoopGroup(threads, factory);
    }

    public static Class<? extends io.netty.channel.ServerChannel> getServerChannel() {
        return epoll ? EpollServerSocketChannel.class : NioServerSocketChannel.class;
    }

    public static Class<? extends Channel> getChannel() {
        return epoll ? EpollSocketChannel.class : NioSocketChannel.class;
    }

    public static Class<? extends Channel> getDatagramChannel() {
        return epoll ? EpollDatagramChannel.class : NioDatagramChannel.class;
    }

    public static final class Base
            extends ChannelInitializer<Channel> {
        public void initChannel(Channel ch) {
            try {
                ch.config().setOption(io.netty.channel.ChannelOption.IP_TOS, 24);
            } catch (ChannelException ignored) {
            }


            ch.config().setAllocator(PooledByteBufAllocator.DEFAULT);
            ch.config().setWriteBufferWaterMark(PipelineUtils.MARK);

            ch.pipeline().addLast("timeout", new ReadTimeoutHandler(BungeeCord.getInstance().config.getTimeout(), java.util.concurrent.TimeUnit.MILLISECONDS));
            ch.pipeline().addLast("frame-decoder", new Varint21FrameDecoder());
            ch.pipeline().addLast("frame-prepender", PipelineUtils.framePrepender);

            ch.pipeline().addLast("inbound-boss", new HandlerBoss());
        }
    }
}


