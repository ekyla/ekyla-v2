package net.md_5.bungee.netty;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import net.md_5.bungee.compress.PacketCompressor;
import net.md_5.bungee.protocol.MinecraftDecoder;
import net.md_5.bungee.protocol.MinecraftEncoder;
import net.md_5.bungee.protocol.PacketWrapper;
import net.md_5.bungee.protocol.packet.Kick;

import java.net.InetSocketAddress;


public class ChannelWrapper {
    private final Channel ch;
    private InetSocketAddress remoteAddress;
    private volatile boolean closed;
    private volatile boolean closing;

    public ChannelWrapper(ChannelHandlerContext ctx) {
        this.ch = ctx.channel();
        this.remoteAddress = ((InetSocketAddress) this.ch.remoteAddress());
    }

    public InetSocketAddress getRemoteAddress() {
        return this.remoteAddress;
    }

    public void setRemoteAddress(InetSocketAddress remoteAddress) {
        this.remoteAddress = remoteAddress;
    }

    public boolean isClosed() {
        return this.closed;
    }

    public boolean isClosing() {
        return this.closing;
    }

    public void setProtocol(net.md_5.bungee.protocol.Protocol protocol) {
        this.ch.pipeline().get(MinecraftDecoder.class).setProtocol(protocol);
        this.ch.pipeline().get(MinecraftEncoder.class).setProtocol(protocol);
    }

    public void setVersion(int protocol) {
        this.ch.pipeline().get(MinecraftDecoder.class).setProtocolVersion(protocol);
        this.ch.pipeline().get(MinecraftEncoder.class).setProtocolVersion(protocol);
    }

    public void write(Object packet) {
        if (!this.closed) {
            if ((packet instanceof PacketWrapper)) {
                ((PacketWrapper) packet).setReleased(true);
                this.ch.writeAndFlush(((PacketWrapper) packet).buf, this.ch.voidPromise());
            } else {
                this.ch.writeAndFlush(packet, this.ch.voidPromise());
            }
        }
    }

    public void markClosed() {
        this.closed = this.closing;
    }

    public void close() {
        close(null);
    }

    public void close(Object packet) {
        if (!this.closed) {
            this.closed = this.closing ;

            if ((packet != null) && (this.ch.isActive())) {
                this.ch.writeAndFlush(packet).addListeners(ChannelFutureListener.FIRE_EXCEPTION_ON_FAILURE, ChannelFutureListener.CLOSE);
                this.ch.eventLoop().schedule((Runnable) ChannelWrapper.this.ch::close, 250L, java.util.concurrent.TimeUnit.MILLISECONDS);

            } else {
                this.ch.flush();
                this.ch.close();
            }
        }
    }

    public void delayedClose(final Kick kick) {
        if (!this.closing) {
            this.closing = true;


            this.ch.eventLoop().schedule(() -> ChannelWrapper.this.close(kick), 250L, java.util.concurrent.TimeUnit.MILLISECONDS);
        }
    }


    public void addBefore(String baseName, String name, io.netty.channel.ChannelHandler handler) {
        com.google.common.base.Preconditions.checkState(this.ch.eventLoop().inEventLoop(), "cannot add handler outside of event loop");
        this.ch.pipeline().flush();
        this.ch.pipeline().addBefore(baseName, name, handler);
    }

    public Channel getHandle() {
        return this.ch;
    }

    public void setCompressionThreshold(int compressionThreshold) {
        if ((this.ch.pipeline().get(PacketCompressor.class) == null) && (compressionThreshold != -1)) {
            addBefore("packet-encoder", "compress", new PacketCompressor());
        }
        if (compressionThreshold != -1) {
            this.ch.pipeline().get(PacketCompressor.class).setThreshold(compressionThreshold);
        } else {
            this.ch.pipeline().remove("compress");
        }

        if ((this.ch.pipeline().get(net.md_5.bungee.compress.PacketDecompressor.class) == null) && (compressionThreshold != -1)) {
            addBefore("packet-decoder", "decompress", new net.md_5.bungee.compress.PacketDecompressor());
        }
        if (compressionThreshold == -1) {
            this.ch.pipeline().remove("decompress");
        }
    }
}


