/*    */ package net.md_5.bungee.netty.cipher;
/*    */ 
/*    */ import net.md_5.bungee.jni.cipher.BungeeCipher;
/*    */ 
/*    */ public class CipherDecoder extends io.netty.handler.codec.MessageToMessageDecoder<io.netty.buffer.ByteBuf>
/*    */ {
/*    */   private final BungeeCipher cipher;
/*    */   
/*    */   public CipherDecoder(BungeeCipher cipher) {
/* 10 */     this.cipher = cipher;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   protected void decode(io.netty.channel.ChannelHandlerContext ctx, io.netty.buffer.ByteBuf msg, java.util.List<Object> out)
/*    */     throws Exception
/*    */   {
/* 19 */     out.add(this.cipher.cipher(ctx, msg));
/*    */   }
/*    */   
/*    */   public void handlerRemoved(io.netty.channel.ChannelHandlerContext ctx)
/*    */     throws Exception
/*    */   {
/* 25 */     this.cipher.free();
/*    */   }
/*    */ }


