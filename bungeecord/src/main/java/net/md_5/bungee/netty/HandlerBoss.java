package net.md_5.bungee.netty;

import com.google.common.base.Preconditions;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.codec.DecoderException;
import io.netty.handler.codec.haproxy.HAProxyMessage;
import io.netty.handler.timeout.ReadTimeoutException;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.connection.CancelSendSignal;
import net.md_5.bungee.connection.InitialHandler;
import net.md_5.bungee.connection.PingHandler;
import net.md_5.bungee.protocol.BadPacketException;
import net.md_5.bungee.protocol.OverflowPacketException;
import net.md_5.bungee.protocol.PacketWrapper;
import net.md_5.bungee.util.QuietException;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.logging.Level;


public class HandlerBoss extends ChannelInboundHandlerAdapter {
    private ChannelWrapper channel;
    private PacketHandler handler;

    public void setHandler(PacketHandler handler) {
        Preconditions.checkArgument(handler != null, "handler");
        this.handler = handler;
    }

    public void channelActive(ChannelHandlerContext ctx)
            throws Exception {
        if (this.handler != null) {
            this.channel = new ChannelWrapper(ctx);
            this.handler.connected(this.channel);

            if ((!(this.handler instanceof InitialHandler)) && (!(this.handler instanceof PingHandler))) {
                ProxyServer.getInstance().getLogger().log(Level.INFO, "{0} has connected", this.handler);
            }
        }
    }

    public void channelInactive(ChannelHandlerContext ctx)
            throws Exception {
        if (this.handler != null) {
            this.channel.markClosed();
            this.handler.disconnected(this.channel);

            if ((!(this.handler instanceof InitialHandler)) && (!(this.handler instanceof PingHandler))) {
                ProxyServer.getInstance().getLogger().log(Level.INFO, "{0} has disconnected", this.handler);
            }
        }
    }

    public void channelWritabilityChanged(ChannelHandlerContext ctx)
            throws Exception {
        if (this.handler != null) {
            this.handler.writabilityChanged(this.channel);
        }
    }

    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (msg instanceof HAProxyMessage) {
            HAProxyMessage proxy = (HAProxyMessage) msg;
            InetSocketAddress newAddress = new InetSocketAddress(proxy.sourceAddress(), proxy.sourcePort());

            ProxyServer.getInstance().getLogger().log(Level.FINE, "Set remote address via PROXY {0} -> {1}", new Object[]{channel.getRemoteAddress(), newAddress});

            channel.setRemoteAddress(newAddress);
            return;
        }

        if (handler != null) {
            PacketWrapper packet = (PacketWrapper) msg;
            boolean sendPacket = handler.shouldHandle(packet);
            try {
                if (sendPacket && packet.packet != null) {
                    try {
                        packet.packet.handle(handler);
                    } catch (CancelSendSignal ex) {
                        sendPacket = false;
                    }
                }
                if (sendPacket) {
                    handler.handle(packet);
                }
            } finally {
                packet.trySingleRelease();
            }
        }
    }

    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        if (ctx.channel().isActive()) {
            boolean logExceptions = !(this.handler instanceof PingHandler);

            if (logExceptions) {
                if ((cause instanceof ReadTimeoutException)) {
                    ProxyServer.getInstance().getLogger().log(Level.WARNING, "{0} - read timed out", this.handler);
                } else if (((cause instanceof DecoderException)) && ((cause.getCause() instanceof BadPacketException))) {
                    ProxyServer.getInstance().getLogger().log(Level.WARNING, "{0} - bad packet ID, are mods in use!? {1}", new Object[]{this.handler, cause

                            .getCause().getMessage()});
                } else if (((cause instanceof DecoderException)) && ((cause.getCause() instanceof OverflowPacketException))) {
                    ProxyServer.getInstance().getLogger().log(Level.WARNING, "{0} - overflow in packet detected! {1}", new Object[]{this.handler, cause

                            .getCause().getMessage()});
                } else if (((cause instanceof IOException)) || (((cause instanceof IllegalStateException)) && ((this.handler instanceof InitialHandler)))) {
                    ProxyServer.getInstance().getLogger().log(Level.WARNING, "{0} - {1}: {2}", new Object[]{this.handler, cause

                            .getClass().getSimpleName(), cause.getMessage()});
                } else if ((cause instanceof QuietException)) {
                    ProxyServer.getInstance().getLogger().log(Level.SEVERE, "{0} - encountered exception: {1}", new Object[]{this.handler, cause});

                } else {

                    ProxyServer.getInstance().getLogger().log(Level.SEVERE, this.handler + " - encountered exception", cause);
                }
            }

            if (this.handler != null) {
                try {
                    this.handler.exception(cause);
                } catch (Exception ex) {
                    ProxyServer.getInstance().getLogger().log(Level.SEVERE, this.handler + " - exception processing exception", ex);
                }
            }

            ctx.close();
        }
    }
}


