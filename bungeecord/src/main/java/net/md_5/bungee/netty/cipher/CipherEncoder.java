/*    */ package net.md_5.bungee.netty.cipher;
/*    */ 
/*    */ import net.md_5.bungee.jni.cipher.BungeeCipher;
/*    */ 
/*    */ public class CipherEncoder extends io.netty.handler.codec.MessageToByteEncoder<io.netty.buffer.ByteBuf> {
/*    */   private final BungeeCipher cipher;
/*    */   
/*    */   public CipherEncoder(BungeeCipher cipher) {
/*  9 */     this.cipher = cipher;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   protected void encode(io.netty.channel.ChannelHandlerContext ctx, io.netty.buffer.ByteBuf in, io.netty.buffer.ByteBuf out)
/*    */     throws Exception
/*    */   {
/* 18 */     this.cipher.cipher(in, out);
/*    */   }
/*    */   
/*    */   public void handlerRemoved(io.netty.channel.ChannelHandlerContext ctx)
/*    */     throws Exception
/*    */   {
/* 24 */     this.cipher.free();
/*    */   }
/*    */ }


