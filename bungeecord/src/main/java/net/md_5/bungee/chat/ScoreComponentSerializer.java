/*    */ package net.md_5.bungee.chat;
/*    */ 
/*    */ import com.google.gson.JsonDeserializationContext;
/*    */ import com.google.gson.JsonDeserializer;
/*    */ import com.google.gson.JsonElement;
/*    */ import com.google.gson.JsonObject;
/*    */ import com.google.gson.JsonParseException;
/*    */ import com.google.gson.JsonSerializationContext;
/*    */ import com.google.gson.JsonSerializer;
/*    */ import java.lang.reflect.Type;
/*    */ import net.md_5.bungee.api.chat.ScoreComponent;
/*    */ 
/*    */ public class ScoreComponentSerializer
/*    */   extends BaseComponentSerializer
/*    */   implements JsonSerializer<ScoreComponent>, JsonDeserializer<ScoreComponent>
/*    */ {
/*    */   public ScoreComponent deserialize(JsonElement element, Type type, JsonDeserializationContext context) throws JsonParseException
/*    */   {
/* 19 */     JsonObject json = element.getAsJsonObject();
/* 20 */     JsonObject score = json.get("score").getAsJsonObject();
/* 21 */     if ((!score.has("name")) || (!score.has("objective")))
/*    */     {
/* 23 */       throw new JsonParseException("A score component needs at least a name and an objective");
/*    */     }
/*    */     
/* 26 */     String name = score.get("name").getAsString();
/* 27 */     String objective = score.get("objective").getAsString();
/* 28 */     ScoreComponent component = new ScoreComponent(name, objective);
/* 29 */     if ((score.has("value")) && (!score.get("value").getAsString().isEmpty()))
/*    */     {
/* 31 */       component.setValue(score.get("value").getAsString());
/*    */     }
/*    */     
/* 34 */     deserialize(json, component, context);
/* 35 */     return component;
/*    */   }
/*    */   
/*    */ 
/*    */   public JsonElement serialize(ScoreComponent component, Type type, JsonSerializationContext context)
/*    */   {
/* 41 */     JsonObject root = new JsonObject();
/* 42 */     serialize(root, component, context);
/* 43 */     JsonObject json = new JsonObject();
/* 44 */     json.addProperty("name", component.getName());
/* 45 */     json.addProperty("objective", component.getObjective());
/* 46 */     json.addProperty("value", component.getValue());
/* 47 */     root.add("score", json);
/* 48 */     return root;
/*    */   }
/*    */ }


