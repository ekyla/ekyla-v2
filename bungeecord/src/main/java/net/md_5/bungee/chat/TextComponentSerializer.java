/*    */ package net.md_5.bungee.chat;
/*    */ 
/*    */ import com.google.gson.JsonDeserializationContext;
/*    */ import com.google.gson.JsonDeserializer;
/*    */ import com.google.gson.JsonElement;
/*    */ import com.google.gson.JsonObject;
/*    */ import com.google.gson.JsonParseException;
/*    */ import com.google.gson.JsonSerializationContext;
/*    */ import com.google.gson.JsonSerializer;
/*    */ import java.lang.reflect.Type;
/*    */ import java.util.List;
/*    */ import net.md_5.bungee.api.chat.BaseComponent;
/*    */ import net.md_5.bungee.api.chat.TextComponent;
/*    */ 
/*    */ public class TextComponentSerializer
/*    */   extends BaseComponentSerializer
/*    */   implements JsonSerializer<TextComponent>, JsonDeserializer<TextComponent>
/*    */ {
/*    */   public TextComponent deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException
/*    */   {
/* 21 */     TextComponent component = new TextComponent();
/* 22 */     JsonObject object = json.getAsJsonObject();
/* 23 */     deserialize(object, component, context);
/* 24 */     component.setText(object.get("text").getAsString());
/* 25 */     return component;
/*    */   }
/*    */   
/*    */ 
/*    */   public JsonElement serialize(TextComponent src, Type typeOfSrc, JsonSerializationContext context)
/*    */   {
/* 31 */     List<BaseComponent> extra = src.getExtra();
/* 32 */     JsonObject object = new JsonObject();
/* 33 */     if ((src.hasFormatting()) || ((extra != null) && (!extra.isEmpty())))
/*    */     {
/* 35 */       serialize(object, src, context);
/*    */     }
/* 37 */     object.addProperty("text", src.getText());
/* 38 */     return object;
/*    */   }
/*    */ }


