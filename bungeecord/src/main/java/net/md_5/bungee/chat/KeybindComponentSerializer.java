/*    */ package net.md_5.bungee.chat;
/*    */ 
/*    */ import com.google.gson.JsonDeserializationContext;
/*    */ import com.google.gson.JsonDeserializer;
/*    */ import com.google.gson.JsonElement;
/*    */ import com.google.gson.JsonObject;
/*    */ import com.google.gson.JsonParseException;
/*    */ import com.google.gson.JsonSerializationContext;
/*    */ import com.google.gson.JsonSerializer;
/*    */ import java.lang.reflect.Type;
/*    */ import net.md_5.bungee.api.chat.KeybindComponent;
/*    */ 
/*    */ public class KeybindComponentSerializer
/*    */   extends BaseComponentSerializer
/*    */   implements JsonSerializer<KeybindComponent>, JsonDeserializer<KeybindComponent>
/*    */ {
/*    */   public KeybindComponent deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException
/*    */   {
/* 19 */     KeybindComponent component = new KeybindComponent();
/* 20 */     JsonObject object = json.getAsJsonObject();
/* 21 */     deserialize(object, component, context);
/* 22 */     component.setKeybind(object.get("keybind").getAsString());
/* 23 */     return component;
/*    */   }
/*    */   
/*    */ 
/*    */   public JsonElement serialize(KeybindComponent src, Type typeOfSrc, JsonSerializationContext context)
/*    */   {
/* 29 */     JsonObject object = new JsonObject();
/* 30 */     serialize(object, src, context);
/* 31 */     object.addProperty("keybind", src.getKeybind());
/* 32 */     return object;
/*    */   }
/*    */ }


