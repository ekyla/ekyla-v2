/*     */ package net.md_5.bungee.chat;
/*     */ 
/*     */ import com.google.gson.Gson;
/*     */ import com.google.gson.JsonElement;
/*     */ import com.google.gson.JsonObject;
/*     */ import java.io.IOException;
/*     */ import java.io.InputStreamReader;
/*     */ import java.util.HashMap;
/*     */ import java.util.LinkedList;
/*     */ import java.util.List;
/*     */ import java.util.Map;
/*     */ import java.util.Map.Entry;
/*     */ import java.util.ResourceBundle;
/*     */ 
/*     */ public final class TranslationRegistry
/*     */ {
/*     */   public boolean equals(Object o)
/*     */   {
/*  19 */     if (o == this) return true; if (!(o instanceof TranslationRegistry)) return false; TranslationRegistry other = (TranslationRegistry)o;Object this$providers = getProviders();Object other$providers = other.getProviders();return this$providers == null ? other$providers == null : this$providers.equals(other$providers); } public int hashCode() { int PRIME = 59;int result = 1;Object $providers = getProviders();result = result * 59 + ($providers == null ? 43 : $providers.hashCode());return result; } public String toString() { return "TranslationRegistry(providers=" + getProviders() + ")"; }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*  24 */   public static final TranslationRegistry INSTANCE = new TranslationRegistry();
/*     */   
/*  26 */   public List<TranslationProvider> getProviders() { return this.providers; } private final List<TranslationProvider> providers = new LinkedList();
/*     */   
/*     */   static
/*     */   {
/*     */     try
/*     */     {
/*  32 */       INSTANCE.addProvider(new JsonProvider("/assets/minecraft/lang/en_us.json"));
/*     */     }
/*     */     catch (Exception localException1) {}
/*     */     
/*     */ 
/*     */     try
/*     */     {
/*  39 */       INSTANCE.addProvider(new JsonProvider("/mojang-translations/en_us.json"));
/*     */     }
/*     */     catch (Exception localException2) {}
/*     */     
/*     */ 
/*     */     try
/*     */     {
/*  46 */       INSTANCE.addProvider(new ResourceBundleProvider("mojang-translations/en_US"));
/*     */     }
/*     */     catch (Exception localException3) {}
/*     */   }
/*     */   
/*     */ 
/*     */   private void addProvider(TranslationProvider provider)
/*     */   {
/*  54 */     this.providers.add(provider);
/*     */   }
/*     */   
/*     */   public String translate(String s)
/*     */   {
/*  59 */     for (TranslationProvider provider : this.providers)
/*     */     {
/*  61 */       String translation = provider.translate(s);
/*     */       
/*  63 */       if (translation != null)
/*     */       {
/*  65 */         return translation;
/*     */       }
/*     */     }
/*     */     
/*  69 */     return s;
/*     */   }
/*     */   
/*     */   private static abstract interface TranslationProvider {
/*     */     public abstract String translate(String paramString);
/*     */   }
/*     */   
/*     */   private static class ResourceBundleProvider implements TranslationRegistry.TranslationProvider { private final ResourceBundle bundle;
/*     */     
/*  78 */     public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof ResourceBundleProvider)) return false; ResourceBundleProvider other = (ResourceBundleProvider)o; if (!other.canEqual(this)) return false; Object this$bundle = getBundle();Object other$bundle = other.getBundle();return this$bundle == null ? other$bundle == null : this$bundle.equals(other$bundle); } protected boolean canEqual(Object other) { return other instanceof ResourceBundleProvider; } public int hashCode() { int PRIME = 59;int result = 1;Object $bundle = getBundle();result = result * 59 + ($bundle == null ? 43 : $bundle.hashCode());return result; } public String toString() { return "TranslationRegistry.ResourceBundleProvider(bundle=" + getBundle() + ")"; }
/*     */     
/*     */     public ResourceBundle getBundle()
/*     */     {
/*  82 */       return this.bundle;
/*     */     }
/*     */     
/*     */     public ResourceBundleProvider(String bundlePath) {
/*  86 */       this.bundle = ResourceBundle.getBundle(bundlePath);
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*  92 */     public String translate(String s) { return this.bundle.containsKey(s) ? this.bundle.getString(s) : null; }
/*     */   }
/*     */   
/*     */   private static class JsonProvider implements TranslationRegistry.TranslationProvider {
/*  96 */     public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof JsonProvider)) return false; JsonProvider other = (JsonProvider)o; if (!other.canEqual(this)) return false; Object this$translations = getTranslations();Object other$translations = other.getTranslations();return this$translations == null ? other$translations == null : this$translations.equals(other$translations); } protected boolean canEqual(Object other) { return other instanceof JsonProvider; } public int hashCode() { int PRIME = 59;int result = 1;Object $translations = getTranslations();result = result * 59 + ($translations == null ? 43 : $translations.hashCode());return result; }
/*  97 */     public String toString() { return "TranslationRegistry.JsonProvider()"; }
/*     */     
/*     */ 
/*     */ 
/* 101 */     public Map<String, String> getTranslations() { return this.translations; } private final Map<String, String> translations = new HashMap();
/*     */     
/*     */     public JsonProvider(String resourcePath) throws IOException
/*     */     {
/* 105 */       InputStreamReader rd = new InputStreamReader(JsonProvider.class.getResourceAsStream(resourcePath), com.google.common.base.Charsets.UTF_8);Throwable localThrowable3 = null;
/*     */       try {
/* 107 */         JsonObject obj = (JsonObject)new Gson().fromJson(rd, JsonObject.class);
/* 108 */         for (Map.Entry<String, JsonElement> entries : obj.entrySet())
/*     */         {
/* 110 */           this.translations.put(entries.getKey(), ((JsonElement)entries.getValue()).getAsString());
/*     */         }
/*     */       }
/*     */       catch (Throwable localThrowable1)
/*     */       {
/* 105 */         localThrowable3 = localThrowable1;throw localThrowable1;
/*     */ 
/*     */ 
/*     */       }
/*     */       finally
/*     */       {
/*     */ 
/* 112 */         if (rd != null) if (localThrowable3 != null) try { rd.close(); } catch (Throwable localThrowable2) { localThrowable3.addSuppressed(localThrowable2); } else rd.close();
/*     */       }
/*     */     }
/*     */     
/*     */     public String translate(String s)
/*     */     {
/* 118 */       return (String)this.translations.get(s);
/*     */     }
/*     */   }
/*     */ }


