/*    */ package net.md_5.bungee.chat;
/*    */ 
/*    */ import com.google.gson.Gson;
/*    */ import com.google.gson.GsonBuilder;
/*    */ import com.google.gson.JsonDeserializationContext;
/*    */ import com.google.gson.JsonDeserializer;
/*    */ import com.google.gson.JsonElement;
/*    */ import com.google.gson.JsonObject;
/*    */ import com.google.gson.JsonParseException;
/*    */ import com.google.gson.JsonParser;
/*    */ import java.lang.reflect.Type;
/*    */ import java.util.Set;
/*    */ import net.md_5.bungee.api.chat.BaseComponent;
/*    */ import net.md_5.bungee.api.chat.KeybindComponent;
/*    */ import net.md_5.bungee.api.chat.ScoreComponent;
/*    */ import net.md_5.bungee.api.chat.SelectorComponent;
/*    */ import net.md_5.bungee.api.chat.TextComponent;
/*    */ import net.md_5.bungee.api.chat.TranslatableComponent;
/*    */ 
/*    */ public class ComponentSerializer
/*    */   implements JsonDeserializer<BaseComponent>
/*    */ {
/* 23 */   private static final JsonParser JSON_PARSER = new JsonParser();
/* 24 */   private static final Gson gson = new GsonBuilder()
/* 25 */     .registerTypeAdapter(BaseComponent.class, new ComponentSerializer())
/* 26 */     .registerTypeAdapter(TextComponent.class, new TextComponentSerializer())
/* 27 */     .registerTypeAdapter(TranslatableComponent.class, new TranslatableComponentSerializer())
/* 28 */     .registerTypeAdapter(KeybindComponent.class, new KeybindComponentSerializer())
/* 29 */     .registerTypeAdapter(ScoreComponent.class, new ScoreComponentSerializer())
/* 30 */     .registerTypeAdapter(SelectorComponent.class, new SelectorComponentSerializer())
/* 31 */     .create();
/*    */   
/* 33 */   public static final ThreadLocal<Set<BaseComponent>> serializedComponents = new ThreadLocal();
/*    */   
/*    */   public static BaseComponent[] parse(String json)
/*    */   {
/* 37 */     JsonElement jsonElement = JSON_PARSER.parse(json);
/*    */     
/* 39 */     if (jsonElement.isJsonArray())
/*    */     {
/* 41 */       return (BaseComponent[])gson.fromJson(jsonElement, BaseComponent[].class);
/*    */     }
/*    */     
/* 44 */     return new BaseComponent[] {
/*    */     
/* 46 */       (BaseComponent)gson.fromJson(jsonElement, BaseComponent.class) };
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public static String toString(BaseComponent component)
/*    */   {
/* 53 */     return gson.toJson(component);
/*    */   }
/*    */   
/*    */   public static String toString(BaseComponent... components)
/*    */   {
/* 58 */     if (components.length == 1)
/*    */     {
/* 60 */       return gson.toJson(components[0]);
/*    */     }
/*    */     
/* 63 */     return gson.toJson(new TextComponent(components));
/*    */   }
/*    */   
/*    */ 
/*    */   public BaseComponent deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
/*    */     throws JsonParseException
/*    */   {
/* 70 */     if (json.isJsonPrimitive())
/*    */     {
/* 72 */       return new TextComponent(json.getAsString());
/*    */     }
/* 74 */     JsonObject object = json.getAsJsonObject();
/* 75 */     if (object.has("translate"))
/*    */     {
/* 77 */       return (BaseComponent)context.deserialize(json, TranslatableComponent.class);
/*    */     }
/* 79 */     if (object.has("keybind"))
/*    */     {
/* 81 */       return (BaseComponent)context.deserialize(json, KeybindComponent.class);
/*    */     }
/* 83 */     if (object.has("score"))
/*    */     {
/* 85 */       return (BaseComponent)context.deserialize(json, ScoreComponent.class);
/*    */     }
/* 87 */     if (object.has("selector"))
/*    */     {
/* 89 */       return (BaseComponent)context.deserialize(json, SelectorComponent.class);
/*    */     }
/* 91 */     return (BaseComponent)context.deserialize(json, TextComponent.class);
/*    */   }
/*    */ }


