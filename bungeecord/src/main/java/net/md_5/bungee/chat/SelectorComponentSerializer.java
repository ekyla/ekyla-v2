/*    */ package net.md_5.bungee.chat;
/*    */ 
/*    */ import com.google.gson.JsonDeserializationContext;
/*    */ import com.google.gson.JsonDeserializer;
/*    */ import com.google.gson.JsonElement;
/*    */ import com.google.gson.JsonObject;
/*    */ import com.google.gson.JsonParseException;
/*    */ import com.google.gson.JsonSerializationContext;
/*    */ import com.google.gson.JsonSerializer;
/*    */ import java.lang.reflect.Type;
/*    */ import net.md_5.bungee.api.chat.SelectorComponent;
/*    */ 
/*    */ public class SelectorComponentSerializer
/*    */   extends BaseComponentSerializer
/*    */   implements JsonSerializer<SelectorComponent>, JsonDeserializer<SelectorComponent>
/*    */ {
/*    */   public SelectorComponent deserialize(JsonElement element, Type type, JsonDeserializationContext context) throws JsonParseException
/*    */   {
/* 19 */     JsonObject object = element.getAsJsonObject();
/* 20 */     SelectorComponent component = new SelectorComponent(object.get("selector").getAsString());
/* 21 */     deserialize(object, component, context);
/* 22 */     return component;
/*    */   }
/*    */   
/*    */ 
/*    */   public JsonElement serialize(SelectorComponent component, Type type, JsonSerializationContext context)
/*    */   {
/* 28 */     JsonObject object = new JsonObject();
/* 29 */     serialize(object, component, context);
/* 30 */     object.addProperty("selector", component.getSelector());
/* 31 */     return object;
/*    */   }
/*    */ }


