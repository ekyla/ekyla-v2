/*    */ package net.md_5.bungee.chat;
/*    */ 
/*    */ import com.google.gson.JsonDeserializationContext;
/*    */ import com.google.gson.JsonDeserializer;
/*    */ import com.google.gson.JsonElement;
/*    */ import com.google.gson.JsonObject;
/*    */ import com.google.gson.JsonParseException;
/*    */ import com.google.gson.JsonSerializationContext;
/*    */ import com.google.gson.JsonSerializer;
/*    */ import java.lang.reflect.Type;
/*    */ import java.util.Arrays;
/*    */ import net.md_5.bungee.api.chat.BaseComponent;
/*    */ import net.md_5.bungee.api.chat.TranslatableComponent;
/*    */ 
/*    */ public class TranslatableComponentSerializer
/*    */   extends BaseComponentSerializer
/*    */   implements JsonSerializer<TranslatableComponent>, JsonDeserializer<TranslatableComponent>
/*    */ {
/*    */   public TranslatableComponent deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException
/*    */   {
/* 21 */     TranslatableComponent component = new TranslatableComponent();
/* 22 */     JsonObject object = json.getAsJsonObject();
/* 23 */     deserialize(object, component, context);
/* 24 */     component.setTranslate(object.get("translate").getAsString());
/* 25 */     if (object.has("with"))
/*    */     {
/* 27 */       component.setWith(Arrays.asList((BaseComponent[])context.deserialize(object.get("with"), BaseComponent[].class)));
/*    */     }
/* 29 */     return component;
/*    */   }
/*    */   
/*    */ 
/*    */   public JsonElement serialize(TranslatableComponent src, Type typeOfSrc, JsonSerializationContext context)
/*    */   {
/* 35 */     JsonObject object = new JsonObject();
/* 36 */     serialize(object, src, context);
/* 37 */     object.addProperty("translate", src.getTranslate());
/* 38 */     if (src.getWith() != null)
/*    */     {
/* 40 */       object.add("with", context.serialize(src.getWith()));
/*    */     }
/* 42 */     return object;
/*    */   }
/*    */ }


