/*    */ package net.md_5.bungee.log;
/*    */ 
/*    */ import java.io.IOException;
/*    */ import java.util.Map;
/*    */ import java.util.logging.Handler;
/*    */ import java.util.logging.LogRecord;
/*    */ import jline.console.ConsoleReader;
/*    */ import net.md_5.bungee.api.ChatColor;
/*    */ import org.fusesource.jansi.Ansi;
/*    */ import org.fusesource.jansi.Ansi.Attribute;
/*    */ import org.fusesource.jansi.Ansi.Color;
/*    */ import org.fusesource.jansi.Ansi.Erase;
/*    */ 
/*    */ public class ColouredWriter extends Handler
/*    */ {
/* 16 */   private final Map<ChatColor, String> replacements = new java.util.EnumMap(ChatColor.class);
/* 17 */   private final ChatColor[] colors = ChatColor.values();
/*    */   private final ConsoleReader console;
/*    */   
/*    */   public ColouredWriter(ConsoleReader console)
/*    */   {
/* 22 */     this.console = console;
/*    */     
/* 24 */     this.replacements.put(ChatColor.BLACK, Ansi.ansi().a(Ansi.Attribute.RESET).fg(Ansi.Color.BLACK).boldOff().toString());
/* 25 */     this.replacements.put(ChatColor.DARK_BLUE, Ansi.ansi().a(Ansi.Attribute.RESET).fg(Ansi.Color.BLUE).boldOff().toString());
/* 26 */     this.replacements.put(ChatColor.DARK_GREEN, Ansi.ansi().a(Ansi.Attribute.RESET).fg(Ansi.Color.GREEN).boldOff().toString());
/* 27 */     this.replacements.put(ChatColor.DARK_AQUA, Ansi.ansi().a(Ansi.Attribute.RESET).fg(Ansi.Color.CYAN).boldOff().toString());
/* 28 */     this.replacements.put(ChatColor.DARK_RED, Ansi.ansi().a(Ansi.Attribute.RESET).fg(Ansi.Color.RED).boldOff().toString());
/* 29 */     this.replacements.put(ChatColor.DARK_PURPLE, Ansi.ansi().a(Ansi.Attribute.RESET).fg(Ansi.Color.MAGENTA).boldOff().toString());
/* 30 */     this.replacements.put(ChatColor.GOLD, Ansi.ansi().a(Ansi.Attribute.RESET).fg(Ansi.Color.YELLOW).boldOff().toString());
/* 31 */     this.replacements.put(ChatColor.GRAY, Ansi.ansi().a(Ansi.Attribute.RESET).fg(Ansi.Color.WHITE).boldOff().toString());
/* 32 */     this.replacements.put(ChatColor.DARK_GRAY, Ansi.ansi().a(Ansi.Attribute.RESET).fg(Ansi.Color.BLACK).bold().toString());
/* 33 */     this.replacements.put(ChatColor.BLUE, Ansi.ansi().a(Ansi.Attribute.RESET).fg(Ansi.Color.BLUE).bold().toString());
/* 34 */     this.replacements.put(ChatColor.GREEN, Ansi.ansi().a(Ansi.Attribute.RESET).fg(Ansi.Color.GREEN).bold().toString());
/* 35 */     this.replacements.put(ChatColor.AQUA, Ansi.ansi().a(Ansi.Attribute.RESET).fg(Ansi.Color.CYAN).bold().toString());
/* 36 */     this.replacements.put(ChatColor.RED, Ansi.ansi().a(Ansi.Attribute.RESET).fg(Ansi.Color.RED).bold().toString());
/* 37 */     this.replacements.put(ChatColor.LIGHT_PURPLE, Ansi.ansi().a(Ansi.Attribute.RESET).fg(Ansi.Color.MAGENTA).bold().toString());
/* 38 */     this.replacements.put(ChatColor.YELLOW, Ansi.ansi().a(Ansi.Attribute.RESET).fg(Ansi.Color.YELLOW).bold().toString());
/* 39 */     this.replacements.put(ChatColor.WHITE, Ansi.ansi().a(Ansi.Attribute.RESET).fg(Ansi.Color.WHITE).bold().toString());
/* 40 */     this.replacements.put(ChatColor.MAGIC, Ansi.ansi().a(Ansi.Attribute.BLINK_SLOW).toString());
/* 41 */     this.replacements.put(ChatColor.BOLD, Ansi.ansi().a(Ansi.Attribute.UNDERLINE_DOUBLE).toString());
/* 42 */     this.replacements.put(ChatColor.STRIKETHROUGH, Ansi.ansi().a(Ansi.Attribute.STRIKETHROUGH_ON).toString());
/* 43 */     this.replacements.put(ChatColor.UNDERLINE, Ansi.ansi().a(Ansi.Attribute.UNDERLINE).toString());
/* 44 */     this.replacements.put(ChatColor.ITALIC, Ansi.ansi().a(Ansi.Attribute.ITALIC).toString());
/* 45 */     this.replacements.put(ChatColor.RESET, Ansi.ansi().a(Ansi.Attribute.RESET).toString());
/*    */   }
/*    */   
/*    */   public void print(String s)
/*    */   {
/* 50 */     for (ChatColor color : this.colors)
/*    */     {
/* 52 */       s = s.replaceAll("(?i)" + color.toString(), (String)this.replacements.get(color));
/*    */     }
/*    */     try
/*    */     {
/* 56 */       this.console.print(Ansi.ansi().eraseLine(Ansi.Erase.ALL).toString() + '\r' + s + Ansi.ansi().reset().toString());
/* 57 */       this.console.drawLine();
/* 58 */       this.console.flush();
/*    */     }
/*    */     catch (IOException localIOException1) {}
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void publish(LogRecord record)
/*    */   {
/* 67 */     if (isLoggable(record))
/*    */     {
/* 69 */       print(getFormatter().format(record));
/*    */     }
/*    */   }
/*    */   
/*    */   public void flush() {}
/*    */   
/*    */   public void close()
/*    */     throws SecurityException
/*    */   {}
/*    */ }


