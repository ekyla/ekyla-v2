/*    */ package net.md_5.bungee.log;
/*    */ 
/*    */ import java.io.PrintWriter;
/*    */ import java.io.StringWriter;
/*    */ import java.text.DateFormat;
/*    */ import java.text.SimpleDateFormat;
/*    */ import java.util.logging.Formatter;
/*    */ import java.util.logging.Level;
/*    */ import java.util.logging.LogRecord;
/*    */ 
/*    */ public class ConciseFormatter extends Formatter
/*    */ {
/* 13 */   private final DateFormat date = new SimpleDateFormat(System.getProperty("net.md_5.bungee.log-date-format", "HH:mm:ss"));
/*    */   
/*    */ 
/*    */ 
/*    */   public String format(LogRecord record)
/*    */   {
/* 19 */     StringBuilder formatted = new StringBuilder();
/*    */     
/* 21 */     formatted.append(this.date.format(Long.valueOf(record.getMillis())));
/* 22 */     formatted.append(" [");
/* 23 */     formatted.append(record.getLevel().getLocalizedName());
/* 24 */     formatted.append("] ");
/* 25 */     formatted.append(formatMessage(record));
/* 26 */     formatted.append('\n');
/*    */     
/* 28 */     if (record.getThrown() != null)
/*    */     {
/* 30 */       StringWriter writer = new StringWriter();
/* 31 */       record.getThrown().printStackTrace(new PrintWriter(writer));
/* 32 */       formatted.append(writer);
/*    */     }
/*    */     
/* 35 */     return formatted.toString();
/*    */   }
/*    */ }


