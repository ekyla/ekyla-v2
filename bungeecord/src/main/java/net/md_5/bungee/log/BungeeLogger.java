/*    */ package net.md_5.bungee.log;
/*    */ 
/*    */ import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
/*    */ import java.io.IOException;
/*    */ import java.io.PrintStream;
/*    */ import java.util.logging.FileHandler;
/*    */ import java.util.logging.Formatter;
/*    */ import java.util.logging.Level;
/*    */ import java.util.logging.LogRecord;
/*    */ import java.util.logging.Logger;
/*    */ import jline.console.ConsoleReader;
/*    */ 
/*    */ public class BungeeLogger extends Logger
/*    */ {
/* 15 */   private final Formatter formatter = new ConciseFormatter();
/* 16 */   private final LogDispatcher dispatcher = new LogDispatcher(this);
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   @SuppressFBWarnings({"SC_START_IN_CTOR"})
/*    */   public BungeeLogger(String loggerName, String filePattern, ConsoleReader reader)
/*    */   {
/* 25 */     super(loggerName, null);
/* 26 */     setLevel(Level.ALL);
/*    */     
/*    */     try
/*    */     {
/* 30 */       FileHandler fileHandler = new FileHandler(filePattern, 16777216, 8, true);
/* 31 */       fileHandler.setFormatter(this.formatter);
/* 32 */       addHandler(fileHandler);
/*    */       
/* 34 */       ColouredWriter consoleHandler = new ColouredWriter(reader);
/* 35 */       consoleHandler.setLevel(Level.INFO);
/* 36 */       consoleHandler.setFormatter(this.formatter);
/* 37 */       addHandler(consoleHandler);
/*    */     }
/*    */     catch (IOException ex) {
/* 40 */       System.err.println("Could not register logger!");
/* 41 */       ex.printStackTrace();
/*    */     }
/*    */     
/* 44 */     this.dispatcher.start();
/*    */   }
/*    */   
/*    */ 
/*    */   public void log(LogRecord record)
/*    */   {
/* 50 */     this.dispatcher.queue(record);
/*    */   }
/*    */   
/*    */   void doLog(LogRecord record)
/*    */   {
/* 55 */     super.log(record);
/*    */   }
/*    */ }


