/*    */ package net.md_5.bungee;
/*    */ 
/*    */ import net.md_5.bungee.protocol.packet.ClientStatus;
/*    */ import net.md_5.bungee.protocol.packet.PluginMessage;
/*    */ import net.md_5.bungee.protocol.packet.Respawn;
/*    */ 
/*    */ 
/*    */ public class PacketConstants
/*    */ {
/* 10 */   public static final Respawn DIM1_SWITCH = new Respawn(1, (short)0, (short)0, "default");
/* 11 */   public static final Respawn DIM2_SWITCH = new Respawn(-1, (short)0, (short)0, "default");
/* 12 */   public static final ClientStatus CLIENT_LOGIN = new ClientStatus((byte)0);
/* 13 */   public static final PluginMessage FORGE_MOD_REQUEST = new PluginMessage("FML", new byte[] { 0, 0, 0, 0, 0, 2 }, false);
/*    */ }


