package net.md_5.bungee.api;

import java.util.Collection;
import java.util.Map;
import net.md_5.bungee.api.config.ListenerInfo;
import net.md_5.bungee.api.config.ServerInfo;

@Deprecated
public abstract interface ProxyConfig
{
  public abstract int getTimeout();
  
  public abstract String getUuid();
  
  public abstract Collection<ListenerInfo> getListeners();
  
  public abstract Map<String, ServerInfo> getServers();
  
  public abstract boolean isOnlineMode();
  
  public abstract boolean isLogCommands();
  
  public abstract int getPlayerLimit();
  
  public abstract Collection<String> getDisabledCommands();
  
  @Deprecated
  public abstract int getThrottle();
  
  @Deprecated
  public abstract boolean isIpForward();
  
  @Deprecated
  public abstract String getFavicon();
  
  public abstract Favicon getFaviconObject();
}


