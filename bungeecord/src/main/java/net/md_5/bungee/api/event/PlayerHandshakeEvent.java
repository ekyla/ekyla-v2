/*    */ package net.md_5.bungee.api.event;
/*    */ 
/*    */ import net.md_5.bungee.api.connection.PendingConnection;
/*    */ import net.md_5.bungee.api.plugin.Event;
/*    */ import net.md_5.bungee.protocol.packet.Handshake;
/*    */ 
/*    */ 
/*    */ 
/*    */ public class PlayerHandshakeEvent
/*    */   extends Event
/*    */ {
/*    */   private final PendingConnection connection;
/*    */   private final Handshake handshake;
/*    */   
/* 15 */   public String toString() { return "PlayerHandshakeEvent(connection=" + getConnection() + ", handshake=" + getHandshake() + ")"; }
/* 16 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof PlayerHandshakeEvent)) return false; PlayerHandshakeEvent other = (PlayerHandshakeEvent)o; if (!other.canEqual(this)) return false; Object this$connection = getConnection();Object other$connection = other.getConnection(); if (this$connection == null ? other$connection != null : !this$connection.equals(other$connection)) return false; Object this$handshake = getHandshake();Object other$handshake = other.getHandshake();return this$handshake == null ? other$handshake == null : this$handshake.equals(other$handshake); } protected boolean canEqual(Object other) { return other instanceof PlayerHandshakeEvent; } public int hashCode() { int PRIME = 59;int result = 1;Object $connection = getConnection();result = result * 59 + ($connection == null ? 43 : $connection.hashCode());Object $handshake = getHandshake();result = result * 59 + ($handshake == null ? 43 : $handshake.hashCode());return result;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public PendingConnection getConnection()
/*    */   {
/* 23 */     return this.connection;
/*    */   }
/*    */   
/*    */   public Handshake getHandshake() {
/* 27 */     return this.handshake;
/*    */   }
/*    */   
/*    */   public PlayerHandshakeEvent(PendingConnection connection, Handshake handshake) {
/* 31 */     this.connection = connection;
/* 32 */     this.handshake = handshake;
/*    */   }
/*    */ }


