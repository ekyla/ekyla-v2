/*    */ package net.md_5.bungee.api.chat;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public final class SelectorComponent
/*    */   extends BaseComponent
/*    */ {
/*    */   private String selector;
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 23 */   public void setSelector(String selector) { this.selector = selector; }
/* 24 */   public String toString() { return "SelectorComponent(selector=" + getSelector() + ")"; }
/* 25 */   public SelectorComponent(String selector) { this.selector = selector; }
/* 26 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof SelectorComponent)) return false; SelectorComponent other = (SelectorComponent)o; if (!other.canEqual(this)) return false; if (!super.equals(o)) return false; Object this$selector = getSelector();Object other$selector = other.getSelector();return this$selector == null ? other$selector == null : this$selector.equals(other$selector); } protected boolean canEqual(Object other) { return other instanceof SelectorComponent; } public int hashCode() { int PRIME = 59;int result = super.hashCode();Object $selector = getSelector();result = result * 59 + ($selector == null ? 43 : $selector.hashCode());return result;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public String getSelector()
/*    */   {
/* 34 */     return this.selector;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public SelectorComponent(SelectorComponent original)
/*    */   {
/* 43 */     super(original);
/* 44 */     setSelector(original.getSelector());
/*    */   }
/*    */   
/*    */ 
/*    */   public SelectorComponent duplicate()
/*    */   {
/* 50 */     return new SelectorComponent(this);
/*    */   }
/*    */   
/*    */ 
/*    */   protected void toLegacyText(StringBuilder builder)
/*    */   {
/* 56 */     builder.append(this.selector);
/* 57 */     super.toLegacyText(builder);
/*    */   }
/*    */ }


