/*    */ package net.md_5.bungee.api.event;
/*    */ 
/*    */ import lombok.NonNull;
/*    */ 
/*    */ public class ServerDisconnectEvent extends net.md_5.bungee.api.plugin.Event
/*    */ {
/*    */   @NonNull
/*    */   private final net.md_5.bungee.api.connection.ProxiedPlayer player;
/*    */   @NonNull
/*    */   private final net.md_5.bungee.api.config.ServerInfo target;
/*    */   
/*    */   public ServerDisconnectEvent(@NonNull net.md_5.bungee.api.connection.ProxiedPlayer player, @NonNull net.md_5.bungee.api.config.ServerInfo target) {
/* 13 */     if (player == null) throw new NullPointerException("player is marked @NonNull but is null"); if (target == null) throw new NullPointerException("target is marked @NonNull but is null"); this.player = player;this.target = target; }
/* 14 */   public String toString() { return "ServerDisconnectEvent(player=" + getPlayer() + ", target=" + getTarget() + ")"; }
/* 15 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof ServerDisconnectEvent)) return false; ServerDisconnectEvent other = (ServerDisconnectEvent)o; if (!other.canEqual(this)) return false; Object this$player = getPlayer();Object other$player = other.getPlayer(); if (this$player == null ? other$player != null : !this$player.equals(other$player)) return false; Object this$target = getTarget();Object other$target = other.getTarget();return this$target == null ? other$target == null : this$target.equals(other$target); } protected boolean canEqual(Object other) { return other instanceof ServerDisconnectEvent; } public int hashCode() { int PRIME = 59;int result = 1;Object $player = getPlayer();result = result * 59 + ($player == null ? 43 : $player.hashCode());Object $target = getTarget();result = result * 59 + ($target == null ? 43 : $target.hashCode());return result;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   @NonNull
/*    */   public net.md_5.bungee.api.connection.ProxiedPlayer getPlayer()
/*    */   {
/* 23 */     return this.player;
/*    */   }
/*    */   
/*    */   @NonNull
/*    */   public net.md_5.bungee.api.config.ServerInfo getTarget() {
/* 28 */     return this.target;
/*    */   }
/*    */ }


