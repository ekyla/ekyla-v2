package net.md_5.bungee.api.event;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Event;

import java.util.Objects;


public class PostLoginEvent extends Event {

    private final ProxiedPlayer player;
    private boolean cancelTeleportation;

    public PostLoginEvent(ProxiedPlayer player) {
        this.player = player;
    }

    public boolean isCancelTeleportation() {
        return cancelTeleportation;
    }

    public void setCancelTeleportation(boolean cancelTeleportation) {
        this.cancelTeleportation = cancelTeleportation;
    }

    public String toString() {
        return "PostLoginEvent(player=" + getPlayer() + ")";
    }

    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof PostLoginEvent)) return false;
        PostLoginEvent other = (PostLoginEvent) o;
        if (!other.canEqual(this)) return false;
        Object this$player = getPlayer();
        Object other$player = other.getPlayer();
        return Objects.equals(this$player, other$player);
    }

    protected boolean canEqual(Object other) {
        return other instanceof PostLoginEvent;
    }

    public int hashCode() {
        int PRIME = 59;
        int result = 1;
        Object $player = getPlayer();
        result = result * 59 + ($player == null ? 43 : $player.hashCode());
        return result;
    }


    public ProxiedPlayer getPlayer() {
        return this.player;
    }
}


