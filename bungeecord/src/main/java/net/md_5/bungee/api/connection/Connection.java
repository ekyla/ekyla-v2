package net.md_5.bungee.api.connection;

import java.net.InetSocketAddress;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.protocol.DefinedPacket;

public abstract interface Connection
{
  public abstract InetSocketAddress getAddress();
  
  @Deprecated
  public abstract void disconnect(String paramString);
  
  public abstract void disconnect(BaseComponent... paramVarArgs);
  
  public abstract void disconnect(BaseComponent paramBaseComponent);
  
  public abstract boolean isConnected();
  
  public abstract Unsafe unsafe();
  
  public static abstract interface Unsafe
  {
    public abstract void sendPacket(DefinedPacket paramDefinedPacket);
  }
}


