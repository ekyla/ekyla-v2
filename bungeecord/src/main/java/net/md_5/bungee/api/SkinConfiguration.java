package net.md_5.bungee.api;

public abstract interface SkinConfiguration
{
  public abstract boolean hasCape();
  
  public abstract boolean hasJacket();
  
  public abstract boolean hasLeftSleeve();
  
  public abstract boolean hasRightSleeve();
  
  public abstract boolean hasLeftPants();
  
  public abstract boolean hasRightPants();
  
  public abstract boolean hasHat();
}


