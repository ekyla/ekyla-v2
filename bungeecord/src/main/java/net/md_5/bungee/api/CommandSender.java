package net.md_5.bungee.api;

import java.util.Collection;
import net.md_5.bungee.api.chat.BaseComponent;

public abstract interface CommandSender
{
  public abstract String getName();
  
  @Deprecated
  public abstract void sendMessage(String paramString);
  
  @Deprecated
  public abstract void sendMessages(String... paramVarArgs);
  
  public abstract void sendMessage(BaseComponent... paramVarArgs);
  
  public abstract void sendMessage(BaseComponent paramBaseComponent);
  
  public abstract Collection<String> getGroups();
  
  public abstract void addGroups(String... paramVarArgs);
  
  public abstract void removeGroups(String... paramVarArgs);
  
  public abstract boolean hasPermission(String paramString);
  
  public abstract void setPermission(String paramString, boolean paramBoolean);
  
  public abstract Collection<String> getPermissions();
}


