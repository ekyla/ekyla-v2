package net.md_5.bungee.api.scheduler;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import net.md_5.bungee.api.plugin.Plugin;

public abstract interface TaskScheduler
{
  public abstract void cancel(int paramInt);
  
  public abstract void cancel(ScheduledTask paramScheduledTask);
  
  public abstract int cancel(Plugin paramPlugin);
  
  public abstract ScheduledTask runAsync(Plugin paramPlugin, Runnable paramRunnable);
  
  public abstract ScheduledTask schedule(Plugin paramPlugin, Runnable paramRunnable, long paramLong, TimeUnit paramTimeUnit);
  
  public abstract ScheduledTask schedule(Plugin paramPlugin, Runnable paramRunnable, long paramLong1, long paramLong2, TimeUnit paramTimeUnit);
  
  public abstract Unsafe unsafe();
  
  public static abstract interface Unsafe
  {
    public abstract ExecutorService getExecutorService(Plugin paramPlugin);
  }
}


