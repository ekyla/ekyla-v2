/*     */ package net.md_5.bungee.api.plugin;
/*     */ 
/*     */ import com.google.common.util.concurrent.ThreadFactoryBuilder;
/*     */ import java.io.File;
/*     */ import java.io.InputStream;
/*     */ import java.util.concurrent.ExecutorService;
/*     */ import java.util.concurrent.Executors;
/*     */ import java.util.logging.Logger;
/*     */ import net.md_5.bungee.api.ProxyServer;
/*     */ 
/*     */ 
/*     */ 
/*     */ public class Plugin
/*     */ {
/*     */   private PluginDescription description;
/*     */   private ProxyServer proxy;
/*     */   private File file;
/*     */   private Logger logger;
/*     */   private ExecutorService service;
/*     */   
/*  21 */   public PluginDescription getDescription() { return this.description; }
/*     */   
/*  23 */   public ProxyServer getProxy() { return this.proxy; }
/*     */   
/*  25 */   public File getFile() { return this.file; }
/*     */   
/*  27 */   public Logger getLogger() { return this.logger; }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public final File getDataFolder()
/*     */   {
/*  61 */     return new File(getProxy().getPluginsFolder(), getDescription().getName());
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public final InputStream getResourceAsStream(String name)
/*     */   {
/*  74 */     return getClass().getClassLoader().getResourceAsStream(name);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   final void init(ProxyServer proxy, PluginDescription description)
/*     */   {
/*  85 */     this.proxy = proxy;
/*  86 */     this.description = description;
/*  87 */     this.file = description.getFile();
/*  88 */     this.logger = new PluginLogger(this);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   @Deprecated
/*     */   public ExecutorService getExecutorService()
/*     */   {
/*  97 */     if (this.service == null)
/*     */     {
/*  99 */       String name = getDescription() == null ? "unknown" : getDescription().getName();
/* 100 */       this.service = Executors.newCachedThreadPool(new ThreadFactoryBuilder().setNameFormat(name + " Pool Thread #%1$d")
/* 101 */         .setThreadFactory(new net.md_5.bungee.api.scheduler.GroupedThreadFactory(this, name)).build());
/*     */     }
/* 103 */     return this.service;
/*     */   }
/*     */   
/*     */   public void onLoad() {}
/*     */   
/*     */   public void onEnable() {}
/*     */   
/*     */   public void onDisable() {}
/*     */ }


