package net.md_5.bungee.api.config;

import java.util.Collection;
import java.util.Map;

public abstract interface ConfigurationAdapter
{
  public abstract void load();
  
  public abstract int getInt(String paramString, int paramInt);
  
  public abstract String getString(String paramString1, String paramString2);
  
  public abstract boolean getBoolean(String paramString, boolean paramBoolean);
  
  public abstract Collection<?> getList(String paramString, Collection<?> paramCollection);
  
  public abstract Map<String, ServerInfo> getServers();
  
  public abstract Collection<ListenerInfo> getListeners();
  
  public abstract Collection<String> getGroups(String paramString);
  
  public abstract Collection<String> getPermissions(String paramString);
}


