/*    */ package net.md_5.bungee.api.event;
/*    */ 
/*    */ import net.md_5.bungee.api.connection.ProxiedPlayer;
/*    */ import net.md_5.bungee.api.plugin.Event;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class SettingsChangedEvent
/*    */   extends Event
/*    */ {
/*    */   private final ProxiedPlayer player;
/*    */   
/* 22 */   public SettingsChangedEvent(ProxiedPlayer player) { this.player = player; }
/* 23 */   public String toString() { return "SettingsChangedEvent(player=" + getPlayer() + ")"; }
/* 24 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof SettingsChangedEvent)) return false; SettingsChangedEvent other = (SettingsChangedEvent)o; if (!other.canEqual(this)) return false; Object this$player = getPlayer();Object other$player = other.getPlayer();return this$player == null ? other$player == null : this$player.equals(other$player); } protected boolean canEqual(Object other) { return other instanceof SettingsChangedEvent; } public int hashCode() { int PRIME = 59;int result = 1;Object $player = getPlayer();result = result * 59 + ($player == null ? 43 : $player.hashCode());return result;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public ProxiedPlayer getPlayer()
/*    */   {
/* 31 */     return this.player;
/*    */   }
/*    */ }


