/*    */ package net.md_5.bungee.api.plugin;
/*    */ 
/*    */ import java.util.logging.LogRecord;
/*    */ import java.util.logging.Logger;
/*    */ import net.md_5.bungee.api.ProxyServer;
/*    */ 
/*    */ public class PluginLogger extends Logger
/*    */ {
/*    */   private final String pluginName;
/*    */   
/*    */   protected PluginLogger(Plugin plugin)
/*    */   {
/* 13 */     super(plugin.getClass().getCanonicalName(), null);
/* 14 */     this.pluginName = ("[" + plugin.getDescription().getName() + "] ");
/* 15 */     setParent(plugin.getProxy().getLogger());
/*    */   }
/*    */   
/*    */ 
/*    */   public void log(LogRecord logRecord)
/*    */   {
/* 21 */     logRecord.setMessage(this.pluginName + logRecord.getMessage());
/* 22 */     super.log(logRecord);
/*    */   }
/*    */ }


