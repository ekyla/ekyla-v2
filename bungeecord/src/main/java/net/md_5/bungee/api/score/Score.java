/*    */ package net.md_5.bungee.api.score;
/*    */ 
/*    */ public class Score {
/*    */   private final String itemName;
/*    */   private final String scoreName;
/*    */   private final int value;
/*    */   
/*  8 */   public Score(String itemName, String scoreName, int value) { this.itemName = itemName;this.scoreName = scoreName;this.value = value; } public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof Score)) return false; Score other = (Score)o; if (!other.canEqual(this)) return false; Object this$itemName = getItemName();Object other$itemName = other.getItemName(); if (this$itemName == null ? other$itemName != null : !this$itemName.equals(other$itemName)) return false; Object this$scoreName = getScoreName();Object other$scoreName = other.getScoreName(); if (this$scoreName == null ? other$scoreName != null : !this$scoreName.equals(other$scoreName)) return false; return getValue() == other.getValue(); } protected boolean canEqual(Object other) { return other instanceof Score; } public int hashCode() { int PRIME = 59;int result = 1;Object $itemName = getItemName();result = result * 59 + ($itemName == null ? 43 : $itemName.hashCode());Object $scoreName = getScoreName();result = result * 59 + ($scoreName == null ? 43 : $scoreName.hashCode());result = result * 59 + getValue();return result; } public String toString() { return "Score(itemName=" + getItemName() + ", scoreName=" + getScoreName() + ", value=" + getValue() + ")"; }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public String getItemName()
/*    */   {
/* 15 */     return this.itemName;
/*    */   }
/*    */   
/*    */   public String getScoreName() {
/* 19 */     return this.scoreName;
/*    */   }
/*    */   
/*    */   public int getValue() {
/* 23 */     return this.value;
/*    */   }
/*    */ }


