package net.md_5.bungee.api;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public abstract interface Title
{
  public abstract Title title(BaseComponent paramBaseComponent);
  
  public abstract Title title(BaseComponent... paramVarArgs);
  
  public abstract Title subTitle(BaseComponent paramBaseComponent);
  
  public abstract Title subTitle(BaseComponent... paramVarArgs);
  
  public abstract Title fadeIn(int paramInt);
  
  public abstract Title stay(int paramInt);
  
  public abstract Title fadeOut(int paramInt);
  
  public abstract Title clear();
  
  public abstract Title reset();
  
  public abstract Title send(ProxiedPlayer paramProxiedPlayer);
}


