/*    */ package net.md_5.bungee.api.chat;
/*    */ 
/*    */ import net.md_5.bungee.api.ChatColor;
/*    */ 
/*    */ 
/*    */ public final class KeybindComponent
/*    */   extends BaseComponent
/*    */ {
/*    */   private String keybind;
/*    */   
/* 11 */   public void setKeybind(String keybind) { this.keybind = keybind; }
/* 12 */   public String toString() { return "KeybindComponent(keybind=" + getKeybind() + ")"; }
/*    */   
/* 14 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof KeybindComponent)) return false; KeybindComponent other = (KeybindComponent)o; if (!other.canEqual(this)) return false; if (!super.equals(o)) return false; Object this$keybind = getKeybind();Object other$keybind = other.getKeybind();return this$keybind == null ? other$keybind == null : this$keybind.equals(other$keybind); } protected boolean canEqual(Object other) { return other instanceof KeybindComponent; } public int hashCode() { int PRIME = 59;int result = super.hashCode();Object $keybind = getKeybind();result = result * 59 + ($keybind == null ? 43 : $keybind.hashCode());return result;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public String getKeybind()
/*    */   {
/* 23 */     return this.keybind;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public KeybindComponent(KeybindComponent original)
/*    */   {
/* 32 */     super(original);
/* 33 */     setKeybind(original.getKeybind());
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public KeybindComponent(String keybind)
/*    */   {
/* 44 */     setKeybind(keybind);
/*    */   }
/*    */   
/*    */ 
/*    */   public BaseComponent duplicate()
/*    */   {
/* 50 */     return new KeybindComponent(this);
/*    */   }
/*    */   
/*    */ 
/*    */   protected void toPlainText(StringBuilder builder)
/*    */   {
/* 56 */     builder.append(getKeybind());
/* 57 */     super.toPlainText(builder);
/*    */   }
/*    */   
/*    */ 
/*    */   protected void toLegacyText(StringBuilder builder)
/*    */   {
/* 63 */     builder.append(getColor());
/* 64 */     if (isBold())
/*    */     {
/* 66 */       builder.append(ChatColor.BOLD);
/*    */     }
/* 68 */     if (isItalic())
/*    */     {
/* 70 */       builder.append(ChatColor.ITALIC);
/*    */     }
/* 72 */     if (isUnderlined())
/*    */     {
/* 74 */       builder.append(ChatColor.UNDERLINE);
/*    */     }
/* 76 */     if (isStrikethrough())
/*    */     {
/* 78 */       builder.append(ChatColor.STRIKETHROUGH);
/*    */     }
/* 80 */     if (isObfuscated())
/*    */     {
/* 82 */       builder.append(ChatColor.MAGIC);
/*    */     }
/* 84 */     builder.append(getKeybind());
/*    */     
/* 86 */     super.toLegacyText(builder);
/*    */   }
/*    */   
/*    */   public KeybindComponent() {}
/*    */ }


