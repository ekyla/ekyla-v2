/*    */ package net.md_5.bungee.api.event;
/*    */ 
/*    */ import lombok.NonNull;
/*    */ import net.md_5.bungee.api.config.ServerInfo;
/*    */ import net.md_5.bungee.api.connection.ProxiedPlayer;
/*    */ import net.md_5.bungee.api.plugin.Event;
/*    */ 
/*    */ public class ServerConnectEvent extends Event implements net.md_5.bungee.api.plugin.Cancellable
/*    */ {
/*    */   private final ProxiedPlayer player;
/*    */   @NonNull
/*    */   private ServerInfo target;
/*    */   private boolean cancelled;
/*    */   private final Reason reason;
/*    */   
/*    */   public void setTarget(@NonNull ServerInfo target)
/*    */   {
/* 18 */     if (target == null) throw new NullPointerException("target is marked @NonNull but is null"); this.target = target; } public void setCancelled(boolean cancelled) { this.cancelled = cancelled; }
/* 19 */   public String toString() { return "ServerConnectEvent(player=" + getPlayer() + ", target=" + getTarget() + ", cancelled=" + isCancelled() + ", reason=" + getReason() + ")"; }
/* 20 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof ServerConnectEvent)) return false; ServerConnectEvent other = (ServerConnectEvent)o; if (!other.canEqual(this)) return false; Object this$player = getPlayer();Object other$player = other.getPlayer(); if (this$player == null ? other$player != null : !this$player.equals(other$player)) return false; Object this$target = getTarget();Object other$target = other.getTarget(); if (this$target == null ? other$target != null : !this$target.equals(other$target)) return false; if (isCancelled() != other.isCancelled()) return false; Object this$reason = getReason();Object other$reason = other.getReason();return this$reason == null ? other$reason == null : this$reason.equals(other$reason); } protected boolean canEqual(Object other) { return other instanceof ServerConnectEvent; } public int hashCode() { int PRIME = 59;int result = 1;Object $player = getPlayer();result = result * 59 + ($player == null ? 43 : $player.hashCode());Object $target = getTarget();result = result * 59 + ($target == null ? 43 : $target.hashCode());result = result * 59 + (isCancelled() ? 79 : 97);Object $reason = getReason();result = result * 59 + ($reason == null ? 43 : $reason.hashCode());return result;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public ProxiedPlayer getPlayer()
/*    */   {
/* 27 */     return this.player;
/*    */   }
/*    */   
/*    */   @NonNull
/*    */   public ServerInfo getTarget() {
/* 32 */     return this.target;
/*    */   }
/*    */   
/*    */ 
/* 36 */   public boolean isCancelled() { return this.cancelled; }
/* 37 */   public Reason getReason() { return this.reason; }
/*    */   
/*    */   @Deprecated
/*    */   public ServerConnectEvent(ProxiedPlayer player, ServerInfo target)
/*    */   {
/* 42 */     this(player, target, Reason.UNKNOWN);
/*    */   }
/*    */   
/*    */   public ServerConnectEvent(ProxiedPlayer player, ServerInfo target, Reason reason)
/*    */   {
/* 47 */     this.player = player;
/* 48 */     this.target = target;
/* 49 */     this.reason = reason;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public static enum Reason
/*    */   {
/* 59 */     LOBBY_FALLBACK, 
/*    */     
/*    */ 
/*    */ 
/* 63 */     COMMAND, 
/*    */     
/*    */ 
/*    */ 
/*    */ 
/* 68 */     SERVER_DOWN_REDIRECT, 
/*    */     
/*    */ 
/*    */ 
/* 72 */     KICK_REDIRECT, 
/*    */     
/*    */ 
/*    */ 
/* 76 */     PLUGIN_MESSAGE, 
/*    */     
/*    */ 
/*    */ 
/* 80 */     JOIN_PROXY, 
/*    */     
/*    */ 
/*    */ 
/* 84 */     PLUGIN, 
/*    */     
/*    */ 
/*    */ 
/* 88 */     UNKNOWN;
/*    */     
/*    */     private Reason() {}
/*    */   }
/*    */ }


