package net.md_5.bungee.api.chat;


import java.util.Objects;

public final class ClickEvent {
    private final Action action;
    private final String value;

    public ClickEvent(Action action, String value) {
        this.action = action;
        this.value = value;
    }

    public String toString() {
        return "ClickEvent(action=" + getAction() + ", value=" + getValue() + ")";
    }

    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof ClickEvent)) return false;
        ClickEvent other = (ClickEvent) o;
        Object this$action = getAction();
        Object other$action = other.getAction();
        if (!Objects.equals(this$action, other$action)) return false;
        Object this$value = getValue();
        Object other$value = other.getValue();
        return Objects.equals(this$value, other$value);
    }

    public int hashCode() {
        int PRIME = 59;
        int result = 1;
        Object $action = getAction();
        result = result * 59 + ($action == null ? 43 : $action.hashCode());
        Object $value = getValue();
        result = result * 59 + ($value == null ? 43 : $value.hashCode());
        return result;
    }

    public Action getAction() {
        return this.action;
    }


    public String getValue() {
        return this.value;
    }


    public enum Action {
        OPEN_URL,


        OPEN_FILE,


        RUN_COMMAND,


        SUGGEST_COMMAND,


        CHANGE_PAGE
    }
}
