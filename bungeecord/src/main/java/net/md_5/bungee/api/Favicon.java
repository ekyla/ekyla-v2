package net.md_5.bungee.api;

import com.google.common.io.BaseEncoding;
import com.google.gson.TypeAdapter;
import com.google.gson.internal.bind.TypeAdapters;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import lombok.NonNull;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;


public class Favicon {
    private static final TypeAdapter<Favicon> FAVICON_TYPE_ADAPTER = new TypeAdapter<Favicon>() {
        public void write(JsonWriter out, Favicon value)
                throws IOException {
            TypeAdapters.STRING.write(out, value == null ? null : value.getEncoded());
        }

        public Favicon read(JsonReader in)
                throws IOException {
            String enc = TypeAdapters.STRING.read(in);
            return enc == null ? null : Favicon.create(enc);
        }
    };
    @NonNull
    private final String encoded;

    private Favicon(@NonNull String encoded) {
        if (encoded == null) throw new NullPointerException("encoded is marked @NonNull but is null");
        this.encoded = encoded;
    }

    public static TypeAdapter<Favicon> getFaviconTypeAdapter() {
        return FAVICON_TYPE_ADAPTER;
    }

    public static Favicon create(BufferedImage image) {
        // check size
        if (image.getWidth() != 64 || image.getHeight() != 64) {
            throw new IllegalArgumentException("Server icon must be exactly 64x64 pixels");
        }

        // dump image PNG
        byte[] imageBytes;
        try {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            ImageIO.write(image, "PNG", stream);
            imageBytes = stream.toByteArray();
        } catch (IOException e) {
            // ByteArrayOutputStream should never throw this
            throw new AssertionError(e);
        }

        // encode with header
        String encoded = "data:image/png;base64," + BaseEncoding.base64().encode(imageBytes);

        // check encoded image size
        if (encoded.length() > Short.MAX_VALUE) {
            throw new IllegalArgumentException("Favicon file too large for server to process");
        }

        // create
        return new Favicon(encoded);
    }

    @Deprecated
    public static Favicon create(String encodedString) {
        return new Favicon(encodedString);
    }

    @NonNull
    public String getEncoded() {
        return this.encoded;
    }
}


