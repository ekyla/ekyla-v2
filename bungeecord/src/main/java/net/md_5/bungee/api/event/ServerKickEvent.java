/*    */ package net.md_5.bungee.api.event;
/*    */ 
/*    */ import net.md_5.bungee.api.config.ServerInfo;
/*    */ 
/*    */ public class ServerKickEvent
/*    */   extends net.md_5.bungee.api.plugin.Event
/*    */   implements net.md_5.bungee.api.plugin.Cancellable
/*    */ {
/*    */   private boolean cancelled;
/*    */   private final net.md_5.bungee.api.connection.ProxiedPlayer player;
/*    */   private final ServerInfo kickedFrom;
/*    */   private net.md_5.bungee.api.chat.BaseComponent[] kickReasonComponent;
/*    */   private ServerInfo cancelServer;
/*    */   private State state;
/*    */   
/* 16 */   public void setCancelled(boolean cancelled) { this.cancelled = cancelled; } public void setKickReasonComponent(net.md_5.bungee.api.chat.BaseComponent[] kickReasonComponent) { this.kickReasonComponent = kickReasonComponent; } public void setCancelServer(ServerInfo cancelServer) { this.cancelServer = cancelServer; } public void setState(State state) { this.state = state; }
/* 17 */   public String toString() { return "ServerKickEvent(cancelled=" + isCancelled() + ", player=" + getPlayer() + ", kickedFrom=" + getKickedFrom() + ", kickReasonComponent=" + java.util.Arrays.deepToString(getKickReasonComponent()) + ", cancelServer=" + getCancelServer() + ", state=" + getState() + ")"; }
/* 18 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof ServerKickEvent)) return false; ServerKickEvent other = (ServerKickEvent)o; if (!other.canEqual(this)) return false; if (isCancelled() != other.isCancelled()) return false; Object this$player = getPlayer();Object other$player = other.getPlayer(); if (this$player == null ? other$player != null : !this$player.equals(other$player)) return false; Object this$kickedFrom = getKickedFrom();Object other$kickedFrom = other.getKickedFrom(); if (this$kickedFrom == null ? other$kickedFrom != null : !this$kickedFrom.equals(other$kickedFrom)) return false; if (!java.util.Arrays.deepEquals(getKickReasonComponent(), other.getKickReasonComponent())) return false; Object this$cancelServer = getCancelServer();Object other$cancelServer = other.getCancelServer(); if (this$cancelServer == null ? other$cancelServer != null : !this$cancelServer.equals(other$cancelServer)) return false; Object this$state = getState();Object other$state = other.getState();return this$state == null ? other$state == null : this$state.equals(other$state); } protected boolean canEqual(Object other) { return other instanceof ServerKickEvent; } public int hashCode() { int PRIME = 59;int result = 1;result = result * 59 + (isCancelled() ? 79 : 97);Object $player = getPlayer();result = result * 59 + ($player == null ? 43 : $player.hashCode());Object $kickedFrom = getKickedFrom();result = result * 59 + ($kickedFrom == null ? 43 : $kickedFrom.hashCode());result = result * 59 + java.util.Arrays.deepHashCode(getKickReasonComponent());Object $cancelServer = getCancelServer();result = result * 59 + ($cancelServer == null ? 43 : $cancelServer.hashCode());Object $state = getState();result = result * 59 + ($state == null ? 43 : $state.hashCode());return result;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public boolean isCancelled()
/*    */   {
/* 25 */     return this.cancelled;
/*    */   }
/*    */   
/*    */   public net.md_5.bungee.api.connection.ProxiedPlayer getPlayer() {
/* 29 */     return this.player;
/*    */   }
/*    */   
/*    */   public ServerInfo getKickedFrom()
/*    */   {
/* 34 */     return this.kickedFrom;
/*    */   }
/*    */   
/*    */   public net.md_5.bungee.api.chat.BaseComponent[] getKickReasonComponent() {
/* 38 */     return this.kickReasonComponent;
/*    */   }
/*    */   
/*    */   public ServerInfo getCancelServer() {
/* 42 */     return this.cancelServer;
/*    */   }
/*    */   
/*    */   public State getState() {
/* 46 */     return this.state;
/*    */   }
/*    */   
/*    */   public static enum State
/*    */   {
/* 51 */     CONNECTING,  CONNECTED,  UNKNOWN;
/*    */     
/*    */     private State() {}
/*    */   }
/*    */   
/*    */   @Deprecated
/* 57 */   public ServerKickEvent(net.md_5.bungee.api.connection.ProxiedPlayer player, net.md_5.bungee.api.chat.BaseComponent[] kickReasonComponent, ServerInfo cancelServer) { this(player, kickReasonComponent, cancelServer, State.UNKNOWN); }
/*    */   
/*    */ 
/*    */   @Deprecated
/*    */   public ServerKickEvent(net.md_5.bungee.api.connection.ProxiedPlayer player, net.md_5.bungee.api.chat.BaseComponent[] kickReasonComponent, ServerInfo cancelServer, State state)
/*    */   {
/* 63 */     this(player, player.getServer().getInfo(), kickReasonComponent, cancelServer, state);
/*    */   }
/*    */   
/*    */   public ServerKickEvent(net.md_5.bungee.api.connection.ProxiedPlayer player, ServerInfo kickedFrom, net.md_5.bungee.api.chat.BaseComponent[] kickReasonComponent, ServerInfo cancelServer, State state)
/*    */   {
/* 68 */     this.player = player;
/* 69 */     this.kickedFrom = kickedFrom;
/* 70 */     this.kickReasonComponent = kickReasonComponent;
/* 71 */     this.cancelServer = cancelServer;
/* 72 */     this.state = state;
/*    */   }
/*    */   
/*    */   @Deprecated
/*    */   public String getKickReason()
/*    */   {
/* 78 */     return net.md_5.bungee.api.chat.BaseComponent.toLegacyText(this.kickReasonComponent);
/*    */   }
/*    */   
/*    */   @Deprecated
/*    */   public void setKickReason(String reason)
/*    */   {
/* 84 */     this.kickReasonComponent = net.md_5.bungee.api.chat.TextComponent.fromLegacyText(reason);
/*    */   }
/*    */ }


