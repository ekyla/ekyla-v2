package net.md_5.bungee.api;

public abstract interface Callback<V>
{
  public abstract void done(V paramV, Throwable paramThrowable);
}


