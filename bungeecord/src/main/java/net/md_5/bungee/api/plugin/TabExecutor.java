package net.md_5.bungee.api.plugin;

import net.md_5.bungee.api.CommandSender;

public abstract interface TabExecutor
{
  public abstract Iterable<String> onTabComplete(CommandSender paramCommandSender, String[] paramArrayOfString);
}


