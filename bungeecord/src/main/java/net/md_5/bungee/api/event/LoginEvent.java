/*    */ package net.md_5.bungee.api.event;
/*    */ 
/*    */ import java.util.Arrays;
/*    */ import net.md_5.bungee.api.Callback;
/*    */ import net.md_5.bungee.api.chat.BaseComponent;
/*    */ import net.md_5.bungee.api.connection.PendingConnection;
/*    */ import net.md_5.bungee.api.plugin.Cancellable;
/*    */ 
/*    */ public class LoginEvent
/*    */   extends AsyncEvent<LoginEvent>
/*    */   implements Cancellable
/*    */ {
/*    */   private boolean cancelled;
/*    */   private BaseComponent[] cancelReasonComponents;
/*    */   private final PendingConnection connection;
/*    */   
/* 17 */   public void setCancelled(boolean cancelled) { this.cancelled = cancelled; }
/* 18 */   public String toString() { return "LoginEvent(cancelled=" + isCancelled() + ", cancelReasonComponents=" + Arrays.deepToString(getCancelReasonComponents()) + ", connection=" + getConnection() + ")"; }
/* 19 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof LoginEvent)) return false; LoginEvent other = (LoginEvent)o; if (!other.canEqual(this)) return false; if (isCancelled() != other.isCancelled()) return false; if (!Arrays.deepEquals(getCancelReasonComponents(), other.getCancelReasonComponents())) return false; Object this$connection = getConnection();Object other$connection = other.getConnection();return this$connection == null ? other$connection == null : this$connection.equals(other$connection); } protected boolean canEqual(Object other) { return other instanceof LoginEvent; } public int hashCode() { int PRIME = 59;int result = 1;result = result * 59 + (isCancelled() ? 79 : 97);result = result * 59 + Arrays.deepHashCode(getCancelReasonComponents());Object $connection = getConnection();result = result * 59 + ($connection == null ? 43 : $connection.hashCode());return result;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public boolean isCancelled()
/*    */   {
/* 26 */     return this.cancelled;
/*    */   }
/*    */   
/*    */   public BaseComponent[] getCancelReasonComponents()
/*    */   {
/* 31 */     return this.cancelReasonComponents;
/*    */   }
/*    */   
/*    */   public PendingConnection getConnection() {
/* 35 */     return this.connection;
/*    */   }
/*    */   
/*    */   public LoginEvent(PendingConnection connection, Callback<LoginEvent> done) {
/* 39 */     super(done);
/* 40 */     this.connection = connection;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   @Deprecated
/*    */   public String getCancelReason()
/*    */   {
/* 50 */     return BaseComponent.toLegacyText(getCancelReasonComponents());
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   @Deprecated
/*    */   public void setCancelReason(String cancelReason)
/*    */   {
/* 62 */     setCancelReason(net.md_5.bungee.api.chat.TextComponent.fromLegacyText(cancelReason));
/*    */   }
/*    */   
/*    */   public void setCancelReason(BaseComponent... cancelReason)
/*    */   {
/* 67 */     this.cancelReasonComponents = cancelReason;
/*    */   }
/*    */ }


