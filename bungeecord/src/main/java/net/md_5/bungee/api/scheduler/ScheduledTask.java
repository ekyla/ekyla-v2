package net.md_5.bungee.api.scheduler;

import net.md_5.bungee.api.plugin.Plugin;

public abstract interface ScheduledTask
{
  public abstract int getId();
  
  public abstract Plugin getOwner();
  
  public abstract Runnable getTask();
  
  public abstract void cancel();
}


