package net.md_5.bungee.api.connection;

import net.md_5.bungee.api.*;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.score.Scoreboard;

import java.util.Map;
import java.util.UUID;

public interface ProxiedPlayer extends Connection, CommandSender {

    String getDisplayName();

    void setDisplayName(String paramString);

    void sendMessage(ChatMessageType paramChatMessageType, BaseComponent... paramVarArgs);

    void sendMessage(ChatMessageType paramChatMessageType, BaseComponent paramBaseComponent);

    void connect(ServerInfo paramServerInfo);

    void connect(ServerInfo paramServerInfo, ServerConnectEvent.Reason paramReason);

    void connect(ServerInfo paramServerInfo, Callback<Boolean> paramCallback);

    void connect(ServerInfo paramServerInfo, Callback<Boolean> paramCallback, ServerConnectEvent.Reason paramReason);

    void connect(ServerConnectRequest paramServerConnectRequest);

    Server getServer();

    int getPing();

    void sendData(String paramString, byte[] paramArrayOfByte);

    PendingConnection getPendingConnection();

    void chat(String paramString);

    ServerInfo getReconnectServer();

    void setReconnectServer(ServerInfo paramServerInfo);

    @Deprecated
    String getUUID();

    UUID getUniqueId();

    java.util.Locale getLocale();

    byte getViewDistance();

    ChatMode getChatMode();

    boolean hasChatColors();

    net.md_5.bungee.api.SkinConfiguration getSkinParts();

    MainHand getMainHand();

    void setTabHeader(BaseComponent paramBaseComponent1, BaseComponent paramBaseComponent2);

    void setTabHeader(BaseComponent[] paramArrayOfBaseComponent1, BaseComponent[] paramArrayOfBaseComponent2);

    void resetTabHeader();

    void sendTitle(Title paramTitle);

    boolean isForgeUser();

    Map<String, String> getModList();

    Scoreboard getScoreboard();

    public static enum ChatMode {
        SHOWN,


        COMMANDS_ONLY,


        HIDDEN;

        private ChatMode() {
        }
    }

    public static enum MainHand {
        LEFT,
        RIGHT;

        private MainHand() {
        }
    }
}


