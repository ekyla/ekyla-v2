/*    */ package net.md_5.bungee.api.event;
/*    */ 
/*    */ import net.md_5.bungee.api.connection.ProxiedPlayer;
/*    */ import net.md_5.bungee.api.plugin.Event;
/*    */ 
/*    */ 
/*    */ public class ServerSwitchEvent
/*    */   extends Event
/*    */ {
/*    */   private final ProxiedPlayer player;
/*    */   
/* 12 */   public ServerSwitchEvent(ProxiedPlayer player) { this.player = player; }
/* 13 */   public String toString() { return "ServerSwitchEvent(player=" + getPlayer() + ")"; }
/* 14 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof ServerSwitchEvent)) return false; ServerSwitchEvent other = (ServerSwitchEvent)o; if (!other.canEqual(this)) return false; Object this$player = getPlayer();Object other$player = other.getPlayer();return this$player == null ? other$player == null : this$player.equals(other$player); } protected boolean canEqual(Object other) { return other instanceof ServerSwitchEvent; } public int hashCode() { int PRIME = 59;int result = 1;Object $player = getPlayer();result = result * 59 + ($player == null ? 43 : $player.hashCode());return result;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public ProxiedPlayer getPlayer()
/*    */   {
/* 21 */     return this.player;
/*    */   }
/*    */ }


