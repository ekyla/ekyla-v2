package net.md_5.bungee.api.config;

import java.net.InetSocketAddress;
import java.util.Collection;
import net.md_5.bungee.api.Callback;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public abstract interface ServerInfo
{
  public abstract String getName();
  
  public abstract InetSocketAddress getAddress();
  
  public abstract Collection<ProxiedPlayer> getPlayers();
  
  public abstract String getMotd();
  
  public abstract boolean isRestricted();
  
  public abstract String getPermission();
  
  public abstract boolean canAccess(CommandSender paramCommandSender);
  
  public abstract void sendData(String paramString, byte[] paramArrayOfByte);
  
  public abstract boolean sendData(String paramString, byte[] paramArrayOfByte, boolean paramBoolean);
  
  public abstract void ping(Callback<ServerPing> paramCallback);
}


