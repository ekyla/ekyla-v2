/*    */ package net.md_5.bungee.api;
/*    */ 
/*    */ import com.google.common.base.Preconditions;
/*    */ import java.net.InetSocketAddress;
/*    */ import net.md_5.bungee.api.config.ListenerInfo;
/*    */ import net.md_5.bungee.api.config.ServerInfo;
/*    */ import net.md_5.bungee.api.connection.PendingConnection;
/*    */ import net.md_5.bungee.api.connection.ProxiedPlayer;
/*    */ 
/*    */ public abstract class AbstractReconnectHandler implements ReconnectHandler
/*    */ {
/*    */   public ServerInfo getServer(ProxiedPlayer player)
/*    */   {
/* 14 */     ServerInfo server = getForcedHost(player.getPendingConnection());
/* 15 */     if (server == null)
/*    */     {
/* 17 */       server = getStoredServer(player);
/* 18 */       if (server == null)
/*    */       {
/* 20 */         server = ProxyServer.getInstance().getServerInfo(player.getPendingConnection().getListener().getDefaultServer());
/*    */       }
/*    */       
/* 23 */       Preconditions.checkState(server != null, "Default server not defined");
/*    */     }
/*    */     
/* 26 */     return server;
/*    */   }
/*    */   
/*    */   public static ServerInfo getForcedHost(PendingConnection con)
/*    */   {
/* 31 */     if (con.getVirtualHost() == null)
/*    */     {
/* 33 */       return null;
/*    */     }
/*    */     
/* 36 */     String forced = (String)con.getListener().getForcedHosts().get(con.getVirtualHost().getHostString());
/*    */     
/* 38 */     if ((forced == null) && (con.getListener().isForceDefault()))
/*    */     {
/* 40 */       forced = con.getListener().getDefaultServer();
/*    */     }
/* 42 */     return ProxyServer.getInstance().getServerInfo(forced);
/*    */   }
/*    */   
/*    */   protected abstract ServerInfo getStoredServer(ProxiedPlayer paramProxiedPlayer);
/*    */ }


