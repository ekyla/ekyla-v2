/*     */ package net.md_5.bungee.api.chat;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ import java.util.regex.Matcher;
/*     */ import java.util.regex.Pattern;
/*     */ import net.md_5.bungee.api.ChatColor;
/*     */ import net.md_5.bungee.chat.TranslationRegistry;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public final class TranslatableComponent
/*     */   extends BaseComponent
/*     */ {
/*  16 */   public void setTranslate(String translate) { this.translate = translate; }
/*  17 */   public String toString() { return "TranslatableComponent(format=" + getFormat() + ", translate=" + getTranslate() + ", with=" + getWith() + ")"; }
/*     */   
/*  19 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof TranslatableComponent)) return false; TranslatableComponent other = (TranslatableComponent)o; if (!other.canEqual(this)) return false; if (!super.equals(o)) return false; Object this$format = getFormat();Object other$format = other.getFormat(); if (this$format == null ? other$format != null : !this$format.equals(other$format)) return false; Object this$translate = getTranslate();Object other$translate = other.getTranslate(); if (this$translate == null ? other$translate != null : !this$translate.equals(other$translate)) return false; Object this$with = getWith();Object other$with = other.getWith();return this$with == null ? other$with == null : this$with.equals(other$with); } protected boolean canEqual(Object other) { return other instanceof TranslatableComponent; } public int hashCode() { int PRIME = 59;int result = super.hashCode();Object $format = getFormat();result = result * 59 + ($format == null ? 43 : $format.hashCode());Object $translate = getTranslate();result = result * 59 + ($translate == null ? 43 : $translate.hashCode());Object $with = getWith();result = result * 59 + ($with == null ? 43 : $with.hashCode());return result;
/*     */   }
/*     */   
/*     */ 
/*  23 */   public Pattern getFormat() { return this.format; } private final Pattern format = Pattern.compile("%(?:(\\d+)\\$)?([A-Za-z%]|$)");
/*     */   private String translate;
/*     */   private List<BaseComponent> with;
/*     */   
/*     */   public String getTranslate()
/*     */   {
/*  29 */     return this.translate;
/*     */   }
/*     */   
/*     */   public List<BaseComponent> getWith() {
/*  33 */     return this.with;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public TranslatableComponent(TranslatableComponent original)
/*     */   {
/*  42 */     super(original);
/*  43 */     setTranslate(original.getTranslate());
/*     */     
/*  45 */     if (original.getWith() != null)
/*     */     {
/*  47 */       List<BaseComponent> temp = new ArrayList();
/*  48 */       for (BaseComponent baseComponent : original.getWith())
/*     */       {
/*  50 */         temp.add(baseComponent.duplicate());
/*     */       }
/*  52 */       setWith(temp);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public TranslatableComponent(String translate, Object... with)
/*     */   {
/*  68 */     setTranslate(translate);
/*  69 */     if ((with != null) && (with.length != 0))
/*     */     {
/*  71 */       List<BaseComponent> temp = new ArrayList();
/*  72 */       for (Object w : with)
/*     */       {
/*  74 */         if ((w instanceof BaseComponent))
/*     */         {
/*  76 */           temp.add((BaseComponent)w);
/*     */         }
/*     */         else {
/*  79 */           temp.add(new TextComponent(String.valueOf(w)));
/*     */         }
/*     */       }
/*  82 */       setWith(temp);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public BaseComponent duplicate()
/*     */   {
/*  94 */     return new TranslatableComponent(this);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setWith(List<BaseComponent> components)
/*     */   {
/* 105 */     for (BaseComponent component : components)
/*     */     {
/* 107 */       component.parent = this;
/*     */     }
/* 109 */     this.with = components;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void addWith(String text)
/*     */   {
/* 120 */     addWith(new TextComponent(text));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void addWith(BaseComponent component)
/*     */   {
/* 131 */     if (this.with == null)
/*     */     {
/* 133 */       this.with = new ArrayList();
/*     */     }
/* 135 */     component.parent = this;
/* 136 */     this.with.add(component);
/*     */   }
/*     */   
/*     */ 
/*     */   protected void toPlainText(StringBuilder builder)
/*     */   {
/* 142 */     String trans = TranslationRegistry.INSTANCE.translate(this.translate);
/*     */     
/* 144 */     Matcher matcher = this.format.matcher(trans);
/* 145 */     int position = 0;
/* 146 */     int i = 0;
/* 147 */     while (matcher.find(position))
/*     */     {
/* 149 */       int pos = matcher.start();
/* 150 */       if (pos != position)
/*     */       {
/* 152 */         builder.append(trans.substring(position, pos));
/*     */       }
/* 154 */       position = matcher.end();
/*     */       
/* 156 */       String formatCode = matcher.group(2);
/* 157 */       switch (formatCode.charAt(0))
/*     */       {
/*     */       case 'd': 
/*     */       case 's': 
/* 161 */         String withIndex = matcher.group(1);
/* 162 */         ((BaseComponent)this.with.get(withIndex != null ? Integer.parseInt(withIndex) - 1 : i++)).toPlainText(builder);
/* 163 */         break;
/*     */       case '%': 
/* 165 */         builder.append('%');
/*     */       }
/*     */       
/*     */     }
/* 169 */     if (trans.length() != position)
/*     */     {
/* 171 */       builder.append(trans.substring(position, trans.length()));
/*     */     }
/*     */     
/* 174 */     super.toPlainText(builder);
/*     */   }
/*     */   
/*     */ 
/*     */   protected void toLegacyText(StringBuilder builder)
/*     */   {
/* 180 */     String trans = TranslationRegistry.INSTANCE.translate(this.translate);
/*     */     
/* 182 */     Matcher matcher = this.format.matcher(trans);
/* 183 */     int position = 0;
/* 184 */     int i = 0;
/* 185 */     while (matcher.find(position))
/*     */     {
/* 187 */       int pos = matcher.start();
/* 188 */       if (pos != position)
/*     */       {
/* 190 */         addFormat(builder);
/* 191 */         builder.append(trans.substring(position, pos));
/*     */       }
/* 193 */       position = matcher.end();
/*     */       
/* 195 */       String formatCode = matcher.group(2);
/* 196 */       switch (formatCode.charAt(0))
/*     */       {
/*     */       case 'd': 
/*     */       case 's': 
/* 200 */         String withIndex = matcher.group(1);
/* 201 */         ((BaseComponent)this.with.get(withIndex != null ? Integer.parseInt(withIndex) - 1 : i++)).toLegacyText(builder);
/* 202 */         break;
/*     */       case '%': 
/* 204 */         addFormat(builder);
/* 205 */         builder.append('%');
/*     */       }
/*     */       
/*     */     }
/* 209 */     if (trans.length() != position)
/*     */     {
/* 211 */       addFormat(builder);
/* 212 */       builder.append(trans.substring(position, trans.length()));
/*     */     }
/* 214 */     super.toLegacyText(builder);
/*     */   }
/*     */   
/*     */   private void addFormat(StringBuilder builder)
/*     */   {
/* 219 */     builder.append(getColor());
/* 220 */     if (isBold())
/*     */     {
/* 222 */       builder.append(ChatColor.BOLD);
/*     */     }
/* 224 */     if (isItalic())
/*     */     {
/* 226 */       builder.append(ChatColor.ITALIC);
/*     */     }
/* 228 */     if (isUnderlined())
/*     */     {
/* 230 */       builder.append(ChatColor.UNDERLINE);
/*     */     }
/* 232 */     if (isStrikethrough())
/*     */     {
/* 234 */       builder.append(ChatColor.STRIKETHROUGH);
/*     */     }
/* 236 */     if (isObfuscated())
/*     */     {
/* 238 */       builder.append(ChatColor.MAGIC);
/*     */     }
/*     */   }
/*     */   
/*     */   public TranslatableComponent() {}
/*     */ }


