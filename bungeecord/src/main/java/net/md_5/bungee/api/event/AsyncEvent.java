package net.md_5.bungee.api.event;

import com.google.common.base.Preconditions;
import net.md_5.bungee.api.Callback;
import net.md_5.bungee.api.plugin.Event;
import net.md_5.bungee.api.plugin.Plugin;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;


public class AsyncEvent<T>
        extends Event {
    private final Callback<T> done;
    private final Map<Plugin, AtomicInteger> intents = new ConcurrentHashMap<>();
    private final AtomicBoolean fired = new AtomicBoolean();
    private final AtomicInteger latch = new AtomicInteger();

    public AsyncEvent(Callback<T> done) {
        this.done = done;
    }

    public String toString() {
        return "AsyncEvent(super=" + super.toString() + ", done=" + this.done + ", intents=" + this.intents + ", fired=" + this.fired + ", latch=" + this.latch + ")";
    }

    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof AsyncEvent)) return false;
        AsyncEvent<?> other = (AsyncEvent) o;
        if (!other.canEqual(this)) return false;
        if (!super.equals(o)) return false;
        if (!Objects.equals(this.done, other.done)) return false;
        if (!Objects.equals(this.intents, other.intents)) return false;
        if (!Objects.equals(this.fired, other.fired)) return false;
        return Objects.equals(this.latch, other.latch);
    }

    protected boolean canEqual(Object other) {
        return other instanceof AsyncEvent;
    }

    public int hashCode() {
        int PRIME = 59;
        int result = super.hashCode();
        Object $done = this.done;
        result = result * 59 + ($done == null ? 43 : $done.hashCode());
        result = result * 59 + ((Object) this.intents).hashCode();
        result = result * 59 + this.fired.hashCode();
        result = result * 59 + this.latch.hashCode();
        return result;
    }

    public void postCall() {
        if (this.latch.get() == 0) {
            this.done.done((T) this, null);
        }
        this.fired.set(true);
    }


    public void registerIntent(Plugin plugin) {
        Preconditions.checkState(!this.fired.get(), "Event %s has already been fired", this);

        AtomicInteger intentCount = this.intents.get(plugin);
        if (intentCount == null) {
            this.intents.put(plugin, new AtomicInteger(1));
        } else {
            intentCount.incrementAndGet();
        }
        this.latch.incrementAndGet();
    }


    public void completeIntent(Plugin plugin) {
        AtomicInteger intentCount = this.intents.get(plugin);
        Preconditions.checkState((intentCount != null) && (intentCount.get() > 0), "Plugin %s has not registered intents for event %s", plugin, this);

        intentCount.decrementAndGet();
        if (this.fired.get()) {
            if (this.latch.decrementAndGet() == 0) {
                this.done.done((T) this, null);
            }
        } else {
            this.latch.decrementAndGet();
        }
    }
}


