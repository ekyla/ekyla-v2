/*    */ package net.md_5.bungee.api.chat;
/*    */ 
/*    */ import java.util.Arrays;
/*    */ 
/*    */ public final class HoverEvent {
/*    */   private final Action action;
/*    */   private final BaseComponent[] value;
/*    */   
/*  9 */   public String toString() { return "HoverEvent(action=" + getAction() + ", value=" + Arrays.deepToString(getValue()) + ")"; }
/* 10 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof HoverEvent)) return false; HoverEvent other = (HoverEvent)o;Object this$action = getAction();Object other$action = other.getAction(); if (this$action == null ? other$action != null : !this$action.equals(other$action)) return false; return Arrays.deepEquals(getValue(), other.getValue()); } public int hashCode() { int PRIME = 59;int result = 1;Object $action = getAction();result = result * 59 + ($action == null ? 43 : $action.hashCode());result = result * 59 + Arrays.deepHashCode(getValue());return result; }
/* 11 */   public HoverEvent(Action action, BaseComponent[] value) { this.action = action;this.value = value;
/*    */   }
/*    */   
/*    */ 
/* 15 */   public Action getAction() { return this.action; }
/* 16 */   public BaseComponent[] getValue() { return this.value; }
/*    */   
/*    */ 
/*    */   public static enum Action
/*    */   {
/* 21 */     SHOW_TEXT, 
/* 22 */     SHOW_ACHIEVEMENT, 
/* 23 */     SHOW_ITEM, 
/* 24 */     SHOW_ENTITY;
/*    */     
/*    */     private Action() {}
/*    */   }
/*    */ }


