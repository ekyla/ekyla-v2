/*    */ package net.md_5.bungee.api.event;
/*    */ 
/*    */ import net.md_5.bungee.api.CommandSender;
/*    */ import net.md_5.bungee.api.plugin.Event;
/*    */ 
/*    */ 
/*    */ 
/*    */ public class PermissionCheckEvent
/*    */   extends Event
/*    */ {
/*    */   private final CommandSender sender;
/*    */   private final String permission;
/*    */   private boolean hasPermission;
/*    */   
/* 15 */   public void setHasPermission(boolean hasPermission) { this.hasPermission = hasPermission; }
/* 16 */   public PermissionCheckEvent(CommandSender sender, String permission, boolean hasPermission) { this.sender = sender;this.permission = permission;this.hasPermission = hasPermission; }
/* 17 */   public String toString() { return "PermissionCheckEvent(sender=" + getSender() + ", permission=" + getPermission() + ", hasPermission=" + this.hasPermission + ")"; }
/* 18 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof PermissionCheckEvent)) return false; PermissionCheckEvent other = (PermissionCheckEvent)o; if (!other.canEqual(this)) return false; Object this$sender = getSender();Object other$sender = other.getSender(); if (this$sender == null ? other$sender != null : !this$sender.equals(other$sender)) return false; Object this$permission = getPermission();Object other$permission = other.getPermission(); if (this$permission == null ? other$permission != null : !this$permission.equals(other$permission)) return false; return this.hasPermission == other.hasPermission; } protected boolean canEqual(Object other) { return other instanceof PermissionCheckEvent; } public int hashCode() { int PRIME = 59;int result = 1;Object $sender = getSender();result = result * 59 + ($sender == null ? 43 : $sender.hashCode());Object $permission = getPermission();result = result * 59 + ($permission == null ? 43 : $permission.hashCode());result = result * 59 + (this.hasPermission ? 79 : 97);return result;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public CommandSender getSender()
/*    */   {
/* 25 */     return this.sender;
/*    */   }
/*    */   
/*    */   public String getPermission() {
/* 29 */     return this.permission;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public boolean hasPermission()
/*    */   {
/* 38 */     return this.hasPermission;
/*    */   }
/*    */ }


