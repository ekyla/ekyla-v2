/*    */ package net.md_5.bungee.api;
/*    */ 
/*    */ import lombok.NonNull;
/*    */ 
/*    */ public class ServerConnectRequest { @NonNull
/*    */   private final net.md_5.bungee.api.config.ServerInfo target;
/*    */   @NonNull
/*    */   private final net.md_5.bungee.api.event.ServerConnectEvent.Reason reason;
/*    */   private final Callback<Result> callback;
/*    */   private final int connectTimeout;
/*    */   private final boolean retry;
/*    */   
/* 13 */   ServerConnectRequest(@NonNull net.md_5.bungee.api.config.ServerInfo target, @NonNull net.md_5.bungee.api.event.ServerConnectEvent.Reason reason, Callback<Result> callback, int connectTimeout, boolean retry) { if (target == null) throw new NullPointerException("target is marked @NonNull but is null"); if (reason == null) throw new NullPointerException("reason is marked @NonNull but is null"); this.target = target;this.reason = reason;this.callback = callback;this.connectTimeout = connectTimeout;this.retry = retry; } public static Builder builder() { return new Builder(); }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public static enum Result
/*    */   {
/* 26 */     EVENT_CANCEL, 
/*    */     
/*    */ 
/*    */ 
/* 30 */     ALREADY_CONNECTED, 
/*    */     
/*    */ 
/*    */ 
/* 34 */     ALREADY_CONNECTING, 
/*    */     
/*    */ 
/*    */ 
/* 38 */     SUCCESS, 
/*    */     
/*    */ 
/*    */ 
/* 42 */     FAIL;
/*    */     
/*    */     private Result() {}
/*    */   }
/*    */   
/*    */   @NonNull
/*    */   public net.md_5.bungee.api.config.ServerInfo getTarget() {
/* 49 */     return this.target;
/*    */   }
/*    */   
/*    */   @NonNull
/*    */   public net.md_5.bungee.api.event.ServerConnectEvent.Reason getReason() {
/* 54 */     return this.reason;
/*    */   }
/*    */   
/*    */   public Callback<Result> getCallback() {
/* 58 */     return this.callback;
/*    */   }
/*    */   
/*    */   public int getConnectTimeout() {
/* 62 */     return this.connectTimeout;
/*    */   }
/*    */   
/*    */   public boolean isRetry()
/*    */   {
/* 67 */     return this.retry;
/*    */   }
/*    */   
/*    */   public static class Builder
/*    */   {
/*    */     private net.md_5.bungee.api.config.ServerInfo target;
/*    */     private net.md_5.bungee.api.event.ServerConnectEvent.Reason reason;
/*    */     private Callback<ServerConnectRequest.Result> callback;
/*    */     private boolean retry;
/*    */     
/* 13 */     public String toString() { return "ServerConnectRequest.Builder(target=" + this.target + ", reason=" + this.reason + ", callback=" + this.callback + ", connectTimeout=" + this.connectTimeout + ", retry=" + this.retry + ")"; } public ServerConnectRequest build() { return new ServerConnectRequest(this.target, this.reason, this.callback, this.connectTimeout, this.retry); } public Builder retry(boolean retry) { this.retry = retry;return this; } public Builder connectTimeout(int connectTimeout) { this.connectTimeout = connectTimeout;return this; } public Builder callback(Callback<ServerConnectRequest.Result> callback) { this.callback = callback;return this; } public Builder reason(@NonNull net.md_5.bungee.api.event.ServerConnectEvent.Reason reason) { if (reason == null) throw new NullPointerException("reason is marked @NonNull but is null"); this.reason = reason;return this; } public Builder target(@NonNull net.md_5.bungee.api.config.ServerInfo target) { if (target == null) throw new NullPointerException("target is marked @NonNull but is null"); this.target = target;return this;
/*    */     }
/*    */     
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 76 */     private int connectTimeout = 5000;
/*    */   }
/*    */ }


