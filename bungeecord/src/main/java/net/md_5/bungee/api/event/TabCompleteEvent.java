/*    */ package net.md_5.bungee.api.event;
/*    */ 
/*    */ import net.md_5.bungee.api.connection.Connection;
/*    */ 
/*    */ public class TabCompleteEvent
/*    */   extends TargetedEvent
/*    */   implements net.md_5.bungee.api.plugin.Cancellable
/*    */ {
/*    */   private boolean cancelled;
/*    */   private final String cursor;
/*    */   private final java.util.List<String> suggestions;
/*    */   
/* 13 */   public void setCancelled(boolean cancelled) { this.cancelled = cancelled; }
/* 14 */   public String toString() { return "TabCompleteEvent(super=" + super.toString() + ", cancelled=" + isCancelled() + ", cursor=" + getCursor() + ", suggestions=" + getSuggestions() + ")"; }
/* 15 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof TabCompleteEvent)) return false; TabCompleteEvent other = (TabCompleteEvent)o; if (!other.canEqual(this)) return false; if (!super.equals(o)) return false; if (isCancelled() != other.isCancelled()) return false; Object this$cursor = getCursor();Object other$cursor = other.getCursor(); if (this$cursor == null ? other$cursor != null : !this$cursor.equals(other$cursor)) return false; Object this$suggestions = getSuggestions();Object other$suggestions = other.getSuggestions();return this$suggestions == null ? other$suggestions == null : this$suggestions.equals(other$suggestions); } protected boolean canEqual(Object other) { return other instanceof TabCompleteEvent; } public int hashCode() { int PRIME = 59;int result = super.hashCode();result = result * 59 + (isCancelled() ? 79 : 97);Object $cursor = getCursor();result = result * 59 + ($cursor == null ? 43 : $cursor.hashCode());Object $suggestions = getSuggestions();result = result * 59 + ($suggestions == null ? 43 : $suggestions.hashCode());return result;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public boolean isCancelled()
/*    */   {
/* 22 */     return this.cancelled;
/*    */   }
/*    */   
/*    */   public String getCursor() {
/* 26 */     return this.cursor;
/*    */   }
/*    */   
/*    */   public java.util.List<String> getSuggestions()
/*    */   {
/* 31 */     return this.suggestions;
/*    */   }
/*    */   
/*    */   public TabCompleteEvent(Connection sender, Connection receiver, String cursor, java.util.List<String> suggestions) {
/* 35 */     super(sender, receiver);
/* 36 */     this.cursor = cursor;
/* 37 */     this.suggestions = suggestions;
/*    */   }
/*    */ }


