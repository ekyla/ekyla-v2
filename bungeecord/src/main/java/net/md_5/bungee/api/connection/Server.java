package net.md_5.bungee.api.connection;

import net.md_5.bungee.api.config.ServerInfo;

public abstract interface Server
  extends Connection
{
  public abstract ServerInfo getInfo();
  
  public abstract void sendData(String paramString, byte[] paramArrayOfByte);
}


