package net.md_5.bungee.api;

import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public abstract interface ReconnectHandler
{
  public abstract ServerInfo getServer(ProxiedPlayer paramProxiedPlayer);
  
  public abstract void setServer(ProxiedPlayer paramProxiedPlayer);
  
  public abstract void save();
  
  public abstract void close();
}


