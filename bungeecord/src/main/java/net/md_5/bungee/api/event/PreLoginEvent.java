/*    */ package net.md_5.bungee.api.event;
/*    */ 
/*    */ import java.util.Arrays;
/*    */ import net.md_5.bungee.api.Callback;
/*    */ import net.md_5.bungee.api.chat.BaseComponent;
/*    */ import net.md_5.bungee.api.chat.TextComponent;
/*    */ import net.md_5.bungee.api.connection.PendingConnection;
/*    */ import net.md_5.bungee.api.plugin.Cancellable;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class PreLoginEvent
/*    */   extends AsyncEvent<PreLoginEvent>
/*    */   implements Cancellable
/*    */ {
/*    */   private boolean cancelled;
/*    */   private BaseComponent[] cancelReasonComponents;
/*    */   private final PendingConnection connection;
/*    */   
/* 22 */   public void setCancelled(boolean cancelled) { this.cancelled = cancelled; }
/* 23 */   public String toString() { return "PreLoginEvent(cancelled=" + isCancelled() + ", cancelReasonComponents=" + Arrays.deepToString(getCancelReasonComponents()) + ", connection=" + getConnection() + ")"; }
/* 24 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof PreLoginEvent)) return false; PreLoginEvent other = (PreLoginEvent)o; if (!other.canEqual(this)) return false; if (isCancelled() != other.isCancelled()) return false; if (!Arrays.deepEquals(getCancelReasonComponents(), other.getCancelReasonComponents())) return false; Object this$connection = getConnection();Object other$connection = other.getConnection();return this$connection == null ? other$connection == null : this$connection.equals(other$connection); } protected boolean canEqual(Object other) { return other instanceof PreLoginEvent; } public int hashCode() { int PRIME = 59;int result = 1;result = result * 59 + (isCancelled() ? 79 : 97);result = result * 59 + Arrays.deepHashCode(getCancelReasonComponents());Object $connection = getConnection();result = result * 59 + ($connection == null ? 43 : $connection.hashCode());return result;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public boolean isCancelled()
/*    */   {
/* 31 */     return this.cancelled;
/*    */   }
/*    */   
/*    */   public BaseComponent[] getCancelReasonComponents()
/*    */   {
/* 36 */     return this.cancelReasonComponents;
/*    */   }
/*    */   
/*    */   public PendingConnection getConnection() {
/* 40 */     return this.connection;
/*    */   }
/*    */   
/*    */   public PreLoginEvent(PendingConnection connection, Callback<PreLoginEvent> done) {
/* 44 */     super(done);
/* 45 */     this.connection = connection;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   @Deprecated
/*    */   public String getCancelReason()
/*    */   {
/* 55 */     return BaseComponent.toLegacyText(getCancelReasonComponents());
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   @Deprecated
/*    */   public void setCancelReason(String cancelReason)
/*    */   {
/* 67 */     setCancelReason(TextComponent.fromLegacyText(cancelReason));
/*    */   }
/*    */   
/*    */   public void setCancelReason(BaseComponent... cancelReason)
/*    */   {
/* 72 */     this.cancelReasonComponents = cancelReason;
/*    */   }
/*    */ }


