package net.md_5.bungee.api.connection;

import java.net.InetSocketAddress;
import java.util.UUID;
import net.md_5.bungee.api.config.ListenerInfo;

public abstract interface PendingConnection
  extends Connection
{
  public abstract String getName();
  
  public abstract int getVersion();
  
  public abstract InetSocketAddress getVirtualHost();
  
  public abstract ListenerInfo getListener();
  
  @Deprecated
  public abstract String getUUID();
  
  public abstract UUID getUniqueId();
  
  public abstract void setUniqueId(UUID paramUUID);
  
  public abstract boolean isOnlineMode();
  
  public abstract void setOnlineMode(boolean paramBoolean);
  
  public abstract boolean isLegacy();
}


