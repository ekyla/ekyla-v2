/*    */ package net.md_5.bungee.api.event;
/*    */ 
/*    */ import java.util.List;
/*    */ import net.md_5.bungee.api.connection.Connection;
/*    */ import net.md_5.bungee.api.plugin.Cancellable;
/*    */ 
/*    */ 
/*    */ 
/*    */ public class TabCompleteResponseEvent
/*    */   extends TargetedEvent
/*    */   implements Cancellable
/*    */ {
/*    */   private boolean cancelled;
/*    */   private final List<String> suggestions;
/*    */   
/* 16 */   public void setCancelled(boolean cancelled) { this.cancelled = cancelled; }
/* 17 */   public String toString() { return "TabCompleteResponseEvent(super=" + super.toString() + ", cancelled=" + isCancelled() + ", suggestions=" + getSuggestions() + ")"; }
/* 18 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof TabCompleteResponseEvent)) return false; TabCompleteResponseEvent other = (TabCompleteResponseEvent)o; if (!other.canEqual(this)) return false; if (!super.equals(o)) return false; if (isCancelled() != other.isCancelled()) return false; Object this$suggestions = getSuggestions();Object other$suggestions = other.getSuggestions();return this$suggestions == null ? other$suggestions == null : this$suggestions.equals(other$suggestions); } protected boolean canEqual(Object other) { return other instanceof TabCompleteResponseEvent; } public int hashCode() { int PRIME = 59;int result = super.hashCode();result = result * 59 + (isCancelled() ? 79 : 97);Object $suggestions = getSuggestions();result = result * 59 + ($suggestions == null ? 43 : $suggestions.hashCode());return result;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public boolean isCancelled()
/*    */   {
/* 25 */     return this.cancelled;
/*    */   }
/*    */   
/*    */ 
/*    */   public List<String> getSuggestions()
/*    */   {
/* 31 */     return this.suggestions;
/*    */   }
/*    */   
/*    */   public TabCompleteResponseEvent(Connection sender, Connection receiver, List<String> suggestions) {
/* 35 */     super(sender, receiver);
/* 36 */     this.suggestions = suggestions;
/*    */   }
/*    */ }


