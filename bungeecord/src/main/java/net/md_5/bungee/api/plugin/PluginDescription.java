/*    */ package net.md_5.bungee.api.plugin;
/*    */ 
/*    */ import java.util.Set;
/*    */ 
/*    */ 
/*    */ public class PluginDescription
/*    */ {
/*    */   private String name;
/*    */   private String main;
/*    */   private String version;
/*    */   private String author;
/*    */   
/* 13 */   public void setName(String name) { this.name = name; } public void setMain(String main) { this.main = main; } public void setVersion(String version) { this.version = version; } public void setAuthor(String author) { this.author = author; } public void setDepends(Set<String> depends) { this.depends = depends; } public void setSoftDepends(Set<String> softDepends) { this.softDepends = softDepends; } public void setFile(java.io.File file) { this.file = file; } public void setDescription(String description) { this.description = description; } public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof PluginDescription)) return false; PluginDescription other = (PluginDescription)o; if (!other.canEqual(this)) return false; Object this$name = getName();Object other$name = other.getName(); if (this$name == null ? other$name != null : !this$name.equals(other$name)) return false; Object this$main = getMain();Object other$main = other.getMain(); if (this$main == null ? other$main != null : !this$main.equals(other$main)) return false; Object this$version = getVersion();Object other$version = other.getVersion(); if (this$version == null ? other$version != null : !this$version.equals(other$version)) return false; Object this$author = getAuthor();Object other$author = other.getAuthor(); if (this$author == null ? other$author != null : !this$author.equals(other$author)) return false; Object this$depends = getDepends();Object other$depends = other.getDepends(); if (this$depends == null ? other$depends != null : !this$depends.equals(other$depends)) return false; Object this$softDepends = getSoftDepends();Object other$softDepends = other.getSoftDepends(); if (this$softDepends == null ? other$softDepends != null : !this$softDepends.equals(other$softDepends)) return false; Object this$file = getFile();Object other$file = other.getFile(); if (this$file == null ? other$file != null : !this$file.equals(other$file)) return false; Object this$description = getDescription();Object other$description = other.getDescription();return this$description == null ? other$description == null : this$description.equals(other$description); } protected boolean canEqual(Object other) { return other instanceof PluginDescription; } public int hashCode() { int PRIME = 59;int result = 1;Object $name = getName();result = result * 59 + ($name == null ? 43 : $name.hashCode());Object $main = getMain();result = result * 59 + ($main == null ? 43 : $main.hashCode());Object $version = getVersion();result = result * 59 + ($version == null ? 43 : $version.hashCode());Object $author = getAuthor();result = result * 59 + ($author == null ? 43 : $author.hashCode());Object $depends = getDepends();result = result * 59 + ($depends == null ? 43 : $depends.hashCode());Object $softDepends = getSoftDepends();result = result * 59 + ($softDepends == null ? 43 : $softDepends.hashCode());Object $file = getFile();result = result * 59 + ($file == null ? 43 : $file.hashCode());Object $description = getDescription();result = result * 59 + ($description == null ? 43 : $description.hashCode());return result; } public String toString() { return "PluginDescription(name=" + getName() + ", main=" + getMain() + ", version=" + getVersion() + ", author=" + getAuthor() + ", depends=" + getDepends() + ", softDepends=" + getSoftDepends() + ", file=" + getFile() + ", description=" + getDescription() + ")"; }
/*    */   
/* 15 */   public PluginDescription(String name, String main, String version, String author, Set<String> depends, Set<String> softDepends, java.io.File file, String description) { this.name = name;this.main = main;this.version = version;this.author = author;this.depends = depends;this.softDepends = softDepends;this.file = file;this.description = description;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public String getName()
/*    */   {
/* 22 */     return this.name;
/*    */   }
/*    */   
/*    */   public String getMain() {
/* 26 */     return this.main;
/*    */   }
/*    */   
/*    */   public String getVersion() {
/* 30 */     return this.version;
/*    */   }
/*    */   
/*    */   public String getAuthor() {
/* 34 */     return this.author;
/*    */   }
/*    */   
/*    */ 
/* 38 */   public Set<String> getDepends() { return this.depends; } private Set<String> depends = new java.util.HashSet();
/*    */   
/*    */ 
/*    */ 
/* 42 */   public Set<String> getSoftDepends() { return this.softDepends; } private Set<String> softDepends = new java.util.HashSet();
/*    */   
/*    */ 
/*    */ 
/* 46 */   public java.io.File getFile() { return this.file; } private java.io.File file = null;
/*    */   
/*    */ 
/*    */ 
/* 50 */   public String getDescription() { return this.description; } private String description = null;
/*    */   
/*    */   public PluginDescription() {}
/*    */ }


