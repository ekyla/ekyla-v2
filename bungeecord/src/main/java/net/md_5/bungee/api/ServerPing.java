/*     */ package net.md_5.bungee.api;
/*     */ 
/*     */ import java.util.Arrays;
/*     */ import java.util.List;
/*     */ import java.util.UUID;
/*     */ import net.md_5.bungee.api.chat.BaseComponent;
/*     */ import net.md_5.bungee.api.chat.TextComponent;
/*     */ 
/*     */ 
/*     */ 
/*     */ public class ServerPing
/*     */ {
/*     */   private Protocol version;
/*     */   private Players players;
/*     */   private BaseComponent description;
/*     */   private Favicon favicon;
/*     */   
/*  18 */   public void setVersion(Protocol version) { this.version = version; } public void setPlayers(Players players) { this.players = players; } public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof ServerPing)) return false; ServerPing other = (ServerPing)o; if (!other.canEqual(this)) return false; Object this$version = getVersion();Object other$version = other.getVersion(); if (this$version == null ? other$version != null : !this$version.equals(other$version)) return false; Object this$players = getPlayers();Object other$players = other.getPlayers(); if (this$players == null ? other$players != null : !this$players.equals(other$players)) return false; Object this$description = getDescription();Object other$description = other.getDescription(); if (this$description == null ? other$description != null : !this$description.equals(other$description)) return false; Object this$favicon = getFavicon();Object other$favicon = other.getFavicon(); if (this$favicon == null ? other$favicon != null : !this$favicon.equals(other$favicon)) return false; Object this$modinfo = getModinfo();Object other$modinfo = other.getModinfo();return this$modinfo == null ? other$modinfo == null : this$modinfo.equals(other$modinfo); } protected boolean canEqual(Object other) { return other instanceof ServerPing; } public int hashCode() { int PRIME = 59;int result = 1;Object $version = getVersion();result = result * 59 + ($version == null ? 43 : $version.hashCode());Object $players = getPlayers();result = result * 59 + ($players == null ? 43 : $players.hashCode());Object $description = getDescription();result = result * 59 + ($description == null ? 43 : $description.hashCode());Object $favicon = getFavicon();result = result * 59 + ($favicon == null ? 43 : $favicon.hashCode());Object $modinfo = getModinfo();result = result * 59 + ($modinfo == null ? 43 : $modinfo.hashCode());return result; }
/*  19 */   public String toString() { return "ServerPing(version=" + getVersion() + ", players=" + getPlayers() + ", description=" + getDescription() + ", modinfo=" + getModinfo() + ")"; }
/*     */   
/*  21 */   public ServerPing(Protocol version, Players players, BaseComponent description, Favicon favicon) { this.version = version;this.players = players;this.description = description;this.favicon = favicon;
/*     */   }
/*     */   
/*     */ 
/*  25 */   public Protocol getVersion() { return this.version; }
/*     */   
/*  27 */   public static class Protocol { public void setName(String name) { this.name = name; } public void setProtocol(int protocol) { this.protocol = protocol; } public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof Protocol)) return false; Protocol other = (Protocol)o; if (!other.canEqual(this)) return false; Object this$name = getName();Object other$name = other.getName(); if (this$name == null ? other$name != null : !this$name.equals(other$name)) return false; return getProtocol() == other.getProtocol(); } protected boolean canEqual(Object other) { return other instanceof Protocol; } public int hashCode() { int PRIME = 59;int result = 1;Object $name = getName();result = result * 59 + ($name == null ? 43 : $name.hashCode());result = result * 59 + getProtocol();return result; } public String toString() { return "ServerPing.Protocol(name=" + getName() + ", protocol=" + getProtocol() + ")"; }
/*  28 */     public Protocol(String name, int protocol) { this.name = name;this.protocol = protocol; }
/*     */     
/*     */     private String name;
/*     */     private int protocol;
/*  32 */     public String getName() { return this.name; }
/*  33 */     public int getProtocol() { return this.protocol; } }
/*     */   
/*  35 */   public Players getPlayers() { return this.players; }
/*     */   public static class Players { private int max;
/*  37 */     public void setMax(int max) { this.max = max; } public void setOnline(int online) { this.online = online; } public void setSample(ServerPing.PlayerInfo[] sample) { this.sample = sample; } public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof Players)) return false; Players other = (Players)o; if (!other.canEqual(this)) return false; if (getMax() != other.getMax()) return false; if (getOnline() != other.getOnline()) return false; return Arrays.deepEquals(getSample(), other.getSample()); } protected boolean canEqual(Object other) { return other instanceof Players; } public int hashCode() { int PRIME = 59;int result = 1;result = result * 59 + getMax();result = result * 59 + getOnline();result = result * 59 + Arrays.deepHashCode(getSample());return result; } public String toString() { return "ServerPing.Players(max=" + getMax() + ", online=" + getOnline() + ", sample=" + Arrays.deepToString(getSample()) + ")"; }
/*  38 */     public Players(int max, int online, ServerPing.PlayerInfo[] sample) { this.max = max;this.online = online;this.sample = sample; }
/*     */     
/*     */     private int online;
/*     */     private ServerPing.PlayerInfo[] sample;
/*  42 */     public int getMax() { return this.max; }
/*  43 */     public int getOnline() { return this.online; }
/*  44 */     public ServerPing.PlayerInfo[] getSample() { return this.sample; }
/*     */     
/*     */      }
/*  47 */   public static class PlayerInfo { public void setName(String name) { this.name = name; } public void setUniqueId(UUID uniqueId) { this.uniqueId = uniqueId; } public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof PlayerInfo)) return false; PlayerInfo other = (PlayerInfo)o; if (!other.canEqual(this)) return false; Object this$name = getName();Object other$name = other.getName(); if (this$name == null ? other$name != null : !this$name.equals(other$name)) return false; Object this$uniqueId = getUniqueId();Object other$uniqueId = other.getUniqueId();return this$uniqueId == null ? other$uniqueId == null : this$uniqueId.equals(other$uniqueId); } protected boolean canEqual(Object other) { return other instanceof PlayerInfo; } public int hashCode() { int PRIME = 59;int result = 1;Object $name = getName();result = result * 59 + ($name == null ? 43 : $name.hashCode());Object $uniqueId = getUniqueId();result = result * 59 + ($uniqueId == null ? 43 : $uniqueId.hashCode());return result; } public String toString() { return "ServerPing.PlayerInfo(name=" + getName() + ", uniqueId=" + getUniqueId() + ")"; }
/*  48 */     public PlayerInfo(String name, UUID uniqueId) { this.name = name;this.uniqueId = uniqueId; }
/*     */     
/*     */     private String name;
/*     */     private UUID uniqueId;
/*  52 */     public String getName() { return this.name; }
/*  53 */     public UUID getUniqueId() { return this.uniqueId; }
/*     */     
/*  55 */     private static final UUID md5UUID = net.md_5.bungee.Util.getUUID("af74a02d19cb445bb07f6866a861f783");
/*     */     
/*     */     public PlayerInfo(String name, String id)
/*     */     {
/*  59 */       setName(name);
/*  60 */       setId(id);
/*     */     }
/*     */     
/*     */     public void setId(String id)
/*     */     {
/*     */       try
/*     */       {
/*  67 */         this.uniqueId = net.md_5.bungee.Util.getUUID(id);
/*     */       }
/*     */       catch (Exception e)
/*     */       {
/*  71 */         this.uniqueId = md5UUID;
/*     */       }
/*     */     }
/*     */     
/*     */     public String getId()
/*     */     {
/*  77 */       return this.uniqueId.toString().replace("-", "");
/*     */     }
/*     */   }
/*     */   
/*     */   public static class ModInfo
/*     */   {
/*     */     public void setType(String type) {
/*  84 */       this.type = type; } public void setModList(List<ServerPing.ModItem> modList) { this.modList = modList; } public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof ModInfo)) return false; ModInfo other = (ModInfo)o; if (!other.canEqual(this)) return false; Object this$type = getType();Object other$type = other.getType(); if (this$type == null ? other$type != null : !this$type.equals(other$type)) return false; Object this$modList = getModList();Object other$modList = other.getModList();return this$modList == null ? other$modList == null : this$modList.equals(other$modList); } protected boolean canEqual(Object other) { return other instanceof ModInfo; } public int hashCode() { int PRIME = 59;int result = 1;Object $type = getType();result = result * 59 + ($type == null ? 43 : $type.hashCode());Object $modList = getModList();result = result * 59 + ($modList == null ? 43 : $modList.hashCode());return result; } public String toString() { return "ServerPing.ModInfo(type=" + getType() + ", modList=" + getModList() + ")"; }
/*     */     
/*     */ 
/*     */ 
/*  88 */     public String getType() { return this.type; } private String type = "FML";
/*  89 */     public List<ServerPing.ModItem> getModList() { return this.modList; } private List<ServerPing.ModItem> modList = new java.util.ArrayList();
/*     */   }
/*     */   
/*  92 */   public static class ModItem { public void setModid(String modid) { this.modid = modid; } public void setVersion(String version) { this.version = version; } public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof ModItem)) return false; ModItem other = (ModItem)o; if (!other.canEqual(this)) return false; Object this$modid = getModid();Object other$modid = other.getModid(); if (this$modid == null ? other$modid != null : !this$modid.equals(other$modid)) return false; Object this$version = getVersion();Object other$version = other.getVersion();return this$version == null ? other$version == null : this$version.equals(other$version); } protected boolean canEqual(Object other) { return other instanceof ModItem; } public int hashCode() { int PRIME = 59;int result = 1;Object $modid = getModid();result = result * 59 + ($modid == null ? 43 : $modid.hashCode());Object $version = getVersion();result = result * 59 + ($version == null ? 43 : $version.hashCode());return result; } public String toString() { return "ServerPing.ModItem(modid=" + getModid() + ", version=" + getVersion() + ")"; }
/*  93 */     public ModItem(String modid, String version) { this.modid = modid;this.version = version; }
/*     */     
/*     */     private String modid;
/*     */     private String version;
/*  97 */     public String getModid() { return this.modid; }
/*  98 */     public String getVersion() { return this.version; }
/*     */   }
/*     */   
/*     */   public ModInfo getModinfo()
/*     */   {
/* 103 */     return this.modinfo; } private final ModInfo modinfo = new ModInfo();
/*     */   
/*     */   @Deprecated
/*     */   public ServerPing(Protocol version, Players players, String description, String favicon)
/*     */   {
/* 108 */     this(version, players, new TextComponent(TextComponent.fromLegacyText(description)), favicon == null ? null : Favicon.create(favicon));
/*     */   }
/*     */   
/*     */   @Deprecated
/*     */   public ServerPing(Protocol version, Players players, String description, Favicon favicon)
/*     */   {
/* 114 */     this(version, players, new TextComponent(TextComponent.fromLegacyText(description)), favicon);
/*     */   }
/*     */   
/*     */   @Deprecated
/*     */   public String getFavicon()
/*     */   {
/* 120 */     return getFaviconObject() == null ? null : getFaviconObject().getEncoded();
/*     */   }
/*     */   
/*     */   public Favicon getFaviconObject()
/*     */   {
/* 125 */     return this.favicon;
/*     */   }
/*     */   
/*     */   @Deprecated
/*     */   public void setFavicon(String favicon)
/*     */   {
/* 131 */     setFavicon(favicon == null ? null : Favicon.create(favicon));
/*     */   }
/*     */   
/*     */   public void setFavicon(Favicon favicon)
/*     */   {
/* 136 */     this.favicon = favicon;
/*     */   }
/*     */   
/*     */   @Deprecated
/*     */   public void setDescription(String description)
/*     */   {
/* 142 */     this.description = new TextComponent(TextComponent.fromLegacyText(description));
/*     */   }
/*     */   
/*     */   @Deprecated
/*     */   public String getDescription()
/*     */   {
/* 148 */     return BaseComponent.toLegacyText(new BaseComponent[] { this.description });
/*     */   }
/*     */   
/*     */   public void setDescriptionComponent(BaseComponent description)
/*     */   {
/* 153 */     this.description = description;
/*     */   }
/*     */   
/*     */   public BaseComponent getDescriptionComponent()
/*     */   {
/* 158 */     return this.description;
/*     */   }
/*     */   
/*     */   public ServerPing() {}
/*     */ }


