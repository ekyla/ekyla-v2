/*    */ package net.md_5.bungee.api.score;
/*    */ 
/*    */ import java.util.Set;
/*    */ 
/*    */ public class Team { @lombok.NonNull
/*    */   private final String name;
/*    */   private String displayName;
/*    */   private String prefix;
/*    */   private String suffix; private byte friendlyFire; private String nameTagVisibility;
/* 10 */   public Team(@lombok.NonNull String name) { if (name == null) throw new NullPointerException("name is marked @NonNull but is null"); this.name = name; } public void setDisplayName(String displayName) { this.displayName = displayName; } public void setPrefix(String prefix) { this.prefix = prefix; } public void setSuffix(String suffix) { this.suffix = suffix; } public void setFriendlyFire(byte friendlyFire) { this.friendlyFire = friendlyFire; } public void setNameTagVisibility(String nameTagVisibility) { this.nameTagVisibility = nameTagVisibility; } public void setCollisionRule(String collisionRule) { this.collisionRule = collisionRule; } public void setColor(int color) { this.color = color; } public void setPlayers(Set<String> players) { this.players = players; } public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof Team)) return false; Team other = (Team)o; if (!other.canEqual(this)) return false; Object this$name = getName();Object other$name = other.getName(); if (this$name == null ? other$name != null : !this$name.equals(other$name)) return false; Object this$displayName = getDisplayName();Object other$displayName = other.getDisplayName(); if (this$displayName == null ? other$displayName != null : !this$displayName.equals(other$displayName)) return false; Object this$prefix = getPrefix();Object other$prefix = other.getPrefix(); if (this$prefix == null ? other$prefix != null : !this$prefix.equals(other$prefix)) return false; Object this$suffix = getSuffix();Object other$suffix = other.getSuffix(); if (this$suffix == null ? other$suffix != null : !this$suffix.equals(other$suffix)) return false; if (getFriendlyFire() != other.getFriendlyFire()) return false; Object this$nameTagVisibility = getNameTagVisibility();Object other$nameTagVisibility = other.getNameTagVisibility(); if (this$nameTagVisibility == null ? other$nameTagVisibility != null : !this$nameTagVisibility.equals(other$nameTagVisibility)) return false; Object this$collisionRule = getCollisionRule();Object other$collisionRule = other.getCollisionRule(); if (this$collisionRule == null ? other$collisionRule != null : !this$collisionRule.equals(other$collisionRule)) return false; if (getColor() != other.getColor()) return false; Object this$players = getPlayers();Object other$players = other.getPlayers();return this$players == null ? other$players == null : this$players.equals(other$players); } protected boolean canEqual(Object other) { return other instanceof Team; } public int hashCode() { int PRIME = 59;int result = 1;Object $name = getName();result = result * 59 + ($name == null ? 43 : $name.hashCode());Object $displayName = getDisplayName();result = result * 59 + ($displayName == null ? 43 : $displayName.hashCode());Object $prefix = getPrefix();result = result * 59 + ($prefix == null ? 43 : $prefix.hashCode());Object $suffix = getSuffix();result = result * 59 + ($suffix == null ? 43 : $suffix.hashCode());result = result * 59 + getFriendlyFire();Object $nameTagVisibility = getNameTagVisibility();result = result * 59 + ($nameTagVisibility == null ? 43 : $nameTagVisibility.hashCode());Object $collisionRule = getCollisionRule();result = result * 59 + ($collisionRule == null ? 43 : $collisionRule.hashCode());result = result * 59 + getColor();Object $players = getPlayers();result = result * 59 + ($players == null ? 43 : $players.hashCode());return result; } public String toString() { return "Team(name=" + getName() + ", displayName=" + getDisplayName() + ", prefix=" + getPrefix() + ", suffix=" + getSuffix() + ", friendlyFire=" + getFriendlyFire() + ", nameTagVisibility=" + getNameTagVisibility() + ", collisionRule=" + getCollisionRule() + ", color=" + getColor() + ", players=" + getPlayers() + ")"; }
/*    */   
/*    */   private String collisionRule;
/*    */   private int color;
/*    */   @lombok.NonNull
/* 15 */   public String getName() { return this.name; }
/* 16 */   public String getDisplayName() { return this.displayName; }
/* 17 */   public String getPrefix() { return this.prefix; }
/* 18 */   public String getSuffix() { return this.suffix; }
/* 19 */   public byte getFriendlyFire() { return this.friendlyFire; }
/* 20 */   public String getNameTagVisibility() { return this.nameTagVisibility; }
/* 21 */   public String getCollisionRule() { return this.collisionRule; }
/* 22 */   public int getColor() { return this.color; }
/* 23 */   private Set<String> players = new java.util.HashSet();
/*    */   
/*    */   public java.util.Collection<String> getPlayers()
/*    */   {
/* 27 */     return java.util.Collections.unmodifiableSet(this.players);
/*    */   }
/*    */   
/*    */   public void addPlayer(String name)
/*    */   {
/* 32 */     this.players.add(name);
/*    */   }
/*    */   
/*    */   public void removePlayer(String name)
/*    */   {
/* 37 */     this.players.remove(name);
/*    */   }
/*    */ }


