package net.md_5.bungee.api.scheduler;

import net.md_5.bungee.api.plugin.Plugin;

import java.util.Objects;

@Deprecated
public class GroupedThreadFactory implements java.util.concurrent.ThreadFactory {
    private final ThreadGroup group;

    public GroupedThreadFactory(Plugin plugin, String name) {
        this.group = new BungeeGroup(name);
    }

    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof GroupedThreadFactory)) return false;
        GroupedThreadFactory other = (GroupedThreadFactory) o;
        if (!other.canEqual(this)) return false;
        Object this$group = getGroup();
        Object other$group = other.getGroup();
        return Objects.equals(this$group, other$group);
    }

    protected boolean canEqual(Object other) {
        return other instanceof GroupedThreadFactory;
    }

    public int hashCode() {
        int PRIME = 59;
        int result = 1;
        Object $group = getGroup();
        result = result * 59 + ($group == null ? 43 : $group.hashCode());
        return result;
    }

    public String toString() {
        return "GroupedThreadFactory(group=" + getGroup() + ")";
    }

    public ThreadGroup getGroup() {
        return this.group;
    }

    public Thread newThread(Runnable r) {
        return new Thread(this.group, r);
    }

    public static final class BungeeGroup extends ThreadGroup {
        private BungeeGroup(String name) {
            super(name);
        }
    }
}


