/*    */ package net.md_5.bungee.api.plugin;
/*    */ 
/*    */ import net.md_5.bungee.api.CommandSender;
/*    */ 
/*    */ public abstract class Command
/*    */ {
/*    */   private final String name;
/*    */   private final String permission;
/*    */   private final String[] aliases;
/*    */   
/*    */   public boolean equals(Object o) {
/* 12 */     if (o == this) return true; if (!(o instanceof Command)) return false; Command other = (Command)o; if (!other.canEqual(this)) return false; Object this$name = getName();Object other$name = other.getName(); if (this$name == null ? other$name != null : !this$name.equals(other$name)) return false; Object this$permission = getPermission();Object other$permission = other.getPermission(); if (this$permission == null ? other$permission != null : !this$permission.equals(other$permission)) return false; return java.util.Arrays.deepEquals(getAliases(), other.getAliases()); } protected boolean canEqual(Object other) { return other instanceof Command; } public int hashCode() { int PRIME = 59;int result = 1;Object $name = getName();result = result * 59 + ($name == null ? 43 : $name.hashCode());Object $permission = getPermission();result = result * 59 + ($permission == null ? 43 : $permission.hashCode());result = result * 59 + java.util.Arrays.deepHashCode(getAliases());return result; } public String toString() { return "Command(name=" + getName() + ", permission=" + getPermission() + ", aliases=" + java.util.Arrays.deepToString(getAliases()) + ")"; }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/* 17 */   public String getName() { return this.name; }
/* 18 */   public String getPermission() { return this.permission; }
/* 19 */   public String[] getAliases() { return this.aliases; }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public Command(String name)
/*    */   {
/* 28 */     this(name, null, new String[0]);
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public Command(String name, String permission, String... aliases)
/*    */   {
/* 41 */     com.google.common.base.Preconditions.checkArgument(name != null, "name");
/* 42 */     this.name = name;
/* 43 */     this.permission = permission;
/* 44 */     this.aliases = aliases;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public boolean hasPermission(CommandSender sender)
/*    */   {
/* 63 */     return (this.permission == null) || (this.permission.isEmpty()) || (sender.hasPermission(this.permission));
/*    */   }
/*    */   
/*    */   public abstract void execute(CommandSender paramCommandSender, String[] paramArrayOfString);
/*    */ }


