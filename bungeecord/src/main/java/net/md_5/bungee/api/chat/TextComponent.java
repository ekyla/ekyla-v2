/*     */ package net.md_5.bungee.api.chat;
/*     */ 
/*     */ import java.util.ArrayList;
/*     */ import java.util.Arrays;
/*     */ import java.util.regex.Matcher;
/*     */ import java.util.regex.Pattern;
/*     */ import net.md_5.bungee.api.ChatColor;
/*     */ 
/*     */ 
/*     */ 
/*     */ public final class TextComponent
/*     */   extends BaseComponent
/*     */ {
/*  14 */   public void setText(String text) { this.text = text; }
/*  15 */   public TextComponent(String text) { this.text = text; }
/*  16 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof TextComponent)) return false; TextComponent other = (TextComponent)o; if (!other.canEqual(this)) return false; if (!super.equals(o)) return false; Object this$text = getText();Object other$text = other.getText();return this$text == null ? other$text == null : this$text.equals(other$text); } protected boolean canEqual(Object other) { return other instanceof TextComponent; } public int hashCode() { int PRIME = 59;int result = super.hashCode();Object $text = getText();result = result * 59 + ($text == null ? 43 : $text.hashCode());return result;
/*     */   }
/*     */   
/*     */ 
/*  20 */   private static final Pattern url = Pattern.compile("^(?:(https?)://)?([-\\w_\\.]{2,}\\.[a-z]{2,4})(/\\S*)?$");
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   private String text;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */   public static BaseComponent[] fromLegacyText(String message)
/*     */   {
/*  32 */     return fromLegacyText(message, ChatColor.WHITE);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public static BaseComponent[] fromLegacyText(String message, ChatColor defaultColor)
/*     */   {
/*  47 */     ArrayList<BaseComponent> components = new ArrayList();
/*  48 */     StringBuilder builder = new StringBuilder();
/*  49 */     TextComponent component = new TextComponent();
/*  50 */     Matcher matcher = url.matcher(message);
/*     */     
/*  52 */     for (int i = 0; i < message.length(); i++)
/*     */     {
/*  54 */       char c = message.charAt(i);
/*  55 */       if (c == '§')
/*     */       {
/*  57 */         i++; if (i >= message.length()) {
/*     */           break;
/*     */         }
/*     */         
/*  61 */         c = message.charAt(i);
/*  62 */         if ((c >= 'A') && (c <= 'Z'))
/*     */         {
/*  64 */           c = (char)(c + ' ');
/*     */         }
/*  66 */         ChatColor format = ChatColor.getByChar(c);
/*  67 */         if (format != null)
/*     */         {
/*     */ 
/*     */ 
/*  71 */           if (builder.length() > 0)
/*     */           {
/*  73 */             TextComponent old = component;
/*  74 */             component = new TextComponent(old);
/*  75 */             old.setText(builder.toString());
/*  76 */             builder = new StringBuilder();
/*  77 */             components.add(old);
/*     */           }
/*  79 */           switch (format)
/*     */           {
/*     */           case BOLD: 
/*  82 */             component.setBold(Boolean.valueOf(true));
/*  83 */             break;
/*     */           case ITALIC: 
/*  85 */             component.setItalic(Boolean.valueOf(true));
/*  86 */             break;
/*     */           case UNDERLINE: 
/*  88 */             component.setUnderlined(Boolean.valueOf(true));
/*  89 */             break;
/*     */           case STRIKETHROUGH: 
/*  91 */             component.setStrikethrough(Boolean.valueOf(true));
/*  92 */             break;
/*     */           case MAGIC: 
/*  94 */             component.setObfuscated(Boolean.valueOf(true));
/*  95 */             break;
/*     */           case RESET: 
/*  97 */             format = defaultColor;
/*     */           default: 
/*  99 */             component = new TextComponent();
/* 100 */             component.setColor(format);
/* 101 */             break;
/*     */           }
/*     */         }
/*     */       } else {
/* 105 */         int pos = message.indexOf(' ', i);
/* 106 */         if (pos == -1)
/*     */         {
/* 108 */           pos = message.length();
/*     */         }
/* 110 */         if (matcher.region(i, pos).find())
/*     */         {
/*     */ 
/* 113 */           if (builder.length() > 0)
/*     */           {
/* 115 */             TextComponent old = component;
/* 116 */             component = new TextComponent(old);
/* 117 */             old.setText(builder.toString());
/* 118 */             builder = new StringBuilder();
/* 119 */             components.add(old);
/*     */           }
/*     */           
/* 122 */           TextComponent old = component;
/* 123 */           component = new TextComponent(old);
/* 124 */           String urlString = message.substring(i, pos);
/* 125 */           component.setText(urlString);
/* 126 */           component.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, 
/* 127 */             "http://" + urlString));
/* 128 */           components.add(component);
/* 129 */           i += pos - i - 1;
/* 130 */           component = old;
/*     */         }
/*     */         else {
/* 133 */           builder.append(c);
/*     */         }
/*     */       } }
/* 136 */     component.setText(builder.toString());
/* 137 */     components.add(component);
/*     */     
/* 139 */     return (BaseComponent[])components.toArray(new BaseComponent[components.size()]);
/*     */   }
/*     */   
/*     */ 
/*     */   public String getText()
/*     */   {
/* 145 */     return this.text;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public TextComponent()
/*     */   {
/* 152 */     this.text = "";
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public TextComponent(TextComponent textComponent)
/*     */   {
/* 163 */     super(textComponent);
/* 164 */     setText(textComponent.getText());
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public TextComponent(BaseComponent... extras)
/*     */   {
/* 175 */     setText("");
/* 176 */     setExtra(new ArrayList(Arrays.asList(extras)));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public BaseComponent duplicate()
/*     */   {
/* 187 */     return new TextComponent(this);
/*     */   }
/*     */   
/*     */ 
/*     */   protected void toPlainText(StringBuilder builder)
/*     */   {
/* 193 */     builder.append(this.text);
/* 194 */     super.toPlainText(builder);
/*     */   }
/*     */   
/*     */ 
/*     */   protected void toLegacyText(StringBuilder builder)
/*     */   {
/* 200 */     builder.append(getColor());
/* 201 */     if (isBold())
/*     */     {
/* 203 */       builder.append(ChatColor.BOLD);
/*     */     }
/* 205 */     if (isItalic())
/*     */     {
/* 207 */       builder.append(ChatColor.ITALIC);
/*     */     }
/* 209 */     if (isUnderlined())
/*     */     {
/* 211 */       builder.append(ChatColor.UNDERLINE);
/*     */     }
/* 213 */     if (isStrikethrough())
/*     */     {
/* 215 */       builder.append(ChatColor.STRIKETHROUGH);
/*     */     }
/* 217 */     if (isObfuscated())
/*     */     {
/* 219 */       builder.append(ChatColor.MAGIC);
/*     */     }
/* 221 */     builder.append(this.text);
/* 222 */     super.toLegacyText(builder);
/*     */   }
/*     */   
/*     */ 
/*     */   public String toString()
/*     */   {
/* 228 */     return String.format("TextComponent{text=%s, %s}", new Object[] { this.text, super.toString() });
/*     */   }
/*     */ }


