/*    */ package net.md_5.bungee.api.plugin;
/*    */ 
/*    */ import java.net.URL;
/*    */ import java.net.URLClassLoader;
/*    */ import java.util.Set;
/*    */ import java.util.concurrent.CopyOnWriteArraySet;
/*    */ 
/*    */ public class PluginClassloader
/*    */   extends URLClassLoader
/*    */ {
/* 11 */   private static final Set<PluginClassloader> allLoaders = new CopyOnWriteArraySet();
/*    */   
/*    */   static
/*    */   {
/* 15 */     ClassLoader.registerAsParallelCapable();
/*    */   }
/*    */   
/*    */   public PluginClassloader(URL[] urls)
/*    */   {
/* 20 */     super(urls);
/* 21 */     allLoaders.add(this);
/*    */   }
/*    */   
/*    */   protected Class<?> loadClass(String name, boolean resolve)
/*    */     throws ClassNotFoundException
/*    */   {
/* 27 */     return loadClass0(name, resolve, true);
/*    */   }
/*    */   
/*    */   private Class<?> loadClass0(String name, boolean resolve, boolean checkOther) throws ClassNotFoundException
/*    */   {
/*    */     try
/*    */     {
/* 34 */       return super.loadClass(name, resolve);
/*    */     }
/*    */     catch (ClassNotFoundException localClassNotFoundException)
/*    */     {
/* 38 */       if (checkOther)
/*    */       {
/* 40 */         for (PluginClassloader loader : allLoaders)
/*    */         {
/* 42 */           if (loader != this)
/*    */           {
/*    */             try
/*    */             {
/* 46 */               return loader.loadClass0(name, resolve, false);
/*    */             }
/*    */             catch (ClassNotFoundException localClassNotFoundException1) {}
/*    */           }
/*    */         }
/*    */       }
/*    */       
/* 53 */       throw new ClassNotFoundException(name);
/*    */     }
/*    */   }
/*    */ }


