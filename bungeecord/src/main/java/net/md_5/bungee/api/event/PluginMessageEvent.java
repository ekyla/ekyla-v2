/*    */ package net.md_5.bungee.api.event;
/*    */ 
/*    */ import net.md_5.bungee.api.connection.Connection;
/*    */ 
/*    */ public class PluginMessageEvent extends TargetedEvent
/*    */   implements net.md_5.bungee.api.plugin.Cancellable
/*    */ {
/*    */   private boolean cancelled;
/*    */   private final String tag;
/*    */   private final byte[] data;
/*    */   
/* 12 */   public void setCancelled(boolean cancelled) { this.cancelled = cancelled; }
/* 13 */   public String toString() { return "PluginMessageEvent(super=" + super.toString() + ", cancelled=" + isCancelled() + ", tag=" + getTag() + ")"; }
/* 14 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof PluginMessageEvent)) return false; PluginMessageEvent other = (PluginMessageEvent)o; if (!other.canEqual(this)) return false; if (!super.equals(o)) return false; if (isCancelled() != other.isCancelled()) return false; Object this$tag = getTag();Object other$tag = other.getTag(); if (this$tag == null ? other$tag != null : !this$tag.equals(other$tag)) return false; return java.util.Arrays.equals(getData(), other.getData()); } protected boolean canEqual(Object other) { return other instanceof PluginMessageEvent; } public int hashCode() { int PRIME = 59;int result = super.hashCode();result = result * 59 + (isCancelled() ? 79 : 97);Object $tag = getTag();result = result * 59 + ($tag == null ? 43 : $tag.hashCode());result = result * 59 + java.util.Arrays.hashCode(getData());return result;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public boolean isCancelled()
/*    */   {
/* 21 */     return this.cancelled;
/*    */   }
/*    */   
/*    */   public String getTag() {
/* 25 */     return this.tag;
/*    */   }
/*    */   
/*    */   public byte[] getData() {
/* 29 */     return this.data;
/*    */   }
/*    */   
/*    */   public PluginMessageEvent(Connection sender, Connection receiver, String tag, byte[] data) {
/* 33 */     super(sender, receiver);
/* 34 */     this.tag = tag;
/* 35 */     this.data = data;
/*    */   }
/*    */ }


