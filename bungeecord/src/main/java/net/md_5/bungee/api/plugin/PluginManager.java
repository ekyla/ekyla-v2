package net.md_5.bungee.api.plugin;

import com.google.common.base.Preconditions;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.event.EventBus;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.introspector.PropertyUtils;

import java.io.File;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Level;

public class PluginManager {
    private final ProxyServer proxy;
    private final Yaml yaml;
    private final EventBus eventBus;
    private final Map<String, Plugin> plugins = new LinkedHashMap<>();
    private final Map<String, Command> commandMap = new HashMap<>();
    private final Multimap<Plugin, Command> commandsByPlugin = ArrayListMultimap.create();
    private final Multimap<Plugin, Listener> listenersByPlugin = ArrayListMultimap.create();
    private Map<String, PluginDescription> toLoad = new HashMap<>();

    public PluginManager(ProxyServer proxy, Yaml yaml, EventBus eventBus) {
        this.proxy = proxy;
        this.yaml = yaml;
        this.eventBus = eventBus;
    }


    public PluginManager(ProxyServer proxy) {
        this.proxy = proxy;


        org.yaml.snakeyaml.constructor.Constructor yamlConstructor = new org.yaml.snakeyaml.constructor.Constructor();
        PropertyUtils propertyUtils = yamlConstructor.getPropertyUtils();
        propertyUtils.setSkipMissingProperties(true);
        yamlConstructor.setPropertyUtils(propertyUtils);
        this.yaml = new Yaml(yamlConstructor);

        this.eventBus = new EventBus(proxy.getLogger());
    }


    public void registerCommand(Plugin plugin, Command command) {
        this.commandMap.put(command.getName().toLowerCase(Locale.ROOT), command);
        for (String alias : command.getAliases()) {
            this.commandMap.put(alias.toLowerCase(Locale.ROOT), command);
        }
        this.commandsByPlugin.put(plugin, command);
    }


    public void unregisterCommand(Command command) {
        while (commandMap.values().remove(command)) ;
        commandsByPlugin.values().remove(command);
    }


    public void unregisterCommands(Plugin plugin) {
        for (Iterator<Command> it = this.commandsByPlugin.get(plugin).iterator(); it.hasNext(); ) {
            Command command = it.next();
            while (this.commandMap.values().remove(command)) ;
            it.remove();
        }
    }

    private Command getCommandIfEnabled(String commandName, CommandSender sender) {
        String commandLower = commandName.toLowerCase(Locale.ROOT);


        if (((sender instanceof ProxiedPlayer)) && (this.proxy.getDisabledCommands().contains(commandLower))) {
            return null;
        }

        return this.commandMap.get(commandLower);
    }


    public boolean isExecutableCommand(String commandName, CommandSender sender) {
        return getCommandIfEnabled(commandName, sender) != null;
    }

    public boolean dispatchCommand(CommandSender sender, String commandLine) {
        return dispatchCommand(sender, commandLine, null);
    }


    public boolean dispatchCommand(CommandSender sender, String commandLine, List<String> tabResults) {
        String[] split = commandLine.split(" ", -1);

        if ((split.length == 0) || (split[0].isEmpty())) {
            return false;
        }

        Command command = getCommandIfEnabled(split[0], sender);
        if (command == null) {
            return false;
        }

        if (!command.hasPermission(sender)) {
            if (tabResults == null) {
                sender.sendMessage(this.proxy.getTranslation("no_permission"));
            }
            return true;
        }

        String[] args = Arrays.copyOfRange(split, 1, split.length);
        try {
            if (tabResults == null) {
                if (this.proxy.getConfig().isLogCommands()) {
                    this.proxy.getLogger().log(Level.INFO, "{0} executed command: /{1}", new Object[]{sender

                            .getName(), commandLine});
                }

                command.execute(sender, args);
            } else if ((commandLine.contains(" ")) && ((command instanceof TabExecutor))) {
                for (String s : ((TabExecutor) command).onTabComplete(sender, args)) {
                    tabResults.add(s);
                }
            }
        } catch (Exception ex) {
            sender.sendMessage(ChatColor.RED + "An internal error occurred whilst executing this command, please check the console log for details.");
            ProxyServer.getInstance().getLogger().log(Level.WARNING, "Error in dispatching command", ex);
        }
        return true;
    }


    public Collection<Plugin> getPlugins() {
        return this.plugins.values();
    }


    public Plugin getPlugin(String name) {
        return this.plugins.get(name);
    }

    public void loadPlugins() {
        Map<PluginDescription, Boolean> pluginStatuses = new HashMap<>();
        for (Map.Entry<String, PluginDescription> entry : this.toLoad.entrySet()) {
            PluginDescription plugin = entry.getValue();
            if (!enablePlugin(pluginStatuses, new Stack<>(), plugin)) {
                ProxyServer.getInstance().getLogger().log(Level.WARNING, "Failed to enable {0}", entry.getKey());
            }
        }
        this.toLoad.clear();
        this.toLoad = null;
    }

    public void enablePlugins() {
        for (Plugin plugin : plugins.values()) {
            try {
                plugin.onEnable();
                ProxyServer.getInstance().getLogger().log(Level.INFO, "Enabled plugin {0} version {1} by {2}", new Object[]
                        {
                                plugin.getDescription().getName(), plugin.getDescription().getVersion(), plugin.getDescription().getAuthor()
                        });
            } catch (Throwable t) {
                ProxyServer.getInstance().getLogger().log(Level.WARNING, "Exception encountered when loading plugin: " + plugin.getDescription().getName(), t);
            }
        }
    }

    private boolean enablePlugin(Map<PluginDescription, Boolean> pluginStatuses, Stack<PluginDescription> dependStack, PluginDescription plugin) {
        if (pluginStatuses.containsKey(plugin)) {
            return pluginStatuses.get(plugin);
        }

        // combine all dependencies for 'for loop'
        Set<String> dependencies = new HashSet<>();
        dependencies.addAll(plugin.getDepends());
        dependencies.addAll(plugin.getSoftDepends());

        // success status
        boolean status = true;

        // try to load dependencies first
        for (String dependName : dependencies) {
            PluginDescription depend = toLoad.get(dependName);
            Boolean dependStatus = (depend != null) ? pluginStatuses.get(depend) : Boolean.FALSE;

            if (dependStatus == null) {
                if (dependStack.contains(depend)) {
                    StringBuilder dependencyGraph = new StringBuilder();
                    for (PluginDescription element : dependStack) {
                        dependencyGraph.append(element.getName()).append(" -> ");
                    }
                    dependencyGraph.append(plugin.getName()).append(" -> ").append(dependName);
                    ProxyServer.getInstance().getLogger().log(Level.WARNING, "Circular dependency detected: {0}", dependencyGraph);
                    status = false;
                } else {
                    dependStack.push(plugin);
                    dependStatus = this.enablePlugin(pluginStatuses, dependStack, depend);
                    dependStack.pop();
                }
            }

            if (dependStatus == Boolean.FALSE && plugin.getDepends().contains(dependName)) // only fail if this wasn't a soft dependency
            {
                ProxyServer.getInstance().getLogger().log(Level.WARNING, "{0} (required by {1}) is unavailable", new Object[]
                        {
                                String.valueOf(dependName), plugin.getName()
                        });
                status = false;
            }

            if (!status) {
                break;
            }
        }

        if (status) {
            try {
                URLClassLoader loader = new PluginClassloader(new URL[]{plugin.getFile().toURI().toURL()});
                Class<?> main = loader.loadClass(plugin.getMain());
                Plugin clazz = (Plugin) main.getDeclaredConstructor().newInstance();

                clazz.init(proxy, plugin);
                plugins.put(plugin.getName(), clazz);
                clazz.onLoad();
                ProxyServer.getInstance().getLogger().log(Level.INFO, "Loaded plugin {0} version {1} by {2}", new Object[]
                        {
                                plugin.getName(), plugin.getVersion(), plugin.getAuthor()
                        });
            } catch (Throwable t) {
                proxy.getLogger().log(Level.WARNING, "Error enabling plugin " + plugin.getName(), t);
            }
        }

        pluginStatuses.put(plugin, status);
        return status;
    }


    public void detectPlugins(File folder) {
        Preconditions.checkNotNull(folder, "folder");
        Preconditions.checkArgument(folder.isDirectory(), "Must load from a directory");

        for (File file : folder.listFiles()) {
            if (file.isFile() && file.getName().endsWith(".jar")) {
                try (JarFile jar = new JarFile(file)) {
                    JarEntry pdf = jar.getJarEntry("bungee.yml");
                    if (pdf == null) {
                        pdf = jar.getJarEntry("plugin.yml");
                    }
                    Preconditions.checkNotNull(pdf, "Plugin must have a plugin.yml or bungee.yml");

                    try (InputStream in = jar.getInputStream(pdf)) {
                        PluginDescription desc = yaml.loadAs(in, PluginDescription.class);
                        Preconditions.checkNotNull(desc.getName(), "Plugin from %s has no name", file);
                        Preconditions.checkNotNull(desc.getMain(), "Plugin from %s has no main", file);

                        desc.setFile(file);
                        toLoad.put(desc.getName(), desc);
                    }
                } catch (Exception ex) {
                    ProxyServer.getInstance().getLogger().log(Level.WARNING, "Could not load plugin from file " + file, ex);
                }
            }
        }
    }

    public <T extends Event> T callEvent(T event) {
        Preconditions.checkNotNull(event, "event");

        long start = System.nanoTime();
        this.eventBus.post(event);
        event.postCall();

        long elapsed = System.nanoTime() - start;
        if (elapsed > 250000000L) {
            ProxyServer.getInstance().getLogger().log(Level.WARNING, "Event {0} took {1}ns to process!", new Object[]{event, elapsed});
        }

        return event;
    }


    public void registerListener(Plugin plugin, Listener listener) {
        for (Method method : listener.getClass().getDeclaredMethods()) {
            Preconditions.checkArgument(!method.isAnnotationPresent(com.google.common.eventbus.Subscribe.class), "Listener %s has registered using deprecated subscribe annotation! Please update to @EventHandler.", listener);
        }

        this.eventBus.register(listener);
        this.listenersByPlugin.put(plugin, listener);
    }


    public void unregisterListener(Listener listener) {
        this.eventBus.unregister(listener);
        this.listenersByPlugin.values().remove(listener);
    }


    public void unregisterListeners(Plugin plugin) {
        for (Iterator<Listener> it = this.listenersByPlugin.get(plugin).iterator(); it.hasNext(); ) {
            this.eventBus.unregister(it.next());
            it.remove();
        }
    }


    public Collection<Map.Entry<String, Command>> getCommands() {
        return Collections.unmodifiableCollection(this.commandMap.entrySet());
    }
}


