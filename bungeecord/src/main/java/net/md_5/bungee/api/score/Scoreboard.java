/*     */ package net.md_5.bungee.api.score;
/*     */ 
/*     */ import java.util.Map;
/*     */ 
/*     */ 
/*     */ public class Scoreboard
/*     */ {
/*     */   private String name;
/*     */   private Position position;
/*     */   
/*  11 */   public void setName(String name) { this.name = name; } public void setPosition(Position position) { this.position = position; } public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof Scoreboard)) return false; Scoreboard other = (Scoreboard)o; if (!other.canEqual(this)) return false; Object this$name = getName();Object other$name = other.getName(); if (this$name == null ? other$name != null : !this$name.equals(other$name)) return false; Object this$position = getPosition();Object other$position = other.getPosition(); if (this$position == null ? other$position != null : !this$position.equals(other$position)) return false; Object this$objectives = getObjectives();Object other$objectives = other.getObjectives(); if (this$objectives == null ? other$objectives != null : !this$objectives.equals(other$objectives)) return false; Object this$scores = getScores();Object other$scores = other.getScores(); if (this$scores == null ? other$scores != null : !this$scores.equals(other$scores)) return false; Object this$teams = getTeams();Object other$teams = other.getTeams();return this$teams == null ? other$teams == null : this$teams.equals(other$teams); } protected boolean canEqual(Object other) { return other instanceof Scoreboard; } public int hashCode() { int PRIME = 59;int result = 1;Object $name = getName();result = result * 59 + ($name == null ? 43 : $name.hashCode());Object $position = getPosition();result = result * 59 + ($position == null ? 43 : $position.hashCode());Object $objectives = getObjectives();result = result * 59 + ($objectives == null ? 43 : $objectives.hashCode());Object $scores = getScores();result = result * 59 + ($scores == null ? 43 : $scores.hashCode());Object $teams = getTeams();result = result * 59 + ($teams == null ? 43 : $teams.hashCode());return result; } public String toString() { return "Scoreboard(name=" + getName() + ", position=" + getPosition() + ", objectives=" + getObjectives() + ", scores=" + getScores() + ", teams=" + getTeams() + ")"; }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public String getName()
/*     */   {
/*  19 */     return this.name;
/*     */   }
/*     */   
/*     */   public Position getPosition() {
/*  23 */     return this.position;
/*     */   }
/*     */   
/*     */ 
/*  27 */   private final Map<String, Objective> objectives = new java.util.HashMap();
/*     */   
/*     */ 
/*     */ 
/*  31 */   private final Map<String, Score> scores = new java.util.HashMap();
/*     */   
/*     */ 
/*     */ 
/*  35 */   private final Map<String, Team> teams = new java.util.HashMap();
/*     */   
/*     */   public java.util.Collection<Objective> getObjectives()
/*     */   {
/*  39 */     return java.util.Collections.unmodifiableCollection(this.objectives.values());
/*     */   }
/*     */   
/*     */   public java.util.Collection<Score> getScores()
/*     */   {
/*  44 */     return java.util.Collections.unmodifiableCollection(this.scores.values());
/*     */   }
/*     */   
/*     */   public java.util.Collection<Team> getTeams()
/*     */   {
/*  49 */     return java.util.Collections.unmodifiableCollection(this.teams.values());
/*     */   }
/*     */   
/*     */   public void addObjective(Objective objective)
/*     */   {
/*  54 */     com.google.common.base.Preconditions.checkNotNull(objective, "objective");
/*  55 */     com.google.common.base.Preconditions.checkArgument(!this.objectives.containsKey(objective.getName()), "Objective %s already exists in this scoreboard", objective.getName());
/*  56 */     this.objectives.put(objective.getName(), objective);
/*     */   }
/*     */   
/*     */   public void addScore(Score score)
/*     */   {
/*  61 */     com.google.common.base.Preconditions.checkNotNull(score, "score");
/*  62 */     this.scores.put(score.getItemName(), score);
/*     */   }
/*     */   
/*     */   public Score getScore(String name)
/*     */   {
/*  67 */     return (Score)this.scores.get(name);
/*     */   }
/*     */   
/*     */   public void addTeam(Team team)
/*     */   {
/*  72 */     com.google.common.base.Preconditions.checkNotNull(team, "team");
/*  73 */     com.google.common.base.Preconditions.checkArgument(!this.teams.containsKey(team.getName()), "Team %s already exists in this scoreboard", team.getName());
/*  74 */     this.teams.put(team.getName(), team);
/*     */   }
/*     */   
/*     */   public Team getTeam(String name)
/*     */   {
/*  79 */     return (Team)this.teams.get(name);
/*     */   }
/*     */   
/*     */   public Objective getObjective(String name)
/*     */   {
/*  84 */     return (Objective)this.objectives.get(name);
/*     */   }
/*     */   
/*     */   public void removeObjective(String objectiveName)
/*     */   {
/*  89 */     this.objectives.remove(objectiveName);
/*     */   }
/*     */   
/*     */   public void removeScore(String scoreName)
/*     */   {
/*  94 */     this.scores.remove(scoreName);
/*     */   }
/*     */   
/*     */   public void removeTeam(String teamName)
/*     */   {
/*  99 */     this.teams.remove(teamName);
/*     */   }
/*     */   
/*     */   public void clear()
/*     */   {
/* 104 */     this.name = null;
/* 105 */     this.position = null;
/* 106 */     this.objectives.clear();
/* 107 */     this.scores.clear();
/* 108 */     this.teams.clear();
/*     */   }
/*     */ }


