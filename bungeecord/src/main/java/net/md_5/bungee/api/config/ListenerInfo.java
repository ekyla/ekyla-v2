/*     */ package net.md_5.bungee.api.config;
/*     */ 
/*     */ import java.util.List;
/*     */ 
/*     */ public class ListenerInfo {
/*     */   private final java.net.InetSocketAddress host;
/*     */   private final String motd;
/*     */   private final int maxPlayers;
/*     */   private final int tabListSize;
/*     */   private final List<String> serverPriority;
/*     */   private final boolean forceDefault;
/*     */   
/*  13 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof ListenerInfo)) return false; ListenerInfo other = (ListenerInfo)o; if (!other.canEqual(this)) return false; Object this$host = getHost();Object other$host = other.getHost(); if (this$host == null ? other$host != null : !this$host.equals(other$host)) return false; Object this$motd = getMotd();Object other$motd = other.getMotd(); if (this$motd == null ? other$motd != null : !this$motd.equals(other$motd)) return false; if (getMaxPlayers() != other.getMaxPlayers()) return false; if (getTabListSize() != other.getTabListSize()) return false; Object this$serverPriority = getServerPriority();Object other$serverPriority = other.getServerPriority(); if (this$serverPriority == null ? other$serverPriority != null : !this$serverPriority.equals(other$serverPriority)) return false; if (isForceDefault() != other.isForceDefault()) return false; Object this$forcedHosts = getForcedHosts();Object other$forcedHosts = other.getForcedHosts(); if (this$forcedHosts == null ? other$forcedHosts != null : !this$forcedHosts.equals(other$forcedHosts)) return false; Object this$tabListType = getTabListType();Object other$tabListType = other.getTabListType(); if (this$tabListType == null ? other$tabListType != null : !this$tabListType.equals(other$tabListType)) return false; if (isSetLocalAddress() != other.isSetLocalAddress()) return false; if (isPingPassthrough() != other.isPingPassthrough()) return false; if (getQueryPort() != other.getQueryPort()) return false; if (isQueryEnabled() != other.isQueryEnabled()) return false; return isProxyProtocol() == other.isProxyProtocol(); } protected boolean canEqual(Object other) { return other instanceof ListenerInfo; } public int hashCode() { int PRIME = 59;int result = 1;Object $host = getHost();result = result * 59 + ($host == null ? 43 : $host.hashCode());Object $motd = getMotd();result = result * 59 + ($motd == null ? 43 : $motd.hashCode());result = result * 59 + getMaxPlayers();result = result * 59 + getTabListSize();Object $serverPriority = getServerPriority();result = result * 59 + ($serverPriority == null ? 43 : $serverPriority.hashCode());result = result * 59 + (isForceDefault() ? 79 : 97);Object $forcedHosts = getForcedHosts();result = result * 59 + ($forcedHosts == null ? 43 : $forcedHosts.hashCode());Object $tabListType = getTabListType();result = result * 59 + ($tabListType == null ? 43 : $tabListType.hashCode());result = result * 59 + (isSetLocalAddress() ? 79 : 97);result = result * 59 + (isPingPassthrough() ? 79 : 97);result = result * 59 + getQueryPort();result = result * 59 + (isQueryEnabled() ? 79 : 97);result = result * 59 + (isProxyProtocol() ? 79 : 97);return result; } public String toString() { return "ListenerInfo(host=" + getHost() + ", motd=" + getMotd() + ", maxPlayers=" + getMaxPlayers() + ", tabListSize=" + getTabListSize() + ", serverPriority=" + getServerPriority() + ", forceDefault=" + isForceDefault() + ", forcedHosts=" + getForcedHosts() + ", tabListType=" + getTabListType() + ", setLocalAddress=" + isSetLocalAddress() + ", pingPassthrough=" + isPingPassthrough() + ", queryPort=" + getQueryPort() + ", queryEnabled=" + isQueryEnabled() + ", proxyProtocol=" + isProxyProtocol() + ")"; }
/*  14 */   public ListenerInfo(java.net.InetSocketAddress host, String motd, int maxPlayers, int tabListSize, List<String> serverPriority, boolean forceDefault, java.util.Map<String, String> forcedHosts, String tabListType, boolean setLocalAddress, boolean pingPassthrough, int queryPort, boolean queryEnabled, boolean proxyProtocol) { this.host = host;this.motd = motd;this.maxPlayers = maxPlayers;this.tabListSize = tabListSize;this.serverPriority = serverPriority;this.forceDefault = forceDefault;this.forcedHosts = forcedHosts;this.tabListType = tabListType;this.setLocalAddress = setLocalAddress;this.pingPassthrough = pingPassthrough;this.queryPort = queryPort;this.queryEnabled = queryEnabled;this.proxyProtocol = proxyProtocol;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public java.net.InetSocketAddress getHost()
/*     */   {
/*  21 */     return this.host;
/*     */   }
/*     */   
/*     */   public String getMotd() {
/*  25 */     return this.motd;
/*     */   }
/*     */   
/*     */   public int getMaxPlayers() {
/*  29 */     return this.maxPlayers;
/*     */   }
/*     */   
/*     */   public int getTabListSize() {
/*  33 */     return this.tabListSize;
/*     */   }
/*     */   
/*     */   public List<String> getServerPriority()
/*     */   {
/*  38 */     return this.serverPriority;
/*     */   }
/*     */   
/*     */   public boolean isForceDefault()
/*     */   {
/*  43 */     return this.forceDefault;
/*     */   }
/*     */   
/*     */   public java.util.Map<String, String> getForcedHosts()
/*     */   {
/*  48 */     return this.forcedHosts;
/*     */   }
/*     */   
/*     */   public String getTabListType() {
/*  52 */     return this.tabListType;
/*     */   }
/*     */   
/*     */   public boolean isSetLocalAddress() {
/*  56 */     return this.setLocalAddress;
/*     */   }
/*     */   
/*     */   public boolean isPingPassthrough()
/*     */   {
/*  61 */     return this.pingPassthrough;
/*     */   }
/*     */   
/*     */   public int getQueryPort() {
/*  65 */     return this.queryPort;
/*     */   }
/*     */   
/*     */   public boolean isQueryEnabled() {
/*  69 */     return this.queryEnabled;
/*     */   }
/*     */   
/*     */   public boolean isProxyProtocol() {
/*  73 */     return this.proxyProtocol;
/*     */   }
/*     */   
/*     */   @Deprecated
/*     */   public ListenerInfo(java.net.InetSocketAddress host, String motd, int maxPlayers, int tabListSize, List<String> serverPriority, boolean forceDefault, java.util.Map<String, String> forcedHosts, String tabListType, boolean setLocalAddress, boolean pingPassthrough, int queryPort, boolean queryEnabled) {
/*  78 */     this(host, motd, maxPlayers, tabListSize, serverPriority, forceDefault, forcedHosts, tabListType, setLocalAddress, pingPassthrough, queryPort, queryEnabled, false);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   @Deprecated
/*     */   public String getDefaultServer()
/*     */   {
/*  90 */     return (String)this.serverPriority.get(0);
/*     */   }
/*     */   
/*     */   private final java.util.Map<String, String> forcedHosts;
/*     */   private final String tabListType;
/*     */   private final boolean setLocalAddress;
/*     */   private final boolean pingPassthrough;
/*     */   private final int queryPort;
/*     */   private final boolean queryEnabled;
/*     */   private final boolean proxyProtocol;
/*     */   @Deprecated
/*     */   public String getFallbackServer()
/*     */   {
/* 103 */     return this.serverPriority.size() > 1 ? (String)this.serverPriority.get(1) : getDefaultServer();
/*     */   }
/*     */ }


