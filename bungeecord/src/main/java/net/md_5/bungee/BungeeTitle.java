package net.md_5.bungee;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.protocol.packet.Title;

public class BungeeTitle implements net.md_5.bungee.api.Title {
    private net.md_5.bungee.protocol.packet.Title title;
    private net.md_5.bungee.protocol.packet.Title subtitle;
    private net.md_5.bungee.protocol.packet.Title times;
    private net.md_5.bungee.protocol.packet.Title clear;
    private net.md_5.bungee.protocol.packet.Title reset;

    private static net.md_5.bungee.protocol.packet.Title createPacket(Title.Action action) {
        net.md_5.bungee.protocol.packet.Title title = new net.md_5.bungee.protocol.packet.Title();
        title.setAction(action);

        if (action == Title.Action.TIMES) {

            title.setFadeIn(20);
            title.setStay(60);
            title.setFadeOut(20);
        }
        return title;
    }

    private static void sendPacket(ProxiedPlayer player, net.md_5.bungee.protocol.DefinedPacket packet) {
        if (packet != null) {
            player.unsafe().sendPacket(packet);
        }
    }

    public net.md_5.bungee.api.Title title(BaseComponent text) {
        if (this.title == null) {
            this.title = createPacket(Title.Action.TITLE);
        }

        this.title.setText(net.md_5.bungee.chat.ComponentSerializer.toString(text));
        return this;
    }

    public net.md_5.bungee.api.Title title(BaseComponent... text) {
        if (this.title == null) {
            this.title = createPacket(Title.Action.TITLE);
        }

        this.title.setText(net.md_5.bungee.chat.ComponentSerializer.toString(text));
        return this;
    }

    public net.md_5.bungee.api.Title subTitle(BaseComponent text) {
        if (this.subtitle == null) {
            this.subtitle = createPacket(Title.Action.SUBTITLE);
        }

        this.subtitle.setText(net.md_5.bungee.chat.ComponentSerializer.toString(text));
        return this;
    }

    public net.md_5.bungee.api.Title subTitle(BaseComponent... text) {
        if (this.subtitle == null) {
            this.subtitle = createPacket(Title.Action.SUBTITLE);
        }

        this.subtitle.setText(net.md_5.bungee.chat.ComponentSerializer.toString(text));
        return this;
    }

    public net.md_5.bungee.api.Title fadeIn(int ticks) {
        if (this.times == null) {
            this.times = createPacket(Title.Action.TIMES);
        }

        this.times.setFadeIn(ticks);
        return this;
    }

    public net.md_5.bungee.api.Title stay(int ticks) {
        if (this.times == null) {
            this.times = createPacket(Title.Action.TIMES);
        }

        this.times.setStay(ticks);
        return this;
    }

    public net.md_5.bungee.api.Title fadeOut(int ticks) {
        if (this.times == null) {
            this.times = createPacket(Title.Action.TIMES);
        }

        this.times.setFadeOut(ticks);
        return this;
    }

    public net.md_5.bungee.api.Title clear() {
        if (this.clear == null) {
            this.clear = createPacket(Title.Action.CLEAR);
        }

        this.title = null;

        return this;
    }

    public net.md_5.bungee.api.Title reset() {
        if (this.reset == null) {
            this.reset = createPacket(Title.Action.RESET);
        }


        this.title = null;
        this.subtitle = null;
        this.times = null;

        return this;
    }

    public net.md_5.bungee.api.Title send(ProxiedPlayer player) {
        sendPacket(player, this.clear);
        sendPacket(player, this.reset);
        sendPacket(player, this.times);
        sendPacket(player, this.subtitle);
        sendPacket(player, this.title);
        return this;
    }
}


