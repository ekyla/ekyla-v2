package net.md_5.bungee;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ComponentBuilder;

import java.security.Security;
import java.util.Arrays;
import java.util.Collections;

public class BungeeCordLauncher {
    public static void main(String[] args) throws Exception {
        Security.setProperty("networkaddress.cache.ttl", "30");
        Security.setProperty("networkaddress.cache.negative.ttl", "10");

        OptionParser parser = new OptionParser();
        parser.allowsUnrecognizedOptions();
        parser.acceptsAll(Collections.singletonList("help"), "Show the help");
        parser.acceptsAll(Arrays.asList("v", "version"), "Print version and exit");
        parser.acceptsAll(Collections.singletonList("noconsole"), "Disable console input");

        OptionSet options = parser.parse(args);

        if (options.has("help")) {
            parser.printHelpOn(System.out);
            return;
        }
        if (options.has("version")) {
            System.out.println(BungeeCord.class.getPackage().getImplementationVersion());
            return;
        }

        /*
        if ((BungeeCord.class.getPackage().getSpecificationVersion() != null) && (System.getProperty("IReallyKnowWhatIAmDoingISwear") == null)) {
            Date buildDate = new SimpleDateFormat("yyyyMMdd").parse(BungeeCord.class.getPackage().getSpecificationVersion());

            Calendar deadline = Calendar.getInstance();
            deadline.add(3, -8);
            if (buildDate.before(deadline.getTime())) {
                System.err.println("*** Warning, this build is outdated ***");
                System.err.println("*** Please download a new build from http://ci.md-5.net/job/BungeeCord ***");
                System.err.println("*** You will get NO support regarding this build ***");
                System.err.println("*** Server will start in 10 seconds ***");
                Thread.sleep(TimeUnit.SECONDS.toMillis(10L));
            }
        }
        */

        BungeeCord bungee = new BungeeCord();
        ProxyServer.setInstance(bungee);
        bungee.getLogger().info("Enabled Ekyla Proxy version " + bungee.getVersion());
        bungee.start();

        if (!options.has("noconsole")) {
            String line;
            while ((bungee.isRunning) && ((line = bungee.getConsoleReader().readLine(">")) != null)) {
                if (!bungee.getPluginManager().dispatchCommand(net.md_5.bungee.command.ConsoleCommandSender.getInstance(), line)) {
                    bungee.getConsole().sendMessage(new ComponentBuilder("Command not found").color(ChatColor.RED).create());
                }
            }
        }
    }
}


