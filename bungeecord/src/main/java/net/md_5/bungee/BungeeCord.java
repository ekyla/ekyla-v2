package net.md_5.bungee;

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.util.ResourceLeakDetector;
import jline.console.ConsoleReader;
import net.md_5.bungee.api.*;
import net.md_5.bungee.api.chat.*;
import net.md_5.bungee.api.config.ConfigurationAdapter;
import net.md_5.bungee.api.config.ListenerInfo;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;
import net.md_5.bungee.chat.*;
import net.md_5.bungee.command.*;
import net.md_5.bungee.compress.CompressFactory;
import net.md_5.bungee.conf.Configuration;
import net.md_5.bungee.conf.YamlConfig;
import net.md_5.bungee.log.BungeeLogger;
import net.md_5.bungee.log.LoggingOutputStream;
import net.md_5.bungee.module.ModuleManager;
import net.md_5.bungee.netty.PipelineUtils;
import net.md_5.bungee.protocol.DefinedPacket;
import net.md_5.bungee.protocol.ProtocolConstants;
import net.md_5.bungee.protocol.packet.Chat;
import net.md_5.bungee.protocol.packet.PluginMessage;
import net.md_5.bungee.query.RemoteQuery;
import net.md_5.bungee.scheduler.BungeeScheduler;
import net.md_5.bungee.util.CaseInsensitiveMap;
import org.fusesource.jansi.AnsiConsole;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.text.MessageFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;


public class BungeeCord extends ProxyServer {
    public final Configuration config = new Configuration();
    public final PluginManager pluginManager;
    public final Gson gson = new GsonBuilder()
            .registerTypeAdapter(BaseComponent.class, new ComponentSerializer())
            .registerTypeAdapter(TextComponent.class, new TextComponentSerializer())
            .registerTypeAdapter(TranslatableComponent.class, new TranslatableComponentSerializer())
            .registerTypeAdapter(KeybindComponent.class, new KeybindComponentSerializer())
            .registerTypeAdapter(ScoreComponent.class, new ScoreComponentSerializer())
            .registerTypeAdapter(SelectorComponent.class, new SelectorComponentSerializer())
            .registerTypeAdapter(ServerPing.PlayerInfo.class, new PlayerInfoSerializer())
            .registerTypeAdapter(Favicon.class, Favicon.getFaviconTypeAdapter()).create();
    private final Timer saveThread = new Timer("Reconnect Saver");
    private final Timer metricsThread = new Timer("Metrics Thread");
    private final Collection<Channel> listeners = new HashSet<>();
    private final Map<String, UserConnection> connections = new CaseInsensitiveMap<>();
    private final Map<UUID, UserConnection> connectionsByOfflineUUID = new HashMap<>();
    private final Map<UUID, UserConnection> connectionsByUUID = new HashMap<>();
    private final ReadWriteLock connectionLock = new ReentrantReadWriteLock();
    private final Collection<String> pluginChannels = new HashSet<>();
    private final File pluginsFolder = new File("plugins");
    private final BungeeScheduler scheduler = new BungeeScheduler();
    private final ConsoleReader consoleReader;
    private final Logger logger;
    private final ModuleManager moduleManager = new ModuleManager();
    public volatile boolean isRunning;
    public EventLoopGroup eventLoops;
    private ResourceBundle baseBundle;
    private ResourceBundle customBundle;
    private ReconnectHandler reconnectHandler;
    private ConfigurationAdapter configurationAdapter = new YamlConfig();
    private ConnectionThrottle connectionThrottle;

    @SuppressFBWarnings({"DM_DEFAULT_ENCODING"})
    public BungeeCord() throws IOException {
        registerChannel("BungeeCord");


        Preconditions.checkState(new File(".").getAbsolutePath().indexOf('!') == -1, "Cannot use BungeeCord in directory with ! in path.");

        System.setSecurityManager(new BungeeSecurityManager());

        try {
            this.baseBundle = ResourceBundle.getBundle("messages");
        } catch (MissingResourceException ex) {
            this.baseBundle = ResourceBundle.getBundle("messages", Locale.ENGLISH);
        }
        reloadMessages();


        System.setProperty("library.jansi.version", "BungeeCord");

        AnsiConsole.systemInstall();
        this.consoleReader = new ConsoleReader();
        this.consoleReader.setExpandEvents(false);
        this.consoleReader.addCompleter(new ConsoleCommandCompleter(this));

        this.logger = new BungeeLogger("BungeeCord", "proxy.log", this.consoleReader);
        System.setErr(new PrintStream(new LoggingOutputStream(this.logger, java.util.logging.Level.SEVERE), true));
        System.setOut(new PrintStream(new LoggingOutputStream(this.logger, java.util.logging.Level.INFO), true));

        this.pluginManager = new PluginManager(this);
        getPluginManager().registerCommand(null, new CommandReload());
        getPluginManager().registerCommand(null, new CommandEnd());
        getPluginManager().registerCommand(null, new CommandIP());
        getPluginManager().registerCommand(null, new CommandBungee());
        getPluginManager().registerCommand(null, new CommandPerms());

        if (!Boolean.getBoolean("net.md_5.bungee.native.disable")) {
            if (EncryptionUtil.nativeFactory.load()) {
                this.logger.info("Using mbed TLS based native cipher.");
            } else {
                this.logger.info("Using standard Java JCE cipher.");
            }
            if (CompressFactory.zlib.load()) {
                this.logger.info("Using zlib based native compressor.");
            } else {
                this.logger.info("Using standard Java compressor.");
            }
        }
    }

    public static BungeeCord getInstance() {
        return (BungeeCord) ProxyServer.getInstance();
    }

    public Configuration getConfig() {
        return this.config;
    }

    public PluginManager getPluginManager() {
        return this.pluginManager;
    }

    public ReconnectHandler getReconnectHandler() {
        return this.reconnectHandler;
    }

    public void setReconnectHandler(ReconnectHandler reconnectHandler) {
        this.reconnectHandler = reconnectHandler;
    }

    public ConfigurationAdapter getConfigurationAdapter() {
        return this.configurationAdapter;
    }

    public void setConfigurationAdapter(ConfigurationAdapter configurationAdapter) {
        this.configurationAdapter = configurationAdapter;
    }

    public File getPluginsFolder() {
        return this.pluginsFolder;
    }

    public BungeeScheduler getScheduler() {
        return this.scheduler;
    }

    public ConsoleReader getConsoleReader() {
        return this.consoleReader;
    }

    public Logger getLogger() {
        return this.logger;
    }

    public ConnectionThrottle getConnectionThrottle() {
        return this.connectionThrottle;
    }

    @SuppressFBWarnings({"RV_RETURN_VALUE_IGNORED_BAD_PRACTICE"})
    public void start()
            throws Exception {
        System.setProperty("io.netty.selectorAutoRebuildThreshold", "0");
        if (System.getProperty("io.netty.leakDetectionjava.util.logging.Level") == null) {
            ResourceLeakDetector.setLevel(ResourceLeakDetector.Level.DISABLED);
        }

        this.eventLoops = PipelineUtils.newEventLoopGroup(0, new ThreadFactoryBuilder().setNameFormat("Netty IO Thread #%1$d").build());

        File moduleDirectory = new File("modules");
        this.moduleManager.load(this, moduleDirectory);
        this.pluginManager.detectPlugins(moduleDirectory);

        this.pluginsFolder.mkdir();
        this.pluginManager.detectPlugins(this.pluginsFolder);

        this.pluginManager.loadPlugins();
        this.config.load();

        if (this.config.isForgeSupport()) {
            registerChannel("FML");
            registerChannel("FML|HS");
            registerChannel("FORGE");

            getLogger().warning("MinecraftForge support is currently unmaintained and may have unresolved issues. Please use at your own risk.");
        }

        this.isRunning = true;

        this.pluginManager.enablePlugins();

        if (this.config.getThrottle() > 0) {
            this.connectionThrottle = new ConnectionThrottle(this.config.getThrottle(), this.config.getThrottleLimit());
        }
        startListeners();

        this.saveThread.scheduleAtFixedRate(new TimerTask() {

            public void run() {
                if (BungeeCord.this.getReconnectHandler() != null) {
                    BungeeCord.this.getReconnectHandler().save();
                }
            }
        }, 0L, TimeUnit.MINUTES


                .toMillis(5L));
        this.metricsThread.scheduleAtFixedRate(new Metrics(), 0L, TimeUnit.MINUTES.toMillis(10L));
    }

    public void startListeners() {
        for (final ListenerInfo info : this.config.getListeners()) {
            if (info.isProxyProtocol()) {
                getLogger().log(java.util.logging.Level.WARNING, "Using PROXY protocol for listener {0}, please ensure this listener is adequately firewalled.", info.getHost());

                if (this.connectionThrottle != null) {
                    this.connectionThrottle = null;
                    getLogger().log(java.util.logging.Level.WARNING, "Since PROXY protocol is in use, internal connection throttle has been disabled.");
                }
            }

            ChannelFutureListener listener = future -> {
                if (future.isSuccess()) {
                    BungeeCord.this.listeners.add(future.channel());
                    BungeeCord.this.getLogger().log(java.util.logging.Level.INFO, "Listening on {0}", info.getHost());
                } else {
                    BungeeCord.this.getLogger().log(java.util.logging.Level.WARNING, "Could not bind to host " + info.getHost(), future.cause());


                }


            };
            new ServerBootstrap().channel(PipelineUtils.getServerChannel()).option(ChannelOption.SO_REUSEADDR, true).childAttr(PipelineUtils.LISTENER, info).childHandler(PipelineUtils.SERVER_CHILD).group(this.eventLoops).localAddress(info.getHost())
                    .bind().addListener(listener);

            if (info.isQueryEnabled()) {
                ChannelFutureListener bindListener = future -> {
                    if (!future.isSuccess()) {
                        BungeeCord.this.getLogger().log(java.util.logging.Level.WARNING, "Could not bind to host " + info.getHost(), future.cause());
                    } else {
                        BungeeCord.this.listeners.add(future.channel());
                        BungeeCord.this.getLogger().log(java.util.logging.Level.INFO, "Started query on {0}", future.channel().localAddress());
                    }
                };
                new RemoteQuery(this, info).start(PipelineUtils.getDatagramChannel(), new InetSocketAddress(info.getHost().getAddress(), info.getQueryPort()), this.eventLoops, bindListener);
            }
        }
    }

    public void stopListeners() {
        for (Channel listener : this.listeners) {
            getLogger().log(java.util.logging.Level.INFO, "Closing listener {0}", listener);
            try {
                listener.close().syncUninterruptibly();
            } catch (ChannelException ex) {
                getLogger().severe("Could not close listen thread");
            }
        }
        this.listeners.clear();
    }


    public void stop() {
        stop(getTranslation("restart"));
    }


    public synchronized void stop(final String reason) {
        if (!this.isRunning) {
            return;
        }
        this.isRunning = false;

        new Thread("Shutdown Thread") {

            @Override
            @SuppressFBWarnings("DM_EXIT")
            @SuppressWarnings("TooBroadCatch")
            public void run() {
                stopListeners();
                getLogger().info("Closing pending connections");

                connectionLock.readLock().lock();
                try {
                    getLogger().log(Level.INFO, "Disconnecting {0} connections", connections.size());
                    for (UserConnection user : connections.values()) {
                        user.disconnect(reason);
                    }
                } finally {
                    connectionLock.readLock().unlock();
                }

                try {
                    Thread.sleep(500);
                } catch (InterruptedException ignored) {
                }

                if (reconnectHandler != null) {
                    getLogger().info("Saving reconnect locations");
                    reconnectHandler.save();
                    reconnectHandler.close();
                }
                saveThread.cancel();
                metricsThread.cancel();

                // TODO: Fix this shit
                getLogger().info("Disabling plugins");
                for (Plugin plugin : Lists.reverse(new ArrayList<>(pluginManager.getPlugins()))) {
                    try {
                        plugin.onDisable();
                        for (Handler handler : plugin.getLogger().getHandlers()) {
                            handler.close();
                        }
                    } catch (Throwable t) {
                        getLogger().log(Level.SEVERE, "Exception disabling plugin " + plugin.getDescription().getName(), t);
                    }
                    getScheduler().cancel(plugin);
                    plugin.getExecutorService().shutdownNow();
                }

                getLogger().info("Closing IO threads");
                eventLoops.shutdownGracefully();
                try {
                    eventLoops.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
                } catch (InterruptedException ignored) {
                }

                getLogger().info("Thank you and goodbye");
                // Need to close loggers after last message!
                for (Handler handler : getLogger().getHandlers()) {
                    handler.close();
                }
                System.exit(0);
            }
        }.start();
    }

    public void broadcast(DefinedPacket packet) {
        this.connectionLock.readLock().lock();
        try {
            for (UserConnection con : this.connections.values()) {
                con.unsafe().sendPacket(packet);
            }
        } finally {
            this.connectionLock.readLock().unlock();
        }
    }


    public String getName() {
        return "BungeeCord";
    }


    public String getVersion() {
        return BungeeCord.class.getPackage().getImplementationVersion() == null ? "unknown" : BungeeCord.class.getPackage().getImplementationVersion();
    }

    public void reloadMessages() {
        File file = new File("messages.properties");
        if (file.isFile()) {
            try {
                FileReader rd = new FileReader(file);
                Throwable localThrowable3 = null;
                try {
                    this.customBundle = new PropertyResourceBundle(rd);
                } catch (Throwable localThrowable1) {
                    localThrowable3 = localThrowable1;
                    throw localThrowable1;
                } finally {
                    if (localThrowable3 != null) try {
                        rd.close();
                    } catch (Throwable localThrowable2) {
                        localThrowable3.addSuppressed(localThrowable2);
                    }
                    else rd.close();
                }
            } catch (IOException ex) {
                getLogger().log(java.util.logging.Level.SEVERE, "Could not load custom messages.properties", ex);
            }
        }
    }


    public String getTranslation(String name, Object... args) {
        String translation = "<translation '" + name + "' missing>";
        try {
            translation = MessageFormat.format((this.customBundle != null) && (this.customBundle.containsKey(name)) ? this.customBundle.getString(name) : this.baseBundle.getString(name), args);
        } catch (MissingResourceException ignored) {
        }

        return translation;
    }


    public Collection<ProxiedPlayer> getPlayers() {
        this.connectionLock.readLock().lock();
        try {
            return Collections.unmodifiableCollection(new HashSet<>(this.connections.values()));
        } finally {
            this.connectionLock.readLock().unlock();
        }
    }


    public int getOnlineCount() {
        return this.connections.size();
    }


    public ProxiedPlayer getPlayer(String name) {
        this.connectionLock.readLock().lock();
        try {
            return this.connections.get(name);
        } finally {
            this.connectionLock.readLock().unlock();
        }
    }

    public UserConnection getPlayerByOfflineUUID(UUID name) {
        this.connectionLock.readLock().lock();
        try {
            return this.connectionsByOfflineUUID.get(name);
        } finally {
            this.connectionLock.readLock().unlock();
        }
    }


    public ProxiedPlayer getPlayer(UUID uuid) {
        this.connectionLock.readLock().lock();
        try {
            return this.connectionsByUUID.get(uuid);
        } finally {
            this.connectionLock.readLock().unlock();
        }
    }


    public Map<String, ServerInfo> getServers() {
        return this.config.getServers();
    }


    public ServerInfo getServerInfo(String name) {
        return getServers().get(name);
    }

    public void registerChannel(String channel) {
        synchronized (this.pluginChannels) {

            this.pluginChannels.add(channel);
        }
    }

    public void unregisterChannel(String channel) {
        synchronized (this.pluginChannels) {

            this.pluginChannels.remove(channel);
        }
    }

    public PluginMessage registerChannels(int protocolVersion) {
        if (protocolVersion >= 393) {
            return new PluginMessage("minecraft:register", Util.format(this.pluginChannels.stream().map(PluginMessage.MODERNISE::apply).collect(Collectors.toList()), "\000").getBytes(Charsets.UTF_8), false);
        }

        return new PluginMessage("REGISTER", Util.format(this.pluginChannels, "\000").getBytes(Charsets.UTF_8), false);
    }


    public int getProtocolVersion() {
        return ProtocolConstants.SUPPORTED_VERSION_IDS.get(ProtocolConstants.SUPPORTED_VERSION_IDS.size() - 1);
    }


    public String getGameVersion() {
        return ProtocolConstants.SUPPORTED_VERSIONS.get(0) + "-" + ProtocolConstants.SUPPORTED_VERSIONS.get(ProtocolConstants.SUPPORTED_VERSIONS.size() - 1);
    }


    public ServerInfo constructServerInfo(String name, InetSocketAddress address, String motd, boolean restricted) {
        return new BungeeServerInfo(name, address, motd, restricted);
    }


    public CommandSender getConsole() {
        return ConsoleCommandSender.getInstance();
    }


    public void broadcast(String message) {
        broadcast(TextComponent.fromLegacyText(message));
    }


    public void broadcast(BaseComponent... message) {
        getConsole().sendMessage(BaseComponent.toLegacyText(message));
        broadcast(new Chat(ComponentSerializer.toString(message)));
    }


    public void broadcast(BaseComponent message) {
        getConsole().sendMessage(message.toLegacyText());
        broadcast(new Chat(ComponentSerializer.toString(message)));
    }


    public Collection<String> getDisabledCommands() {
        return this.config.getDisabledCommands();
    }


    public Collection<ProxiedPlayer> matchPlayer(final String partialName) {
        Preconditions.checkNotNull(partialName, "partialName");

        ProxiedPlayer exactMatch = getPlayer(partialName);
        if (exactMatch != null) {
            return Collections.singleton(exactMatch);
        }

        return Sets.newHashSet(getPlayers().stream().filter(input -> input != null && input.getName().toLowerCase(Locale.ROOT).startsWith(partialName.toLowerCase(Locale.ROOT))).collect(Collectors.toList()));
    }


    public Title createTitle() {
        return new BungeeTitle();
    }

    public Collection<String> getChannels() {
        return Collections.unmodifiableCollection(pluginChannels);
    }

    public void addConnection(UserConnection con) {
        connectionLock.writeLock().lock();
        try {
            connections.put(con.getName(), con);
            connectionsByUUID.put(con.getUniqueId(), con);
            connectionsByOfflineUUID.put(con.getPendingConnection().getOfflineId(), con);
        } finally {
            connectionLock.writeLock().unlock();
        }
    }


    public void removeConnection(UserConnection con) {
        connectionLock.writeLock().lock();
        try {
            if (connections.get(con.getName()) == con) {
                connections.remove(con.getName());
                connectionsByUUID.remove(con.getUniqueId());
                connectionsByOfflineUUID.remove(con.getPendingConnection().getOfflineId());
            }
        } finally {
            connectionLock.writeLock().unlock();
        }
    }
}
