/*    */ package net.md_5.bungee.compress;
/*    */ 
/*    */ import com.google.common.base.Preconditions;
/*    */ import io.netty.buffer.ByteBuf;
/*    */ import io.netty.buffer.ByteBufAllocator;
/*    */ import io.netty.channel.ChannelHandlerContext;
/*    */ import io.netty.handler.codec.MessageToMessageDecoder;
/*    */ import java.util.List;
/*    */ import net.md_5.bungee.jni.NativeCode;
/*    */ import net.md_5.bungee.jni.zlib.BungeeZlib;
/*    */ 
/*    */ public class PacketDecompressor extends MessageToMessageDecoder<ByteBuf>
/*    */ {
/* 14 */   private final BungeeZlib zlib = (BungeeZlib)CompressFactory.zlib.newInstance();
/*    */   
/*    */   public void handlerAdded(ChannelHandlerContext ctx)
/*    */     throws Exception
/*    */   {
/* 19 */     this.zlib.init(false, 0);
/*    */   }
/*    */   
/*    */   public void handlerRemoved(ChannelHandlerContext ctx)
/*    */     throws Exception
/*    */   {
/* 25 */     this.zlib.free();
/*    */   }
/*    */   
/*    */   protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out)
/*    */     throws Exception
/*    */   {
/* 31 */     int size = net.md_5.bungee.protocol.DefinedPacket.readVarInt(in);
/* 32 */     if (size == 0)
/*    */     {
/* 34 */       out.add(in.slice().retain());
/* 35 */       in.skipBytes(in.readableBytes());
/*    */     }
/*    */     else {
/* 38 */       ByteBuf decompressed = ctx.alloc().directBuffer();
/*    */       
/*    */       try
/*    */       {
/* 42 */         this.zlib.process(in, decompressed);
/* 43 */         Preconditions.checkState(decompressed.readableBytes() == size, "Decompressed packet size mismatch");
/*    */         
/* 45 */         out.add(decompressed);
/* 46 */         decompressed = null;
/*    */       }
/*    */       finally {
/* 49 */         if (decompressed != null)
/*    */         {
/* 51 */           decompressed.release();
/*    */         }
/*    */       }
/*    */     }
/*    */   }
/*    */ }


