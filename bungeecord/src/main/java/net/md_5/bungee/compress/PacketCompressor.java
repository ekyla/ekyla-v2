/*    */ package net.md_5.bungee.compress;
/*    */ 
/*    */ import io.netty.buffer.ByteBuf;
/*    */ import io.netty.channel.ChannelHandlerContext;
/*    */ import io.netty.handler.codec.MessageToByteEncoder;
/*    */ import net.md_5.bungee.jni.NativeCode;
/*    */ import net.md_5.bungee.jni.zlib.BungeeZlib;
/*    */ import net.md_5.bungee.protocol.DefinedPacket;
/*    */ 
/*    */ 
/*    */ public class PacketCompressor
/*    */   extends MessageToByteEncoder<ByteBuf>
/*    */ {
/* 14 */   private final BungeeZlib zlib = (BungeeZlib)CompressFactory.zlib.newInstance();
/* 15 */   public void setThreshold(int threshold) { this.threshold = threshold; } private int threshold = 256;
/*    */   
/*    */ 
/*    */   public void handlerAdded(ChannelHandlerContext ctx)
/*    */     throws Exception
/*    */   {
/* 21 */     this.zlib.init(true, -1);
/*    */   }
/*    */   
/*    */   public void handlerRemoved(ChannelHandlerContext ctx)
/*    */     throws Exception
/*    */   {
/* 27 */     this.zlib.free();
/*    */   }
/*    */   
/*    */   protected void encode(ChannelHandlerContext ctx, ByteBuf msg, ByteBuf out)
/*    */     throws Exception
/*    */   {
/* 33 */     int origSize = msg.readableBytes();
/* 34 */     if (origSize < this.threshold)
/*    */     {
/* 36 */       DefinedPacket.writeVarInt(0, out);
/* 37 */       out.writeBytes(msg);
/*    */     }
/*    */     else {
/* 40 */       DefinedPacket.writeVarInt(origSize, out);
/*    */       
/* 42 */       this.zlib.process(msg, out);
/*    */     }
/*    */   }
/*    */ }


