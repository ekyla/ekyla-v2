/*    */ package net.md_5.bungee;
/*    */ 
/*    */ import net.md_5.bungee.api.chat.BaseComponent;
/*    */ import net.md_5.bungee.netty.ChannelWrapper;
/*    */ 
/*    */ public class ServerConnection implements net.md_5.bungee.api.connection.Server
/*    */ {
/*    */   private final ChannelWrapper ch;
/*    */   private final BungeeServerInfo info;
/*    */   private boolean isObsolete;
/*    */   
/*    */   public ServerConnection(ChannelWrapper ch, BungeeServerInfo info)
/*    */   {
/* 14 */     this.ch = ch;this.info = info;
/*    */   }
/*    */   
/*    */ 
/* 18 */   public ChannelWrapper getCh() { return this.ch; }
/*    */   
/* 20 */   public BungeeServerInfo getInfo() { return this.info; }
/*    */   
/* 22 */   public boolean isObsolete() { return this.isObsolete; }
/* 23 */   public void setObsolete(boolean isObsolete) { this.isObsolete = isObsolete; }
/*    */   
/* 25 */   public boolean isForgeServer() { getClass();return false; } private final boolean forgeServer = false;
/*    */   
/* 27 */   public long getSentPingId() { return this.sentPingId; } private long sentPingId = -1L;
/* 28 */   public void setSentPingId(long sentPingId) { this.sentPingId = sentPingId; }
/*    */   
/*    */ 
/* 31 */   private final net.md_5.bungee.api.connection.Connection.Unsafe unsafe = new net.md_5.bungee.api.connection.Connection.Unsafe()
/*    */   {
/*    */ 
/*    */     public void sendPacket(net.md_5.bungee.protocol.DefinedPacket packet)
/*    */     {
/* 36 */       ServerConnection.this.ch.write(packet);
/*    */     }
/*    */   };
/*    */   
/*    */ 
/*    */   public void sendData(String channel, byte[] data)
/*    */   {
/* 43 */     unsafe().sendPacket(new net.md_5.bungee.protocol.packet.PluginMessage(channel, data, false));
/*    */   }
/*    */   
/*    */ 
/*    */   public void disconnect(String reason)
/*    */   {
/* 49 */     disconnect(new BaseComponent[0]);
/*    */   }
/*    */   
/*    */ 
/*    */   public void disconnect(BaseComponent... reason)
/*    */   {
/* 55 */     com.google.common.base.Preconditions.checkArgument(reason.length == 0, "Server cannot have disconnect reason");
/*    */     
/* 57 */     this.ch.delayedClose(null);
/*    */   }
/*    */   
/*    */ 
/*    */   public void disconnect(BaseComponent reason)
/*    */   {
/* 63 */     disconnect(new BaseComponent[0]);
/*    */   }
/*    */   
/*    */ 
/*    */   public java.net.InetSocketAddress getAddress()
/*    */   {
/* 69 */     return getInfo().getAddress();
/*    */   }
/*    */   
/*    */ 
/*    */   public boolean isConnected()
/*    */   {
/* 75 */     return !this.ch.isClosed();
/*    */   }
/*    */   
/*    */ 
/*    */   public net.md_5.bungee.api.connection.Connection.Unsafe unsafe()
/*    */   {
/* 81 */     return this.unsafe;
/*    */   }
/*    */ }


