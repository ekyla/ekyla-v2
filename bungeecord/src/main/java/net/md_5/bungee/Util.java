package net.md_5.bungee;

import com.google.common.base.Joiner;
import com.google.common.primitives.UnsignedLongs;

import java.net.InetSocketAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.UUID;


public class Util {
    public static final int DEFAULT_PORT = 25565;

    public static InetSocketAddress getAddr(String hostline) {
        URI uri;
        try {
            uri = new URI("tcp://" + hostline);
        } catch (URISyntaxException ex) {
            throw new IllegalArgumentException("Bad hostline: " + hostline, ex);
        }
        if (uri.getHost() == null) {
            throw new IllegalArgumentException("Invalid host/address: " + hostline);
        }
        return new InetSocketAddress(uri.getHost(), uri.getPort() == -1 ? 25565 : uri.getPort());
    }


    public static String hex(int i) {
        return String.format("0x%02X", i);
    }


    public static String exception(Throwable t) {
        StackTraceElement[] trace = t.getStackTrace();
        return t.getClass().getSimpleName() + " : " + t.getMessage() + (trace.length > 0 ? " @ " + t
                .getStackTrace()[0].getClassName() + ":" + t.getStackTrace()[0].getLineNumber() : "");
    }

    public static String csv(Iterable<?> objects) {
        return format(objects, ", ");
    }

    public static String format(Iterable<?> objects, String separators) {
        return Joiner.on(separators).join(objects);
    }


    public static UUID getUUID(String uuid) {
        return new UUID(UnsignedLongs.parseUnsignedLong(uuid.substring(0, 16), 16), UnsignedLongs.parseUnsignedLong(uuid.substring(16), 16));
    }
}


