package net.md_5.bungee;

import com.google.common.base.Preconditions;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFutureListener;
import net.md_5.bungee.api.Callback;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.connection.PingHandler;
import net.md_5.bungee.netty.HandlerBoss;
import net.md_5.bungee.netty.PipelineUtils;
import net.md_5.bungee.protocol.DefinedPacket;
import net.md_5.bungee.protocol.packet.PluginMessage;

import java.net.InetSocketAddress;
import java.util.*;

public class BungeeServerInfo implements ServerInfo {
    private final String name;
    private final InetSocketAddress address;
    private final Collection<ProxiedPlayer> players = new ArrayList<>();
    private final String motd;
    private final boolean restricted;
    private final Queue<DefinedPacket> packetQueue = new LinkedList<>();

    public BungeeServerInfo(String name, InetSocketAddress address, String motd, boolean restricted) {
        this.name = name;
        this.address = address;
        this.motd = motd;
        this.restricted = restricted;
    }

    public String toString() {
        return "BungeeServerInfo(name=" + getName() + ", address=" + getAddress() + ", restricted=" + isRestricted() + ")";
    }

    public String getName() {
        return this.name;
    }

    public InetSocketAddress getAddress() {
        return this.address;
    }

    public String getMotd() {
        return this.motd;
    }

    public boolean isRestricted() {
        return this.restricted;
    }

    public Queue<DefinedPacket> getPacketQueue() {
        return this.packetQueue;
    }

    public void addPlayer(ProxiedPlayer player) {
        synchronized (this.players) {

            this.players.add(player);
        }
    }

    public void removePlayer(ProxiedPlayer player) {
        synchronized (this.players) {

            this.players.remove(player);
        }
    }


    public String getPermission() {
        return "bungeecord.server." + this.name;
    }


    public boolean canAccess(CommandSender player) {
        Preconditions.checkNotNull(player, "player");
        return (!this.restricted) || (player.hasPermission(getPermission()));
    }


    public boolean equals(Object obj) {
        return ((obj instanceof ServerInfo)) && (java.util.Objects.equals(getAddress(), ((ServerInfo) obj).getAddress()));
    }


    public int hashCode() {
        return this.address.hashCode();
    }


    public void sendData(String channel, byte[] data) {
        sendData(channel, data, true);
    }


    public boolean sendData(String channel, byte[] data, boolean queue) {
        Preconditions.checkNotNull(channel, "channel");
        Preconditions.checkNotNull(data, "data");

        synchronized (this.packetQueue) {
            Server server = this.players.isEmpty() ? null : this.players.iterator().next().getServer();
            if (server != null) {
                server.sendData(channel, data);
                return true;
            }
            if (queue) {
                this.packetQueue.add(new PluginMessage(channel, data, false));
            }
            return false;
        }
    }


    public void ping(Callback<ServerPing> callback) {
        ping(callback, ProxyServer.getInstance().getProtocolVersion());
    }

    public void ping(final Callback<ServerPing> callback, final int protocolVersion) {
        Preconditions.checkNotNull(callback, "callback");

        ChannelFutureListener listener = future -> {
            if (future.isSuccess()) {
                future.channel().pipeline().get(HandlerBoss.class).setHandler(new PingHandler(BungeeServerInfo.this, callback, protocolVersion));
            } else {
                callback.done(null, future.cause());
            }
        };
        new Bootstrap().channel(PipelineUtils.getChannel()).group(BungeeCord.getInstance().eventLoops).handler(PipelineUtils.BASE).option(io.netty.channel.ChannelOption.CONNECT_TIMEOUT_MILLIS, 5000).remoteAddress(getAddress()).connect()
                .addListener(listener);
    }

    public Collection<ProxiedPlayer> getPlayers() {
        return Collections.unmodifiableCollection(new HashSet<>(players));
    }
}


