/*     */ package net.md_5.bungee.conf;
/*     */ 
/*     */ import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
/*     */ import java.io.File;
/*     */ import java.io.FileInputStream;
/*     */ import java.io.FileWriter;
/*     */ import java.io.IOException;
/*     */ import java.io.InputStream;
/*     */ import java.net.InetSocketAddress;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Arrays;
/*     */ import java.util.Collection;
/*     */ import java.util.Collections;
/*     */ import java.util.HashMap;
/*     */ import java.util.HashSet;
/*     */ import java.util.LinkedHashMap;
/*     */ import java.util.List;
/*     */ import java.util.Locale;
/*     */ import java.util.Map;
/*     */ import java.util.Map.Entry;
/*     */ import java.util.logging.Level;
/*     */ import java.util.logging.Logger;
/*     */ import net.md_5.bungee.Util;
/*     */ import net.md_5.bungee.api.ChatColor;
/*     */ import net.md_5.bungee.api.ProxyServer;
/*     */ import net.md_5.bungee.api.config.ConfigurationAdapter;
/*     */ import net.md_5.bungee.api.config.ListenerInfo;
/*     */ import net.md_5.bungee.api.config.ServerInfo;
/*     */ import net.md_5.bungee.util.CaseInsensitiveMap;
/*     */ import org.yaml.snakeyaml.DumperOptions;
/*     */ import org.yaml.snakeyaml.DumperOptions.FlowStyle;
/*     */ import org.yaml.snakeyaml.Yaml;
/*     */ import org.yaml.snakeyaml.error.YAMLException;
/*     */ 
/*     */ public class YamlConfig
/*     */   implements ConfigurationAdapter
/*     */ {
/*     */   private final Yaml yaml;
/*     */   private Map<String, Object> config;
/*     */   
/*     */   private static enum DefaultTabList
/*     */   {
/*  43 */     GLOBAL,  GLOBAL_PING,  SERVER;
/*     */     
/*     */     private DefaultTabList() {} }
/*     */   
/*  47 */   private final File file = new File("config.yml");
/*     */   
/*     */   public YamlConfig()
/*     */   {
/*  51 */     DumperOptions options = new DumperOptions();
/*  52 */     options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
/*  53 */     this.yaml = new Yaml(options);
/*     */   }
/*     */   
/*     */ 
/*     */   public void load()
/*     */   {
/*     */     try
/*     */     {
/*  61 */       this.file.createNewFile();
/*     */       
/*  63 */       InputStream is = new FileInputStream(this.file);Throwable localThrowable3 = null;
/*     */       try
/*     */       {
/*     */         try {
/*  67 */           this.config = ((Map)this.yaml.load(is));
/*     */         }
/*     */         catch (YAMLException ex) {
/*  70 */           throw new RuntimeException("Invalid configuration encountered - this is a configuration error and NOT a bug! Please attempt to fix the error or see https://www.spigotmc.org/ for help.", ex);
/*     */         }
/*     */       }
/*     */       catch (Throwable localThrowable1)
/*     */       {
/*  63 */         localThrowable3 = localThrowable1;throw localThrowable1;
/*     */ 
/*     */ 
/*     */ 
/*     */       }
/*     */       finally
/*     */       {
/*     */ 
/*     */ 
/*  72 */         if (is != null) if (localThrowable3 != null) try { is.close(); } catch (Throwable localThrowable2) { localThrowable3.addSuppressed(localThrowable2); } else is.close();
/*     */       }
/*  74 */       if (this.config == null)
/*     */       {
/*  76 */         this.config = new CaseInsensitiveMap();
/*     */       }
/*     */       else {
/*  79 */         this.config = new CaseInsensitiveMap(this.config);
/*     */       }
/*     */     }
/*     */     catch (IOException ex) {
/*  83 */       throw new RuntimeException("Could not load configuration!", ex);
/*     */     }
/*     */     
/*  86 */     Map<String, Object> permissions = (Map)get("permissions", null);
/*  87 */     if (permissions == null)
/*     */     {
/*  89 */       set("permissions.default", Arrays.asList(new String[] { "bungeecord.command.server", "bungeecord.command.list" }));
/*     */       
/*     */ 
/*     */ 
/*  93 */       set("permissions.admin", Arrays.asList(new String[] { "bungeecord.command.alert", "bungeecord.command.end", "bungeecord.command.ip", "bungeecord.command.reload" }));
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*  99 */     Object groups = (Map)get("groups", null);
/* 100 */     if (groups == null)
/*     */     {
/* 102 */       set("groups.md_5", Collections.singletonList("admin"));
/*     */     }
/*     */   }
/*     */   
/*     */   private <T> T get(String path, T def)
/*     */   {
/* 108 */     return (T)get(path, def, this.config);
/*     */   }
/*     */   
/*     */ 
/*     */   private <T> T get(String path, T def, Map submap)
/*     */   {
/* 114 */     int index = path.indexOf('.');
/* 115 */     if (index == -1)
/*     */     {
/* 117 */       Object val = submap.get(path);
/* 118 */       if ((val == null) && (def != null))
/*     */       {
/* 120 */         val = def;
/* 121 */         submap.put(path, def);
/* 122 */         save();
/*     */       }
/* 124 */       return (T)val;
/*     */     }
/*     */     
/* 127 */     String first = path.substring(0, index);
/* 128 */     String second = path.substring(index + 1, path.length());
/* 129 */     Map sub = (Map)submap.get(first);
/* 130 */     if (sub == null)
/*     */     {
/* 132 */       sub = new LinkedHashMap();
/* 133 */       submap.put(first, sub);
/*     */     }
/* 135 */     return (T)get(second, def, sub);
/*     */   }
/*     */   
/*     */ 
/*     */   private void set(String path, Object val)
/*     */   {
/* 141 */     set(path, val, this.config);
/*     */   }
/*     */   
/*     */ 
/*     */   private void set(String path, Object val, Map submap)
/*     */   {
/* 147 */     int index = path.indexOf('.');
/* 148 */     if (index == -1)
/*     */     {
/* 150 */       if (val == null)
/*     */       {
/* 152 */         submap.remove(path);
/*     */       }
/*     */       else {
/* 155 */         submap.put(path, val);
/*     */       }
/* 157 */       save();
/*     */     }
/*     */     else {
/* 160 */       String first = path.substring(0, index);
/* 161 */       String second = path.substring(index + 1, path.length());
/* 162 */       Map sub = (Map)submap.get(first);
/* 163 */       if (sub == null)
/*     */       {
/* 165 */         sub = new LinkedHashMap();
/* 166 */         submap.put(first, sub);
/*     */       }
/* 168 */       set(second, val, sub);
/*     */     }
/*     */   }
/*     */   
/*     */   private void save()
/*     */   {
/*     */     try
/*     */     {
/* 176 */       FileWriter wr = new FileWriter(this.file);Throwable localThrowable3 = null;
/*     */       try {
/* 178 */         this.yaml.dump(this.config, wr);
/*     */       }
/*     */       catch (Throwable localThrowable1)
/*     */       {
/* 176 */         localThrowable3 = localThrowable1;throw localThrowable1;
/*     */       }
/*     */       finally {
/* 179 */         if (wr != null) if (localThrowable3 != null) try { wr.close(); } catch (Throwable localThrowable2) { localThrowable3.addSuppressed(localThrowable2); } else wr.close();
/*     */       }
/*     */     } catch (IOException ex) {
/* 182 */       ProxyServer.getInstance().getLogger().log(Level.WARNING, "Could not save config", ex);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   public int getInt(String path, int def)
/*     */   {
/* 189 */     return ((Integer)get(path, Integer.valueOf(def))).intValue();
/*     */   }
/*     */   
/*     */ 
/*     */   public String getString(String path, String def)
/*     */   {
/* 195 */     return (String)get(path, def);
/*     */   }
/*     */   
/*     */ 
/*     */   public boolean getBoolean(String path, boolean def)
/*     */   {
/* 201 */     return ((Boolean)get(path, Boolean.valueOf(def))).booleanValue();
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public Map<String, ServerInfo> getServers()
/*     */   {
/* 208 */     Map<String, Map<String, Object>> base = (Map)get("servers", Collections.singletonMap("lobby", new HashMap()));
/* 209 */     Map<String, ServerInfo> ret = new HashMap();
/*     */     
/* 211 */     for (Map.Entry<String, Map<String, Object>> entry : base.entrySet())
/*     */     {
/* 213 */       Map<String, Object> val = (Map)entry.getValue();
/* 214 */       String name = (String)entry.getKey();
/* 215 */       String addr = (String)get("address", "localhost:25565", val);
/* 216 */       String motd = ChatColor.translateAlternateColorCodes('&', (String)get("motd", "&1Just another BungeeCord - Forced Host", val));
/* 217 */       boolean restricted = ((Boolean)get("restricted", Boolean.valueOf(false), val)).booleanValue();
/* 218 */       InetSocketAddress address = Util.getAddr(addr);
/* 219 */       ServerInfo info = ProxyServer.getInstance().constructServerInfo(name, address, motd, restricted);
/* 220 */       ret.put(name, info);
/*     */     }
/*     */     
/* 223 */     return ret;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   @SuppressFBWarnings({"RCN_REDUNDANT_NULLCHECK_OF_NONNULL_VALUE"})
/*     */   public Collection<ListenerInfo> getListeners()
/*     */   {
/* 231 */     Collection<Map<String, Object>> base = (Collection)get("listeners", Arrays.asList(new Map[] { new HashMap() }));
/*     */     
/*     */ 
/*     */ 
/* 235 */     Map<String, String> forcedDef = new HashMap();
/* 236 */     forcedDef.put("pvp.md-5.net", "pvp");
/*     */     
/* 238 */     Collection<ListenerInfo> ret = new HashSet();
/*     */     
/* 240 */     for (Map<String, Object> val : base)
/*     */     {
/* 242 */       String motd = (String)get("motd", "&1Another Bungee server", val);
/* 243 */       motd = ChatColor.translateAlternateColorCodes('&', motd);
/*     */       
/* 245 */       int maxPlayers = ((Integer)get("max_players", Integer.valueOf(1), val)).intValue();
/* 246 */       boolean forceDefault = ((Boolean)get("force_default_server", Boolean.valueOf(false), val)).booleanValue();
/* 247 */       String host = (String)get("host", "0.0.0.0:25577", val);
/* 248 */       int tabListSize = ((Integer)get("tab_size", Integer.valueOf(60), val)).intValue();
/* 249 */       InetSocketAddress address = Util.getAddr(host);
/* 250 */       Map<String, String> forced = new CaseInsensitiveMap((Map)get("forced_hosts", forcedDef, val));
/* 251 */       String tabListName = (String)get("tab_list", "GLOBAL_PING", val);
/* 252 */       DefaultTabList value = DefaultTabList.valueOf(tabListName.toUpperCase(Locale.ROOT));
/* 253 */       if (value == null)
/*     */       {
/* 255 */         value = DefaultTabList.GLOBAL_PING;
/*     */       }
/* 257 */       boolean setLocalAddress = ((Boolean)get("bind_local_address", Boolean.valueOf(true), val)).booleanValue();
/* 258 */       boolean pingPassthrough = ((Boolean)get("ping_passthrough", Boolean.valueOf(false), val)).booleanValue();
/*     */       
/* 260 */       boolean query = ((Boolean)get("query_enabled", Boolean.valueOf(false), val)).booleanValue();
/* 261 */       int queryPort = ((Integer)get("query_port", Integer.valueOf(25577), val)).intValue();
/*     */       
/* 263 */       boolean proxyProtocol = ((Boolean)get("proxy_protocol", Boolean.valueOf(false), val)).booleanValue();
/* 264 */       List<String> serverPriority = new ArrayList((Collection)get("priorities", Collections.EMPTY_LIST, val));
/*     */       
/*     */ 
/*     */ 
/* 268 */       String defaultServer = (String)get("default_server", null, val);
/* 269 */       String fallbackServer = (String)get("fallback_server", null, val);
/* 270 */       if (defaultServer != null)
/*     */       {
/* 272 */         serverPriority.add(defaultServer);
/* 273 */         set("default_server", null, val);
/*     */       }
/* 275 */       if (fallbackServer != null)
/*     */       {
/* 277 */         serverPriority.add(fallbackServer);
/* 278 */         set("fallback_server", null, val);
/*     */       }
/*     */       
/*     */ 
/* 282 */       if (serverPriority.isEmpty())
/*     */       {
/* 284 */         serverPriority.add("lobby");
/*     */       }
/* 286 */       set("priorities", serverPriority, val);
/*     */       
/* 288 */       ListenerInfo info = new ListenerInfo(address, motd, maxPlayers, tabListSize, serverPriority, forceDefault, forced, value.toString(), setLocalAddress, pingPassthrough, queryPort, query, proxyProtocol);
/* 289 */       ret.add(info);
/*     */     }
/*     */     
/* 292 */     return ret;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public Collection<String> getGroups(String player)
/*     */   {
/* 299 */     Collection<String> groups = (Collection)get("groups." + player, null);
/* 300 */     Collection<String> ret = groups == null ? new HashSet() : new HashSet(groups);
/* 301 */     ret.add("default");
/* 302 */     return ret;
/*     */   }
/*     */   
/*     */ 
/*     */   public Collection<?> getList(String path, Collection<?> def)
/*     */   {
/* 308 */     return (Collection)get(path, def);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public Collection<String> getPermissions(String group)
/*     */   {
/* 315 */     Collection<String> permissions = (Collection)get("permissions." + group, null);
/* 316 */     return permissions == null ? Collections.EMPTY_SET : permissions;
/*     */   }
/*     */ }


