package net.md_5.bungee.conf;

import com.google.common.base.Preconditions;
import gnu.trove.map.TMap;
import net.md_5.bungee.api.Favicon;
import net.md_5.bungee.api.ProxyConfig;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ConfigurationAdapter;
import net.md_5.bungee.api.config.ListenerInfo;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.util.CaseInsensitiveMap;
import net.md_5.bungee.util.CaseInsensitiveSet;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;


public class Configuration
        implements ProxyConfig {
    private int timeout = 30000;
    private String uuid = UUID.randomUUID().toString();
    private Collection<ListenerInfo> listeners;
    private TMap<String, ServerInfo> servers;
    private boolean onlineMode = true;
    private boolean logCommands;
    private boolean logPings = true;
    private int playerLimit = -1;
    private Collection<String> disabledCommands;
    private int throttle = 4000;
    private int throttleLimit = 3;
    private boolean ipForward;
    private Favicon favicon;
    private int compressionThreshold = 256;
    private boolean preventProxyConnections;
    private boolean forgeSupport;

    public int getTimeout() {
        return this.timeout;
    }

    public String getUuid() {
        return this.uuid;
    }

    public Collection<ListenerInfo> getListeners() {
        return this.listeners;
    }

    public TMap<String, ServerInfo> getServers() {
        return this.servers;
    }

    public boolean isOnlineMode() {
        return this.onlineMode;
    }

    public boolean isLogCommands() {
        return this.logCommands;
    }

    public boolean isLogPings() {
        return this.logPings;
    }

    public int getPlayerLimit() {
        return this.playerLimit;
    }

    public Collection<String> getDisabledCommands() {
        return this.disabledCommands;
    }

    public int getThrottle() {
        return this.throttle;
    }

    public int getThrottleLimit() {
        return this.throttleLimit;
    }

    public boolean isIpForward() {
        return this.ipForward;
    }

    public int getCompressionThreshold() {
        return this.compressionThreshold;
    }

    public boolean isPreventProxyConnections() {
        return this.preventProxyConnections;
    }

    public boolean isForgeSupport() {
        return this.forgeSupport;
    }

    public void load() {
        ConfigurationAdapter adapter = ProxyServer.getInstance().getConfigurationAdapter();
        adapter.load();

        File fav = new File("server-icon.png");
        if (fav.exists()) {
            try {
                this.favicon = Favicon.create(ImageIO.read(fav));
            } catch (IOException | IllegalArgumentException ex) {
                ProxyServer.getInstance().getLogger().log(Level.WARNING, "Could not load server icon", ex);
            }
        }

        this.listeners = adapter.getListeners();
        this.timeout = adapter.getInt("timeout", this.timeout);
        this.uuid = adapter.getString("stats", this.uuid);
        this.onlineMode = adapter.getBoolean("online_mode", this.onlineMode);
        this.logCommands = adapter.getBoolean("log_commands", this.logCommands);
        this.logPings = adapter.getBoolean("log_pings", this.logPings);
        this.playerLimit = adapter.getInt("player_limit", this.playerLimit);
        this.throttle = adapter.getInt("connection_throttle", this.throttle);
        this.throttleLimit = adapter.getInt("connection_throttle_limit", this.throttleLimit);
        this.ipForward = adapter.getBoolean("ip_forward", this.ipForward);
        this.compressionThreshold = adapter.getInt("network_compression_threshold", this.compressionThreshold);
        this.preventProxyConnections = adapter.getBoolean("prevent_proxy_connections", this.preventProxyConnections);
        this.forgeSupport = adapter.getBoolean("forge_support", this.forgeSupport);

        this.disabledCommands = new CaseInsensitiveSet((Collection<String>) adapter.getList("disabled_commands", Arrays.asList(new String[]{"disabledcommandhere"})));

        Preconditions.checkArgument((this.listeners != null) && (!this.listeners.isEmpty()), "No listeners defined.");

        Map<String, ServerInfo> newServers = adapter.getServers();
        Preconditions.checkArgument((newServers != null) && (!newServers.isEmpty()), "No servers defined");

        if (this.servers == null) {
            this.servers = new CaseInsensitiveMap<>(newServers);
        } else {
            for (ServerInfo oldServer : this.servers.values()) {

                Preconditions.checkArgument(newServers.containsKey(oldServer.getName()), "Server %s removed on reload!", oldServer.getName());
            }


            for (Map.Entry<String, ServerInfo> newServer : newServers.entrySet()) {
                if (!this.servers.containsValue(newServer.getValue())) {
                    this.servers.put(newServer.getKey(), newServer.getValue());
                }
            }
        }

        for (ListenerInfo listener : this.listeners) {
            for (int i = 0; i < listener.getServerPriority().size(); i++) {
                String server = listener.getServerPriority().get(i);
                Preconditions.checkArgument(this.servers.containsKey(server), "Server %s (priority %s) is not defined", server, i);
            }
            for (String server : listener.getForcedHosts().values()) {
                if (!this.servers.containsKey(server)) {
                    ProxyServer.getInstance().getLogger().log(Level.WARNING, "Forced host server {0} is not defined", server);
                }
            }
        }
        int i;
    }

    @Deprecated
    public String getFavicon() {
        return getFaviconObject().getEncoded();
    }

    public Favicon getFaviconObject() {
        return this.favicon;
    }
}


