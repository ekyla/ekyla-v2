/*     */ package net.md_5.bungee.config;
/*     */ 
/*     */ import com.google.common.base.Charsets;
/*     */ import java.io.File;
/*     */ import java.io.FileInputStream;
/*     */ import java.io.FileOutputStream;
/*     */ import java.io.IOException;
/*     */ import java.io.InputStream;
/*     */ import java.io.OutputStreamWriter;
/*     */ import java.io.Reader;
/*     */ import java.io.Writer;
/*     */ import java.util.LinkedHashMap;
/*     */ import java.util.Map;
/*     */ import org.yaml.snakeyaml.DumperOptions;
/*     */ import org.yaml.snakeyaml.DumperOptions.FlowStyle;
/*     */ import org.yaml.snakeyaml.Yaml;
/*     */ import org.yaml.snakeyaml.constructor.Constructor;
/*     */ import org.yaml.snakeyaml.nodes.Node;
/*     */ import org.yaml.snakeyaml.representer.Represent;
/*     */ import org.yaml.snakeyaml.representer.Representer;
/*     */ 
/*     */ 
/*     */ 
/*     */ public class YamlConfiguration
/*     */   extends ConfigurationProvider
/*     */ {
/*  27 */   private final ThreadLocal<Yaml> yaml = new ThreadLocal()
/*     */   {
/*     */ 
/*     */     protected Yaml initialValue()
/*     */     {
/*  32 */       Representer representer = new Representer() {};
/*  46 */       DumperOptions options = new DumperOptions();
/*  47 */       options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
/*     */       
/*  49 */       return new Yaml(new Constructor(), representer, options);
/*     */     }
/*     */   };
/*     */   
/*     */   public void save(Configuration config, File file)
/*     */     throws IOException
/*     */   {
/*  56 */     Writer writer = new OutputStreamWriter(new FileOutputStream(file), Charsets.UTF_8);Throwable localThrowable3 = null;
/*     */     try {
/*  58 */       save(config, writer);
/*     */     }
/*     */     catch (Throwable localThrowable1)
/*     */     {
/*  56 */       localThrowable3 = localThrowable1;throw localThrowable1;
/*     */     }
/*     */     finally {
/*  59 */       if (writer != null) if (localThrowable3 != null) try { writer.close(); } catch (Throwable localThrowable2) { localThrowable3.addSuppressed(localThrowable2); } else writer.close();
/*     */     }
/*     */   }
/*     */   
/*     */   public void save(Configuration config, Writer writer)
/*     */   {
/*  65 */     ((Yaml)this.yaml.get()).dump(config.self, writer);
/*     */   }
/*     */   
/*     */   public Configuration load(File file)
/*     */     throws IOException
/*     */   {
/*  71 */     return load(file, null);
/*     */   }
/*     */   
/*     */   public Configuration load(File file, Configuration defaults)
/*     */     throws IOException
/*     */   {
/*  77 */     FileInputStream is = new FileInputStream(file);Throwable localThrowable3 = null;
/*     */     try {
/*  79 */       return load(is, defaults);
/*     */     }
/*     */     catch (Throwable localThrowable4)
/*     */     {
/*  77 */       localThrowable3 = localThrowable4;throw localThrowable4;
/*     */     }
/*     */     finally {
/*  80 */       if (is != null) if (localThrowable3 != null) try { is.close(); } catch (Throwable localThrowable2) { localThrowable3.addSuppressed(localThrowable2); } else is.close();
/*     */     }
/*     */   }
/*     */   
/*     */   public Configuration load(Reader reader)
/*     */   {
/*  86 */     return load(reader, null);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public Configuration load(Reader reader, Configuration defaults)
/*     */   {
/*  93 */     Map<String, Object> map = (Map)((Yaml)this.yaml.get()).loadAs(reader, LinkedHashMap.class);
/*  94 */     if (map == null)
/*     */     {
/*  96 */       map = new LinkedHashMap();
/*     */     }
/*  98 */     return new Configuration(map, defaults);
/*     */   }
/*     */   
/*     */ 
/*     */   public Configuration load(InputStream is)
/*     */   {
/* 104 */     return load(is, null);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public Configuration load(InputStream is, Configuration defaults)
/*     */   {
/* 111 */     Map<String, Object> map = (Map)((Yaml)this.yaml.get()).loadAs(is, LinkedHashMap.class);
/* 112 */     if (map == null)
/*     */     {
/* 114 */       map = new LinkedHashMap();
/*     */     }
/* 116 */     return new Configuration(map, defaults);
/*     */   }
/*     */   
/*     */ 
/*     */   public Configuration load(String string)
/*     */   {
/* 122 */     return load(string, null);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */   public Configuration load(String string, Configuration defaults)
/*     */   {
/* 129 */     Map<String, Object> map = (Map)((Yaml)this.yaml.get()).loadAs(string, LinkedHashMap.class);
/* 130 */     if (map == null)
/*     */     {
/* 132 */       map = new LinkedHashMap();
/*     */     }
/* 134 */     return new Configuration(map, defaults);
/*     */   }
/*     */ }


