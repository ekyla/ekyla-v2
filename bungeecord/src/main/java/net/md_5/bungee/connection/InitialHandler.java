package net.md_5.bungee.connection;


import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import net.md_5.bungee.*;
import net.md_5.bungee.api.AbstractReconnectHandler;
import net.md_5.bungee.api.Callback;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.ServerPing.Protocol;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ListenerInfo;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.Connection;
import net.md_5.bungee.api.connection.PendingConnection;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.*;
import net.md_5.bungee.chat.ComponentSerializer;
import net.md_5.bungee.http.HttpClient;
import net.md_5.bungee.jni.cipher.BungeeCipher;
import net.md_5.bungee.netty.ChannelWrapper;
import net.md_5.bungee.netty.HandlerBoss;
import net.md_5.bungee.netty.PacketHandler;
import net.md_5.bungee.netty.cipher.CipherDecoder;
import net.md_5.bungee.netty.cipher.CipherEncoder;
import net.md_5.bungee.protocol.PacketWrapper;
import net.md_5.bungee.protocol.ProtocolConstants;
import net.md_5.bungee.protocol.packet.*;
import net.md_5.bungee.util.BoundedArrayList;
import net.md_5.bungee.util.BufUtil;
import net.md_5.bungee.util.QuietException;

import javax.crypto.SecretKey;
import java.math.BigInteger;
import java.net.InetSocketAddress;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;


public class InitialHandler extends PacketHandler implements PendingConnection {
    private final BungeeCord bungee;
    private final ListenerInfo listener;
    private final List<PluginMessage> relayMessages = new BoundedArrayList<>(128);
    private ChannelWrapper ch;
    private final Connection.Unsafe unsafe = packet -> InitialHandler.this.ch.write(packet);
    private Handshake handshake;
    private LoginRequest loginRequest;
    private EncryptionRequest request;
    private State thisState = State.HANDSHAKE;
    private boolean onlineMode = BungeeCord.getInstance().config.isOnlineMode();
    private InetSocketAddress virtualHost;
    private String name;
    private UUID uniqueId;
    private UUID offlineId;
    private LoginResult loginProfile;
    private boolean legacy;
    private String extraDataInHandshake = "";

    public InitialHandler(BungeeCord bungee, ListenerInfo listener) {
        this.bungee = bungee;
        this.listener = listener;
    }

    private static String getFirstLine(String str) {
        int pos = str.indexOf('\n');
        return pos == -1 ? str : str.substring(0, pos);
    }

    public ListenerInfo getListener() {
        return this.listener;
    }

    public Handshake getHandshake() {
        return this.handshake;
    }

    public LoginRequest getLoginRequest() {
        return this.loginRequest;
    }

    public List<PluginMessage> getRelayMessages() {
        return this.relayMessages;
    }

    public boolean isOnlineMode() {
        return this.onlineMode;
    }

    public void setOnlineMode(boolean onlineMode) {
        Preconditions.checkState(this.thisState == State.USERNAME, "Can only set online mode status whilst state is username");
        this.onlineMode = onlineMode;
    }

    public InetSocketAddress getVirtualHost() {
        return this.virtualHost;
    }

    public UUID getUniqueId() {
        return this.uniqueId;
    }

    public void setUniqueId(UUID uuid) {
        Preconditions.checkState(this.thisState == State.USERNAME, "Can only set uuid while state is username");
        Preconditions.checkState(!this.onlineMode, "Can only set uuid when online mode is false");
        this.uniqueId = uuid;
    }

    public UUID getOfflineId() {
        return this.offlineId;
    }

    public LoginResult getLoginProfile() {
        return this.loginProfile;
    }

    public boolean isLegacy() {
        return this.legacy;
    }

    public String getExtraDataInHandshake() {
        return this.extraDataInHandshake;
    }

    public boolean shouldHandle(PacketWrapper packet) {
        return !this.ch.isClosing();
    }

    public void connected(ChannelWrapper channel) {
        this.ch = channel;
    }

    public void exception(Throwable t) {
        disconnect(ChatColor.RED + Util.exception(t));
    }

    public void handle(PacketWrapper packet) throws Exception {
        if (packet.packet == null) {
            throw new QuietException("Unexpected packet received during login process! " + BufUtil.dump(packet.buf, 16));
        }
    }

    public void handle(PluginMessage pluginMessage)
            throws Exception {
        if (PluginMessage.SHOULD_RELAY.test(pluginMessage)) {
            this.relayMessages.add(pluginMessage);
        }
    }

    public void handle(LegacyHandshake legacyHandshake)
            throws Exception {
        this.legacy = true;
        this.ch.close(this.bungee.getTranslation("outdated_client", this.bungee.getGameVersion()));
    }

    public void handle(LegacyPing ping)
            throws Exception {
        this.legacy = true;
        final boolean v1_5 = ping.isV1_5();


        ServerPing legacy = new ServerPing(new Protocol(this.bungee.getName() + " " + this.bungee.getGameVersion(), this.bungee.getProtocolVersion()), new ServerPing.Players(this.listener.getMaxPlayers(), this.bungee.getOnlineCount(), null), new TextComponent(TextComponent.fromLegacyText(this.listener.getMotd())), null);

        Callback<ProxyPingEvent> callback = (result, error) -> {
            if (InitialHandler.this.ch.isClosed()) {
                return;
            }

            ServerPing legacy1 = result.getResponse();
            String kickMessage;
            if (v1_5) {


                kickMessage = ChatColor.DARK_BLUE + "\000" + 127 + '\000' + legacy1.getVersion().getName() + '\000' + InitialHandler.getFirstLine(legacy1.getDescription()) + '\000' + legacy1.getPlayers().getOnline() + '\000' + legacy1.getPlayers().getMax();

            } else {

                kickMessage = ChatColor.stripColor(InitialHandler.getFirstLine(legacy1.getDescription())) + '§' + legacy1.getPlayers().getOnline() + '§' + legacy1.getPlayers().getMax();
            }

            InitialHandler.this.ch.close(kickMessage);
        };
        this.bungee.getPluginManager().callEvent(new ProxyPingEvent(this, legacy, callback));
    }

    public void handle(StatusRequest statusRequest)
            throws Exception {
        Preconditions.checkState(this.thisState == State.STATUS, "Not expecting STATUS");

        ServerInfo forced = AbstractReconnectHandler.getForcedHost(this);
        String motd = forced != null ? forced.getMotd() : this.listener.getMotd();

        Callback<ServerPing> pingBack = (result, error) -> {
            if (error != null) {
                result = new ServerPing();
                result.setDescription(InitialHandler.this.bungee.getTranslation("ping_cannot_connect"));
                InitialHandler.this.bungee.getLogger().log(Level.WARNING, "Error pinging remote server", error);
            }

            Callback<ProxyPingEvent> callback = (pingResult, error1) -> {
                Gson gson = BungeeCord.getInstance().gson;
                InitialHandler.this.unsafe.sendPacket(new StatusResponse(gson.toJson(pingResult.getResponse())));
                if (InitialHandler.this.bungee.getConnectionThrottle() != null) {
                    InitialHandler.this.bungee.getConnectionThrottle().unthrottle(InitialHandler.this.getAddress().getAddress());
                }

            };
            InitialHandler.this.bungee.getPluginManager().callEvent(new ProxyPingEvent(InitialHandler.this, result, callback));
        };

        if ((forced != null) && (this.listener.isPingPassthrough())) {
            ((BungeeServerInfo) forced).ping(pingBack, this.handshake.getProtocolVersion());
        } else {
            int protocol = ProtocolConstants.SUPPORTED_VERSION_IDS.contains(this.handshake.getProtocolVersion()) ? this.handshake.getProtocolVersion() : this.bungee.getProtocolVersion();
            pingBack.done(new ServerPing(new Protocol(this.bungee
                    .getName() + " " + this.bungee.getGameVersion(), protocol), new ServerPing.Players(this.listener
                    .getMaxPlayers(), this.bungee.getOnlineCount(), null), motd,
                    BungeeCord.getInstance().config.getFaviconObject()), null);
        }


        this.thisState = State.PING;
    }

    public void handle(PingPacket ping)
            throws Exception {
        Preconditions.checkState(this.thisState == State.PING, "Not expecting PING");
        this.unsafe.sendPacket(ping);
        disconnect("");
    }

    public void handle(Handshake handshake)
            throws Exception {
        Preconditions.checkState(this.thisState == State.HANDSHAKE, "Not expecting HANDSHAKE");
        this.handshake = handshake;
        this.ch.setVersion(handshake.getProtocolVersion());


        if (handshake.getHost().contains("\000")) {
            String[] split = handshake.getHost().split("\000", 2);
            handshake.setHost(split[0]);
            this.extraDataInHandshake = ("\000" + split[1]);
        }


        if (handshake.getHost().endsWith(".")) {
            handshake.setHost(handshake.getHost().substring(0, handshake.getHost().length() - 1));
        }

        this.virtualHost = InetSocketAddress.createUnresolved(handshake.getHost(), handshake.getPort());
        if (this.bungee.getConfig().isLogPings()) {
            this.bungee.getLogger().log(Level.INFO, "{0} has connected", this);
        }

        this.bungee.getPluginManager().callEvent(new PlayerHandshakeEvent(this, handshake));

        switch (handshake.getRequestedProtocol()) {

            case 1:
                this.thisState = State.STATUS;
                this.ch.setProtocol(net.md_5.bungee.protocol.Protocol.STATUS);
                break;

            case 2:
                if (!this.bungee.getConfig().isLogPings()) {
                    this.bungee.getLogger().log(Level.INFO, "{0} has connected", this);
                }
                this.thisState = State.USERNAME;
                this.ch.setProtocol(net.md_5.bungee.protocol.Protocol.LOGIN);

                if (!ProtocolConstants.SUPPORTED_VERSION_IDS.contains(handshake.getProtocolVersion())) {
                    if (handshake.getProtocolVersion() > this.bungee.getProtocolVersion()) {
                        disconnect(this.bungee.getTranslation("outdated_server", this.bungee.getGameVersion()));
                    } else {
                        disconnect(this.bungee.getTranslation("outdated_client", this.bungee.getGameVersion()));
                    }
                    return;
                }
                break;
            default:
                throw new IllegalArgumentException("Cannot request protocol " + handshake.getRequestedProtocol());
        }

    }

    public void handle(LoginRequest loginRequest) throws Exception {
        Preconditions.checkState(this.thisState == State.USERNAME, "Not expecting USERNAME");
        this.loginRequest = loginRequest;

        if (getName().contains(".")) {
            disconnect(this.bungee.getTranslation("name_invalid"));
            return;
        }

        if (getName().length() > 16) {
            disconnect(this.bungee.getTranslation("name_too_long"));
            return;
        }

        int limit = BungeeCord.getInstance().config.getPlayerLimit();
        if ((limit > 0) && (this.bungee.getOnlineCount() > limit)) {
            disconnect(this.bungee.getTranslation("proxy_full"));
            return;
        }


        if ((!isOnlineMode()) && (this.bungee.getPlayer(getUniqueId()) != null)) {
            disconnect(this.bungee.getTranslation("already_connected_proxy"));
            return;
        }

        Callback<PreLoginEvent> callback = (result, error) -> {

            if (result.isCancelled()) {
                InitialHandler.this.disconnect(result.getCancelReasonComponents());
                return;
            }
            if (InitialHandler.this.ch.isClosed()) {
                return;
            }
            if (InitialHandler.this.onlineMode) {
                InitialHandler.this.unsafe().sendPacket(InitialHandler.this.request = EncryptionUtil.encryptRequest());
            } else {
                InitialHandler.this.finish();
            }
            InitialHandler.this.thisState = State.ENCRYPT;
        };
        this.bungee.getPluginManager().callEvent(new PreLoginEvent(this, callback));
    }

    public void handle(EncryptionResponse encryptResponse)
            throws Exception {
        Preconditions.checkState(this.thisState == State.ENCRYPT, "Not expecting ENCRYPT");

        SecretKey sharedKey = EncryptionUtil.getSecret(encryptResponse, this.request);
        BungeeCipher decrypt = EncryptionUtil.getCipher(false, sharedKey);
        this.ch.addBefore("frame-decoder", "decrypt", new CipherDecoder(decrypt));
        BungeeCipher encrypt = EncryptionUtil.getCipher(true, sharedKey);
        this.ch.addBefore("frame-prepender", "encrypt", new CipherEncoder(encrypt));

        String encName = URLEncoder.encode(getName(), "UTF-8");

        MessageDigest sha = MessageDigest.getInstance("SHA-1");
        for (byte[] bit : new byte[][]{this.request

                .getServerId().getBytes("ISO_8859_1"), sharedKey.getEncoded(), EncryptionUtil.keys.getPublic().getEncoded()}) {

            sha.update(bit);
        }
        String encodedHash = URLEncoder.encode(new BigInteger(sha.digest()).toString(16), "UTF-8");

        String preventProxy = BungeeCord.getInstance().config.isPreventProxyConnections() ? "&ip=" + URLEncoder.encode(getAddress().getAddress().getHostAddress(), "UTF-8") : "";
        String authURL = "https://sessionserver.mojang.com/session/minecraft/hasJoined?username=" + encName + "&serverId=" + encodedHash + preventProxy;

        Callback<String> handler = (result, error) -> {
            if (error == null) {
                LoginResult obj = BungeeCord.getInstance().gson.fromJson(result, LoginResult.class);
                if ((obj != null) && (obj.getId() != null)) {
                    InitialHandler.this.loginProfile = obj;
                    InitialHandler.this.name = obj.getName();
                    InitialHandler.this.uniqueId = Util.getUUID(obj.getId());
                    InitialHandler.this.finish();
                    return;
                }
                InitialHandler.this.disconnect(InitialHandler.this.bungee.getTranslation("offline_mode_player"));
            } else {
                InitialHandler.this.disconnect(InitialHandler.this.bungee.getTranslation("mojang_fail"));
                InitialHandler.this.bungee.getLogger().log(Level.SEVERE, "Error authenticating " + InitialHandler.this.getName() + " with minecraft.net", error);
            }

        };
        HttpClient.get(authURL, this.ch.getHandle().eventLoop(), handler);
    }

    private void finish() {
        if (isOnlineMode()) {


            ProxiedPlayer oldName = this.bungee.getPlayer(getName());
            if (oldName != null) {

                oldName.disconnect(this.bungee.getTranslation("already_connected_proxy"));
            }

            ProxiedPlayer oldID = this.bungee.getPlayer(getUniqueId());
            if (oldID != null) {

                oldID.disconnect(this.bungee.getTranslation("already_connected_proxy"));
            }
        } else {
            ProxiedPlayer oldName = this.bungee.getPlayer(getName());
            if (oldName != null) {

                disconnect(this.bungee.getTranslation("already_connected_proxy"));
                return;
            }
        }


        this.offlineId = UUID.nameUUIDFromBytes(("OfflinePlayer:" + getName()).getBytes(Charsets.UTF_8));
        if (this.uniqueId == null) {
            this.uniqueId = this.offlineId;
        }

        Callback<LoginEvent> complete = (result, error) -> {
            if (result.isCancelled()) {
                InitialHandler.this.disconnect(result.getCancelReasonComponents());
                return;
            }
            if (InitialHandler.this.ch.isClosed()) {
                return;
            }

            InitialHandler.this.ch.getHandle().eventLoop().execute(() -> {
                if (!InitialHandler.this.ch.isClosing()) {
                    UserConnection userCon = new UserConnection(InitialHandler.this.bungee, InitialHandler.this.ch, InitialHandler.this.getName(), InitialHandler.this);
                    userCon.setCompressionThreshold(BungeeCord.getInstance().config.getCompressionThreshold());
                    userCon.init();

                    InitialHandler.this.unsafe.sendPacket(new LoginSuccess(InitialHandler.this.getUniqueId().toString(), InitialHandler.this.getName()));
                    InitialHandler.this.ch.setProtocol(net.md_5.bungee.protocol.Protocol.GAME);

                    InitialHandler.this.ch.getHandle().pipeline().get(HandlerBoss.class).setHandler(new UpstreamBridge(InitialHandler.this.bungee, userCon));
                    PostLoginEvent postLoginEvent = new PostLoginEvent(userCon);
                    InitialHandler.this.bungee.getPluginManager().callEvent(postLoginEvent);

                    if (!postLoginEvent.isCancelTeleportation()) {
                        ServerInfo server;
                        if (InitialHandler.this.bungee.getReconnectHandler() != null) {
                            server = InitialHandler.this.bungee.getReconnectHandler().getServer(userCon);
                        } else {
                            server = AbstractReconnectHandler.getForcedHost(InitialHandler.this);
                        }
                        if (server == null) {
                            server = InitialHandler.this.bungee.getServerInfo(InitialHandler.this.listener.getDefaultServer());
                        }

                        userCon.connect(server, null, true, ServerConnectEvent.Reason.JOIN_PROXY);
                    }

                    InitialHandler.this.thisState = State.FINISHED;
                }

            });
        };
        this.bungee.getPluginManager().callEvent(new LoginEvent(this, complete));
    }

    public void handle(LoginPayloadResponse response)
            throws Exception {
        disconnect("Unexpected custom LoginPayloadResponse");
    }

    public void disconnect(String reason) {
        disconnect(TextComponent.fromLegacyText(reason));
    }

    public void disconnect(BaseComponent... reason) {
        if ((this.thisState != State.STATUS) && (this.thisState != State.PING)) {
            this.ch.delayedClose(new Kick(ComponentSerializer.toString(reason)));
        } else {
            this.ch.close();
        }
    }

    public void disconnect(BaseComponent reason) {
        disconnect(new BaseComponent[]{reason});
    }

    public String getName() {
        return this.loginRequest == null ? null : this.name != null ? this.name : this.loginRequest.getData();
    }

    public int getVersion() {
        return this.handshake == null ? -1 : this.handshake.getProtocolVersion();
    }

    public InetSocketAddress getAddress() {
        return this.ch.getRemoteAddress();
    }

    public Connection.Unsafe unsafe() {
        return this.unsafe;
    }

    public String getUUID() {
        return this.uniqueId.toString().replace("-", "");
    }

    public String toString() {
        return "[" + (getName() != null ? getName() : getAddress()) + "] <-> InitialHandler";
    }

    public boolean isConnected() {
        return !this.ch.isClosed();
    }


    private enum State {
        HANDSHAKE, STATUS, PING, USERNAME, ENCRYPT, FINISHED;

        State() {
        }
    }
}


