/*    */ package net.md_5.bungee.connection;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class CancelSendSignal
/*    */   extends Error
/*    */ {
/* 10 */   public static final CancelSendSignal INSTANCE = new CancelSendSignal();
/*    */   
/*    */ 
/*    */   public Throwable initCause(Throwable cause)
/*    */   {
/* 15 */     return this;
/*    */   }
/*    */   
/*    */ 
/*    */   public Throwable fillInStackTrace()
/*    */   {
/* 21 */     return this;
/*    */   }
/*    */ }


