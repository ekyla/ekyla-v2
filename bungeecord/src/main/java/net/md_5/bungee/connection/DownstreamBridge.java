package net.md_5.bungee.connection;

import com.google.common.io.ByteArrayDataOutput;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.builder.RequiredArgumentBuilder;
import com.mojang.brigadier.context.StringRange;
import com.mojang.brigadier.suggestion.Suggestion;
import com.mojang.brigadier.suggestion.Suggestions;
import com.mojang.brigadier.tree.LiteralCommandNode;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import net.md_5.bungee.ServerConnection;
import net.md_5.bungee.UserConnection;
import net.md_5.bungee.Util;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.*;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.score.Objective;
import net.md_5.bungee.api.score.Score;
import net.md_5.bungee.api.score.Scoreboard;
import net.md_5.bungee.netty.ChannelWrapper;
import net.md_5.bungee.protocol.DefinedPacket;
import net.md_5.bungee.protocol.PacketWrapper;
import net.md_5.bungee.protocol.packet.*;
import net.md_5.bungee.tab.TabList;

import java.io.DataInput;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DownstreamBridge extends net.md_5.bungee.netty.PacketHandler {
    private final ProxyServer bungee;
    private final UserConnection con;
    private final ServerConnection server;

    public DownstreamBridge(ProxyServer bungee, UserConnection con, ServerConnection server) {
        this.bungee = bungee;
        this.con = con;
        this.server = server;
    }


    public void exception(Throwable t) {
        if (this.server.isObsolete()) {

            return;
        }

        ServerInfo def = this.con.updateAndGetNextServer(this.server.getInfo());
        if (def != null) {
            this.server.setObsolete(true);
            this.con.connectNow(def, ServerConnectEvent.Reason.SERVER_DOWN_REDIRECT);
            this.con.sendMessage(this.bungee.getTranslation("server_went_down"));
        } else {
            this.con.disconnect(Util.exception(t));
        }
    }


    public void disconnected(ChannelWrapper channel) {
        this.server.getInfo().removePlayer(this.con);
        if (this.bungee.getReconnectHandler() != null) {
            this.bungee.getReconnectHandler().setServer(this.con);
        }

        if (!this.server.isObsolete()) {
            this.con.disconnect(this.bungee.getTranslation("lost_connection"));
        }

        ServerDisconnectEvent serverDisconnectEvent = new ServerDisconnectEvent(this.con, this.server.getInfo());
        this.bungee.getPluginManager().callEvent(serverDisconnectEvent);
    }

    public boolean shouldHandle(PacketWrapper packet) {
        return !this.server.isObsolete();
    }

    public void handle(PacketWrapper packet)
            throws Exception {
        this.con.getEntityRewrite().rewriteClientbound(packet.buf, this.con.getServerEntityId(), this.con.getClientEntityId(), this.con.getPendingConnection().getVersion());
        this.con.sendPacket(packet);
    }

    public void handle(KeepAlive alive)
            throws Exception {
        this.server.setSentPingId(alive.getRandomId());
        this.con.setSentPingTime(System.currentTimeMillis());
    }

    public void handle(net.md_5.bungee.protocol.packet.PlayerListItem playerList)
            throws Exception {
        this.con.getTabListHandler().onUpdate(TabList.rewrite(playerList));
        throw CancelSendSignal.INSTANCE;
    }

    public void handle(ScoreboardObjective objective)
            throws Exception {
        Scoreboard serverScoreboard = this.con.getServerSentScoreboard();
        switch (objective.getAction()) {
            case 0:
                serverScoreboard.addObjective(new Objective(objective.getName(), objective.getValue(), objective.getType().toString()));
                break;
            case 1:
                serverScoreboard.removeObjective(objective.getName());
                break;
            case 2:
                Objective oldObjective = serverScoreboard.getObjective(objective.getName());
                if (oldObjective != null) {
                    oldObjective.setValue(objective.getValue());
                    oldObjective.setType(objective.getType().toString());
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown objective action: " + objective.getAction());
        }
    }

    public void handle(ScoreboardScore score)
            throws Exception {
        Scoreboard serverScoreboard = this.con.getServerSentScoreboard();
        switch (score.getAction()) {
            case 0:
                Score s = new Score(score.getItemName(), score.getScoreName(), score.getValue());
                serverScoreboard.removeScore(score.getItemName());
                serverScoreboard.addScore(s);
                break;
            case 1:
                serverScoreboard.removeScore(score.getItemName());
                break;
            default:
                throw new IllegalArgumentException("Unknown scoreboard action: " + score.getAction());
        }
    }

    public void handle(ScoreboardDisplay displayScoreboard)
            throws Exception {
        Scoreboard serverScoreboard = this.con.getServerSentScoreboard();
        serverScoreboard.setName(displayScoreboard.getName());
        serverScoreboard.setPosition(net.md_5.bungee.api.score.Position.values()[displayScoreboard.getPosition()]);
    }

    public void handle(net.md_5.bungee.protocol.packet.Team team)
            throws Exception {
        Scoreboard serverScoreboard = this.con.getServerSentScoreboard();

        if (team.getMode() == 1) {
            serverScoreboard.removeTeam(team.getName());
            return;
        }


        net.md_5.bungee.api.score.Team t;

        if (team.getMode() == 0) {
            t = new net.md_5.bungee.api.score.Team(team.getName());
            serverScoreboard.addTeam(t);
        } else {
            t = serverScoreboard.getTeam(team.getName());
        }

        if (t != null) {
            if ((team.getMode() == 0) || (team.getMode() == 2)) {
                t.setDisplayName(team.getDisplayName());
                t.setPrefix(team.getPrefix());
                t.setSuffix(team.getSuffix());
                t.setFriendlyFire(team.getFriendlyFire());
                t.setNameTagVisibility(team.getNameTagVisibility());
                t.setCollisionRule(team.getCollisionRule());
                t.setColor(team.getColor());
            }
            if (team.getPlayers() != null) {
                for (String s : team.getPlayers()) {
                    if ((team.getMode() == 0) || (team.getMode() == 3)) {
                        t.addPlayer(s);
                    } else if (team.getMode() == 4) {
                        t.removePlayer(s);
                    }
                }
            }
        }
    }

    public void handle(PluginMessage pluginMessage)
            throws Exception {
        DataInput in = pluginMessage.getStream();
        PluginMessageEvent event = new PluginMessageEvent(this.server, this.con, pluginMessage.getTag(), pluginMessage.getData().clone());

        if (this.bungee.getPluginManager().callEvent(event).isCancelled()) {
            throw CancelSendSignal.INSTANCE;
        }

        if (pluginMessage.getTag().equals(this.con.getPendingConnection().getVersion() >= 393 ? "minecraft:brand" : "MC|Brand")) {
            ByteBuf brand = io.netty.buffer.Unpooled.wrappedBuffer(pluginMessage.getData());
            String serverBrand = DefinedPacket.readString(brand);
            brand.release();

            com.google.common.base.Preconditions.checkState(!serverBrand.contains(this.bungee.getName()), "Cannot connect proxy to itself!");

            brand = ByteBufAllocator.DEFAULT.heapBuffer();
            DefinedPacket.writeString(this.bungee.getName() + " (" + this.bungee.getVersion() + ") <- " + serverBrand, brand);
            pluginMessage.setData(DefinedPacket.toArray(brand));
            brand.release();

            this.con.unsafe().sendPacket(pluginMessage);
            throw CancelSendSignal.INSTANCE;
        }

        if (pluginMessage.getTag().equals("BungeeCord")) {
            ByteArrayDataOutput out = com.google.common.io.ByteStreams.newDataOutput();
            String subChannel = in.readUTF();

            if (subChannel.equals("ForwardToPlayer")) {
                ProxiedPlayer target = this.bungee.getPlayer(in.readUTF());
                if (target != null) {

                    String channel = in.readUTF();
                    short len = in.readShort();
                    byte[] data = new byte[len];
                    in.readFully(data);


                    out.writeUTF(channel);
                    out.writeShort(data.length);
                    out.write(data);
                    byte[] payload = out.toByteArray();

                    target.getServer().sendData("BungeeCord", payload);
                }


                out = null;
            }
            short len;
            if (subChannel.equals("Forward")) {

                String target = in.readUTF();
                String channel = in.readUTF();
                len = in.readShort();
                byte[] data = new byte[len];
                in.readFully(data);


                out.writeUTF(channel);
                out.writeShort(data.length);
                out.write(data);
                byte[] payload = out.toByteArray();


                out = null;

                if (target.equals("ALL")) {
                    for (ServerInfo server : this.bungee.getServers().values()) {
                        if (server != this.server.getInfo()) {
                            server.sendData("BungeeCord", payload);
                        }
                    }
                } else if (target.equals("ONLINE")) {
                    for (ServerInfo server : this.bungee.getServers().values()) {
                        if (server != this.server.getInfo()) {
                            server.sendData("BungeeCord", payload, false);
                        }
                    }
                } else {
                    ServerInfo server = this.bungee.getServerInfo(target);
                    if (server != null) {
                        server.sendData("BungeeCord", payload);
                    }
                }
            }
            if (subChannel.equals("Connect")) {
                ServerInfo server = this.bungee.getServerInfo(in.readUTF());
                if (server != null) {
                    this.con.connect(server, ServerConnectEvent.Reason.PLUGIN_MESSAGE);
                }
            }
            if (subChannel.equals("ConnectOther")) {
                ProxiedPlayer player = this.bungee.getPlayer(in.readUTF());
                if (player != null) {
                    ServerInfo server = this.bungee.getServerInfo(in.readUTF());
                    if (server != null) {
                        player.connect(server);
                    }
                }
            }
            if (subChannel.equals("IP")) {
                out.writeUTF("IP");
                out.writeUTF(this.con.getAddress().getHostString());
                out.writeInt(this.con.getAddress().getPort());
            }
            if (subChannel.equals("PlayerCount")) {
                String target = in.readUTF();
                out.writeUTF("PlayerCount");
                if (target.equals("ALL")) {
                    out.writeUTF("ALL");
                    out.writeInt(this.bungee.getOnlineCount());
                } else {
                    ServerInfo server = this.bungee.getServerInfo(target);
                    if (server != null) {
                        out.writeUTF(server.getName());
                        out.writeInt(server.getPlayers().size());
                    }
                }
            }
            if (subChannel.equals("PlayerList")) {
                String target = in.readUTF();
                out.writeUTF("PlayerList");
                if (target.equals("ALL")) {
                    out.writeUTF("ALL");
                    out.writeUTF(Util.csv(this.bungee.getPlayers()));
                } else {
                    ServerInfo server = this.bungee.getServerInfo(target);
                    if (server != null) {
                        out.writeUTF(server.getName());
                        out.writeUTF(Util.csv(server.getPlayers()));
                    }
                }
            }
            if (subChannel.equals("GetServers")) {
                out.writeUTF("GetServers");
                out.writeUTF(Util.csv(this.bungee.getServers().keySet()));
            }
            if (subChannel.equals("Message")) {
                String target = in.readUTF();
                String message = in.readUTF();
                if (target.equals("ALL")) {
                    for (ProxiedPlayer player : this.bungee.getPlayers()) {
                        player.sendMessage(message);
                    }
                } else {
                    ProxiedPlayer player = this.bungee.getPlayer(target);
                    if (player != null) {
                        player.sendMessage(message);
                    }
                }
            }
            if (subChannel.equals("GetServer")) {
                out.writeUTF("GetServer");
                out.writeUTF(this.server.getInfo().getName());
            }
            if (subChannel.equals("UUID")) {
                out.writeUTF("UUID");
                out.writeUTF(this.con.getUUID());
            }
            if (subChannel.equals("UUIDOther")) {
                ProxiedPlayer player = this.bungee.getPlayer(in.readUTF());
                if (player != null) {
                    out.writeUTF("UUIDOther");
                    out.writeUTF(player.getName());
                    out.writeUTF(player.getUUID());
                }
            }
            if (subChannel.equals("ServerIP")) {
                ServerInfo info = this.bungee.getServerInfo(in.readUTF());
                if ((info != null) && (!info.getAddress().isUnresolved())) {
                    out.writeUTF("ServerIP");
                    out.writeUTF(info.getName());
                    out.writeUTF(info.getAddress().getAddress().getHostAddress());
                    out.writeShort(info.getAddress().getPort());
                }
            }
            if (subChannel.equals("KickPlayer")) {
                ProxiedPlayer player = this.bungee.getPlayer(in.readUTF());
                if (player != null) {
                    String kickReason = in.readUTF();
                    player.disconnect(new net.md_5.bungee.api.chat.TextComponent(kickReason));
                }
            }


            if (out != null) {
                byte[] b = out.toByteArray();
                if (b.length != 0) {
                    this.server.sendData("BungeeCord", b);
                }
            }

            throw CancelSendSignal.INSTANCE;
        }
    }

    public void handle(Kick kick)
            throws Exception {
        ServerInfo def = this.con.updateAndGetNextServer(this.server.getInfo());
        ServerKickEvent event = this.bungee.getPluginManager().callEvent(new ServerKickEvent(this.con, this.server.getInfo(), net.md_5.bungee.chat.ComponentSerializer.parse(kick.getMessage()), def, ServerKickEvent.State.CONNECTED));
        if ((event.isCancelled()) && (event.getCancelServer() != null)) {
            this.con.connectNow(event.getCancelServer(), ServerConnectEvent.Reason.KICK_REDIRECT);
        } else {
            this.con.disconnect0(event.getKickReasonComponent());
        }
        this.server.setObsolete(true);
        throw CancelSendSignal.INSTANCE;
    }

    public void handle(SetCompression setCompression)
            throws Exception {
        this.server.getCh().setCompressionThreshold(setCompression.getThreshold());
    }

    public void handle(TabCompleteResponse tabCompleteResponse)
            throws Exception {
        List<String> commands = tabCompleteResponse.getCommands();
        if (commands == null) {
            commands = tabCompleteResponse.getSuggestions().getList().stream().map(Suggestion::getText).collect(Collectors.toList());
        }

        TabCompleteResponseEvent tabCompleteResponseEvent = new TabCompleteResponseEvent(this.server, this.con, new ArrayList<>(commands));
        if (!this.bungee.getPluginManager().callEvent(tabCompleteResponseEvent).isCancelled()) {

            if (!commands.equals(tabCompleteResponseEvent.getSuggestions())) {
                if (tabCompleteResponse.getCommands() != null) {

                    tabCompleteResponse.setCommands(tabCompleteResponseEvent.getSuggestions());
                } else {
                    final StringRange range = tabCompleteResponse.getSuggestions().getRange();
                    tabCompleteResponse.setSuggestions(new Suggestions(range, tabCompleteResponseEvent.getSuggestions().stream().map(input -> new Suggestion(range, input)).collect(Collectors.toList())));
                }
            }

            this.con.unsafe().sendPacket(tabCompleteResponse);
        }

        throw CancelSendSignal.INSTANCE;
    }


    public void handle(BossBar bossBar) {
        switch (bossBar.getAction()) {

            case 0:
                this.con.getSentBossBars().add(bossBar.getUuid());
                break;

            case 1:
                this.con.getSentBossBars().remove(bossBar.getUuid());
        }

    }


    public void handle(Respawn respawn) {
        this.con.setDimension(respawn.getDimension());
    }

    @Override
    public void handle(Commands commands) throws Exception {
        boolean modified = false;

        for (Map.Entry<String, Command> command : bungee.getPluginManager().getCommands()) {
            if (!bungee.getDisabledCommands().contains(command.getKey()) && commands.getRoot().getChild(command.getKey()) == null && command.getValue().hasPermission(con)) {
                LiteralCommandNode dummy = LiteralArgumentBuilder.literal(command.getKey())
                        .then(RequiredArgumentBuilder.argument("args", StringArgumentType.greedyString())
                                .suggests(Commands.SuggestionRegistry.ASK_SERVER))
                        .build();
                commands.getRoot().addChild(dummy);

                modified = true;
            }
        }

        if (modified) {
            con.unsafe().sendPacket(commands);
            throw CancelSendSignal.INSTANCE;
        }
    }


    public String toString() {
        return "[" + this.con.getName() + "] <-> DownstreamBridge <-> [" + this.server.getInfo().getName() + "]";
    }
}


