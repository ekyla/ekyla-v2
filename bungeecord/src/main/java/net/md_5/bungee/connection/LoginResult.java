/*    */ package net.md_5.bungee.connection;
/*    */ 
/*    */ import java.util.Arrays;
/*    */ 
/*    */ public class LoginResult {
/*  6 */   public void setId(String id) { this.id = id; } public void setName(String name) { this.name = name; } public void setProperties(Property[] properties) { this.properties = properties; } public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof LoginResult)) return false; LoginResult other = (LoginResult)o; if (!other.canEqual(this)) return false; Object this$id = getId();Object other$id = other.getId(); if (this$id == null ? other$id != null : !this$id.equals(other$id)) return false; Object this$name = getName();Object other$name = other.getName(); if (this$name == null ? other$name != null : !this$name.equals(other$name)) return false; return Arrays.deepEquals(getProperties(), other.getProperties()); } protected boolean canEqual(Object other) { return other instanceof LoginResult; } public int hashCode() { int PRIME = 59;int result = 1;Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $name = getName();result = result * 59 + ($name == null ? 43 : $name.hashCode());result = result * 59 + Arrays.deepHashCode(getProperties());return result; } public String toString() { return "LoginResult(id=" + getId() + ", name=" + getName() + ", properties=" + Arrays.deepToString(getProperties()) + ")"; }
/*  7 */   public LoginResult(String id, String name, Property[] properties) { this.id = id;this.name = name;this.properties = properties; }
/*    */   
/*    */   private String id;
/*    */   
/* 11 */   public String getId() { return this.id; }
/* 12 */   public String getName() { return this.name; }
/* 13 */   public Property[] getProperties() { return this.properties; }
/*    */   
/* 15 */   public static class Property { public void setName(String name) { this.name = name; } public void setValue(String value) { this.value = value; } public void setSignature(String signature) { this.signature = signature; } public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof Property)) return false; Property other = (Property)o; if (!other.canEqual(this)) return false; Object this$name = getName();Object other$name = other.getName(); if (this$name == null ? other$name != null : !this$name.equals(other$name)) return false; Object this$value = getValue();Object other$value = other.getValue(); if (this$value == null ? other$value != null : !this$value.equals(other$value)) return false; Object this$signature = getSignature();Object other$signature = other.getSignature();return this$signature == null ? other$signature == null : this$signature.equals(other$signature); } protected boolean canEqual(Object other) { return other instanceof Property; } public int hashCode() { int PRIME = 59;int result = 1;Object $name = getName();result = result * 59 + ($name == null ? 43 : $name.hashCode());Object $value = getValue();result = result * 59 + ($value == null ? 43 : $value.hashCode());Object $signature = getSignature();result = result * 59 + ($signature == null ? 43 : $signature.hashCode());return result; } public String toString() { return "LoginResult.Property(name=" + getName() + ", value=" + getValue() + ", signature=" + getSignature() + ")"; }
/* 16 */     public Property(String name, String value, String signature) { this.name = name;this.value = value;this.signature = signature; }
/*    */     
/*    */     private String name;
/*    */     
/* 20 */     public String getName() { return this.name; }
/* 21 */     public String getValue() { return this.value; }
/* 22 */     public String getSignature() { return this.signature; }
/*    */     
/*    */     private String value;
/*    */     private String signature;
/*    */   }
/*    */   
/*    */   private String name;
/*    */   private Property[] properties;
/*    */ }


