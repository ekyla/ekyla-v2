/*     */ package net.md_5.bungee.connection;
/*     */ 
/*     */

import com.google.common.base.Preconditions;
import com.mojang.brigadier.context.StringRange;
import com.mojang.brigadier.suggestion.Suggestion;
import com.mojang.brigadier.suggestion.Suggestions;
import io.netty.channel.Channel;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.UserConnection;
import net.md_5.bungee.Util;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.*;
import net.md_5.bungee.netty.ChannelWrapper;
import net.md_5.bungee.netty.PacketHandler;
import net.md_5.bungee.protocol.PacketWrapper;
import net.md_5.bungee.protocol.packet.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */ 
/*     */ public class UpstreamBridge extends PacketHandler
/*     */ {
/*     */   private final ProxyServer bungee;
/*     */   private final UserConnection con;
/*     */   
/*     */   public UpstreamBridge(ProxyServer bungee, UserConnection con)
/*     */   {
/*  42 */     this.bungee = bungee;
/*  43 */     this.con = con;
/*     */     
/*  45 */     BungeeCord.getInstance().addConnection(con);
/*  46 */     con.getTabListHandler().onConnect();
/*  47 */     con.unsafe().sendPacket(BungeeCord.getInstance().registerChannels(con.getPendingConnection().getVersion()));
/*     */   }
/*     */   
/*     */   public void exception(Throwable t)
/*     */     throws Exception
/*     */   {
/*  53 */     this.con.disconnect(Util.exception(t));
/*     */   }
/*     */   
/*     */ 
/*     */   public void disconnected(ChannelWrapper channel)
/*     */     throws Exception
/*     */   {
/*  60 */     PlayerDisconnectEvent event = new PlayerDisconnectEvent(this.con);
/*  61 */     this.bungee.getPluginManager().callEvent(event);
/*  62 */     this.con.getTabListHandler().onDisconnect();
/*  63 */     BungeeCord.getInstance().removeConnection(this.con);
/*     */     
/*  65 */     if (this.con.getServer() != null)
/*     */     {
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  73 */       PlayerListItem packet = new PlayerListItem();
/*  74 */       packet.setAction(PlayerListItem.Action.REMOVE_PLAYER);
/*  75 */       PlayerListItem.Item item = new PlayerListItem.Item();
/*  76 */       item.setUuid(this.con.getUniqueId());
/*  77 */       packet.setItems(new PlayerListItem.Item[] { item });
/*     */       
/*     */ 
/*     */ 
/*  81 */       for (ProxiedPlayer player : this.con.getServer().getInfo().getPlayers())
/*     */       {
/*  83 */         player.unsafe().sendPacket(packet);
/*     */       }
/*  85 */       this.con.getServer().disconnect("Quitting");
/*     */     }
/*     */   }
/*     */   
/*     */   public void writabilityChanged(ChannelWrapper channel)
/*     */     throws Exception
/*     */   {
/*  92 */     if (this.con.getServer() != null)
/*     */     {
/*  94 */       Channel server = this.con.getServer().getCh().getHandle();
/*  95 */       if (channel.getHandle().isWritable())
/*     */       {
/*  97 */         server.config().setAutoRead(true);
/*     */       }
/*     */       else {
/* 100 */         server.config().setAutoRead(false);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public boolean shouldHandle(PacketWrapper packet)
/*     */     throws Exception
/*     */   {
/* 108 */     return (this.con.getServer() != null) || ((packet.packet instanceof PluginMessage));
/*     */   }
/*     */   
/*     */   public void handle(PacketWrapper packet)
/*     */     throws Exception
/*     */   {
/* 114 */     if (this.con.getServer() != null)
/*     */     {
/* 116 */       this.con.getEntityRewrite().rewriteServerbound(packet.buf, this.con.getClientEntityId(), this.con.getServerEntityId(), this.con.getPendingConnection().getVersion());
/* 117 */       this.con.getServer().getCh().write(packet);
/*     */     }
/*     */   }
/*     */   
/*     */   public void handle(KeepAlive alive)
/*     */     throws Exception
/*     */   {
/* 124 */     if (alive.getRandomId() == this.con.getServer().getSentPingId())
/*     */     {
/* 126 */       int newPing = (int)(System.currentTimeMillis() - this.con.getSentPingTime());
/* 127 */       this.con.getTabListHandler().onPingChange(newPing);
/* 128 */       this.con.setPing(newPing);
/*     */     }
/*     */     else {
/* 131 */       throw CancelSendSignal.INSTANCE;
/*     */     }
/*     */   }
/*     */   
/*     */   public void handle(Chat chat)
/*     */     throws Exception
/*     */   {
/* 138 */     int maxLength = this.con.getPendingConnection().getVersion() >= 315 ? 256 : 100;
/* 139 */     Preconditions.checkArgument(chat.getMessage().length() <= maxLength, "Chat message too long");
/*     */     
/* 141 */     ChatEvent chatEvent = new ChatEvent(this.con, this.con.getServer(), chat.getMessage());
/* 142 */     if (!((ChatEvent)this.bungee.getPluginManager().callEvent(chatEvent)).isCancelled())
/*     */     {
/* 144 */       chat.setMessage(chatEvent.getMessage());
/* 145 */       if ((!chatEvent.isCommand()) || (!this.bungee.getPluginManager().dispatchCommand(this.con, chat.getMessage().substring(1))))
/*     */       {
/* 147 */         this.con.getServer().unsafe().sendPacket(chat);
/*     */       }
/*     */     }
/* 150 */     throw CancelSendSignal.INSTANCE;
/*     */   }
/*     */   
/*     */   public void handle(TabCompleteRequest tabComplete)
/*     */     throws Exception
/*     */   {
/* 156 */     List<String> suggestions = new ArrayList();
/*     */     
/* 158 */     if (tabComplete.getCursor().startsWith("/"))
/*     */     {
/* 160 */       this.bungee.getPluginManager().dispatchCommand(this.con, tabComplete.getCursor().substring(1), suggestions);
/*     */     }
/*     */     
/* 163 */     TabCompleteEvent tabCompleteEvent = new TabCompleteEvent(this.con, this.con.getServer(), tabComplete.getCursor(), suggestions);
/* 164 */     this.bungee.getPluginManager().callEvent(tabCompleteEvent);
/*     */     
/* 166 */     if (tabCompleteEvent.isCancelled())
/*     */     {
/* 168 */       throw CancelSendSignal.INSTANCE;
/*     */     }
/*     */     
/* 171 */     List<String> results = tabCompleteEvent.getSuggestions();
/* 172 */     if (!results.isEmpty())
/*     */     {
/*     */ 
/*     */ 
/* 176 */       if (this.con.getPendingConnection().getVersion() < 393)
/*     */       {
/* 178 */         this.con.unsafe().sendPacket(new TabCompleteResponse(results));
/*     */       }
/*     */       else {
/* 181 */         int start = tabComplete.getCursor().lastIndexOf(' ') + 1;
/* 182 */         int end = tabComplete.getCursor().length();
/* 183 */         StringRange range = StringRange.between(start, end);
/*     */         
/* 185 */         List<Suggestion> brigadier = new LinkedList();
/* 186 */         for (String s : results)
/*     */         {
/* 188 */           brigadier.add(new Suggestion(range, s));
/*     */         }
/*     */         
/* 191 */         this.con.unsafe().sendPacket(new TabCompleteResponse(tabComplete.getTransactionId(), new Suggestions(range, brigadier)));
/*     */       }
/* 193 */       throw CancelSendSignal.INSTANCE;
/*     */     }
/*     */   }
/*     */   
/*     */   public void handle(ClientSettings settings)
/*     */     throws Exception
/*     */   {
/* 200 */     this.con.setSettings(settings);
/*     */     
/* 202 */     SettingsChangedEvent settingsEvent = new SettingsChangedEvent(this.con);
/* 203 */     this.bungee.getPluginManager().callEvent(settingsEvent);
/*     */   }
/*     */   
/*     */   public void handle(PluginMessage pluginMessage)
/*     */     throws Exception
/*     */   {
/* 209 */     if (pluginMessage.getTag().equals("BungeeCord"))
/*     */     {
/* 211 */       throw CancelSendSignal.INSTANCE;
/*     */     }
/*     */     
/* 214 */     if (BungeeCord.getInstance().config.isForgeSupport())
/*     */     {
/*     */ 
/* 217 */       if ((pluginMessage.getTag().equals("FML")) && (pluginMessage.getStream().readUnsignedByte() == 1))
/*     */       {
/* 219 */         throw CancelSendSignal.INSTANCE;
/*     */       }
/*     */       
/*     */ 
/* 223 */       if (pluginMessage.getTag().equals("FML|HS"))
/*     */       {
/*     */ 
/* 226 */         this.con.getForgeClientHandler().handle(pluginMessage);
/* 227 */         throw CancelSendSignal.INSTANCE;
/*     */       }
/*     */       
/* 230 */       if ((this.con.getServer() != null) && (!this.con.getServer().isForgeServer()) && (pluginMessage.getData().length > 32767))
/*     */       {
/*     */ 
/*     */ 
/* 234 */         throw CancelSendSignal.INSTANCE;
/*     */       }
/*     */     }
/*     */     
/* 238 */     PluginMessageEvent event = new PluginMessageEvent(this.con, this.con.getServer(), pluginMessage.getTag(), (byte[])pluginMessage.getData().clone());
/* 239 */     if (((PluginMessageEvent)this.bungee.getPluginManager().callEvent(event)).isCancelled())
/*     */     {
/* 241 */       throw CancelSendSignal.INSTANCE;
/*     */     }
/*     */     
/*     */ 
/* 245 */     if (PluginMessage.SHOULD_RELAY.test(pluginMessage))
/*     */     {
/* 247 */       this.con.getPendingConnection().getRelayMessages().add(pluginMessage);
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   public String toString()
/*     */   {
/* 254 */     return "[" + this.con.getName() + "] -> UpstreamBridge";
/*     */   }
/*     */ }


