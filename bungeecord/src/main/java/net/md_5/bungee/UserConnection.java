package net.md_5.bungee;

import com.google.common.base.Preconditions;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelInitializer;
import lombok.NonNull;
import net.md_5.bungee.api.*;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.Connection;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PermissionCheckEvent;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.score.Scoreboard;
import net.md_5.bungee.chat.ComponentSerializer;
import net.md_5.bungee.connection.InitialHandler;
import net.md_5.bungee.entitymap.EntityMap;
import net.md_5.bungee.forge.ForgeClientHandler;
import net.md_5.bungee.forge.ForgeServerHandler;
import net.md_5.bungee.netty.ChannelWrapper;
import net.md_5.bungee.netty.HandlerBoss;
import net.md_5.bungee.netty.PipelineUtils;
import net.md_5.bungee.protocol.DefinedPacket;
import net.md_5.bungee.protocol.Protocol;
import net.md_5.bungee.protocol.packet.ClientSettings;
import net.md_5.bungee.protocol.packet.PlayerListHeaderFooter;
import net.md_5.bungee.tab.TabList;
import net.md_5.bungee.util.CaseInsensitiveSet;
import net.md_5.bungee.util.ChatComponentTransformer;

import java.net.InetSocketAddress;
import java.util.*;

public final class UserConnection implements ProxiedPlayer {

    @NonNull
    private final ProxyServer bungee;
    @NonNull
    private final ChannelWrapper ch;
    @NonNull
    private final String name;
    private final InitialHandler pendingConnection;
    private final Collection<ServerInfo> pendingConnects = new HashSet<>();
    private final Collection<String> groups = new CaseInsensitiveSet();
    private final Collection<String> permissions = new CaseInsensitiveSet();
    private final Scoreboard serverSentScoreboard = new Scoreboard();
    private final Collection<UUID> sentBossBars = new HashSet<>();
    private final Connection.Unsafe unsafe = new Unsafe() {
        @Override
        public void sendPacket(DefinedPacket paramDefinedPacket) {
            ch.write(paramDefinedPacket);
        }
    };
    private ServerConnection server;
    private int dimension;
    private boolean dimensionChange = true;
    private long sentPingTime;
    private int ping = 100;
    private ServerInfo reconnectServer;
    private TabList tabListHandler;
    private int gamemode;
    private int compressionThreshold = -1;
    private Queue<String> serverJoinQueue;
    private int clientEntityId;
    private int serverEntityId;
    private ClientSettings settings;
    private String displayName;
    private EntityMap entityRewrite;
    private Locale locale;
    private ForgeClientHandler forgeClientHandler;
    private ForgeServerHandler forgeServerHandler;

    public UserConnection(@NonNull ProxyServer bungee, @NonNull ChannelWrapper ch, @NonNull String name, InitialHandler pendingConnection) {
        if (bungee == null) throw new NullPointerException("bungee is marked @NonNull but is null");
        if (ch == null) throw new NullPointerException("ch is marked @NonNull but is null");
        if (name == null) throw new NullPointerException("name is marked @NonNull but is null");
        this.ch = ch;
        this.bungee = bungee;
        this.name = name;
        this.pendingConnection = pendingConnection;
    }


    @NonNull
    public String getName() {
        return this.name;
    }

    public InitialHandler getPendingConnection() {
        return this.pendingConnection;
    }

    public ServerConnection getServer() {
        return this.server;
    }

    public void setServer(ServerConnection server) {
        this.server = server;
    }

    public int getDimension() {
        return this.dimension;
    }

    public void setDimension(int dimension) {
        this.dimension = dimension;
    }

    public boolean isDimensionChange() {
        return this.dimensionChange;
    }

    public void setDimensionChange(boolean dimensionChange) {
        this.dimensionChange = dimensionChange;
    }

    public Collection<ServerInfo> getPendingConnects() {
        return this.pendingConnects;
    }

    public long getSentPingTime() {
        return this.sentPingTime;
    }

    public void setSentPingTime(long sentPingTime) {
        this.sentPingTime = sentPingTime;
    }

    public int getPing() {
        return this.ping;
    }

    public void setPing(int ping) {
        this.ping = ping;
    }

    public ServerInfo getReconnectServer() {
        return this.reconnectServer;
    }

    public void setReconnectServer(ServerInfo reconnectServer) {
        this.reconnectServer = reconnectServer;
    }

    public TabList getTabListHandler() {
        return this.tabListHandler;
    }

    public int getGamemode() {
        return this.gamemode;
    }

    public void setGamemode(int gamemode) {
        this.gamemode = gamemode;
    }

    public int getCompressionThreshold() {
        return this.compressionThreshold;
    }

    public void setCompressionThreshold(int compressionThreshold) {
        if ((!this.ch.isClosing()) && (this.compressionThreshold == -1) && (compressionThreshold >= 0)) {
            this.compressionThreshold = compressionThreshold;
            this.unsafe.sendPacket(new net.md_5.bungee.protocol.packet.SetCompression(compressionThreshold));
            this.ch.setCompressionThreshold(compressionThreshold);
        }
    }

    public void setServerJoinQueue(Queue<String> serverJoinQueue) {
        this.serverJoinQueue = serverJoinQueue;
    }

    public int getClientEntityId() {
        return this.clientEntityId;
    }

    public void setClientEntityId(int clientEntityId) {
        this.clientEntityId = clientEntityId;
    }

    public int getServerEntityId() {
        return this.serverEntityId;
    }

    public void setServerEntityId(int serverEntityId) {
        this.serverEntityId = serverEntityId;
    }

    public ClientSettings getSettings() {
        return this.settings;
    }

    public void setSettings(ClientSettings settings) {
        this.settings = settings;
        this.locale = null;
    }

    public Scoreboard getServerSentScoreboard() {
        return this.serverSentScoreboard;
    }

    public Collection<UUID> getSentBossBars() {
        return this.sentBossBars;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public void setDisplayName(String name) {
        Preconditions.checkNotNull(name, "displayName");
        this.displayName = name;
    }

    public EntityMap getEntityRewrite() {
        return this.entityRewrite;
    }

    public ForgeClientHandler getForgeClientHandler() {
        return this.forgeClientHandler;
    }

    public void setForgeClientHandler(ForgeClientHandler forgeClientHandler) {
        this.forgeClientHandler = forgeClientHandler;
    }

    public ForgeServerHandler getForgeServerHandler() {
        return this.forgeServerHandler;
    }

    public void setForgeServerHandler(ForgeServerHandler forgeServerHandler) {
        this.forgeServerHandler = forgeServerHandler;
    }

    public void init() {
        this.entityRewrite = EntityMap.getEntityMap(getPendingConnection().getVersion());

        this.displayName = this.name;

        this.tabListHandler = new net.md_5.bungee.tab.ServerUnique(this);

        Collection<String> g = this.bungee.getConfigurationAdapter().getGroups(this.name);
        g.addAll(this.bungee.getConfigurationAdapter().getGroups(getUniqueId().toString()));
        for (String s : g) {
            addGroups(s);
        }

        this.forgeClientHandler = new ForgeClientHandler(this);


        this.forgeClientHandler.setFmlTokenInHandshake(getPendingConnection().getExtraDataInHandshake().contains("\000FML\000"));
    }

    public void sendPacket(net.md_5.bungee.protocol.PacketWrapper packet) {
        this.ch.write(packet);
    }

    @Deprecated
    public boolean isActive() {
        return !this.ch.isClosed();
    }

    @Override
    public void connect(ServerInfo target) {
        connect(target, null, ServerConnectEvent.Reason.PLUGIN);
    }

    @Override
    public void connect(ServerInfo target, ServerConnectEvent.Reason reason) {
        connect(target, null, false, reason);
    }

    @Override
    public void connect(ServerInfo target, Callback<Boolean> callback) {
        connect(target, callback, false, ServerConnectEvent.Reason.PLUGIN);
    }

    public void connect(ServerInfo target, Callback<Boolean> callback, ServerConnectEvent.Reason reason) {
        connect(target, callback, false, reason);
    }

    @Deprecated
    public void connectNow(ServerInfo target) {
        connectNow(target, ServerConnectEvent.Reason.UNKNOWN);
    }

    public void connectNow(ServerInfo target, ServerConnectEvent.Reason reason) {
        this.dimensionChange = true;
        connect(target, reason);
    }

    public ServerInfo updateAndGetNextServer(ServerInfo currentTarget) {
        if (this.serverJoinQueue == null) {
            this.serverJoinQueue = new LinkedList<>(getPendingConnection().getListener().getServerPriority());
        }

        ServerInfo next = null;
        while (!this.serverJoinQueue.isEmpty()) {
            ServerInfo candidate = ProxyServer.getInstance().getServerInfo(this.serverJoinQueue.remove());
            if (!java.util.Objects.equals(currentTarget, candidate)) {
                next = candidate;
                break;
            }
        }

        return next;
    }

    public void connect(ServerInfo info, Callback<Boolean> callback, boolean retry) {
        connect(info, callback, retry, ServerConnectEvent.Reason.PLUGIN);
    }

    public void connect(ServerInfo info, final Callback<Boolean> callback, boolean retry, ServerConnectEvent.Reason reason) {
        Preconditions.checkNotNull(info, "info");

        ServerConnectRequest.Builder builder = ServerConnectRequest.builder().retry(retry).reason(reason).target(info);
        if (callback != null) {

            builder.callback((result, error) -> callback.done(result == ServerConnectRequest.Result.SUCCESS ? Boolean.TRUE : Boolean.FALSE, error));
        }

        connect(builder.build());
    }

    public void connect(final ServerConnectRequest request) {
        Preconditions.checkNotNull(request, "request");

        final Callback<ServerConnectRequest.Result> callback = request.getCallback();
        ServerConnectEvent event = new ServerConnectEvent(this, request.getTarget(), request.getReason());
        if (this.bungee.getPluginManager().callEvent(event).isCancelled()) {
            if (callback != null) {
                callback.done(ServerConnectRequest.Result.EVENT_CANCEL, null);
            }

            if ((getServer() == null) && (!this.ch.isClosing())) {
                throw new IllegalStateException("Cancelled ServerConnectEvent with no server or disconnect.");
            }
            return;
        }

        final BungeeServerInfo target = (BungeeServerInfo) event.getTarget();

        if ((getServer() != null) && (java.util.Objects.equals(getServer().getInfo(), target))) {
            if (callback != null) {
                callback.done(ServerConnectRequest.Result.ALREADY_CONNECTED, null);
            }

            sendMessage(this.bungee.getTranslation("already_connected"));
            return;
        }
        if (this.pendingConnects.contains(target)) {
            if (callback != null) {
                callback.done(ServerConnectRequest.Result.ALREADY_CONNECTING, null);
            }

            sendMessage(this.bungee.getTranslation("already_connecting"));
            return;
        }

        this.pendingConnects.add(target);

        ChannelInitializer initializer = new ChannelInitializer() {
            protected void initChannel(Channel ch) {
                PipelineUtils.BASE.initChannel(ch);
                ch.pipeline().addAfter("frame-decoder", "packet-decoder", new net.md_5.bungee.protocol.MinecraftDecoder(Protocol.HANDSHAKE, false, UserConnection.this.getPendingConnection().getVersion()));
                ch.pipeline().addAfter("frame-prepender", "packet-encoder", new net.md_5.bungee.protocol.MinecraftEncoder(Protocol.HANDSHAKE, false, UserConnection.this.getPendingConnection().getVersion()));
                ch.pipeline().get(HandlerBoss.class).setHandler(new ServerConnector(UserConnection.this.bungee, UserConnection.this, target));
            }
        };
        ChannelFutureListener listener = future -> {
            if (callback != null) {
                callback.done(future.isSuccess() ? ServerConnectRequest.Result.SUCCESS : ServerConnectRequest.Result.FAIL, future.cause());
            }

            if (!future.isSuccess()) {
                future.channel().close();
                UserConnection.this.pendingConnects.remove(target);

                ServerInfo def = UserConnection.this.updateAndGetNextServer(target);
                if ((request.isRetry()) && (def != null) && ((UserConnection.this.getServer() == null) || (def != UserConnection.this.getServer().getInfo()))) {
                    UserConnection.this.sendMessage(UserConnection.this.bungee.getTranslation("fallback_lobby"));
                    UserConnection.this.connect(def, null, true, ServerConnectEvent.Reason.LOBBY_FALLBACK);
                } else if (UserConnection.this.dimensionChange) {
                    UserConnection.this.disconnect(UserConnection.this.bungee.getTranslation("fallback_kick", future.cause().getClass().getName()));
                } else {
                    UserConnection.this.sendMessage(UserConnection.this.bungee.getTranslation("fallback_kick", future.cause().getClass().getName()));

                }

            }

        };
        Bootstrap b = new Bootstrap().channel(PipelineUtils.getChannel()).group(this.ch.getHandle().eventLoop()).handler(initializer).option(io.netty.channel.ChannelOption.CONNECT_TIMEOUT_MILLIS, request.getConnectTimeout()).remoteAddress(target.getAddress());

        if ((getPendingConnection().getListener().isSetLocalAddress()) && (!io.netty.util.internal.PlatformDependent.isWindows())) {
            b.localAddress(getPendingConnection().getListener().getHost().getHostString(), 0);
        }
        b.connect().addListener(listener);
    }

    public void disconnect(String reason) {
        disconnect0(TextComponent.fromLegacyText(reason));
    }

    public void disconnect(BaseComponent... reason) {
        disconnect0(reason);
    }

    public void disconnect(BaseComponent reason) {
        disconnect0(reason);
    }

    public void disconnect0(BaseComponent... reason) {
        if (!this.ch.isClosing()) {
            this.bungee.getLogger().log(java.util.logging.Level.INFO, "[{0}] disconnected with: {1}", new Object[]{

                    getName(), BaseComponent.toLegacyText(reason)});


            this.ch.delayedClose(new net.md_5.bungee.protocol.packet.Kick(ComponentSerializer.toString(reason)));

            if (this.server != null) {
                this.server.setObsolete(true);
                this.server.disconnect("Quitting");
            }
        }
    }

    public void chat(String message) {
        Preconditions.checkState(this.server != null, "Not connected to server");
        this.server.getCh().write(new net.md_5.bungee.protocol.packet.Chat(message));
    }

    public void sendMessage(String message) {
        sendMessage(TextComponent.fromLegacyText(message));
    }

    public void sendMessages(String... messages) {
        for (String message : messages) {
            sendMessage(message);
        }
    }

    public void sendMessage(BaseComponent... message) {
        sendMessage(ChatMessageType.SYSTEM, message);
    }

    public void sendMessage(BaseComponent message) {
        sendMessage(ChatMessageType.SYSTEM, message);
    }

    private void sendMessage(ChatMessageType position, String message) {
        unsafe().sendPacket(new net.md_5.bungee.protocol.packet.Chat(message, (byte) position.ordinal()));
    }

    public void sendMessage(ChatMessageType position, BaseComponent... message) {
        message = ChatComponentTransformer.getInstance().transform(this, message);


        if (position == ChatMessageType.ACTION_BAR) {
            sendMessage(position, ComponentSerializer.toString(new TextComponent(BaseComponent.toLegacyText(message))));
        } else {
            sendMessage(position, ComponentSerializer.toString(message));
        }
    }

    public void sendMessage(ChatMessageType position, BaseComponent message) {
        message = ChatComponentTransformer.getInstance().transform(this, message)[0];


        if (position == ChatMessageType.ACTION_BAR) {
            sendMessage(position, ComponentSerializer.toString(new TextComponent(BaseComponent.toLegacyText(message))));
        } else {
            sendMessage(position, ComponentSerializer.toString(message));
        }
    }

    public void sendData(String channel, byte[] data) {
        unsafe().sendPacket(new net.md_5.bungee.protocol.packet.PluginMessage(channel, data, this.forgeClientHandler.isForgeUser()));
    }

    public InetSocketAddress getAddress() {
        return this.ch.getRemoteAddress();
    }

    public Collection<String> getGroups() {
        return Collections.unmodifiableCollection(this.groups);
    }

    public void addGroups(String... groups) {
        for (String group : groups) {
            this.groups.add(group);
            for (String permission : this.bungee.getConfigurationAdapter().getPermissions(group)) {
                setPermission(permission, true);
            }
        }
    }

    public void removeGroups(String... groups) {
        for (String group : groups) {
            this.groups.remove(group);
            for (String permission : this.bungee.getConfigurationAdapter().getPermissions(group)) {
                setPermission(permission, false);
            }
        }
    }

    public boolean hasPermission(String permission) {
        return this.bungee.getPluginManager().callEvent(new PermissionCheckEvent(this, permission, this.permissions.contains(permission))).hasPermission();
    }

    public void setPermission(String permission, boolean value) {
        if (value) {
            this.permissions.add(permission);
        } else {
            this.permissions.remove(permission);
        }
    }

    public Collection<String> getPermissions() {
        return Collections.unmodifiableCollection(this.permissions);
    }

    public String toString() {
        return this.name;
    }

    public Connection.Unsafe unsafe() {
        return this.unsafe;
    }

    public String getUUID() {
        return getPendingConnection().getUUID();
    }

    public UUID getUniqueId() {
        return getPendingConnection().getUniqueId();
    }

    public Locale getLocale() {
        return (this.locale == null) && (this.settings != null) ? (this.locale = Locale.forLanguageTag(this.settings.getLocale().replace('_', '-'))) : this.locale;
    }

    public byte getViewDistance() {
        return this.settings != null ? this.settings.getViewDistance() : 10;
    }

    public ProxiedPlayer.ChatMode getChatMode() {
        if (settings == null) {
            return ProxiedPlayer.ChatMode.SHOWN;
        }

        switch (settings.getChatFlags()) {
            default:
            case 0:
                return ProxiedPlayer.ChatMode.SHOWN;
            case 1:
                return ProxiedPlayer.ChatMode.COMMANDS_ONLY;
            case 2:
                return ProxiedPlayer.ChatMode.HIDDEN;
        }
    }

    public boolean hasChatColors() {
        return (this.settings == null) || (this.settings.isChatColours());
    }

    public net.md_5.bungee.api.SkinConfiguration getSkinParts() {
        return this.settings != null ? new PlayerSkinConfiguration(this.settings.getSkinParts()) : PlayerSkinConfiguration.SKIN_SHOW_ALL;
    }

    public ProxiedPlayer.MainHand getMainHand() {
        return (this.settings == null) || (this.settings.getMainHand() == 1) ? ProxiedPlayer.MainHand.RIGHT : ProxiedPlayer.MainHand.LEFT;
    }

    public boolean isForgeUser() {
        return this.forgeClientHandler.isForgeUser();
    }

    public java.util.Map<String, String> getModList() {
        if (this.forgeClientHandler.getClientModList() == null) {


            return com.google.common.collect.ImmutableMap.of();
        }

        return com.google.common.collect.ImmutableMap.copyOf(this.forgeClientHandler.getClientModList());
    }

    public void setTabHeader(BaseComponent header, BaseComponent footer) {
        header = ChatComponentTransformer.getInstance().transform(this, header)[0];
        footer = ChatComponentTransformer.getInstance().transform(this, footer)[0];

        unsafe().sendPacket(new PlayerListHeaderFooter(
                ComponentSerializer.toString(header),
                ComponentSerializer.toString(footer)));
    }

    public void setTabHeader(BaseComponent[] header, BaseComponent[] footer) {
        header = ChatComponentTransformer.getInstance().transform(this, header);
        footer = ChatComponentTransformer.getInstance().transform(this, footer);

        unsafe().sendPacket(new PlayerListHeaderFooter(
                ComponentSerializer.toString(header),
                ComponentSerializer.toString(footer)));
    }

    public void resetTabHeader() {
        setTabHeader((BaseComponent) null, null);
    }

    public void sendTitle(Title title) {
        title.send(this);
    }

    public String getExtraDataInHandshake() {
        return getPendingConnection().getExtraDataInHandshake();
    }

    public boolean isConnected() {
        return !this.ch.isClosed();
    }


    public Scoreboard getScoreboard() {
        return this.serverSentScoreboard;
    }
}


