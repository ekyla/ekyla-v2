package net.md_5.bungee.tab;

import net.md_5.bungee.protocol.packet.PlayerListItem;

public abstract class TabList {
    protected final net.md_5.bungee.api.connection.ProxiedPlayer player;

    public TabList(net.md_5.bungee.api.connection.ProxiedPlayer player) {
        this.player = player;
    }


    public static PlayerListItem rewrite(PlayerListItem playerListItem) {
        for (net.md_5.bungee.protocol.packet.PlayerListItem.Item item : playerListItem.getItems()) {
            if (item.getUuid() != null) {


                net.md_5.bungee.UserConnection player = net.md_5.bungee.BungeeCord.getInstance().getPlayerByOfflineUUID(item.getUuid());
                if (player != null) {
                    item.setUuid(player.getUniqueId());
                    net.md_5.bungee.connection.LoginResult loginResult = player.getPendingConnection().getLoginProfile();
                    if ((loginResult != null) && (loginResult.getProperties() != null)) {
                        String[][] props = new String[loginResult.getProperties().length][];
                        for (int i = 0; i < props.length; i++) {


                            props[i] = new String[]{loginResult.getProperties()[i].getName(), loginResult.getProperties()[i].getValue(), loginResult.getProperties()[i].getSignature()};
                        }

                        item.setProperties(props);
                    } else {
                        item.setProperties(new String[0][0]);
                    }
                    if ((playerListItem.getAction() == net.md_5.bungee.protocol.packet.PlayerListItem.Action.ADD_PLAYER) || (playerListItem.getAction() == net.md_5.bungee.protocol.packet.PlayerListItem.Action.UPDATE_GAMEMODE)) {
                        player.setGamemode(item.getGamemode());
                    }
                    if ((playerListItem.getAction() == net.md_5.bungee.protocol.packet.PlayerListItem.Action.ADD_PLAYER) || (playerListItem.getAction() == net.md_5.bungee.protocol.packet.PlayerListItem.Action.UPDATE_LATENCY)) {
                        player.setPing(item.getPing());
                    }
                }
            }
        }
        return playerListItem;
    }

    public abstract void onDisconnect();

    public abstract void onConnect();

    public abstract void onServerChange();

    public abstract void onPingChange(int paramInt);

    public abstract void onUpdate(PlayerListItem paramPlayerListItem);
}


