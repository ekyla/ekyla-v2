/*    */ package net.md_5.bungee.tab;
/*    */ 
/*    */ import java.util.Collection;
/*    */ import net.md_5.bungee.api.connection.Connection.Unsafe;
/*    */ import net.md_5.bungee.api.connection.ProxiedPlayer;
/*    */ import net.md_5.bungee.protocol.packet.PlayerListItem;
/*    */ import net.md_5.bungee.protocol.packet.PlayerListItem.Action;
/*    */ import net.md_5.bungee.protocol.packet.PlayerListItem.Item;
/*    */ 
/*    */ public class ServerUnique extends TabList
/*    */ {
/* 12 */   private final Collection<java.util.UUID> uuids = new java.util.HashSet();
/*    */   
/*    */   public ServerUnique(ProxiedPlayer player)
/*    */   {
/* 16 */     super(player);
/*    */   }
/*    */   
/*    */ 
/*    */   public void onUpdate(PlayerListItem playerListItem)
/*    */   {
/* 22 */     for (PlayerListItem.Item item : playerListItem.getItems())
/*    */     {
/* 24 */       if (playerListItem.getAction() == PlayerListItem.Action.ADD_PLAYER)
/*    */       {
/* 26 */         this.uuids.add(item.getUuid());
/* 27 */       } else if (playerListItem.getAction() == PlayerListItem.Action.REMOVE_PLAYER)
/*    */       {
/* 29 */         this.uuids.remove(item.getUuid());
/*    */       }
/*    */     }
/* 32 */     this.player.unsafe().sendPacket(playerListItem);
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public void onPingChange(int ping) {}
/*    */   
/*    */ 
/*    */ 
/*    */   public void onServerChange()
/*    */   {
/* 44 */     PlayerListItem packet = new PlayerListItem();
/* 45 */     packet.setAction(PlayerListItem.Action.REMOVE_PLAYER);
/* 46 */     PlayerListItem.Item[] items = new PlayerListItem.Item[this.uuids.size()];
/* 47 */     int i = 0;
/* 48 */     for (java.util.UUID uuid : this.uuids)
/*    */     {
/* 50 */       PlayerListItem.Item item = items[(i++)] = new PlayerListItem.Item();
/* 51 */       item.setUuid(uuid);
/*    */     }
/* 53 */     packet.setItems(items);
/* 54 */     this.player.unsafe().sendPacket(packet);
/* 55 */     this.uuids.clear();
/*    */   }
/*    */   
/*    */   public void onConnect() {}
/*    */   
/*    */   public void onDisconnect() {}
/*    */ }


