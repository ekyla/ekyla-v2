/*     */ package net.md_5.bungee.query;
/*     */ 
/*     */ import io.netty.buffer.ByteBuf;
/*     */ import io.netty.channel.ChannelHandlerContext;
/*     */ import io.netty.channel.socket.DatagramPacket;
/*     */ import java.net.InetSocketAddress;
/*     */ import java.util.Map;
/*     */ import java.util.Map.Entry;
/*     */ import java.util.Random;
/*     */ import java.util.logging.Level;
/*     */ import java.util.logging.Logger;
/*     */ import net.md_5.bungee.api.ProxyServer;
/*     */ import net.md_5.bungee.api.config.ListenerInfo;
/*     */ 
/*     */ public class QueryHandler extends io.netty.channel.SimpleChannelInboundHandler<DatagramPacket>
/*     */ {
/*     */   private final ProxyServer bungee;
/*     */   private final ListenerInfo listener;
/*     */   
/*     */   public QueryHandler(ProxyServer bungee, ListenerInfo listener)
/*     */   {
/*  22 */     this.bungee = bungee;this.listener = listener;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  29 */   private final Random random = new Random();
/*  30 */   private final com.google.common.cache.Cache<java.net.InetAddress, QuerySession> sessions = com.google.common.cache.CacheBuilder.newBuilder().expireAfterWrite(30L, java.util.concurrent.TimeUnit.SECONDS).build();
/*     */   
/*     */   private void writeShort(ByteBuf buf, int s)
/*     */   {
/*  34 */     buf.writeShortLE(s);
/*     */   }
/*     */   
/*     */   private void writeNumber(ByteBuf buf, int i)
/*     */   {
/*  39 */     writeString(buf, Integer.toString(i));
/*     */   }
/*     */   
/*     */   private void writeString(ByteBuf buf, String s)
/*     */   {
/*  44 */     for (char c : s.toCharArray())
/*     */     {
/*  46 */       buf.writeByte(c);
/*     */     }
/*  48 */     buf.writeByte(0);
/*     */   }
/*     */   
/*     */   protected void channelRead0(ChannelHandlerContext ctx, DatagramPacket msg)
/*     */     throws Exception
/*     */   {
/*     */     try
/*     */     {
/*  56 */       handleMessage(ctx, msg);
/*     */     }
/*     */     catch (Throwable t) {
/*  59 */       this.bungee.getLogger().log(Level.WARNING, "Error whilst handling query packet from " + msg.sender(), t);
/*     */     }
/*     */   }
/*     */   
/*     */   private void handleMessage(ChannelHandlerContext ctx, DatagramPacket msg)
/*     */   {
/*  65 */     ByteBuf in = (ByteBuf)msg.content();
/*  66 */     if ((in.readUnsignedByte() != 254) || (in.readUnsignedByte() != 253))
/*     */     {
/*  68 */       this.bungee.getLogger().log(Level.WARNING, "Query - Incorrect magic!: {0}", msg.sender());
/*  69 */       return;
/*     */     }
/*     */     
/*  72 */     ByteBuf out = ctx.alloc().buffer();
/*  73 */     io.netty.channel.AddressedEnvelope response = new DatagramPacket(out, (InetSocketAddress)msg.sender());
/*     */     
/*  75 */     byte type = in.readByte();
/*  76 */     int sessionId = in.readInt();
/*     */     
/*  78 */     if (type == 9)
/*     */     {
/*  80 */       out.writeByte(9);
/*  81 */       out.writeInt(sessionId);
/*     */       
/*  83 */       int challengeToken = this.random.nextInt();
/*  84 */       this.sessions.put(((InetSocketAddress)msg.sender()).getAddress(), new QuerySession(challengeToken, System.currentTimeMillis()));
/*     */       
/*  86 */       writeNumber(out, challengeToken);
/*     */     }
/*     */     
/*  89 */     if (type == 0)
/*     */     {
/*  91 */       int challengeToken = in.readInt();
/*  92 */       QuerySession session = (QuerySession)this.sessions.getIfPresent(((InetSocketAddress)msg.sender()).getAddress());
/*  93 */       if ((session == null) || (session.getToken() != challengeToken))
/*     */       {
/*  95 */         throw new IllegalStateException("No session!");
/*     */       }
/*     */       
/*  98 */       out.writeByte(0);
/*  99 */       out.writeInt(sessionId);
/*     */       
/* 101 */       if (in.readableBytes() == 0)
/*     */       {
/*     */ 
/* 104 */         writeString(out, this.listener.getMotd());
/* 105 */         writeString(out, "SMP");
/* 106 */         writeString(out, "BungeeCord_Proxy");
/* 107 */         writeNumber(out, this.bungee.getOnlineCount());
/* 108 */         writeNumber(out, this.listener.getMaxPlayers());
/* 109 */         writeShort(out, this.listener.getHost().getPort());
/* 110 */         writeString(out, this.listener.getHost().getHostString());
/* 111 */       } else if (in.readableBytes() == 4)
/*     */       {
/*     */ 
/* 114 */         out.writeBytes(new byte[] { 115, 112, 108, 105, 116, 110, 117, 109, 0, Byte.MIN_VALUE, 0 });
/*     */         
/*     */ 
/*     */ 
/* 118 */         Map<String, String> data = new java.util.LinkedHashMap();
/*     */         
/* 120 */         data.put("hostname", this.listener.getMotd());
/* 121 */         data.put("gametype", "SMP");
/*     */         
/* 123 */         data.put("game_id", "MINECRAFT");
/* 124 */         data.put("version", this.bungee.getGameVersion());
/* 125 */         data.put("plugins", "");
/*     */         
/* 127 */         data.put("map", "BungeeCord_Proxy");
/* 128 */         data.put("numplayers", Integer.toString(this.bungee.getOnlineCount()));
/* 129 */         data.put("maxplayers", Integer.toString(this.listener.getMaxPlayers()));
/* 130 */         data.put("hostport", Integer.toString(this.listener.getHost().getPort()));
/* 131 */         data.put("hostip", this.listener.getHost().getHostString());
/*     */         
/* 133 */         for (Map.Entry<String, String> entry : data.entrySet())
/*     */         {
/* 135 */           writeString(out, (String)entry.getKey());
/* 136 */           writeString(out, (String)entry.getValue());
/*     */         }
/* 138 */         out.writeByte(0);
/*     */         
/*     */ 
/* 141 */         writeString(out, "\001player_\000");
/*     */         
/* 143 */         for (net.md_5.bungee.api.connection.ProxiedPlayer p : this.bungee.getPlayers())
/*     */         {
/* 145 */           writeString(out, p.getName());
/*     */         }
/* 147 */         out.writeByte(0);
/*     */       }
/*     */       else
/*     */       {
/* 151 */         throw new IllegalStateException("Invalid data request packet");
/*     */       }
/*     */     }
/*     */     
/* 155 */     ctx.writeAndFlush(response);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/* 161 */   public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
/* 161 */     throws Exception { this.bungee.getLogger().log(Level.WARNING, "Error whilst handling query packet from " + ctx.channel().remoteAddress(), cause); }
/*     */   
/*     */   private static class QuerySession {
/* 164 */     public QuerySession(int token, long time) { this.token = token;this.time = time; } public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof QuerySession)) return false; QuerySession other = (QuerySession)o; if (!other.canEqual(this)) return false; if (getToken() != other.getToken()) return false; return getTime() == other.getTime(); } protected boolean canEqual(Object other) { return other instanceof QuerySession; } public int hashCode() { int PRIME = 59;int result = 1;result = result * 59 + getToken();long $time = getTime();result = result * 59 + (int)($time >>> 32 ^ $time);return result; } public String toString() { return "QueryHandler.QuerySession(token=" + getToken() + ", time=" + getTime() + ")"; }
/*     */     
/*     */     private final int token;
/*     */     private final long time;
/* 168 */     public int getToken() { return this.token; }
/* 169 */     public long getTime() { return this.time; }
/*     */   }
/*     */ }


