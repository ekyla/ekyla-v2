/*    */ package net.md_5.bungee.query;
/*    */ 
/*    */ import io.netty.bootstrap.Bootstrap;
/*    */ 
/*    */ public class RemoteQuery
/*    */ {
/*    */   private final net.md_5.bungee.api.ProxyServer bungee;
/*    */   private final net.md_5.bungee.api.config.ListenerInfo listener;
/*    */   
/*    */   public RemoteQuery(net.md_5.bungee.api.ProxyServer bungee, net.md_5.bungee.api.config.ListenerInfo listener)
/*    */   {
/* 12 */     this.bungee = bungee;this.listener = listener;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void start(Class<? extends io.netty.channel.Channel> channel, java.net.InetSocketAddress address, io.netty.channel.EventLoopGroup eventLoop, io.netty.channel.ChannelFutureListener future)
/*    */   {
/* 26 */     ((Bootstrap)((Bootstrap)((Bootstrap)((Bootstrap)new Bootstrap().channel(channel)).group(eventLoop)).handler(new QueryHandler(this.bungee, this.listener))).localAddress(address)).bind().addListener(future);
/*    */   }
/*    */ }


