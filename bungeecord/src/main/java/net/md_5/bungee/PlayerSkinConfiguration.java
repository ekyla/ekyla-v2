/*    */ package net.md_5.bungee;
/*    */ 
/*    */ import net.md_5.bungee.api.SkinConfiguration;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class PlayerSkinConfiguration
/*    */   implements SkinConfiguration
/*    */ {
/*    */   public PlayerSkinConfiguration(byte bitmask)
/*    */   {
/* 19 */     this.bitmask = bitmask;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/* 24 */   static final SkinConfiguration SKIN_SHOW_ALL = new PlayerSkinConfiguration((byte)Byte.MAX_VALUE);
/*    */   
/*    */   private final byte bitmask;
/*    */   
/*    */ 
/*    */   public boolean hasCape()
/*    */   {
/* 31 */     return (this.bitmask >> 0 & 0x1) == 1;
/*    */   }
/*    */   
/*    */ 
/*    */   public boolean hasJacket()
/*    */   {
/* 37 */     return (this.bitmask >> 1 & 0x1) == 1;
/*    */   }
/*    */   
/*    */ 
/*    */   public boolean hasLeftSleeve()
/*    */   {
/* 43 */     return (this.bitmask >> 2 & 0x1) == 1;
/*    */   }
/*    */   
/*    */ 
/*    */   public boolean hasRightSleeve()
/*    */   {
/* 49 */     return (this.bitmask >> 3 & 0x1) == 1;
/*    */   }
/*    */   
/*    */ 
/*    */   public boolean hasLeftPants()
/*    */   {
/* 55 */     return (this.bitmask >> 4 & 0x1) == 1;
/*    */   }
/*    */   
/*    */ 
/*    */   public boolean hasRightPants()
/*    */   {
/* 61 */     return (this.bitmask >> 5 & 0x1) == 1;
/*    */   }
/*    */   
/*    */ 
/*    */   public boolean hasHat()
/*    */   {
/* 67 */     return (this.bitmask >> 6 & 0x1) == 1;
/*    */   }
/*    */ }


