/*    */ package net.md_5.bungee.http;
/*    */ 
/*    */ import io.netty.handler.codec.http.HttpContent;
/*    */ import io.netty.handler.codec.http.HttpObject;
/*    */ import io.netty.handler.codec.http.HttpResponse;
/*    */ import io.netty.handler.codec.http.HttpResponseStatus;
/*    */ 
/*    */ public class HttpHandler extends io.netty.channel.SimpleChannelInboundHandler<HttpObject>
/*    */ {
/*    */   private final net.md_5.bungee.api.Callback<String> callback;
/*    */   
/*    */   public HttpHandler(net.md_5.bungee.api.Callback<String> callback)
/*    */   {
/* 14 */     this.callback = callback;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/* 19 */   private final StringBuilder buffer = new StringBuilder();
/*    */   
/*    */   /* Error */
/*    */   private void done(io.netty.channel.ChannelHandlerContext ctx)
/*    */   {
/*    */     // Byte code:
/*    */     //   0: aload_0
/*    */     //   1: getfield 1	net/md_5/bungee/http/HttpHandler:callback	Lnet/md_5/bungee/api/Callback;
/*    */     //   4: aload_0
/*    */     //   5: getfield 20	net/md_5/bungee/http/HttpHandler:buffer	Ljava/lang/StringBuilder;
/*    */     //   8: invokevirtual 17	java/lang/StringBuilder:toString	()Ljava/lang/String;
/*    */     //   11: aconst_null
/*    */     //   12: invokeinterface 2 3 0
/*    */     //   17: aload_1
/*    */     //   18: invokeinterface 3 1 0
/*    */     //   23: invokeinterface 4 1 0
/*    */     //   28: pop
/*    */     //   29: goto +18 -> 47
/*    */     //   32: astore_2
/*    */     //   33: aload_1
/*    */     //   34: invokeinterface 3 1 0
/*    */     //   39: invokeinterface 4 1 0
/*    */     //   44: pop
/*    */     //   45: aload_2
/*    */     //   46: athrow
/*    */     //   47: return
/*    */     // Line number table:
/*    */     //   Java source line #68	-> byte code offset #0
/*    */     //   Java source line #71	-> byte code offset #17
/*    */     //   Java source line #72	-> byte code offset #29
/*    */     //   Java source line #71	-> byte code offset #32
/*    */     //   Java source line #72	-> byte code offset #45
/*    */     //   Java source line #73	-> byte code offset #47
/*    */     // Local variable table:
/*    */     //   start	length	slot	name	signature
/*    */     //   0	48	0	this	HttpHandler
/*    */     //   0	48	1	ctx	io.netty.channel.ChannelHandlerContext
/*    */     //   32	14	2	localObject	Object
/*    */     // Exception table:
/*    */     //   from	to	target	type
/*    */     //   0	17	32	finally
/*    */   }
/*    */   
/*    */   protected void channelRead0(io.netty.channel.ChannelHandlerContext ctx, HttpObject msg)
/*    */     throws Exception
/*    */   {
/* 36 */     if ((msg instanceof HttpResponse))
/*    */     {
/* 38 */       HttpResponse response = (HttpResponse)msg;
/* 39 */       int responseCode = response.getStatus().code();
/*    */       
/* 41 */       if (responseCode == HttpResponseStatus.NO_CONTENT.code())
/*    */       {
/* 43 */         done(ctx);
/* 44 */         return;
/*    */       }
/*    */       
/* 47 */       if (responseCode != HttpResponseStatus.OK.code())
/*    */       {
/* 49 */         throw new IllegalStateException("Expected HTTP response 200 OK, got " + response.getStatus());
/*    */       }
/*    */     }
/* 52 */     if ((msg instanceof HttpContent))
/*    */     {
/* 54 */       HttpContent content = (HttpContent)msg;
/* 55 */       this.buffer.append(content.content().toString(java.nio.charset.Charset.forName("UTF-8")));
/*    */       
/* 57 */       if ((msg instanceof io.netty.handler.codec.http.LastHttpContent))
/*    */       {
/* 59 */         done(ctx);
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */   /* Error */
/*    */   public void exceptionCaught(io.netty.channel.ChannelHandlerContext ctx, Throwable cause)
/*    */     throws Exception
/*    */   {
/*    */     // Byte code:
/*    */     //   0: aload_0
/*    */     //   1: getfield 1	net/md_5/bungee/http/HttpHandler:callback	Lnet/md_5/bungee/api/Callback;
/*    */     //   4: aconst_null
/*    */     //   5: aload_2
/*    */     //   6: invokeinterface 2 3 0
/*    */     //   11: aload_1
/*    */     //   12: invokeinterface 3 1 0
/*    */     //   17: invokeinterface 4 1 0
/*    */     //   22: pop
/*    */     //   23: goto +18 -> 41
/*    */     //   26: astore_3
/*    */     //   27: aload_1
/*    */     //   28: invokeinterface 3 1 0
/*    */     //   33: invokeinterface 4 1 0
/*    */     //   38: pop
/*    */     //   39: aload_3
/*    */     //   40: athrow
/*    */     //   41: return
/*    */     // Line number table:
/*    */     //   Java source line #26	-> byte code offset #0
/*    */     //   Java source line #29	-> byte code offset #11
/*    */     //   Java source line #30	-> byte code offset #23
/*    */     //   Java source line #29	-> byte code offset #26
/*    */     //   Java source line #30	-> byte code offset #39
/*    */     //   Java source line #31	-> byte code offset #41
/*    */     // Local variable table:
/*    */     //   start	length	slot	name	signature
/*    */     //   0	42	0	this	HttpHandler
/*    */     //   0	42	1	ctx	io.netty.channel.ChannelHandlerContext
/*    */     //   0	42	2	cause	Throwable
/*    */     //   26	14	3	localObject	Object
/*    */     // Exception table:
/*    */     //   from	to	target	type
/*    */     //   0	11	26	finally
/*    */   }
/*    */ }


