/*    */ package net.md_5.bungee.http;
/*    */ 
/*    */ import io.netty.channel.Channel;
/*    */ 
/*    */ public class HttpInitializer extends io.netty.channel.ChannelInitializer<Channel>
/*    */ {
/*    */   private final net.md_5.bungee.api.Callback<String> callback;
/*    */   private final boolean ssl;
/*    */   private final String host;
/*    */   private final int port;
/*    */   
/*    */   public HttpInitializer(net.md_5.bungee.api.Callback<String> callback, boolean ssl, String host, int port)
/*    */   {
/* 14 */     this.callback = callback;this.ssl = ssl;this.host = host;this.port = port;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   protected void initChannel(Channel ch)
/*    */     throws Exception
/*    */   {
/* 26 */     ch.pipeline().addLast("timeout", new io.netty.handler.timeout.ReadTimeoutHandler(5000L, java.util.concurrent.TimeUnit.MILLISECONDS));
/* 27 */     if (this.ssl)
/*    */     {
/* 29 */       javax.net.ssl.SSLEngine engine = io.netty.handler.ssl.SslContext.newClientContext().newEngine(ch.alloc(), this.host, this.port);
/*    */       
/* 31 */       ch.pipeline().addLast("ssl", new io.netty.handler.ssl.SslHandler(engine));
/*    */     }
/* 33 */     ch.pipeline().addLast("http", new io.netty.handler.codec.http.HttpClientCodec());
/* 34 */     ch.pipeline().addLast("handler", new HttpHandler(this.callback));
/*    */   }
/*    */ }


