/*    */ package net.md_5.bungee.module;
/*    */ 
/*    */ import java.io.PrintStream;
/*    */ import java.net.URL;
/*    */ import java.net.URLConnection;
/*    */ 
/*    */ 
/*    */ public class JenkinsModuleSource
/*    */   implements ModuleSource
/*    */ {
/* 11 */   public String toString() { return "JenkinsModuleSource()"; } public int hashCode() { int result = 1;return result; } protected boolean canEqual(Object other) { return other instanceof JenkinsModuleSource; } public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof JenkinsModuleSource)) return false; JenkinsModuleSource other = (JenkinsModuleSource)o;return other.canEqual(this);
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void retrieve(ModuleSpec module, ModuleVersion version)
/*    */   {
/* 18 */     System.out.println("Attempting to Jenkins download module " + module.getName() + " v" + version.getBuild());
/*    */     try
/*    */     {
/* 21 */       URL website = new URL("https://ci.md-5.net/job/BungeeCord/" + version.getBuild() + "/artifact/module/" + module.getName().replace('_', '-') + "/target/" + module.getName() + ".jar");
/* 22 */       URLConnection con = website.openConnection();
/*    */       
/* 24 */       con.setConnectTimeout(15000);
/* 25 */       con.setReadTimeout(15000);
/*    */       
/* 27 */       com.google.common.io.Files.write(com.google.common.io.ByteStreams.toByteArray(con.getInputStream()), module.getFile());
/* 28 */       System.out.println("Download complete");
/*    */     }
/*    */     catch (java.io.IOException ex) {
/* 31 */       System.out.println("Failed to download: " + net.md_5.bungee.Util.exception(ex));
/*    */     }
/*    */   }
/*    */ }


