package net.md_5.bungee.module;

abstract interface ModuleSource
{
  public abstract void retrieve(ModuleSpec paramModuleSpec, ModuleVersion paramModuleVersion);
}


