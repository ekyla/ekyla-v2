/*    */ package net.md_5.bungee.module;
/*    */ 
/*    */ public class ModuleVersion {
/*    */   private final String build;
/*    */   private final String git;
/*    */   
/*  7 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof ModuleVersion)) return false; ModuleVersion other = (ModuleVersion)o; if (!other.canEqual(this)) return false; Object this$build = getBuild();Object other$build = other.getBuild(); if (this$build == null ? other$build != null : !this$build.equals(other$build)) return false; Object this$git = getGit();Object other$git = other.getGit();return this$git == null ? other$git == null : this$git.equals(other$git); } protected boolean canEqual(Object other) { return other instanceof ModuleVersion; } public int hashCode() { int PRIME = 59;int result = 1;Object $build = getBuild();result = result * 59 + ($build == null ? 43 : $build.hashCode());Object $git = getGit();result = result * 59 + ($git == null ? 43 : $git.hashCode());return result; } public String toString() { return "ModuleVersion(build=" + getBuild() + ", git=" + getGit() + ")"; }
/*  8 */   private ModuleVersion(String build, String git) { this.build = build;this.git = git;
/*    */   }
/*    */   
/*    */ 
/* 12 */   public String getBuild() { return this.build; }
/* 13 */   public String getGit() { return this.git; }
/*    */   
/*    */   public static ModuleVersion parse(String version)
/*    */   {
/* 17 */     int lastColon = version.lastIndexOf(':');
/* 18 */     int secondLastColon = version.lastIndexOf(':', lastColon - 1);
/*    */     
/* 20 */     if ((lastColon == -1) || (secondLastColon == -1))
/*    */     {
/* 22 */       return null;
/*    */     }
/*    */     
/* 25 */     String buildNumber = version.substring(lastColon + 1, version.length());
/* 26 */     String gitCommit = version.substring(secondLastColon + 1, lastColon).replaceAll("\"", "");
/*    */     
/* 28 */     if (("unknown".equals(buildNumber)) || ("unknown".equals(gitCommit)))
/*    */     {
/* 30 */       return null;
/*    */     }
/*    */     
/* 33 */     return new ModuleVersion(buildNumber, gitCommit);
/*    */   }
/*    */ }


