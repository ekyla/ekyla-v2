package net.md_5.bungee;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.TimerTask;


public class Metrics
        extends TimerTask {
    static final int PING_INTERVAL = 10;
    private static final int REVISION = 5;
    private static final String BASE_URL = "https://mcstats.spigotmc.org";
    private static final String REPORT_URL = "/report/%s";
    boolean firstPost = true;

    private static void encodeDataPair(StringBuilder buffer, String key, String value) throws UnsupportedEncodingException {
        buffer.append('&').append(encode(key)).append('=').append(encode(value));
    }

    private static String encode(String text) throws UnsupportedEncodingException {
        return URLEncoder.encode(text, "UTF-8");
    }

    public void run() {
        try {
            postPlugin(!this.firstPost);

            this.firstPost = false;
        } catch (IOException ignored) {
        }
    }

    private void postPlugin(boolean isPing) throws IOException {
        /*
        StringBuilder data = new StringBuilder();
        data.append(encode("guid")).append('=').append(encode(BungeeCord.getInstance().config.getUuid()));
        encodeDataPair(data, "version", ProxyServer.getInstance().getVersion());
        encodeDataPair(data, "server", "0");
        encodeDataPair(data, "players", Integer.toString(ProxyServer.getInstance().getOnlineCount()));
        encodeDataPair(data, "revision", String.valueOf(5));


        if (isPing) {
            encodeDataPair(data, "ping", "true");
        }


        URL url = new URL("https://mcstats.spigotmc.org" + String.format("/report/%s", encode("BungeeCord")));


        URLConnection connection = url.openConnection();

        connection.setDoOutput(true);


        OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
        Throwable localThrowable3 = null;
        try {
            writer.write(data.toString());
            writer.flush();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            response = reader.readLine();
        } catch (Throwable localThrowable1) {
            String response;
            localThrowable3 = localThrowable1;
            throw localThrowable1;

        } finally {

            if (localThrowable3 != null) try {
                writer.close();
            } catch (Throwable localThrowable2) {
                localThrowable3.addSuppressed(localThrowable2);
            }
            else writer.close();
        }
        String response;
        BufferedReader reader;
        reader.close();

        if ((response == null) || (response.startsWith("ERR"))) {
            throw new IOException(response);
        }
        */
    }
}


