/*    */ package net.md_5.bungee.event;
/*    */ 
/*    */ import java.lang.reflect.Method;
/*    */ 
/*    */ public class EventHandlerMethod { private final Object listener;
/*    */   private final Method method;
/*    */   
/*  8 */   public EventHandlerMethod(Object listener, Method method) { this.listener = listener;this.method = method;
/*    */   }
/*    */   
/*    */ 
/* 12 */   public Object getListener() { return this.listener; }
/*    */   
/* 14 */   public Method getMethod() { return this.method; }
/*    */   
/*    */   public void invoke(Object event)
/*    */     throws IllegalAccessException, IllegalArgumentException, java.lang.reflect.InvocationTargetException
/*    */   {
/* 19 */     this.method.invoke(this.listener, new Object[] { event });
/*    */   }
/*    */ }


