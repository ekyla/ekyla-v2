/*    */ package net.md_5.bungee.command;
/*    */ 
/*    */ import net.md_5.bungee.api.ProxyServer;
/*    */ 
/*    */ public class ConsoleCommandCompleter implements jline.console.completer.Completer {
/*    */   private final ProxyServer proxy;
/*    */   
/*    */   public ConsoleCommandCompleter(ProxyServer proxy) {
/*  9 */     this.proxy = proxy;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public int complete(String buffer, int cursor, java.util.List<CharSequence> candidates)
/*    */   {
/* 18 */     java.util.List<String> suggestions = new java.util.ArrayList();
/* 19 */     this.proxy.getPluginManager().dispatchCommand(this.proxy.getConsole(), buffer, suggestions);
/* 20 */     candidates.addAll(suggestions);
/*    */     
/* 22 */     int lastSpace = buffer.lastIndexOf(' ');
/* 23 */     return lastSpace == -1 ? cursor - buffer.length() : cursor - (buffer.length() - lastSpace - 1);
/*    */   }
/*    */ }


