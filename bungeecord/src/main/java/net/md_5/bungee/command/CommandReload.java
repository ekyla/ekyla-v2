/*    */ package net.md_5.bungee.command;
/*    */ 
/*    */ import net.md_5.bungee.BungeeCord;
/*    */ import net.md_5.bungee.api.ChatColor;
/*    */ import net.md_5.bungee.api.CommandSender;
/*    */ import net.md_5.bungee.api.event.ProxyReloadEvent;
/*    */ import net.md_5.bungee.api.plugin.Command;
/*    */ import net.md_5.bungee.conf.Configuration;
/*    */ 
/*    */ public class CommandReload extends Command
/*    */ {
/*    */   public CommandReload()
/*    */   {
/* 14 */     super("greload", "bungeecord.command.reload", new String[0]);
/*    */   }
/*    */   
/*    */ 
/*    */   public void execute(CommandSender sender, String[] args)
/*    */   {
/* 20 */     BungeeCord.getInstance().config.load();
/* 21 */     BungeeCord.getInstance().reloadMessages();
/* 22 */     BungeeCord.getInstance().stopListeners();
/* 23 */     BungeeCord.getInstance().startListeners();
/* 24 */     BungeeCord.getInstance().getPluginManager().callEvent(new ProxyReloadEvent(sender));
/*    */     
/* 26 */     sender.sendMessage(ChatColor.BOLD.toString() + ChatColor.RED.toString() + "BungeeCord has been reloaded. This is NOT advisable and you will not be supported with any issues that arise! Please restart BungeeCord ASAP.");
/*    */   }
/*    */ }


