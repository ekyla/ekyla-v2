/*    */ package net.md_5.bungee.command;
/*    */ 
/*    */ import java.util.HashSet;
/*    */ import java.util.Set;
/*    */ import net.md_5.bungee.Util;
/*    */ import net.md_5.bungee.api.ChatColor;
/*    */ import net.md_5.bungee.api.CommandSender;
/*    */ import net.md_5.bungee.api.ProxyServer;
/*    */ import net.md_5.bungee.api.config.ConfigurationAdapter;
/*    */ import net.md_5.bungee.api.plugin.Command;
/*    */ 
/*    */ public class CommandPerms extends Command
/*    */ {
/*    */   public CommandPerms()
/*    */   {
/* 16 */     super("perms");
/*    */   }
/*    */   
/*    */ 
/*    */   public void execute(CommandSender sender, String[] args)
/*    */   {
/* 22 */     Set<String> permissions = new HashSet();
/* 23 */     for (String group : sender.getGroups())
/*    */     {
/* 25 */       permissions.addAll(ProxyServer.getInstance().getConfigurationAdapter().getPermissions(group));
/*    */     }
/* 27 */     sender.sendMessage(ChatColor.GOLD + "You have the following groups: " + Util.csv(sender.getGroups()));
/*    */     
/* 29 */     for (String permission : permissions)
/*    */     {
/* 31 */       sender.sendMessage(ChatColor.BLUE + "- " + permission);
/*    */     }
/*    */   }
/*    */ }


