/*    */ package net.md_5.bungee.command;
/*    */ 
/*    */ import net.md_5.bungee.api.ChatColor;
/*    */ import net.md_5.bungee.api.CommandSender;
/*    */ import net.md_5.bungee.api.ProxyServer;
/*    */ import net.md_5.bungee.api.connection.ProxiedPlayer;
/*    */ 
/*    */ public class CommandIP
/*    */   extends PlayerCommand
/*    */ {
/*    */   public CommandIP()
/*    */   {
/* 13 */     super("ip", "bungeecord.command.ip", new String[0]);
/*    */   }
/*    */   
/*    */ 
/*    */   public void execute(CommandSender sender, String[] args)
/*    */   {
/* 19 */     if (args.length < 1)
/*    */     {
/* 21 */       sender.sendMessage(ChatColor.RED + "Please follow this command by a user name");
/* 22 */       return;
/*    */     }
/* 24 */     ProxiedPlayer user = ProxyServer.getInstance().getPlayer(args[0]);
/* 25 */     if (user == null)
/*    */     {
/* 27 */       sender.sendMessage(ChatColor.RED + "That user is not online");
/*    */     }
/*    */     else {
/* 30 */       sender.sendMessage(ChatColor.BLUE + "IP of " + args[0] + " is " + user.getAddress());
/*    */     }
/*    */   }
/*    */ }


