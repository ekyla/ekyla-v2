/*    */ package net.md_5.bungee.command;
/*    */ 
/*    */ import java.util.Collection;
/*    */ import java.util.Collections;
/*    */ import java.util.logging.Logger;
/*    */ import net.md_5.bungee.api.CommandSender;
/*    */ import net.md_5.bungee.api.ProxyServer;
/*    */ import net.md_5.bungee.api.chat.BaseComponent;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public final class ConsoleCommandSender
/*    */   implements CommandSender
/*    */ {
/* 19 */   public static ConsoleCommandSender getInstance() { return instance; }
/* 20 */   private static final ConsoleCommandSender instance = new ConsoleCommandSender();
/*    */   
/*    */ 
/*    */   public void sendMessage(String message)
/*    */   {
/* 25 */     ProxyServer.getInstance().getLogger().info(message);
/*    */   }
/*    */   
/*    */ 
/*    */   public void sendMessages(String... messages)
/*    */   {
/* 31 */     for (String message : messages)
/*    */     {
/* 33 */       sendMessage(message);
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public void sendMessage(BaseComponent... message)
/*    */   {
/* 40 */     sendMessage(BaseComponent.toLegacyText(message));
/*    */   }
/*    */   
/*    */ 
/*    */   public void sendMessage(BaseComponent message)
/*    */   {
/* 46 */     sendMessage(message.toLegacyText());
/*    */   }
/*    */   
/*    */ 
/*    */   public String getName()
/*    */   {
/* 52 */     return "CONSOLE";
/*    */   }
/*    */   
/*    */ 
/*    */   public Collection<String> getGroups()
/*    */   {
/* 58 */     return Collections.emptySet();
/*    */   }
/*    */   
/*    */ 
/*    */   public void addGroups(String... groups)
/*    */   {
/* 64 */     throw new UnsupportedOperationException("Console may not have groups");
/*    */   }
/*    */   
/*    */ 
/*    */   public void removeGroups(String... groups)
/*    */   {
/* 70 */     throw new UnsupportedOperationException("Console may not have groups");
/*    */   }
/*    */   
/*    */ 
/*    */   public boolean hasPermission(String permission)
/*    */   {
/* 76 */     return true;
/*    */   }
/*    */   
/*    */ 
/*    */   public void setPermission(String permission, boolean value)
/*    */   {
/* 82 */     throw new UnsupportedOperationException("Console has all permissions");
/*    */   }
/*    */   
/*    */ 
/*    */   public Collection<String> getPermissions()
/*    */   {
/* 88 */     return Collections.emptySet();
/*    */   }
/*    */ }


