/*    */ package net.md_5.bungee.scheduler;
/*    */ 
/*    */ import java.util.concurrent.atomic.AtomicBoolean;
/*    */ 
/*    */ public class BungeeTask implements Runnable, net.md_5.bungee.api.scheduler.ScheduledTask { private final BungeeScheduler sched;
/*    */   private final int id;
/*    */   private final net.md_5.bungee.api.plugin.Plugin owner;
/*    */   private final Runnable task;
/*    */   private final long delay;
/*    */   
/* 11 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof BungeeTask)) return false; BungeeTask other = (BungeeTask)o; if (!other.canEqual(this)) return false; Object this$sched = getSched();Object other$sched = other.getSched(); if (this$sched == null ? other$sched != null : !this$sched.equals(other$sched)) return false; if (getId() != other.getId()) return false; Object this$owner = getOwner();Object other$owner = other.getOwner(); if (this$owner == null ? other$owner != null : !this$owner.equals(other$owner)) return false; Object this$task = getTask();Object other$task = other.getTask(); if (this$task == null ? other$task != null : !this$task.equals(other$task)) return false; if (getDelay() != other.getDelay()) return false; if (getPeriod() != other.getPeriod()) return false; Object this$running = getRunning();Object other$running = other.getRunning();return this$running == null ? other$running == null : this$running.equals(other$running); } protected boolean canEqual(Object other) { return other instanceof BungeeTask; } public int hashCode() { int PRIME = 59;int result = 1;Object $sched = getSched();result = result * 59 + ($sched == null ? 43 : $sched.hashCode());result = result * 59 + getId();Object $owner = getOwner();result = result * 59 + ($owner == null ? 43 : $owner.hashCode());Object $task = getTask();result = result * 59 + ($task == null ? 43 : $task.hashCode());long $delay = getDelay();result = result * 59 + (int)($delay >>> 32 ^ $delay);long $period = getPeriod();result = result * 59 + (int)($period >>> 32 ^ $period);Object $running = getRunning();result = result * 59 + ($running == null ? 43 : $running.hashCode());return result; } public String toString() { return "BungeeTask(sched=" + getSched() + ", id=" + getId() + ", owner=" + getOwner() + ", task=" + getTask() + ", delay=" + getDelay() + ", period=" + getPeriod() + ", running=" + getRunning() + ")"; }
/*    */   
/*    */   private final long period;
/*    */   
/* 15 */   public BungeeScheduler getSched() { return this.sched; }
/* 16 */   public int getId() { return this.id; }
/* 17 */   public net.md_5.bungee.api.plugin.Plugin getOwner() { return this.owner; }
/* 18 */   public Runnable getTask() { return this.task; }
/*    */   
/* 20 */   public long getDelay() { return this.delay; }
/* 21 */   public long getPeriod() { return this.period; }
/* 22 */   public AtomicBoolean getRunning() { return this.running; } private final AtomicBoolean running = new AtomicBoolean(true);
/*    */   
/*    */   public BungeeTask(BungeeScheduler sched, int id, net.md_5.bungee.api.plugin.Plugin owner, Runnable task, long delay, long period, java.util.concurrent.TimeUnit unit)
/*    */   {
/* 26 */     this.sched = sched;
/* 27 */     this.id = id;
/* 28 */     this.owner = owner;
/* 29 */     this.task = task;
/* 30 */     this.delay = unit.toMillis(delay);
/* 31 */     this.period = unit.toMillis(period);
/*    */   }
/*    */   
/*    */ 
/*    */   public void cancel()
/*    */   {
/* 37 */     boolean wasRunning = this.running.getAndSet(false);
/*    */     
/* 39 */     if (wasRunning)
/*    */     {
/* 41 */       this.sched.cancel0(this);
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public void run()
/*    */   {
/* 48 */     if (this.delay > 0L)
/*    */     {
/*    */       try
/*    */       {
/* 52 */         Thread.sleep(this.delay);
/*    */       }
/*    */       catch (InterruptedException ex) {
/* 55 */         Thread.currentThread().interrupt();
/*    */       }
/*    */     }
/*    */     
/* 59 */     while (this.running.get())
/*    */     {
/*    */       try
/*    */       {
/* 63 */         this.task.run();
/*    */       }
/*    */       catch (Throwable t) {
/* 66 */         net.md_5.bungee.api.ProxyServer.getInstance().getLogger().log(java.util.logging.Level.SEVERE, String.format("Task %s encountered an exception", new Object[] { this }), t);
/*    */       }
/*    */       
/*    */ 
/* 70 */       if (this.period <= 0L) {
/*    */         break;
/*    */       }
/*    */       
/*    */ 
/*    */       try
/*    */       {
/* 77 */         Thread.sleep(this.period);
/*    */       }
/*    */       catch (InterruptedException ex) {
/* 80 */         Thread.currentThread().interrupt();
/*    */       }
/*    */     }
/*    */     
/* 84 */     cancel();
/*    */   }
/*    */ }


