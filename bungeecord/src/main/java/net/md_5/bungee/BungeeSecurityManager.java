package net.md_5.bungee;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.scheduler.GroupedThreadFactory;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.security.AccessControlException;
import java.security.Permission;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;

public class BungeeSecurityManager extends SecurityManager {
    private static final boolean ENFORCE = false;
    private final Set<String> seen = new HashSet<>();

    private void checkRestricted(String text) {
        Class[] context = getClassContext();
        int i = 2;
        if (i < context.length) {
            ClassLoader loader = context[i].getClassLoader();


            if ((loader != ClassLoader.getSystemClassLoader()) && (loader != null)) {


                AccessControlException ex = new AccessControlException("Plugin violation: " + text);


                StringWriter stack = new StringWriter();
                ex.printStackTrace(new PrintWriter(stack));
                if (this.seen.add(stack.toString())) {
                    ProxyServer.getInstance().getLogger().log(Level.WARNING, "Plugin performed restricted action, please inform them to use proper API methods: " + text, ex);
                }
            }
        }
    }


    public void checkExit(int status) {
        checkRestricted("Exit: Cannot close VM");
    }


    public void checkAccess(ThreadGroup g) {
        if (!(g instanceof GroupedThreadFactory.BungeeGroup)) {
            checkRestricted("Illegal thread group access");
        }
    }


    public void checkPermission(Permission perm, Object context) {
        checkPermission(perm);
    }


    public void checkPermission(Permission perm) {
        if ("setSecurityManager".equals(perm.getName())) {
            throw new AccessControlException("Restricted Action", perm);
        }
    }
}


