package net.md_5.bungee.protocol.packet;

import net.md_5.bungee.protocol.DefinedPacket;

import java.util.Objects;


public class PlayerListItem
        extends DefinedPacket {
    private Action action;
    private Item[] items;

    public String toString() {
        return "PlayerListItem(action=" + getAction() + ", items=" + java.util.Arrays.deepToString(getItems()) + ")";
    }

    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof PlayerListItem)) return false;
        PlayerListItem other = (PlayerListItem) o;
        if (!other.canEqual(this)) return false;
        Object this$action = getAction();
        Object other$action = other.getAction();
        if (!Objects.equals(this$action, other$action)) return false;
        return java.util.Arrays.deepEquals(getItems(), other.getItems());
    }

    protected boolean canEqual(Object other) {
        return other instanceof PlayerListItem;
    }

    public int hashCode() {
        int PRIME = 59;
        int result = 1;
        Object $action = getAction();
        result = result * 59 + ($action == null ? 43 : $action.hashCode());
        result = result * 59 + java.util.Arrays.deepHashCode(getItems());
        return result;
    }

    public Action getAction() {
        return this.action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public Item[] getItems() {
        return this.items;
    }

    public void setItems(Item[] items) {
        this.items = items;
    }

    public void read(io.netty.buffer.ByteBuf buf, net.md_5.bungee.protocol.ProtocolConstants.Direction direction, int protocolVersion) {
        this.action = Action.values()[DefinedPacket.readVarInt(buf)];
        this.items = new Item[DefinedPacket.readVarInt(buf)];
        for (int i = 0; i < this.items.length; i++) {
            Item item = this.items[i] = new Item();
            item.setUuid(DefinedPacket.readUUID(buf));
            switch (this.action) {
                case ADD_PLAYER:
                    item.username = DefinedPacket.readString(buf);
                    item.properties = new String[DefinedPacket.readVarInt(buf)][];
                    for (int j = 0; j < item.properties.length; j++) {
                        String name = DefinedPacket.readString(buf);
                        String value = DefinedPacket.readString(buf);
                        if (buf.readBoolean()) {


                            item.properties[j] = new String[]{name, value, DefinedPacket.readString(buf)};
                        } else {
                            item.properties[j] = new String[]{name, value};
                        }
                    }


                    item.gamemode = DefinedPacket.readVarInt(buf);
                    item.ping = DefinedPacket.readVarInt(buf);
                    if (buf.readBoolean()) {
                        item.displayName = DefinedPacket.readString(buf);
                    }
                    break;
                case UPDATE_GAMEMODE:
                    item.gamemode = DefinedPacket.readVarInt(buf);
                    break;
                case UPDATE_LATENCY:
                    item.ping = DefinedPacket.readVarInt(buf);
                    break;
                case UPDATE_DISPLAY_NAME:
                    if (buf.readBoolean()) {
                        item.displayName = DefinedPacket.readString(buf);
                    }
                    break;
            }
        }
    }

    public void write(io.netty.buffer.ByteBuf buf, net.md_5.bungee.protocol.ProtocolConstants.Direction direction, int protocolVersion) {
        DefinedPacket.writeVarInt(this.action.ordinal(), buf);
        DefinedPacket.writeVarInt(this.items.length, buf);
        for (Item item : this.items) {
            DefinedPacket.writeUUID(item.uuid, buf);
            switch (this.action) {
                case ADD_PLAYER:
                    DefinedPacket.writeString(item.username, buf);
                    DefinedPacket.writeVarInt(item.properties.length, buf);
                    for (String[] prop : item.properties) {
                        DefinedPacket.writeString(prop[0], buf);
                        DefinedPacket.writeString(prop[1], buf);
                        if (prop.length >= 3) {
                            buf.writeBoolean(true);
                            DefinedPacket.writeString(prop[2], buf);
                        } else {
                            buf.writeBoolean(false);
                        }
                    }
                    DefinedPacket.writeVarInt(item.gamemode, buf);
                    DefinedPacket.writeVarInt(item.ping, buf);
                    buf.writeBoolean(item.displayName != null);
                    if (item.displayName != null) {
                        DefinedPacket.writeString(item.displayName, buf);
                    }
                    break;
                case UPDATE_GAMEMODE:
                    DefinedPacket.writeVarInt(item.gamemode, buf);
                    break;
                case UPDATE_LATENCY:
                    DefinedPacket.writeVarInt(item.ping, buf);
                    break;
                case UPDATE_DISPLAY_NAME:
                    buf.writeBoolean(item.displayName != null);
                    if (item.displayName != null) {
                        DefinedPacket.writeString(item.displayName, buf);
                    }
                    break;
            }
        }
    }

    public void handle(net.md_5.bungee.protocol.AbstractPacketHandler handler)
            throws Exception {
        handler.handle(this);
    }


    public enum Action {
        ADD_PLAYER,
        UPDATE_GAMEMODE,
        UPDATE_LATENCY,
        UPDATE_DISPLAY_NAME,
        REMOVE_PLAYER;

        Action() {
        }
    }

    public static class Item {
        private java.util.UUID uuid;
        private String username;
        private String[][] properties;
        private int gamemode;
        private int ping;
        private String displayName;

        public boolean equals(Object o) {
            if (o == this) return true;
            if (!(o instanceof Item)) return false;
            Item other = (Item) o;
            if (!other.canEqual(this)) return false;
            Object this$uuid = getUuid();
            Object other$uuid = other.getUuid();
            if (!Objects.equals(this$uuid, other$uuid)) return false;
            Object this$username = getUsername();
            Object other$username = other.getUsername();
            if (!Objects.equals(this$username, other$username)) return false;
            if (!java.util.Arrays.deepEquals(getProperties(), other.getProperties())) return false;
            if (getGamemode() != other.getGamemode()) return false;
            if (getPing() != other.getPing()) return false;
            Object this$displayName = getDisplayName();
            Object other$displayName = other.getDisplayName();
            return Objects.equals(this$displayName, other$displayName);
        }

        protected boolean canEqual(Object other) {
            return other instanceof Item;
        }

        public int hashCode() {
            int PRIME = 59;
            int result = 1;
            Object $uuid = getUuid();
            result = result * 59 + ($uuid == null ? 43 : $uuid.hashCode());
            Object $username = getUsername();
            result = result * 59 + ($username == null ? 43 : $username.hashCode());
            result = result * 59 + java.util.Arrays.deepHashCode(getProperties());
            result = result * 59 + getGamemode();
            result = result * 59 + getPing();
            Object $displayName = getDisplayName();
            result = result * 59 + ($displayName == null ? 43 : $displayName.hashCode());
            return result;
        }

        public String toString() {
            return "PlayerListItem.Item(uuid=" + getUuid() + ", username=" + getUsername() + ", properties=" + java.util.Arrays.deepToString(getProperties()) + ", gamemode=" + getGamemode() + ", ping=" + getPing() + ", displayName=" + getDisplayName() + ")";
        }

        public java.util.UUID getUuid() {
            return this.uuid;
        }

        public void setUuid(java.util.UUID uuid) {
            this.uuid = uuid;
        }

        public String getUsername() {
            return this.username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String[][] getProperties() {
            return this.properties;
        }

        public void setProperties(String[][] properties) {
            this.properties = properties;
        }

        public int getGamemode() {
            return this.gamemode;
        }

        public void setGamemode(int gamemode) {
            this.gamemode = gamemode;
        }

        public int getPing() {
            return this.ping;
        }

        public void setPing(int ping) {
            this.ping = ping;
        }

        public String getDisplayName() {
            return this.displayName;
        }

        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }
    }
}


