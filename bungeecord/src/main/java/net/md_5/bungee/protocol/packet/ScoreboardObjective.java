/*    */ package net.md_5.bungee.protocol.packet;
/*    */ 
/*    */ import io.netty.buffer.ByteBuf;
/*    */ 
/*    */ public class ScoreboardObjective
/*    */   extends net.md_5.bungee.protocol.DefinedPacket
/*    */ {
/*    */   private String name;
/*    */   private String value;
/*    */   private HealthDisplay type;
/*    */   private byte action;
/*    */   
/* 13 */   public void setName(String name) { this.name = name; } public void setValue(String value) { this.value = value; } public void setType(HealthDisplay type) { this.type = type; } public void setAction(byte action) { this.action = action; } public String toString() { return "ScoreboardObjective(name=" + getName() + ", value=" + getValue() + ", type=" + getType() + ", action=" + getAction() + ")"; }
/*    */   
/* 15 */   public ScoreboardObjective(String name, String value, HealthDisplay type, byte action) { this.name = name;this.value = value;this.type = type;this.action = action; }
/* 16 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof ScoreboardObjective)) return false; ScoreboardObjective other = (ScoreboardObjective)o; if (!other.canEqual(this)) return false; Object this$name = getName();Object other$name = other.getName(); if (this$name == null ? other$name != null : !this$name.equals(other$name)) return false; Object this$value = getValue();Object other$value = other.getValue(); if (this$value == null ? other$value != null : !this$value.equals(other$value)) return false; Object this$type = getType();Object other$type = other.getType(); if (this$type == null ? other$type != null : !this$type.equals(other$type)) return false; return getAction() == other.getAction(); } protected boolean canEqual(Object other) { return other instanceof ScoreboardObjective; } public int hashCode() { int PRIME = 59;int result = 1;Object $name = getName();result = result * 59 + ($name == null ? 43 : $name.hashCode());Object $value = getValue();result = result * 59 + ($value == null ? 43 : $value.hashCode());Object $type = getType();result = result * 59 + ($type == null ? 43 : $type.hashCode());result = result * 59 + getAction();return result;
/*    */   }
/*    */   
/*    */ 
/* 20 */   public String getName() { return this.name; }
/* 21 */   public String getValue() { return this.value; }
/* 22 */   public HealthDisplay getType() { return this.type; }
/*    */   
/*    */   public byte getAction()
/*    */   {
/* 26 */     return this.action;
/*    */   }
/*    */   
/*    */   public void read(ByteBuf buf, net.md_5.bungee.protocol.ProtocolConstants.Direction direction, int protocolVersion)
/*    */   {
/* 31 */     this.name = readString(buf);
/* 32 */     this.action = buf.readByte();
/* 33 */     if ((this.action == 0) || (this.action == 2))
/*    */     {
/* 35 */       this.value = readString(buf);
/* 36 */       if (protocolVersion >= 393)
/*    */       {
/* 38 */         this.type = HealthDisplay.values()[readVarInt(buf)];
/*    */       }
/*    */       else {
/* 41 */         this.type = HealthDisplay.fromString(readString(buf));
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public void write(ByteBuf buf, net.md_5.bungee.protocol.ProtocolConstants.Direction direction, int protocolVersion)
/*    */   {
/* 49 */     writeString(this.name, buf);
/* 50 */     buf.writeByte(this.action);
/* 51 */     if ((this.action == 0) || (this.action == 2))
/*    */     {
/* 53 */       writeString(this.value, buf);
/* 54 */       if (protocolVersion >= 393)
/*    */       {
/* 56 */         writeVarInt(this.type.ordinal(), buf);
/*    */       }
/*    */       else {
/* 59 */         writeString(this.type.toString(), buf);
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */   public void handle(net.md_5.bungee.protocol.AbstractPacketHandler handler)
/*    */     throws Exception
/*    */   {
/* 67 */     handler.handle(this);
/*    */   }
/*    */   
/*    */   public ScoreboardObjective() {}
/*    */   
/*    */   public static enum HealthDisplay {
/* 73 */     INTEGER,  HEARTS;
/*    */     
/*    */     private HealthDisplay() {}
/*    */     
/*    */     public String toString() {
/* 78 */       return super.toString().toLowerCase(java.util.Locale.ROOT);
/*    */     }
/*    */     
/*    */     public static HealthDisplay fromString(String s)
/*    */     {
/* 83 */       return valueOf(s.toUpperCase(java.util.Locale.ROOT));
/*    */     }
/*    */   }
/*    */ }


