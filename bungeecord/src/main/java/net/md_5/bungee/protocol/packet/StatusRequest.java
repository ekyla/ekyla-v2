/*    */ package net.md_5.bungee.protocol.packet;
/*    */ 
/*    */ import io.netty.buffer.ByteBuf;
/*    */ import net.md_5.bungee.protocol.AbstractPacketHandler;
/*    */ 
/*    */ 
/*    */ public class StatusRequest
/*    */   extends net.md_5.bungee.protocol.DefinedPacket
/*    */ {
/* 10 */   public String toString() { return "StatusRequest()"; }
/*    */   
/* 12 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof StatusRequest)) return false; StatusRequest other = (StatusRequest)o;return other.canEqual(this); } protected boolean canEqual(Object other) { return other instanceof StatusRequest; } public int hashCode() { int result = 1;return result;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void handle(AbstractPacketHandler handler)
/*    */     throws Exception
/*    */   {
/* 29 */     handler.handle(this);
/*    */   }
/*    */   
/*    */   public void read(ByteBuf buf) {}
/*    */   
/*    */   public void write(ByteBuf buf) {}
/*    */ }


