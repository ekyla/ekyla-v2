/*    */ package net.md_5.bungee.protocol.packet;
/*    */ 
/*    */ import io.netty.buffer.ByteBuf;
/*    */ import net.md_5.bungee.protocol.AbstractPacketHandler;
/*    */ 
/*    */ 
/*    */ public class LegacyHandshake
/*    */   extends net.md_5.bungee.protocol.DefinedPacket
/*    */ {
/* 10 */   public String toString() { return "LegacyHandshake()"; }
/*    */   
/* 12 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof LegacyHandshake)) return false; LegacyHandshake other = (LegacyHandshake)o;return other.canEqual(this); } protected boolean canEqual(Object other) { return other instanceof LegacyHandshake; } public int hashCode() { int result = 1;return result;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */   public void read(ByteBuf buf)
/*    */   {
/* 19 */     throw new UnsupportedOperationException("Not supported yet.");
/*    */   }
/*    */   
/*    */ 
/*    */   public void write(ByteBuf buf)
/*    */   {
/* 25 */     throw new UnsupportedOperationException("Not supported yet.");
/*    */   }
/*    */   
/*    */   public void handle(AbstractPacketHandler handler)
/*    */     throws Exception
/*    */   {
/* 31 */     handler.handle(this);
/*    */   }
/*    */ }


