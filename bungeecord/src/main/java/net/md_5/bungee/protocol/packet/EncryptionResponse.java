/*    */ package net.md_5.bungee.protocol.packet;
/*    */ 
/*    */ import java.util.Arrays;
/*    */ 
/*    */ 
/*    */ public class EncryptionResponse
/*    */   extends net.md_5.bungee.protocol.DefinedPacket
/*    */ {
/*    */   private byte[] sharedSecret;
/*    */   private byte[] verifyToken;
/*    */   
/* 12 */   public void setSharedSecret(byte[] sharedSecret) { this.sharedSecret = sharedSecret; } public void setVerifyToken(byte[] verifyToken) { this.verifyToken = verifyToken; } public String toString() { return "EncryptionResponse(sharedSecret=" + Arrays.toString(getSharedSecret()) + ", verifyToken=" + Arrays.toString(getVerifyToken()) + ")"; }
/*    */   
/* 14 */   public EncryptionResponse(byte[] sharedSecret, byte[] verifyToken) { this.sharedSecret = sharedSecret;this.verifyToken = verifyToken; }
/* 15 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof EncryptionResponse)) return false; EncryptionResponse other = (EncryptionResponse)o; if (!other.canEqual(this)) return false; if (!Arrays.equals(getSharedSecret(), other.getSharedSecret())) return false; return Arrays.equals(getVerifyToken(), other.getVerifyToken()); } protected boolean canEqual(Object other) { return other instanceof EncryptionResponse; } public int hashCode() { int PRIME = 59;int result = 1;result = result * 59 + Arrays.hashCode(getSharedSecret());result = result * 59 + Arrays.hashCode(getVerifyToken());return result;
/*    */   }
/*    */   
/*    */ 
/* 19 */   public byte[] getSharedSecret() { return this.sharedSecret; }
/* 20 */   public byte[] getVerifyToken() { return this.verifyToken; }
/*    */   
/*    */ 
/*    */   public void read(io.netty.buffer.ByteBuf buf, net.md_5.bungee.protocol.ProtocolConstants.Direction direction, int protocolVersion)
/*    */   {
/* 25 */     this.sharedSecret = readArray(buf, 128);
/* 26 */     this.verifyToken = readArray(buf, 128);
/*    */   }
/*    */   
/*    */ 
/*    */   public void write(io.netty.buffer.ByteBuf buf, net.md_5.bungee.protocol.ProtocolConstants.Direction direction, int protocolVersion)
/*    */   {
/* 32 */     writeArray(this.sharedSecret, buf);
/* 33 */     writeArray(this.verifyToken, buf);
/*    */   }
/*    */   
/*    */   public void handle(net.md_5.bungee.protocol.AbstractPacketHandler handler)
/*    */     throws Exception
/*    */   {
/* 39 */     handler.handle(this);
/*    */   }
/*    */   
/*    */   public EncryptionResponse() {}
/*    */ }


