/*    */ package net.md_5.bungee.protocol;
/*    */ 
/*    */ import io.netty.buffer.ByteBuf;
/*    */ 
/*    */ public class MinecraftEncoder extends io.netty.handler.codec.MessageToByteEncoder<DefinedPacket> { private Protocol protocol;
/*    */   private boolean server;
/*    */   private int protocolVersion;
/*    */   
/*  9 */   public MinecraftEncoder(Protocol protocol, boolean server, int protocolVersion) { this.protocol = protocol;this.server = server;this.protocolVersion = protocolVersion;
/*    */   }
/*    */   
/*    */   public void setProtocol(Protocol protocol) {
/* 13 */     this.protocol = protocol;
/*    */   }
/*    */   
/* 16 */   public void setProtocolVersion(int protocolVersion) { this.protocolVersion = protocolVersion; }
/*    */   
/*    */ 
/*    */   protected void encode(io.netty.channel.ChannelHandlerContext ctx, DefinedPacket msg, ByteBuf out)
/*    */     throws Exception
/*    */   {
/* 22 */     Protocol.DirectionData prot = this.server ? this.protocol.TO_CLIENT : this.protocol.TO_SERVER;
/* 23 */     DefinedPacket.writeVarInt(prot.getId(msg.getClass(), this.protocolVersion), out);
/* 24 */     msg.write(out, prot.getDirection(), this.protocolVersion);
/*    */   }
/*    */ }


