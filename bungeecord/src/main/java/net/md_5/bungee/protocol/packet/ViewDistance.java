/*    */ package net.md_5.bungee.protocol.packet;
/*    */ 
/*    */ import io.netty.buffer.ByteBuf;
/*    */ 
/*    */ 
/*    */ public class ViewDistance
/*    */   extends net.md_5.bungee.protocol.DefinedPacket
/*    */ {
/*    */   private int distance;
/*    */   
/* 11 */   public void setDistance(int distance) { this.distance = distance; } public String toString() { return "ViewDistance(distance=" + getDistance() + ")"; }
/*    */   
/* 13 */   public ViewDistance(int distance) { this.distance = distance; }
/* 14 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof ViewDistance)) return false; ViewDistance other = (ViewDistance)o; if (!other.canEqual(this)) return false; return getDistance() == other.getDistance(); } protected boolean canEqual(Object other) { return other instanceof ViewDistance; } public int hashCode() { int PRIME = 59;int result = 1;result = result * 59 + getDistance();return result;
/*    */   }
/*    */   
/*    */   public int getDistance() {
/* 18 */     return this.distance;
/*    */   }
/*    */   
/*    */   public void read(ByteBuf buf)
/*    */   {
/* 23 */     this.distance = net.md_5.bungee.protocol.DefinedPacket.readVarInt(buf);
/*    */   }
/*    */   
/*    */ 
/*    */   public void write(ByteBuf buf)
/*    */   {
/* 29 */     net.md_5.bungee.protocol.DefinedPacket.writeVarInt(this.distance, buf);
/*    */   }
/*    */   
/*    */   public void handle(net.md_5.bungee.protocol.AbstractPacketHandler handler)
/*    */     throws Exception
/*    */   {
/* 35 */     handler.handle(this);
/*    */   }
/*    */   
/*    */   public ViewDistance() {}
/*    */ }


