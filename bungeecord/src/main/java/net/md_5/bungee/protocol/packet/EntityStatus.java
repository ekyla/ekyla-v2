/*    */ package net.md_5.bungee.protocol.packet;
/*    */ 
/*    */ import io.netty.buffer.ByteBuf;
/*    */ 
/*    */ public class EntityStatus extends net.md_5.bungee.protocol.DefinedPacket {
/*    */   public static final byte DEBUG_INFO_REDUCED = 22;
/*    */   public static final byte DEBUG_INFO_NORMAL = 23;
/*    */   private int entityId;
/*    */   private byte status;
/*    */   
/* 11 */   public void setEntityId(int entityId) { this.entityId = entityId; } public void setStatus(byte status) { this.status = status; } public String toString() { return "EntityStatus(entityId=" + getEntityId() + ", status=" + getStatus() + ")"; }
/*    */   
/* 13 */   public EntityStatus(int entityId, byte status) { this.entityId = entityId;this.status = status; }
/* 14 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof EntityStatus)) return false; EntityStatus other = (EntityStatus)o; if (!other.canEqual(this)) return false; if (getEntityId() != other.getEntityId()) return false; return getStatus() == other.getStatus(); } protected boolean canEqual(Object other) { return other instanceof EntityStatus; } public int hashCode() { int PRIME = 59;int result = 1;result = result * 59 + getEntityId();result = result * 59 + getStatus();return result;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 21 */   public int getEntityId() { return this.entityId; }
/* 22 */   public byte getStatus() { return this.status; }
/*    */   
/*    */ 
/*    */   public void read(ByteBuf buf)
/*    */   {
/* 27 */     this.entityId = buf.readInt();
/* 28 */     this.status = buf.readByte();
/*    */   }
/*    */   
/*    */ 
/*    */   public void write(ByteBuf buf)
/*    */   {
/* 34 */     buf.writeInt(this.entityId);
/* 35 */     buf.writeByte(this.status);
/*    */   }
/*    */   
/*    */   public void handle(net.md_5.bungee.protocol.AbstractPacketHandler handler)
/*    */     throws Exception
/*    */   {
/* 41 */     handler.handle(this);
/*    */   }
/*    */   
/*    */   public EntityStatus() {}
/*    */ }


