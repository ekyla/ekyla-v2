/*     */ package net.md_5.bungee.protocol.packet;
/*     */ 
/*     */ import io.netty.buffer.ByteBuf;
/*     */ 
/*     */ public class BossBar
/*     */   extends net.md_5.bungee.protocol.DefinedPacket
/*     */ {
/*     */   private java.util.UUID uuid;
/*     */   private int action;
/*     */   private String title;
/*     */   
/*  12 */   public void setUuid(java.util.UUID uuid) { this.uuid = uuid; } public void setAction(int action) { this.action = action; } public void setTitle(String title) { this.title = title; } public void setHealth(float health) { this.health = health; } public void setColor(int color) { this.color = color; } public void setDivision(int division) { this.division = division; } public void setFlags(byte flags) { this.flags = flags; } public String toString() { return "BossBar(uuid=" + getUuid() + ", action=" + getAction() + ", title=" + getTitle() + ", health=" + getHealth() + ", color=" + getColor() + ", division=" + getDivision() + ", flags=" + getFlags() + ")"; }
/*     */   
/*  14 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof BossBar)) return false; BossBar other = (BossBar)o; if (!other.canEqual(this)) return false; Object this$uuid = getUuid();Object other$uuid = other.getUuid(); if (this$uuid == null ? other$uuid != null : !this$uuid.equals(other$uuid)) return false; if (getAction() != other.getAction()) return false; Object this$title = getTitle();Object other$title = other.getTitle(); if (this$title == null ? other$title != null : !this$title.equals(other$title)) return false; if (Float.compare(getHealth(), other.getHealth()) != 0) return false; if (getColor() != other.getColor()) return false; if (getDivision() != other.getDivision()) return false; return getFlags() == other.getFlags(); } protected boolean canEqual(Object other) { return other instanceof BossBar; } public int hashCode() { int PRIME = 59;int result = 1;Object $uuid = getUuid();result = result * 59 + ($uuid == null ? 43 : $uuid.hashCode());result = result * 59 + getAction();Object $title = getTitle();result = result * 59 + ($title == null ? 43 : $title.hashCode());result = result * 59 + Float.floatToIntBits(getHealth());result = result * 59 + getColor();result = result * 59 + getDivision();result = result * 59 + getFlags();return result; }
/*     */   
/*     */   private float health;
/*     */   
/*  18 */   public java.util.UUID getUuid() { return this.uuid; }
/*  19 */   public int getAction() { return this.action; }
/*  20 */   public String getTitle() { return this.title; }
/*  21 */   public float getHealth() { return this.health; }
/*  22 */   public int getColor() { return this.color; }
/*  23 */   public int getDivision() { return this.division; }
/*  24 */   public byte getFlags() { return this.flags; }
/*     */   
/*     */   private int color;
/*     */   public BossBar(java.util.UUID uuid, int action) {
/*  28 */     this.uuid = uuid;
/*  29 */     this.action = action;
/*     */   }
/*     */   
/*     */   private int division;
/*     */   private byte flags;
/*     */   public void read(ByteBuf buf, net.md_5.bungee.protocol.ProtocolConstants.Direction direction, int protocolVersion) {
/*  35 */     this.uuid = readUUID(buf);
/*  36 */     this.action = readVarInt(buf);
/*     */     
/*  38 */     switch (this.action)
/*     */     {
/*     */ 
/*     */     case 0: 
/*  42 */       this.title = readString(buf);
/*  43 */       this.health = buf.readFloat();
/*  44 */       this.color = readVarInt(buf);
/*  45 */       this.division = readVarInt(buf);
/*  46 */       this.flags = buf.readByte();
/*  47 */       break;
/*     */     
/*     */     case 2: 
/*  50 */       this.health = buf.readFloat();
/*  51 */       break;
/*     */     
/*     */     case 3: 
/*  54 */       this.title = readString(buf);
/*  55 */       break;
/*     */     
/*     */     case 4: 
/*  58 */       this.color = readVarInt(buf);
/*  59 */       this.division = readVarInt(buf);
/*  60 */       break;
/*     */     
/*     */     case 5: 
/*  63 */       this.flags = buf.readByte();
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */ 
/*     */   public void write(ByteBuf buf, net.md_5.bungee.protocol.ProtocolConstants.Direction direction, int protocolVersion)
/*     */   {
/*  71 */     writeUUID(this.uuid, buf);
/*  72 */     writeVarInt(this.action, buf);
/*     */     
/*  74 */     switch (this.action)
/*     */     {
/*     */ 
/*     */     case 0: 
/*  78 */       writeString(this.title, buf);
/*  79 */       buf.writeFloat(this.health);
/*  80 */       writeVarInt(this.color, buf);
/*  81 */       writeVarInt(this.division, buf);
/*  82 */       buf.writeByte(this.flags);
/*  83 */       break;
/*     */     
/*     */     case 2: 
/*  86 */       buf.writeFloat(this.health);
/*  87 */       break;
/*     */     
/*     */     case 3: 
/*  90 */       writeString(this.title, buf);
/*  91 */       break;
/*     */     
/*     */     case 4: 
/*  94 */       writeVarInt(this.color, buf);
/*  95 */       writeVarInt(this.division, buf);
/*  96 */       break;
/*     */     
/*     */     case 5: 
/*  99 */       buf.writeByte(this.flags);
/*     */     }
/*     */     
/*     */   }
/*     */   
/*     */   public void handle(net.md_5.bungee.protocol.AbstractPacketHandler handler)
/*     */     throws Exception
/*     */   {
/* 107 */     handler.handle(this);
/*     */   }
/*     */   
/*     */   public BossBar() {}
/*     */ }


