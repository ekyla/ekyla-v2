/*    */ package net.md_5.bungee.protocol;
/*    */ 
/*    */ import java.util.Arrays;
/*    */ import java.util.List;
/*    */ 
/*    */ 
/*    */ public class ProtocolConstants
/*    */ {
/*    */   public static final int MINECRAFT_1_8 = 47;
/*    */   public static final int MINECRAFT_1_9 = 107;
/*    */   public static final int MINECRAFT_1_9_1 = 108;
/*    */   public static final int MINECRAFT_1_9_2 = 109;
/*    */   public static final int MINECRAFT_1_9_4 = 110;
/*    */   public static final int MINECRAFT_1_10 = 210;
/*    */   public static final int MINECRAFT_1_11 = 315;
/*    */   public static final int MINECRAFT_1_11_1 = 316;
/*    */   public static final int MINECRAFT_1_12 = 335;
/*    */   public static final int MINECRAFT_1_12_1 = 338;
/*    */   public static final int MINECRAFT_1_12_2 = 340;
/*    */   public static final int MINECRAFT_1_13 = 393;
/*    */   public static final int MINECRAFT_1_13_1 = 401;
/*    */   public static final int MINECRAFT_1_13_2 = 404;
/*    */   public static final int MINECRAFT_1_14 = 477;
/*    */   public static final int MINECRAFT_1_14_1 = 480;
/*    */   public static final int MINECRAFT_1_14_2 = 485;
/*    */   public static final int MINECRAFT_1_14_3 = 490;
/*    */   public static final int MINECRAFT_1_14_4 = 498;
/* 28 */   public static final List<String> SUPPORTED_VERSIONS = Arrays.asList(new String[] { "1.8.x", "1.9.x", "1.10.x", "1.11.x", "1.12.x", "1.13.x", "1.14.x" });
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 37 */   public static final List<Integer> SUPPORTED_VERSION_IDS = Arrays.asList(new Integer[] {
/* 38 */     Integer.valueOf(47), 
/* 39 */     Integer.valueOf(107), 
/* 40 */     Integer.valueOf(108), 
/* 41 */     Integer.valueOf(109), 
/* 42 */     Integer.valueOf(110), 
/* 43 */     Integer.valueOf(210), 
/* 44 */     Integer.valueOf(315), 
/* 45 */     Integer.valueOf(316), 
/* 46 */     Integer.valueOf(335), 
/* 47 */     Integer.valueOf(338), 
/* 48 */     Integer.valueOf(340), 
/* 49 */     Integer.valueOf(393), 
/* 50 */     Integer.valueOf(401), 
/* 51 */     Integer.valueOf(404), 
/* 52 */     Integer.valueOf(477), 
/* 53 */     Integer.valueOf(480), 
/* 54 */     Integer.valueOf(485), 
/* 55 */     Integer.valueOf(490), 
/* 56 */     Integer.valueOf(498) });
/*    */   
/*    */ 
/*    */ 
/*    */   public static enum Direction
/*    */   {
/* 62 */     TO_CLIENT,  TO_SERVER;
/*    */     
/*    */     private Direction() {}
/*    */   }
/*    */ }


