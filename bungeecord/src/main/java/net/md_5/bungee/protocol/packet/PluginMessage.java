package net.md_5.bungee.protocol.packet;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import io.netty.buffer.ByteBuf;
import net.md_5.bungee.protocol.AbstractPacketHandler;
import net.md_5.bungee.protocol.DefinedPacket;
import net.md_5.bungee.protocol.ProtocolConstants;

import java.io.ByteArrayInputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.util.Arrays;
import java.util.Locale;
import java.util.Objects;
import java.util.function.Predicate;


public class PluginMessage
        extends DefinedPacket {
    public static final Function<String, String> MODERNISE = tag -> {

        if (tag.equals("BungeeCord")) {
            return "bungeecord:main";
        }
        if (tag.equals("bungeecord:main")) {
            return "BungeeCord";
        }


        if (tag.indexOf(':') != -1) {
            return tag;
        }

        return "legacy:" + tag.toLowerCase(Locale.ROOT);
    };
    public static final Predicate<PluginMessage> SHOULD_RELAY = input -> ((input.getTag().equals("REGISTER")) || (input.getTag().equals("minecraft:register")) || (input.getTag().equals("MC|Brand")) || (input.getTag().equals("minecraft:brand"))) && (input.getData().length < 127);
    private String tag;
    private byte[] data;
    private boolean allowExtendedPacket = false;

    public PluginMessage(String tag, byte[] data, boolean allowExtendedPacket) {
        this.tag = tag;
        this.data = data;
        this.allowExtendedPacket = allowExtendedPacket;
    }

    public PluginMessage() {
    }

    public String toString() {
        return "PluginMessage(tag=" + getTag() + ", data=" + Arrays.toString(getData()) + ", allowExtendedPacket=" + isAllowExtendedPacket() + ")";
    }

    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof PluginMessage)) return false;
        PluginMessage other = (PluginMessage) o;
        if (!other.canEqual(this)) return false;
        Object this$tag = getTag();
        Object other$tag = other.getTag();
        if (!Objects.equals(this$tag, other$tag)) return false;
        if (!Arrays.equals(getData(), other.getData())) return false;
        return isAllowExtendedPacket() == other.isAllowExtendedPacket();
    }

    protected boolean canEqual(Object other) {
        return other instanceof PluginMessage;
    }

    public int hashCode() {
        int PRIME = 59;
        int result = 1;
        Object $tag = getTag();
        result = result * 59 + ($tag == null ? 43 : $tag.hashCode());
        result = result * 59 + Arrays.hashCode(getData());
        result = result * 59 + (isAllowExtendedPacket() ? 79 : 97);
        return result;
    }

    public String getTag() {
        return this.tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public byte[] getData() {
        return this.data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public boolean isAllowExtendedPacket() {
        return this.allowExtendedPacket;
    }

    public void setAllowExtendedPacket(boolean allowExtendedPacket) {
        this.allowExtendedPacket = allowExtendedPacket;
    }

    public void read(ByteBuf buf, ProtocolConstants.Direction direction, int protocolVersion) {
        this.tag = (protocolVersion >= 393 ? (String) MODERNISE.apply(readString(buf)) : readString(buf));
        int maxSize = direction == ProtocolConstants.Direction.TO_SERVER ? 32767 : 1048576;
        Preconditions.checkArgument(buf.readableBytes() < maxSize);
        this.data = new byte[buf.readableBytes()];
        buf.readBytes(this.data);
    }

    public void write(ByteBuf buf, ProtocolConstants.Direction direction, int protocolVersion) {
        writeString(protocolVersion >= 393 ? MODERNISE.apply(this.tag) : this.tag, buf);
        buf.writeBytes(this.data);
    }

    public void handle(AbstractPacketHandler handler)
            throws Exception {
        handler.handle(this);
    }

    public DataInput getStream() {
        return new DataInputStream(new ByteArrayInputStream(this.data));
    }
}


