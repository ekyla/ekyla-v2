/*    */ package net.md_5.bungee.protocol.packet;
/*    */ 
/*    */ import io.netty.buffer.ByteBuf;
/*    */ 
/*    */ public class ScoreboardDisplay
/*    */   extends net.md_5.bungee.protocol.DefinedPacket
/*    */ {
/*    */   private byte position;
/*    */   private String name;
/*    */   
/* 11 */   public void setPosition(byte position) { this.position = position; } public void setName(String name) { this.name = name; } public String toString() { return "ScoreboardDisplay(position=" + getPosition() + ", name=" + getName() + ")"; }
/*    */   
/* 13 */   public ScoreboardDisplay(byte position, String name) { this.position = position;this.name = name; }
/* 14 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof ScoreboardDisplay)) return false; ScoreboardDisplay other = (ScoreboardDisplay)o; if (!other.canEqual(this)) return false; if (getPosition() != other.getPosition()) return false; Object this$name = getName();Object other$name = other.getName();return this$name == null ? other$name == null : this$name.equals(other$name); } protected boolean canEqual(Object other) { return other instanceof ScoreboardDisplay; } public int hashCode() { int PRIME = 59;int result = 1;result = result * 59 + getPosition();Object $name = getName();result = result * 59 + ($name == null ? 43 : $name.hashCode());return result;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/* 21 */   public byte getPosition() { return this.position; }
/* 22 */   public String getName() { return this.name; }
/*    */   
/*    */ 
/*    */   public void read(ByteBuf buf)
/*    */   {
/* 27 */     this.position = buf.readByte();
/* 28 */     this.name = readString(buf);
/*    */   }
/*    */   
/*    */ 
/*    */   public void write(ByteBuf buf)
/*    */   {
/* 34 */     buf.writeByte(this.position);
/* 35 */     writeString(this.name, buf);
/*    */   }
/*    */   
/*    */   public void handle(net.md_5.bungee.protocol.AbstractPacketHandler handler)
/*    */     throws Exception
/*    */   {
/* 41 */     handler.handle(this);
/*    */   }
/*    */   
/*    */   public ScoreboardDisplay() {}
/*    */ }


