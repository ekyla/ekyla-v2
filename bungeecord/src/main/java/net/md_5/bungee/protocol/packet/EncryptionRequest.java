/*    */ package net.md_5.bungee.protocol.packet;
/*    */ 
/*    */ import java.util.Arrays;
/*    */ 
/*    */ public class EncryptionRequest
/*    */   extends net.md_5.bungee.protocol.DefinedPacket
/*    */ {
/*    */   private String serverId;
/*    */   private byte[] publicKey;
/*    */   private byte[] verifyToken;
/*    */   
/* 12 */   public void setServerId(String serverId) { this.serverId = serverId; } public void setPublicKey(byte[] publicKey) { this.publicKey = publicKey; } public void setVerifyToken(byte[] verifyToken) { this.verifyToken = verifyToken; } public String toString() { return "EncryptionRequest(serverId=" + getServerId() + ", publicKey=" + Arrays.toString(getPublicKey()) + ", verifyToken=" + Arrays.toString(getVerifyToken()) + ")"; }
/*    */   
/* 14 */   public EncryptionRequest(String serverId, byte[] publicKey, byte[] verifyToken) { this.serverId = serverId;this.publicKey = publicKey;this.verifyToken = verifyToken; }
/* 15 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof EncryptionRequest)) return false; EncryptionRequest other = (EncryptionRequest)o; if (!other.canEqual(this)) return false; Object this$serverId = getServerId();Object other$serverId = other.getServerId(); if (this$serverId == null ? other$serverId != null : !this$serverId.equals(other$serverId)) return false; if (!Arrays.equals(getPublicKey(), other.getPublicKey())) return false; return Arrays.equals(getVerifyToken(), other.getVerifyToken()); } protected boolean canEqual(Object other) { return other instanceof EncryptionRequest; } public int hashCode() { int PRIME = 59;int result = 1;Object $serverId = getServerId();result = result * 59 + ($serverId == null ? 43 : $serverId.hashCode());result = result * 59 + Arrays.hashCode(getPublicKey());result = result * 59 + Arrays.hashCode(getVerifyToken());return result;
/*    */   }
/*    */   
/*    */ 
/* 19 */   public String getServerId() { return this.serverId; }
/* 20 */   public byte[] getPublicKey() { return this.publicKey; }
/* 21 */   public byte[] getVerifyToken() { return this.verifyToken; }
/*    */   
/*    */ 
/*    */   public void read(io.netty.buffer.ByteBuf buf, net.md_5.bungee.protocol.ProtocolConstants.Direction direction, int protocolVersion)
/*    */   {
/* 26 */     this.serverId = readString(buf);
/* 27 */     this.publicKey = readArray(buf);
/* 28 */     this.verifyToken = readArray(buf);
/*    */   }
/*    */   
/*    */ 
/*    */   public void write(io.netty.buffer.ByteBuf buf, net.md_5.bungee.protocol.ProtocolConstants.Direction direction, int protocolVersion)
/*    */   {
/* 34 */     writeString(this.serverId, buf);
/* 35 */     writeArray(this.publicKey, buf);
/* 36 */     writeArray(this.verifyToken, buf);
/*    */   }
/*    */   
/*    */   public void handle(net.md_5.bungee.protocol.AbstractPacketHandler handler)
/*    */     throws Exception
/*    */   {
/* 42 */     handler.handle(this);
/*    */   }
/*    */   
/*    */   public EncryptionRequest() {}
/*    */ }


