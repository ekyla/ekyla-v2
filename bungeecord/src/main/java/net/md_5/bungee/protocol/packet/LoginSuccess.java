/*    */ package net.md_5.bungee.protocol.packet;
/*    */ 
/*    */ import io.netty.buffer.ByteBuf;
/*    */ 
/*    */ public class LoginSuccess
/*    */   extends net.md_5.bungee.protocol.DefinedPacket
/*    */ {
/*    */   private String uuid;
/*    */   private String username;
/*    */   
/* 11 */   public void setUuid(String uuid) { this.uuid = uuid; } public void setUsername(String username) { this.username = username; } public String toString() { return "LoginSuccess(uuid=" + getUuid() + ", username=" + getUsername() + ")"; }
/*    */   
/* 13 */   public LoginSuccess(String uuid, String username) { this.uuid = uuid;this.username = username; }
/* 14 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof LoginSuccess)) return false; LoginSuccess other = (LoginSuccess)o; if (!other.canEqual(this)) return false; Object this$uuid = getUuid();Object other$uuid = other.getUuid(); if (this$uuid == null ? other$uuid != null : !this$uuid.equals(other$uuid)) return false; Object this$username = getUsername();Object other$username = other.getUsername();return this$username == null ? other$username == null : this$username.equals(other$username); } protected boolean canEqual(Object other) { return other instanceof LoginSuccess; } public int hashCode() { int PRIME = 59;int result = 1;Object $uuid = getUuid();result = result * 59 + ($uuid == null ? 43 : $uuid.hashCode());Object $username = getUsername();result = result * 59 + ($username == null ? 43 : $username.hashCode());return result;
/*    */   }
/*    */   
/*    */ 
/* 18 */   public String getUuid() { return this.uuid; }
/* 19 */   public String getUsername() { return this.username; }
/*    */   
/*    */ 
/*    */   public void read(ByteBuf buf)
/*    */   {
/* 24 */     this.uuid = readString(buf);
/* 25 */     this.username = readString(buf);
/*    */   }
/*    */   
/*    */ 
/*    */   public void write(ByteBuf buf)
/*    */   {
/* 31 */     writeString(this.uuid, buf);
/* 32 */     writeString(this.username, buf);
/*    */   }
/*    */   
/*    */   public void handle(net.md_5.bungee.protocol.AbstractPacketHandler handler)
/*    */     throws Exception
/*    */   {
/* 38 */     handler.handle(this);
/*    */   }
/*    */   
/*    */   public LoginSuccess() {}
/*    */ }


