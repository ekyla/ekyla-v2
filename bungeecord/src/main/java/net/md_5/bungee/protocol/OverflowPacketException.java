/*   */ package net.md_5.bungee.protocol;
/*   */ 
/*   */ public class OverflowPacketException
/*   */   extends RuntimeException
/*   */ {
/*   */   public OverflowPacketException(String message)
/*   */   {
/* 8 */     super(message);
/*   */   }
/*   */ }


