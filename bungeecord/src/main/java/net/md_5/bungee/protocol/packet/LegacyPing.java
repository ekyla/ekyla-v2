/*    */ package net.md_5.bungee.protocol.packet;
/*    */ 
/*    */ import io.netty.buffer.ByteBuf;
/*    */ 
/*    */ public class LegacyPing
/*    */   extends net.md_5.bungee.protocol.DefinedPacket
/*    */ {
/*    */   private final boolean v1_5;
/*    */   
/* 10 */   public String toString() { return "LegacyPing(v1_5=" + isV1_5() + ")"; }
/* 11 */   public LegacyPing(boolean v1_5) { this.v1_5 = v1_5; }
/* 12 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof LegacyPing)) return false; LegacyPing other = (LegacyPing)o; if (!other.canEqual(this)) return false; return isV1_5() == other.isV1_5(); } protected boolean canEqual(Object other) { return other instanceof LegacyPing; } public int hashCode() { int PRIME = 59;int result = 1;result = result * 59 + (isV1_5() ? 79 : 97);return result;
/*    */   }
/*    */   
/*    */   public boolean isV1_5() {
/* 16 */     return this.v1_5;
/*    */   }
/*    */   
/*    */   public void read(ByteBuf buf)
/*    */   {
/* 21 */     throw new UnsupportedOperationException("Not supported yet.");
/*    */   }
/*    */   
/*    */ 
/*    */   public void write(ByteBuf buf)
/*    */   {
/* 27 */     throw new UnsupportedOperationException("Not supported yet.");
/*    */   }
/*    */   
/*    */   public void handle(net.md_5.bungee.protocol.AbstractPacketHandler handler)
/*    */     throws Exception
/*    */   {
/* 33 */     handler.handle(this);
/*    */   }
/*    */ }


