/*    */ package net.md_5.bungee.protocol;
/*    */ 
/*    */ public class BadPacketException
/*    */   extends RuntimeException
/*    */ {
/*    */   public BadPacketException(String message)
/*    */   {
/*  8 */     super(message);
/*    */   }
/*    */   
/*    */   public BadPacketException(String message, Throwable cause)
/*    */   {
/* 13 */     super(message, cause);
/*    */   }
/*    */ }


