package net.md_5.bungee.protocol.packet;

import io.netty.buffer.ByteBuf;
import net.md_5.bungee.protocol.ProtocolConstants;


public class SetCompression
        extends net.md_5.bungee.protocol.DefinedPacket {
    private int threshold;

    public SetCompression(int threshold) {
        this.threshold = threshold;
    }

    public SetCompression() {
    }

    public String toString() {
        return "SetCompression(threshold=" + getThreshold() + ")";
    }

    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof SetCompression)) return false;
        SetCompression other = (SetCompression) o;
        if (!other.canEqual(this)) return false;
        return getThreshold() == other.getThreshold();
    }

    protected boolean canEqual(Object other) {
        return other instanceof SetCompression;
    }

    public int hashCode() {
        int PRIME = 59;
        int result = 1;
        result = result * 59 + getThreshold();
        return result;
    }

    public int getThreshold() {
        return this.threshold;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }

    public void read(ByteBuf buf, ProtocolConstants.Direction direction, int protocolVersion) {
        this.threshold = net.md_5.bungee.protocol.DefinedPacket.readVarInt(buf);
    }

    public void write(ByteBuf buf, ProtocolConstants.Direction direction, int protocolVersion) {
        net.md_5.bungee.protocol.DefinedPacket.writeVarInt(this.threshold, buf);
    }

    public void handle(net.md_5.bungee.protocol.AbstractPacketHandler handler)
            throws Exception {
        handler.handle(this);
    }
}


