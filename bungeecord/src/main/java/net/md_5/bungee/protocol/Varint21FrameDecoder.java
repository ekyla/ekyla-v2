/*    */ package net.md_5.bungee.protocol;
/*    */ 
/*    */ import io.netty.buffer.ByteBuf;
/*    */ import io.netty.buffer.ByteBufAllocator;
/*    */ import io.netty.buffer.Unpooled;
/*    */ import io.netty.channel.ChannelHandlerContext;
/*    */ import io.netty.handler.codec.ByteToMessageDecoder;
/*    */ import io.netty.handler.codec.CorruptedFrameException;
/*    */ import java.io.PrintStream;
/*    */ import java.util.List;
/*    */ 
/*    */ public class Varint21FrameDecoder extends ByteToMessageDecoder
/*    */ {
/*    */   private static boolean DIRECT_WARNING;
/*    */   
/*    */   protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception
/*    */   {
/* 18 */     in.markReaderIndex();
/*    */     
/* 20 */     byte[] buf = new byte[3];
/* 21 */     for (int i = 0; i < buf.length; i++)
/*    */     {
/* 23 */       if (!in.isReadable())
/*    */       {
/* 25 */         in.resetReaderIndex();
/* 26 */         return;
/*    */       }
/*    */       
/* 29 */       buf[i] = in.readByte();
/* 30 */       if (buf[i] >= 0)
/*    */       {
/* 32 */         int length = DefinedPacket.readVarInt(Unpooled.wrappedBuffer(buf));
/* 33 */         if (length == 0)
/*    */         {
/* 35 */           throw new CorruptedFrameException("Empty Packet!");
/*    */         }
/*    */         
/* 38 */         if (in.readableBytes() < length)
/*    */         {
/* 40 */           in.resetReaderIndex();
/* 41 */           return;
/*    */         }
/*    */         
/* 44 */         if (in.hasMemoryAddress())
/*    */         {
/* 46 */           out.add(in.slice(in.readerIndex(), length).retain());
/* 47 */           in.skipBytes(length);
/*    */         }
/*    */         else {
/* 50 */           if (!DIRECT_WARNING)
/*    */           {
/* 52 */             DIRECT_WARNING = true;
/* 53 */             System.out.println("Netty is not using direct IO buffers.");
/*    */           }
/*    */           
/*    */ 
/* 57 */           ByteBuf dst = ctx.alloc().directBuffer(length);
/* 58 */           in.readBytes(dst);
/* 59 */           out.add(dst);
/*    */         }
/* 61 */         return;
/*    */       }
/*    */     }
/*    */     
/*    */ 
/* 66 */     throw new CorruptedFrameException("length wider than 21-bit");
/*    */   }
/*    */ }


