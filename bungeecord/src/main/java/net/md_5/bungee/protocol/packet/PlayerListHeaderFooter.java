/*    */ package net.md_5.bungee.protocol.packet;
/*    */ 
/*    */ import io.netty.buffer.ByteBuf;
/*    */ 
/*    */ 
/*    */ public class PlayerListHeaderFooter
/*    */   extends net.md_5.bungee.protocol.DefinedPacket
/*    */ {
/*    */   private String header;
/*    */   private String footer;
/*    */   
/* 12 */   public void setHeader(String header) { this.header = header; } public void setFooter(String footer) { this.footer = footer; } public String toString() { return "PlayerListHeaderFooter(header=" + getHeader() + ", footer=" + getFooter() + ")"; }
/*    */   
/* 14 */   public PlayerListHeaderFooter(String header, String footer) { this.header = header;this.footer = footer; }
/* 15 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof PlayerListHeaderFooter)) return false; PlayerListHeaderFooter other = (PlayerListHeaderFooter)o; if (!other.canEqual(this)) return false; Object this$header = getHeader();Object other$header = other.getHeader(); if (this$header == null ? other$header != null : !this$header.equals(other$header)) return false; Object this$footer = getFooter();Object other$footer = other.getFooter();return this$footer == null ? other$footer == null : this$footer.equals(other$footer); } protected boolean canEqual(Object other) { return other instanceof PlayerListHeaderFooter; } public int hashCode() { int PRIME = 59;int result = 1;Object $header = getHeader();result = result * 59 + ($header == null ? 43 : $header.hashCode());Object $footer = getFooter();result = result * 59 + ($footer == null ? 43 : $footer.hashCode());return result;
/*    */   }
/*    */   
/*    */ 
/* 19 */   public String getHeader() { return this.header; }
/* 20 */   public String getFooter() { return this.footer; }
/*    */   
/*    */ 
/*    */   public void read(ByteBuf buf, net.md_5.bungee.protocol.ProtocolConstants.Direction direction, int protocolVersion)
/*    */   {
/* 25 */     this.header = readString(buf);
/* 26 */     this.footer = readString(buf);
/*    */   }
/*    */   
/*    */ 
/*    */   public void write(ByteBuf buf, net.md_5.bungee.protocol.ProtocolConstants.Direction direction, int protocolVersion)
/*    */   {
/* 32 */     writeString(this.header, buf);
/* 33 */     writeString(this.footer, buf);
/*    */   }
/*    */   
/*    */   public void handle(net.md_5.bungee.protocol.AbstractPacketHandler handler)
/*    */     throws Exception
/*    */   {
/* 39 */     handler.handle(this);
/*    */   }
/*    */   
/*    */   public PlayerListHeaderFooter() {}
/*    */ }


