/*    */ package net.md_5.bungee.protocol.packet;
/*    */ 
/*    */ import io.netty.buffer.ByteBuf;
/*    */ 
/*    */ 
/*    */ public class ClientStatus
/*    */   extends net.md_5.bungee.protocol.DefinedPacket
/*    */ {
/*    */   private byte payload;
/*    */   
/* 11 */   public void setPayload(byte payload) { this.payload = payload; } public String toString() { return "ClientStatus(payload=" + getPayload() + ")"; }
/*    */   
/* 13 */   public ClientStatus(byte payload) { this.payload = payload; }
/* 14 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof ClientStatus)) return false; ClientStatus other = (ClientStatus)o; if (!other.canEqual(this)) return false; return getPayload() == other.getPayload(); } protected boolean canEqual(Object other) { return other instanceof ClientStatus; } public int hashCode() { int PRIME = 59;int result = 1;result = result * 59 + getPayload();return result;
/*    */   }
/*    */   
/*    */   public byte getPayload() {
/* 18 */     return this.payload;
/*    */   }
/*    */   
/*    */   public void read(ByteBuf buf)
/*    */   {
/* 23 */     this.payload = buf.readByte();
/*    */   }
/*    */   
/*    */ 
/*    */   public void write(ByteBuf buf)
/*    */   {
/* 29 */     buf.writeByte(this.payload);
/*    */   }
/*    */   
/*    */   public void handle(net.md_5.bungee.protocol.AbstractPacketHandler handler)
/*    */     throws Exception
/*    */   {
/* 35 */     handler.handle(this);
/*    */   }
/*    */   
/*    */   public ClientStatus() {}
/*    */ }


