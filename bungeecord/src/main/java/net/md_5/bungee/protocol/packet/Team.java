/*     */ package net.md_5.bungee.protocol.packet;
/*     */ 
/*     */ import io.netty.buffer.ByteBuf;
/*     */ 
/*     */ public class Team extends net.md_5.bungee.protocol.DefinedPacket {
/*     */   private String name;
/*     */   private byte mode;
/*     */   private String displayName;
/*     */   private String prefix;
/*     */   private String suffix;
/*     */   
/*  12 */   public void setName(String name) { this.name = name; } public void setMode(byte mode) { this.mode = mode; } public void setDisplayName(String displayName) { this.displayName = displayName; } public void setPrefix(String prefix) { this.prefix = prefix; } public void setSuffix(String suffix) { this.suffix = suffix; } public void setNameTagVisibility(String nameTagVisibility) { this.nameTagVisibility = nameTagVisibility; } public void setCollisionRule(String collisionRule) { this.collisionRule = collisionRule; } public void setColor(int color) { this.color = color; } public void setFriendlyFire(byte friendlyFire) { this.friendlyFire = friendlyFire; } public void setPlayers(String[] players) { this.players = players; } public String toString() { return "Team(name=" + getName() + ", mode=" + getMode() + ", displayName=" + getDisplayName() + ", prefix=" + getPrefix() + ", suffix=" + getSuffix() + ", nameTagVisibility=" + getNameTagVisibility() + ", collisionRule=" + getCollisionRule() + ", color=" + getColor() + ", friendlyFire=" + getFriendlyFire() + ", players=" + java.util.Arrays.deepToString(getPlayers()) + ")"; }
/*     */   
/*  14 */   public Team(String name, byte mode, String displayName, String prefix, String suffix, String nameTagVisibility, String collisionRule, int color, byte friendlyFire, String[] players) { this.name = name;this.mode = mode;this.displayName = displayName;this.prefix = prefix;this.suffix = suffix;this.nameTagVisibility = nameTagVisibility;this.collisionRule = collisionRule;this.color = color;this.friendlyFire = friendlyFire;this.players = players; }
/*  15 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof Team)) return false; Team other = (Team)o; if (!other.canEqual(this)) return false; Object this$name = getName();Object other$name = other.getName(); if (this$name == null ? other$name != null : !this$name.equals(other$name)) return false; if (getMode() != other.getMode()) return false; Object this$displayName = getDisplayName();Object other$displayName = other.getDisplayName(); if (this$displayName == null ? other$displayName != null : !this$displayName.equals(other$displayName)) return false; Object this$prefix = getPrefix();Object other$prefix = other.getPrefix(); if (this$prefix == null ? other$prefix != null : !this$prefix.equals(other$prefix)) return false; Object this$suffix = getSuffix();Object other$suffix = other.getSuffix(); if (this$suffix == null ? other$suffix != null : !this$suffix.equals(other$suffix)) return false; Object this$nameTagVisibility = getNameTagVisibility();Object other$nameTagVisibility = other.getNameTagVisibility(); if (this$nameTagVisibility == null ? other$nameTagVisibility != null : !this$nameTagVisibility.equals(other$nameTagVisibility)) return false; Object this$collisionRule = getCollisionRule();Object other$collisionRule = other.getCollisionRule(); if (this$collisionRule == null ? other$collisionRule != null : !this$collisionRule.equals(other$collisionRule)) return false; if (getColor() != other.getColor()) return false; if (getFriendlyFire() != other.getFriendlyFire()) return false; return java.util.Arrays.deepEquals(getPlayers(), other.getPlayers()); } protected boolean canEqual(Object other) { return other instanceof Team; } public int hashCode() { int PRIME = 59;int result = 1;Object $name = getName();result = result * 59 + ($name == null ? 43 : $name.hashCode());result = result * 59 + getMode();Object $displayName = getDisplayName();result = result * 59 + ($displayName == null ? 43 : $displayName.hashCode());Object $prefix = getPrefix();result = result * 59 + ($prefix == null ? 43 : $prefix.hashCode());Object $suffix = getSuffix();result = result * 59 + ($suffix == null ? 43 : $suffix.hashCode());Object $nameTagVisibility = getNameTagVisibility();result = result * 59 + ($nameTagVisibility == null ? 43 : $nameTagVisibility.hashCode());Object $collisionRule = getCollisionRule();result = result * 59 + ($collisionRule == null ? 43 : $collisionRule.hashCode());result = result * 59 + getColor();result = result * 59 + getFriendlyFire();result = result * 59 + java.util.Arrays.deepHashCode(getPlayers());return result;
/*     */   }
/*     */   
/*     */   public String getName() {
/*  19 */     return this.name;
/*     */   }
/*     */   
/*     */ 
/*  23 */   public byte getMode() { return this.mode; }
/*  24 */   public String getDisplayName() { return this.displayName; }
/*  25 */   public String getPrefix() { return this.prefix; }
/*  26 */   public String getSuffix() { return this.suffix; }
/*  27 */   public String getNameTagVisibility() { return this.nameTagVisibility; }
/*  28 */   public String getCollisionRule() { return this.collisionRule; }
/*  29 */   public int getColor() { return this.color; }
/*  30 */   public byte getFriendlyFire() { return this.friendlyFire; }
/*  31 */   public String[] getPlayers() { return this.players; }
/*     */   
/*     */   private String nameTagVisibility;
/*     */   private String collisionRule;
/*     */   private int color;
/*     */   private byte friendlyFire;
/*     */   private String[] players;
/*  38 */   public Team(String name) { this.name = name;
/*  39 */     this.mode = 1;
/*     */   }
/*     */   
/*     */ 
/*     */   public void read(ByteBuf buf, net.md_5.bungee.protocol.ProtocolConstants.Direction direction, int protocolVersion)
/*     */   {
/*  45 */     this.name = readString(buf);
/*  46 */     this.mode = buf.readByte();
/*  47 */     if ((this.mode == 0) || (this.mode == 2))
/*     */     {
/*  49 */       this.displayName = readString(buf);
/*  50 */       if (protocolVersion < 393)
/*     */       {
/*  52 */         this.prefix = readString(buf);
/*  53 */         this.suffix = readString(buf);
/*     */       }
/*  55 */       this.friendlyFire = buf.readByte();
/*  56 */       this.nameTagVisibility = readString(buf);
/*  57 */       if (protocolVersion >= 107)
/*     */       {
/*  59 */         this.collisionRule = readString(buf);
/*     */       }
/*  61 */       this.color = (protocolVersion >= 393 ? readVarInt(buf) : buf.readByte());
/*  62 */       if (protocolVersion >= 393)
/*     */       {
/*  64 */         this.prefix = readString(buf);
/*  65 */         this.suffix = readString(buf);
/*     */       }
/*     */     }
/*  68 */     if ((this.mode == 0) || (this.mode == 3) || (this.mode == 4))
/*     */     {
/*  70 */       int len = readVarInt(buf);
/*  71 */       this.players = new String[len];
/*  72 */       for (int i = 0; i < len; i++)
/*     */       {
/*  74 */         this.players[i] = readString(buf);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */   public void write(ByteBuf buf, net.md_5.bungee.protocol.ProtocolConstants.Direction direction, int protocolVersion)
/*     */   {
/*  82 */     writeString(this.name, buf);
/*  83 */     buf.writeByte(this.mode);
/*  84 */     if ((this.mode == 0) || (this.mode == 2))
/*     */     {
/*  86 */       writeString(this.displayName, buf);
/*  87 */       if (protocolVersion < 393)
/*     */       {
/*  89 */         writeString(this.prefix, buf);
/*  90 */         writeString(this.suffix, buf);
/*     */       }
/*  92 */       buf.writeByte(this.friendlyFire);
/*  93 */       writeString(this.nameTagVisibility, buf);
/*  94 */       if (protocolVersion >= 107)
/*     */       {
/*  96 */         writeString(this.collisionRule, buf);
/*     */       }
/*     */       
/*  99 */       if (protocolVersion >= 393)
/*     */       {
/* 101 */         writeVarInt(this.color, buf);
/* 102 */         writeString(this.prefix, buf);
/* 103 */         writeString(this.suffix, buf);
/*     */       }
/*     */       else {
/* 106 */         buf.writeByte(this.color);
/*     */       }
/*     */     }
/* 109 */     if ((this.mode == 0) || (this.mode == 3) || (this.mode == 4))
/*     */     {
/* 111 */       writeVarInt(this.players.length, buf);
/* 112 */       for (String player : this.players)
/*     */       {
/* 114 */         writeString(player, buf);
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */   public void handle(net.md_5.bungee.protocol.AbstractPacketHandler handler)
/*     */     throws Exception
/*     */   {
/* 122 */     handler.handle(this);
/*     */   }
/*     */   
/*     */   public Team() {}
/*     */ }


