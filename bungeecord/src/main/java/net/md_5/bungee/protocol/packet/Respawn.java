/*    */ package net.md_5.bungee.protocol.packet;
/*    */ 
/*    */ import io.netty.buffer.ByteBuf;
/*    */ 
/*    */ public class Respawn
/*    */   extends net.md_5.bungee.protocol.DefinedPacket {
/*    */   private int dimension;
/*    */   private short difficulty;
/*    */   private short gameMode;
/*    */   private String levelType;
/*    */   
/* 12 */   public void setDimension(int dimension) { this.dimension = dimension; } public void setDifficulty(short difficulty) { this.difficulty = difficulty; } public void setGameMode(short gameMode) { this.gameMode = gameMode; } public void setLevelType(String levelType) { this.levelType = levelType; } public String toString() { return "Respawn(dimension=" + getDimension() + ", difficulty=" + getDifficulty() + ", gameMode=" + getGameMode() + ", levelType=" + getLevelType() + ")"; }
/*    */   
/* 14 */   public Respawn(int dimension, short difficulty, short gameMode, String levelType) { this.dimension = dimension;this.difficulty = difficulty;this.gameMode = gameMode;this.levelType = levelType; }
/* 15 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof Respawn)) return false; Respawn other = (Respawn)o; if (!other.canEqual(this)) return false; if (getDimension() != other.getDimension()) return false; if (getDifficulty() != other.getDifficulty()) return false; if (getGameMode() != other.getGameMode()) return false; Object this$levelType = getLevelType();Object other$levelType = other.getLevelType();return this$levelType == null ? other$levelType == null : this$levelType.equals(other$levelType); } protected boolean canEqual(Object other) { return other instanceof Respawn; } public int hashCode() { int PRIME = 59;int result = 1;result = result * 59 + getDimension();result = result * 59 + getDifficulty();result = result * 59 + getGameMode();Object $levelType = getLevelType();result = result * 59 + ($levelType == null ? 43 : $levelType.hashCode());return result;
/*    */   }
/*    */   
/*    */ 
/* 19 */   public int getDimension() { return this.dimension; }
/* 20 */   public short getDifficulty() { return this.difficulty; }
/* 21 */   public short getGameMode() { return this.gameMode; }
/* 22 */   public String getLevelType() { return this.levelType; }
/*    */   
/*    */ 
/*    */   public void read(ByteBuf buf, net.md_5.bungee.protocol.ProtocolConstants.Direction direction, int protocolVersion)
/*    */   {
/* 27 */     this.dimension = buf.readInt();
/* 28 */     if (protocolVersion < 477)
/*    */     {
/* 30 */       this.difficulty = buf.readUnsignedByte();
/*    */     }
/* 32 */     this.gameMode = buf.readUnsignedByte();
/* 33 */     this.levelType = readString(buf);
/*    */   }
/*    */   
/*    */ 
/*    */   public void write(ByteBuf buf, net.md_5.bungee.protocol.ProtocolConstants.Direction direction, int protocolVersion)
/*    */   {
/* 39 */     buf.writeInt(this.dimension);
/* 40 */     if (protocolVersion < 477)
/*    */     {
/* 42 */       buf.writeByte(this.difficulty);
/*    */     }
/* 44 */     buf.writeByte(this.gameMode);
/* 45 */     writeString(this.levelType, buf);
/*    */   }
/*    */   
/*    */   public void handle(net.md_5.bungee.protocol.AbstractPacketHandler handler)
/*    */     throws Exception
/*    */   {
/* 51 */     handler.handle(this);
/*    */   }
/*    */   
/*    */   public Respawn() {}
/*    */ }


