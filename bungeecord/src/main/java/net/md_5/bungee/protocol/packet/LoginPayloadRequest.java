/*    */ package net.md_5.bungee.protocol.packet;
/*    */ 
/*    */ import io.netty.buffer.ByteBuf;
/*    */ 
/*    */ public class LoginPayloadRequest
/*    */   extends net.md_5.bungee.protocol.DefinedPacket
/*    */ {
/*    */   private int id;
/*    */   private String channel;
/*    */   private byte[] data;
/*    */   
/* 12 */   public void setId(int id) { this.id = id; } public void setChannel(String channel) { this.channel = channel; } public void setData(byte[] data) { this.data = data; } public String toString() { return "LoginPayloadRequest(id=" + getId() + ", channel=" + getChannel() + ", data=" + java.util.Arrays.toString(getData()) + ")"; }
/*    */   
/* 14 */   public LoginPayloadRequest(int id, String channel, byte[] data) { this.id = id;this.channel = channel;this.data = data; }
/* 15 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof LoginPayloadRequest)) return false; LoginPayloadRequest other = (LoginPayloadRequest)o; if (!other.canEqual(this)) return false; if (getId() != other.getId()) return false; Object this$channel = getChannel();Object other$channel = other.getChannel(); if (this$channel == null ? other$channel != null : !this$channel.equals(other$channel)) return false; return java.util.Arrays.equals(getData(), other.getData()); } protected boolean canEqual(Object other) { return other instanceof LoginPayloadRequest; } public int hashCode() { int PRIME = 59;int result = 1;result = result * 59 + getId();Object $channel = getChannel();result = result * 59 + ($channel == null ? 43 : $channel.hashCode());result = result * 59 + java.util.Arrays.hashCode(getData());return result;
/*    */   }
/*    */   
/*    */ 
/* 19 */   public int getId() { return this.id; }
/* 20 */   public String getChannel() { return this.channel; }
/* 21 */   public byte[] getData() { return this.data; }
/*    */   
/*    */ 
/*    */   public void read(ByteBuf buf)
/*    */   {
/* 26 */     this.id = readVarInt(buf);
/* 27 */     this.channel = readString(buf);
/*    */     
/* 29 */     int len = buf.readableBytes();
/* 30 */     if (len > 1048576)
/*    */     {
/* 32 */       throw new net.md_5.bungee.protocol.OverflowPacketException("Payload may not be larger than 1048576 bytes");
/*    */     }
/* 34 */     this.data = new byte[len];
/* 35 */     buf.readBytes(this.data);
/*    */   }
/*    */   
/*    */ 
/*    */   public void write(ByteBuf buf)
/*    */   {
/* 41 */     writeVarInt(this.id, buf);
/* 42 */     writeString(this.channel, buf);
/* 43 */     buf.writeBytes(this.data);
/*    */   }
/*    */   
/*    */   public void handle(net.md_5.bungee.protocol.AbstractPacketHandler handler)
/*    */     throws Exception
/*    */   {
/* 49 */     handler.handle(this);
/*    */   }
/*    */   
/*    */   public LoginPayloadRequest() {}
/*    */ }


