/*    */ package net.md_5.bungee.protocol.packet;
/*    */ 
/*    */ import io.netty.buffer.ByteBuf;
/*    */ 
/*    */ 
/*    */ public class Chat
/*    */   extends net.md_5.bungee.protocol.DefinedPacket
/*    */ {
/*    */   private String message;
/*    */   private byte position;
/*    */   
/* 12 */   public void setMessage(String message) { this.message = message; } public void setPosition(byte position) { this.position = position; } public String toString() { return "Chat(message=" + getMessage() + ", position=" + getPosition() + ")"; }
/*    */   
/* 14 */   public Chat(String message, byte position) { this.message = message;this.position = position; }
/* 15 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof Chat)) return false; Chat other = (Chat)o; if (!other.canEqual(this)) return false; Object this$message = getMessage();Object other$message = other.getMessage(); if (this$message == null ? other$message != null : !this$message.equals(other$message)) return false; return getPosition() == other.getPosition(); } protected boolean canEqual(Object other) { return other instanceof Chat; } public int hashCode() { int PRIME = 59;int result = 1;Object $message = getMessage();result = result * 59 + ($message == null ? 43 : $message.hashCode());result = result * 59 + getPosition();return result;
/*    */   }
/*    */   
/*    */ 
/* 19 */   public String getMessage() { return this.message; }
/* 20 */   public byte getPosition() { return this.position; }
/*    */   
/*    */   public Chat(String message)
/*    */   {
/* 24 */     this(message, (byte)0);
/*    */   }
/*    */   
/*    */ 
/*    */   public void read(ByteBuf buf, net.md_5.bungee.protocol.ProtocolConstants.Direction direction, int protocolVersion)
/*    */   {
/* 30 */     this.message = readString(buf);
/* 31 */     if (direction == net.md_5.bungee.protocol.ProtocolConstants.Direction.TO_CLIENT)
/*    */     {
/* 33 */       this.position = buf.readByte();
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public void write(ByteBuf buf, net.md_5.bungee.protocol.ProtocolConstants.Direction direction, int protocolVersion)
/*    */   {
/* 40 */     writeString(this.message, buf);
/* 41 */     if (direction == net.md_5.bungee.protocol.ProtocolConstants.Direction.TO_CLIENT)
/*    */     {
/* 43 */       buf.writeByte(this.position);
/*    */     }
/*    */   }
/*    */   
/*    */   public void handle(net.md_5.bungee.protocol.AbstractPacketHandler handler)
/*    */     throws Exception
/*    */   {
/* 50 */     handler.handle(this);
/*    */   }
/*    */   
/*    */   public Chat() {}
/*    */ }


