/*    */ package net.md_5.bungee.protocol.packet;
/*    */ 
/*    */ import io.netty.buffer.ByteBuf;
/*    */ 
/*    */ 
/*    */ public class Kick
/*    */   extends net.md_5.bungee.protocol.DefinedPacket
/*    */ {
/*    */   private String message;
/*    */   
/* 11 */   public void setMessage(String message) { this.message = message; } public String toString() { return "Kick(message=" + getMessage() + ")"; }
/*    */   
/* 13 */   public Kick(String message) { this.message = message; }
/* 14 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof Kick)) return false; Kick other = (Kick)o; if (!other.canEqual(this)) return false; Object this$message = getMessage();Object other$message = other.getMessage();return this$message == null ? other$message == null : this$message.equals(other$message); } protected boolean canEqual(Object other) { return other instanceof Kick; } public int hashCode() { int PRIME = 59;int result = 1;Object $message = getMessage();result = result * 59 + ($message == null ? 43 : $message.hashCode());return result;
/*    */   }
/*    */   
/*    */   public String getMessage() {
/* 18 */     return this.message;
/*    */   }
/*    */   
/*    */   public void read(ByteBuf buf)
/*    */   {
/* 23 */     this.message = readString(buf);
/*    */   }
/*    */   
/*    */ 
/*    */   public void write(ByteBuf buf)
/*    */   {
/* 29 */     writeString(this.message, buf);
/*    */   }
/*    */   
/*    */   public void handle(net.md_5.bungee.protocol.AbstractPacketHandler handler)
/*    */     throws Exception
/*    */   {
/* 35 */     handler.handle(this);
/*    */   }
/*    */   
/*    */   public Kick() {}
/*    */ }


