/*    */ package net.md_5.bungee.protocol.packet;
/*    */ 
/*    */ import io.netty.buffer.ByteBuf;
/*    */ 
/*    */ 
/*    */ public class StatusResponse
/*    */   extends net.md_5.bungee.protocol.DefinedPacket
/*    */ {
/*    */   private String response;
/*    */   
/* 11 */   public void setResponse(String response) { this.response = response; } public String toString() { return "StatusResponse(response=" + getResponse() + ")"; }
/*    */   
/* 13 */   public StatusResponse(String response) { this.response = response; }
/* 14 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof StatusResponse)) return false; StatusResponse other = (StatusResponse)o; if (!other.canEqual(this)) return false; Object this$response = getResponse();Object other$response = other.getResponse();return this$response == null ? other$response == null : this$response.equals(other$response); } protected boolean canEqual(Object other) { return other instanceof StatusResponse; } public int hashCode() { int PRIME = 59;int result = 1;Object $response = getResponse();result = result * 59 + ($response == null ? 43 : $response.hashCode());return result;
/*    */   }
/*    */   
/*    */   public String getResponse() {
/* 18 */     return this.response;
/*    */   }
/*    */   
/*    */   public void read(ByteBuf buf)
/*    */   {
/* 23 */     this.response = readString(buf);
/*    */   }
/*    */   
/*    */ 
/*    */   public void write(ByteBuf buf)
/*    */   {
/* 29 */     writeString(this.response, buf);
/*    */   }
/*    */   
/*    */   public void handle(net.md_5.bungee.protocol.AbstractPacketHandler handler)
/*    */     throws Exception
/*    */   {
/* 35 */     handler.handle(this);
/*    */   }
/*    */   
/*    */   public StatusResponse() {}
/*    */ }


