/*    */ package net.md_5.bungee.protocol.packet;
/*    */ 
/*    */ import io.netty.buffer.ByteBuf;
/*    */ 
/*    */ public class Handshake extends net.md_5.bungee.protocol.DefinedPacket {
/*    */   private int protocolVersion;
/*    */   private String host;
/*    */   private int port;
/*    */   private int requestedProtocol;
/*    */   
/* 11 */   public void setProtocolVersion(int protocolVersion) { this.protocolVersion = protocolVersion; } public void setHost(String host) { this.host = host; } public void setPort(int port) { this.port = port; } public void setRequestedProtocol(int requestedProtocol) { this.requestedProtocol = requestedProtocol; } public String toString() { return "Handshake(protocolVersion=" + getProtocolVersion() + ", host=" + getHost() + ", port=" + getPort() + ", requestedProtocol=" + getRequestedProtocol() + ")"; }
/*    */   
/* 13 */   public Handshake(int protocolVersion, String host, int port, int requestedProtocol) { this.protocolVersion = protocolVersion;this.host = host;this.port = port;this.requestedProtocol = requestedProtocol; }
/* 14 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof Handshake)) return false; Handshake other = (Handshake)o; if (!other.canEqual(this)) return false; if (getProtocolVersion() != other.getProtocolVersion()) return false; Object this$host = getHost();Object other$host = other.getHost(); if (this$host == null ? other$host != null : !this$host.equals(other$host)) return false; if (getPort() != other.getPort()) return false; return getRequestedProtocol() == other.getRequestedProtocol(); } protected boolean canEqual(Object other) { return other instanceof Handshake; } public int hashCode() { int PRIME = 59;int result = 1;result = result * 59 + getProtocolVersion();Object $host = getHost();result = result * 59 + ($host == null ? 43 : $host.hashCode());result = result * 59 + getPort();result = result * 59 + getRequestedProtocol();return result;
/*    */   }
/*    */   
/*    */ 
/* 18 */   public int getProtocolVersion() { return this.protocolVersion; }
/* 19 */   public String getHost() { return this.host; }
/* 20 */   public int getPort() { return this.port; }
/* 21 */   public int getRequestedProtocol() { return this.requestedProtocol; }
/*    */   
/*    */ 
/*    */   public void read(ByteBuf buf)
/*    */   {
/* 26 */     this.protocolVersion = readVarInt(buf);
/* 27 */     this.host = readString(buf);
/* 28 */     this.port = buf.readUnsignedShort();
/* 29 */     this.requestedProtocol = readVarInt(buf);
/*    */   }
/*    */   
/*    */ 
/*    */   public void write(ByteBuf buf)
/*    */   {
/* 35 */     writeVarInt(this.protocolVersion, buf);
/* 36 */     writeString(this.host, buf);
/* 37 */     buf.writeShort(this.port);
/* 38 */     writeVarInt(this.requestedProtocol, buf);
/*    */   }
/*    */   
/*    */   public void handle(net.md_5.bungee.protocol.AbstractPacketHandler handler)
/*    */     throws Exception
/*    */   {
/* 44 */     handler.handle(this);
/*    */   }
/*    */   
/*    */   public Handshake() {}
/*    */ }


