/*    */ package net.md_5.bungee.protocol;
/*    */ 
/*    */ import io.netty.buffer.ByteBuf;
/*    */ import io.netty.buffer.Unpooled;
/*    */ import io.netty.channel.ChannelHandlerContext;
/*    */ import io.netty.channel.ChannelPipeline;
/*    */ import io.netty.handler.codec.ByteToMessageDecoder;
/*    */ import java.util.List;
/*    */ import net.md_5.bungee.protocol.packet.LegacyHandshake;
/*    */ import net.md_5.bungee.protocol.packet.LegacyPing;
/*    */ 
/*    */ public class LegacyDecoder
/*    */   extends ByteToMessageDecoder
/*    */ {
/*    */   protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception
/*    */   {
/* 17 */     if (!in.isReadable())
/*    */     {
/* 19 */       return;
/*    */     }
/*    */     
/* 22 */     in.markReaderIndex();
/* 23 */     short packetID = in.readUnsignedByte();
/*    */     
/* 25 */     if (packetID == 254)
/*    */     {
/* 27 */       out.add(new PacketWrapper(new LegacyPing((in.isReadable()) && (in.readUnsignedByte() == 1)), Unpooled.EMPTY_BUFFER));
/* 28 */       return; }
/* 29 */     if ((packetID == 2) && (in.isReadable()))
/*    */     {
/* 31 */       in.skipBytes(in.readableBytes());
/* 32 */       out.add(new PacketWrapper(new LegacyHandshake(), Unpooled.EMPTY_BUFFER));
/* 33 */       return;
/*    */     }
/*    */     
/* 36 */     in.resetReaderIndex();
/* 37 */     ctx.pipeline().remove(this);
/*    */   }
/*    */ }


