/*    */ package net.md_5.bungee.protocol.packet;
/*    */ 
/*    */ import io.netty.buffer.ByteBuf;
/*    */ import net.md_5.bungee.protocol.AbstractPacketHandler;
/*    */ import net.md_5.bungee.protocol.DefinedPacket;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class LoginRequest
/*    */   extends DefinedPacket
/*    */ {
/*    */   private String data;
/*    */   
/* 15 */   public void setData(String data) { this.data = data; } public String toString() { return "LoginRequest(data=" + getData() + ")"; }
/*    */   
/* 17 */   public LoginRequest(String data) { this.data = data; }
/* 18 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof LoginRequest)) return false; LoginRequest other = (LoginRequest)o; if (!other.canEqual(this)) return false; Object this$data = getData();Object other$data = other.getData();return this$data == null ? other$data == null : this$data.equals(other$data); } protected boolean canEqual(Object other) { return other instanceof LoginRequest; } public int hashCode() { int PRIME = 59;int result = 1;Object $data = getData();result = result * 59 + ($data == null ? 43 : $data.hashCode());return result;
/*    */   }
/*    */   
/*    */   public String getData() {
/* 22 */     return this.data;
/*    */   }
/*    */   
/*    */   public void read(ByteBuf buf)
/*    */   {
/* 27 */     this.data = readString(buf);
/*    */   }
/*    */   
/*    */ 
/*    */   public void write(ByteBuf buf)
/*    */   {
/* 33 */     writeString(this.data, buf);
/*    */   }
/*    */   
/*    */   public void handle(AbstractPacketHandler handler)
/*    */     throws Exception
/*    */   {
/* 39 */     handler.handle(this);
/*    */   }
/*    */   
/*    */   public LoginRequest() {}
/*    */ }


