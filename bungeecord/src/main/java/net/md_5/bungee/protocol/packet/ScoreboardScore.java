/*    */ package net.md_5.bungee.protocol.packet;
/*    */ 
/*    */ import io.netty.buffer.ByteBuf;
/*    */ 
/*    */ public class ScoreboardScore
/*    */   extends net.md_5.bungee.protocol.DefinedPacket {
/*    */   private String itemName;
/*    */   private byte action;
/*    */   private String scoreName;
/*    */   private int value;
/*    */   
/* 12 */   public void setItemName(String itemName) { this.itemName = itemName; } public void setAction(byte action) { this.action = action; } public void setScoreName(String scoreName) { this.scoreName = scoreName; } public void setValue(int value) { this.value = value; } public String toString() { return "ScoreboardScore(itemName=" + getItemName() + ", action=" + getAction() + ", scoreName=" + getScoreName() + ", value=" + getValue() + ")"; }
/*    */   
/* 14 */   public ScoreboardScore(String itemName, byte action, String scoreName, int value) { this.itemName = itemName;this.action = action;this.scoreName = scoreName;this.value = value; }
/* 15 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof ScoreboardScore)) return false; ScoreboardScore other = (ScoreboardScore)o; if (!other.canEqual(this)) return false; Object this$itemName = getItemName();Object other$itemName = other.getItemName(); if (this$itemName == null ? other$itemName != null : !this$itemName.equals(other$itemName)) return false; if (getAction() != other.getAction()) return false; Object this$scoreName = getScoreName();Object other$scoreName = other.getScoreName(); if (this$scoreName == null ? other$scoreName != null : !this$scoreName.equals(other$scoreName)) return false; return getValue() == other.getValue(); } protected boolean canEqual(Object other) { return other instanceof ScoreboardScore; } public int hashCode() { int PRIME = 59;int result = 1;Object $itemName = getItemName();result = result * 59 + ($itemName == null ? 43 : $itemName.hashCode());result = result * 59 + getAction();Object $scoreName = getScoreName();result = result * 59 + ($scoreName == null ? 43 : $scoreName.hashCode());result = result * 59 + getValue();return result;
/*    */   }
/*    */   
/*    */   public String getItemName() {
/* 19 */     return this.itemName;
/*    */   }
/*    */   
/*    */ 
/* 23 */   public byte getAction() { return this.action; }
/* 24 */   public String getScoreName() { return this.scoreName; }
/* 25 */   public int getValue() { return this.value; }
/*    */   
/*    */ 
/*    */   public void read(ByteBuf buf, net.md_5.bungee.protocol.ProtocolConstants.Direction direction, int protocolVersion)
/*    */   {
/* 30 */     this.itemName = readString(buf);
/* 31 */     this.action = buf.readByte();
/* 32 */     this.scoreName = readString(buf);
/* 33 */     if (this.action != 1)
/*    */     {
/* 35 */       this.value = readVarInt(buf);
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public void write(ByteBuf buf, net.md_5.bungee.protocol.ProtocolConstants.Direction direction, int protocolVersion)
/*    */   {
/* 42 */     writeString(this.itemName, buf);
/* 43 */     buf.writeByte(this.action);
/* 44 */     writeString(this.scoreName, buf);
/* 45 */     if (this.action != 1)
/*    */     {
/* 47 */       writeVarInt(this.value, buf);
/*    */     }
/*    */   }
/*    */   
/*    */   public void handle(net.md_5.bungee.protocol.AbstractPacketHandler handler)
/*    */     throws Exception
/*    */   {
/* 54 */     handler.handle(this);
/*    */   }
/*    */   
/*    */   public ScoreboardScore() {}
/*    */ }


