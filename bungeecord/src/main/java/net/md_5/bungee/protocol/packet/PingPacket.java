/*    */ package net.md_5.bungee.protocol.packet;
/*    */ 
/*    */ import io.netty.buffer.ByteBuf;
/*    */ 
/*    */ 
/*    */ public class PingPacket
/*    */   extends net.md_5.bungee.protocol.DefinedPacket
/*    */ {
/*    */   private long time;
/*    */   
/* 11 */   public void setTime(long time) { this.time = time; } public String toString() { return "PingPacket(time=" + getTime() + ")"; }
/*    */   
/* 13 */   public PingPacket(long time) { this.time = time; }
/* 14 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof PingPacket)) return false; PingPacket other = (PingPacket)o; if (!other.canEqual(this)) return false; return getTime() == other.getTime(); } protected boolean canEqual(Object other) { return other instanceof PingPacket; } public int hashCode() { int PRIME = 59;int result = 1;long $time = getTime();result = result * 59 + (int)($time >>> 32 ^ $time);return result;
/*    */   }
/*    */   
/*    */   public long getTime() {
/* 18 */     return this.time;
/*    */   }
/*    */   
/*    */   public void read(ByteBuf buf)
/*    */   {
/* 23 */     this.time = buf.readLong();
/*    */   }
/*    */   
/*    */ 
/*    */   public void write(ByteBuf buf)
/*    */   {
/* 29 */     buf.writeLong(this.time);
/*    */   }
/*    */   
/*    */   public void handle(net.md_5.bungee.protocol.AbstractPacketHandler handler)
/*    */     throws Exception
/*    */   {
/* 35 */     handler.handle(this);
/*    */   }
/*    */   
/*    */   public PingPacket() {}
/*    */ }


