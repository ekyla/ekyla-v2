/*    */ package net.md_5.bungee.protocol.packet;
/*    */ 
/*    */ import io.netty.buffer.ByteBuf;
/*    */ 
/*    */ public class Login
/*    */   extends net.md_5.bungee.protocol.DefinedPacket {
/*    */   private int entityId;
/*    */   private short gameMode;
/*    */   private int dimension;
/*    */   private short difficulty;
/*    */   
/* 12 */   public void setEntityId(int entityId) { this.entityId = entityId; } public void setGameMode(short gameMode) { this.gameMode = gameMode; } public void setDimension(int dimension) { this.dimension = dimension; } public void setDifficulty(short difficulty) { this.difficulty = difficulty; } public void setMaxPlayers(short maxPlayers) { this.maxPlayers = maxPlayers; } public void setLevelType(String levelType) { this.levelType = levelType; } public void setViewDistance(int viewDistance) { this.viewDistance = viewDistance; } public void setReducedDebugInfo(boolean reducedDebugInfo) { this.reducedDebugInfo = reducedDebugInfo; } public String toString() { return "Login(entityId=" + getEntityId() + ", gameMode=" + getGameMode() + ", dimension=" + getDimension() + ", difficulty=" + getDifficulty() + ", maxPlayers=" + getMaxPlayers() + ", levelType=" + getLevelType() + ", viewDistance=" + getViewDistance() + ", reducedDebugInfo=" + isReducedDebugInfo() + ")"; }
/*    */   
/* 14 */   public Login(int entityId, short gameMode, int dimension, short difficulty, short maxPlayers, String levelType, int viewDistance, boolean reducedDebugInfo) { this.entityId = entityId;this.gameMode = gameMode;this.dimension = dimension;this.difficulty = difficulty;this.maxPlayers = maxPlayers;this.levelType = levelType;this.viewDistance = viewDistance;this.reducedDebugInfo = reducedDebugInfo; }
/* 15 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof Login)) return false; Login other = (Login)o; if (!other.canEqual(this)) return false; if (getEntityId() != other.getEntityId()) return false; if (getGameMode() != other.getGameMode()) return false; if (getDimension() != other.getDimension()) return false; if (getDifficulty() != other.getDifficulty()) return false; if (getMaxPlayers() != other.getMaxPlayers()) return false; Object this$levelType = getLevelType();Object other$levelType = other.getLevelType(); if (this$levelType == null ? other$levelType != null : !this$levelType.equals(other$levelType)) return false; if (getViewDistance() != other.getViewDistance()) return false; return isReducedDebugInfo() == other.isReducedDebugInfo(); } protected boolean canEqual(Object other) { return other instanceof Login; } public int hashCode() { int PRIME = 59;int result = 1;result = result * 59 + getEntityId();result = result * 59 + getGameMode();result = result * 59 + getDimension();result = result * 59 + getDifficulty();result = result * 59 + getMaxPlayers();Object $levelType = getLevelType();result = result * 59 + ($levelType == null ? 43 : $levelType.hashCode());result = result * 59 + getViewDistance();result = result * 59 + (isReducedDebugInfo() ? 79 : 97);return result; }
/*    */   
/*    */   private short maxPlayers;
/*    */   
/* 19 */   public int getEntityId() { return this.entityId; }
/* 20 */   public short getGameMode() { return this.gameMode; }
/* 21 */   public int getDimension() { return this.dimension; }
/* 22 */   public short getDifficulty() { return this.difficulty; }
/* 23 */   public short getMaxPlayers() { return this.maxPlayers; }
/* 24 */   public String getLevelType() { return this.levelType; }
/* 25 */   public int getViewDistance() { return this.viewDistance; }
/* 26 */   public boolean isReducedDebugInfo() { return this.reducedDebugInfo; }
/*    */   
/*    */   private String levelType;
/*    */   private int viewDistance;
/*    */   private boolean reducedDebugInfo;
/* 31 */   public void read(ByteBuf buf, net.md_5.bungee.protocol.ProtocolConstants.Direction direction, int protocolVersion) { this.entityId = buf.readInt();
/* 32 */     this.gameMode = buf.readUnsignedByte();
/* 33 */     if (protocolVersion > 107)
/*    */     {
/* 35 */       this.dimension = buf.readInt();
/*    */     }
/*    */     else {
/* 38 */       this.dimension = buf.readByte();
/*    */     }
/* 40 */     if (protocolVersion < 477)
/*    */     {
/* 42 */       this.difficulty = buf.readUnsignedByte();
/*    */     }
/* 44 */     this.maxPlayers = buf.readUnsignedByte();
/* 45 */     this.levelType = readString(buf);
/* 46 */     if (protocolVersion >= 477)
/*    */     {
/* 48 */       this.viewDistance = readVarInt(buf);
/*    */     }
/* 50 */     if (protocolVersion >= 29)
/*    */     {
/* 52 */       this.reducedDebugInfo = buf.readBoolean();
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public void write(ByteBuf buf, net.md_5.bungee.protocol.ProtocolConstants.Direction direction, int protocolVersion)
/*    */   {
/* 59 */     buf.writeInt(this.entityId);
/* 60 */     buf.writeByte(this.gameMode);
/* 61 */     if (protocolVersion > 107)
/*    */     {
/* 63 */       buf.writeInt(this.dimension);
/*    */     }
/*    */     else {
/* 66 */       buf.writeByte(this.dimension);
/*    */     }
/* 68 */     if (protocolVersion < 477)
/*    */     {
/* 70 */       buf.writeByte(this.difficulty);
/*    */     }
/* 72 */     buf.writeByte(this.maxPlayers);
/* 73 */     writeString(this.levelType, buf);
/* 74 */     if (protocolVersion >= 477)
/*    */     {
/* 76 */       writeVarInt(this.viewDistance, buf);
/*    */     }
/* 78 */     if (protocolVersion >= 29)
/*    */     {
/* 80 */       buf.writeBoolean(this.reducedDebugInfo);
/*    */     }
/*    */   }
/*    */   
/*    */   public void handle(net.md_5.bungee.protocol.AbstractPacketHandler handler)
/*    */     throws Exception
/*    */   {
/* 87 */     handler.handle(this);
/*    */   }
/*    */   
/*    */   public Login() {}
/*    */ }


