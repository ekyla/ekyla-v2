/*    */ package net.md_5.bungee.protocol.packet;
/*    */ 
/*    */ import io.netty.buffer.ByteBuf;
/*    */ 
/*    */ 
/*    */ public class LoginPayloadResponse
/*    */   extends net.md_5.bungee.protocol.DefinedPacket
/*    */ {
/*    */   private int id;
/*    */   private byte[] data;
/*    */   
/* 12 */   public void setId(int id) { this.id = id; } public void setData(byte[] data) { this.data = data; } public String toString() { return "LoginPayloadResponse(id=" + getId() + ", data=" + java.util.Arrays.toString(getData()) + ")"; }
/*    */   
/* 14 */   public LoginPayloadResponse(int id, byte[] data) { this.id = id;this.data = data; }
/* 15 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof LoginPayloadResponse)) return false; LoginPayloadResponse other = (LoginPayloadResponse)o; if (!other.canEqual(this)) return false; if (getId() != other.getId()) return false; return java.util.Arrays.equals(getData(), other.getData()); } protected boolean canEqual(Object other) { return other instanceof LoginPayloadResponse; } public int hashCode() { int PRIME = 59;int result = 1;result = result * 59 + getId();result = result * 59 + java.util.Arrays.hashCode(getData());return result;
/*    */   }
/*    */   
/*    */ 
/* 19 */   public int getId() { return this.id; }
/* 20 */   public byte[] getData() { return this.data; }
/*    */   
/*    */ 
/*    */   public void read(ByteBuf buf)
/*    */   {
/* 25 */     this.id = readVarInt(buf);
/*    */     
/* 27 */     if (buf.readBoolean())
/*    */     {
/* 29 */       int len = buf.readableBytes();
/* 30 */       if (len > 1048576)
/*    */       {
/* 32 */         throw new net.md_5.bungee.protocol.OverflowPacketException("Payload may not be larger than 1048576 bytes");
/*    */       }
/* 34 */       this.data = new byte[len];
/* 35 */       buf.readBytes(this.data);
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public void write(ByteBuf buf)
/*    */   {
/* 42 */     writeVarInt(this.id, buf);
/* 43 */     if (this.data != null)
/*    */     {
/* 45 */       buf.writeBoolean(true);
/* 46 */       buf.writeBytes(this.data);
/*    */     }
/*    */     else {
/* 49 */       buf.writeBoolean(false);
/*    */     }
/*    */   }
/*    */   
/*    */   public void handle(net.md_5.bungee.protocol.AbstractPacketHandler handler)
/*    */     throws Exception
/*    */   {
/* 56 */     handler.handle(this);
/*    */   }
/*    */   
/*    */   public LoginPayloadResponse() {}
/*    */ }


