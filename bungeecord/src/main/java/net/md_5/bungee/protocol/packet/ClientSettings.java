/*    */ package net.md_5.bungee.protocol.packet;
/*    */ 
/*    */ import io.netty.buffer.ByteBuf;
/*    */ 
/*    */ public class ClientSettings
/*    */   extends net.md_5.bungee.protocol.DefinedPacket
/*    */ {
/*    */   private String locale;
/*    */   private byte viewDistance;
/*    */   private int chatFlags;
/*    */   
/* 12 */   public void setLocale(String locale) { this.locale = locale; } public void setViewDistance(byte viewDistance) { this.viewDistance = viewDistance; } public void setChatFlags(int chatFlags) { this.chatFlags = chatFlags; } public void setChatColours(boolean chatColours) { this.chatColours = chatColours; } public void setDifficulty(byte difficulty) { this.difficulty = difficulty; } public void setSkinParts(byte skinParts) { this.skinParts = skinParts; } public void setMainHand(int mainHand) { this.mainHand = mainHand; } public String toString() { return "ClientSettings(locale=" + getLocale() + ", viewDistance=" + getViewDistance() + ", chatFlags=" + getChatFlags() + ", chatColours=" + isChatColours() + ", difficulty=" + getDifficulty() + ", skinParts=" + getSkinParts() + ", mainHand=" + getMainHand() + ")"; }
/*    */   
/* 14 */   public ClientSettings(String locale, byte viewDistance, int chatFlags, boolean chatColours, byte difficulty, byte skinParts, int mainHand) { this.locale = locale;this.viewDistance = viewDistance;this.chatFlags = chatFlags;this.chatColours = chatColours;this.difficulty = difficulty;this.skinParts = skinParts;this.mainHand = mainHand; }
/* 15 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof ClientSettings)) return false; ClientSettings other = (ClientSettings)o; if (!other.canEqual(this)) return false; Object this$locale = getLocale();Object other$locale = other.getLocale(); if (this$locale == null ? other$locale != null : !this$locale.equals(other$locale)) return false; if (getViewDistance() != other.getViewDistance()) return false; if (getChatFlags() != other.getChatFlags()) return false; if (isChatColours() != other.isChatColours()) return false; if (getDifficulty() != other.getDifficulty()) return false; if (getSkinParts() != other.getSkinParts()) return false; return getMainHand() == other.getMainHand(); } protected boolean canEqual(Object other) { return other instanceof ClientSettings; } public int hashCode() { int PRIME = 59;int result = 1;Object $locale = getLocale();result = result * 59 + ($locale == null ? 43 : $locale.hashCode());result = result * 59 + getViewDistance();result = result * 59 + getChatFlags();result = result * 59 + (isChatColours() ? 79 : 97);result = result * 59 + getDifficulty();result = result * 59 + getSkinParts();result = result * 59 + getMainHand();return result; }
/*    */   
/*    */   private boolean chatColours;
/*    */   
/* 19 */   public String getLocale() { return this.locale; }
/* 20 */   public byte getViewDistance() { return this.viewDistance; }
/* 21 */   public int getChatFlags() { return this.chatFlags; }
/* 22 */   public boolean isChatColours() { return this.chatColours; }
/* 23 */   public byte getDifficulty() { return this.difficulty; }
/* 24 */   public byte getSkinParts() { return this.skinParts; }
/* 25 */   public int getMainHand() { return this.mainHand; }
/*    */   
/*    */   private byte difficulty;
/*    */   private byte skinParts;
/*    */   private int mainHand;
/* 30 */   public void read(ByteBuf buf, net.md_5.bungee.protocol.ProtocolConstants.Direction direction, int protocolVersion) { this.locale = readString(buf);
/* 31 */     this.viewDistance = buf.readByte();
/* 32 */     this.chatFlags = (protocolVersion >= 107 ? net.md_5.bungee.protocol.DefinedPacket.readVarInt(buf) : buf.readUnsignedByte());
/* 33 */     this.chatColours = buf.readBoolean();
/* 34 */     this.skinParts = buf.readByte();
/* 35 */     if (protocolVersion >= 107)
/*    */     {
/* 37 */       this.mainHand = net.md_5.bungee.protocol.DefinedPacket.readVarInt(buf);
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public void write(ByteBuf buf, net.md_5.bungee.protocol.ProtocolConstants.Direction direction, int protocolVersion)
/*    */   {
/* 44 */     writeString(this.locale, buf);
/* 45 */     buf.writeByte(this.viewDistance);
/* 46 */     if (protocolVersion >= 107)
/*    */     {
/* 48 */       net.md_5.bungee.protocol.DefinedPacket.writeVarInt(this.chatFlags, buf);
/*    */     }
/*    */     else {
/* 51 */       buf.writeByte(this.chatFlags);
/*    */     }
/* 53 */     buf.writeBoolean(this.chatColours);
/* 54 */     buf.writeByte(this.skinParts);
/* 55 */     if (protocolVersion >= 107)
/*    */     {
/* 57 */       net.md_5.bungee.protocol.DefinedPacket.writeVarInt(this.mainHand, buf);
/*    */     }
/*    */   }
/*    */   
/*    */   public void handle(net.md_5.bungee.protocol.AbstractPacketHandler handler)
/*    */     throws Exception
/*    */   {
/* 64 */     handler.handle(this);
/*    */   }
/*    */   
/*    */   public ClientSettings() {}
/*    */ }


