/*    */ package net.md_5.bungee.protocol.packet;
/*    */ 
/*    */ import io.netty.buffer.ByteBuf;
/*    */ 
/*    */ public class TabCompleteRequest extends net.md_5.bungee.protocol.DefinedPacket { private int transactionId;
/*    */   private String cursor;
/*    */   private boolean assumeCommand;
/*    */   private boolean hasPositon;
/*    */   private long position;
/*    */   
/* 11 */   public void setTransactionId(int transactionId) { this.transactionId = transactionId; } public void setCursor(String cursor) { this.cursor = cursor; } public void setAssumeCommand(boolean assumeCommand) { this.assumeCommand = assumeCommand; } public void setHasPositon(boolean hasPositon) { this.hasPositon = hasPositon; } public void setPosition(long position) { this.position = position; } public String toString() { return "TabCompleteRequest(transactionId=" + getTransactionId() + ", cursor=" + getCursor() + ", assumeCommand=" + isAssumeCommand() + ", hasPositon=" + isHasPositon() + ", position=" + getPosition() + ")"; }
/*    */   
/* 13 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof TabCompleteRequest)) return false; TabCompleteRequest other = (TabCompleteRequest)o; if (!other.canEqual(this)) return false; if (getTransactionId() != other.getTransactionId()) return false; Object this$cursor = getCursor();Object other$cursor = other.getCursor(); if (this$cursor == null ? other$cursor != null : !this$cursor.equals(other$cursor)) return false; if (isAssumeCommand() != other.isAssumeCommand()) return false; if (isHasPositon() != other.isHasPositon()) return false; return getPosition() == other.getPosition(); } protected boolean canEqual(Object other) { return other instanceof TabCompleteRequest; } public int hashCode() { int PRIME = 59;int result = 1;result = result * 59 + getTransactionId();Object $cursor = getCursor();result = result * 59 + ($cursor == null ? 43 : $cursor.hashCode());result = result * 59 + (isAssumeCommand() ? 79 : 97);result = result * 59 + (isHasPositon() ? 79 : 97);long $position = getPosition();result = result * 59 + (int)($position >>> 32 ^ $position);return result;
/*    */   }
/*    */   
/*    */ 
/* 17 */   public int getTransactionId() { return this.transactionId; }
/* 18 */   public String getCursor() { return this.cursor; }
/* 19 */   public boolean isAssumeCommand() { return this.assumeCommand; }
/* 20 */   public boolean isHasPositon() { return this.hasPositon; }
/* 21 */   public long getPosition() { return this.position; }
/*    */   
/*    */   public TabCompleteRequest(int transactionId, String cursor)
/*    */   {
/* 25 */     this.transactionId = transactionId;
/* 26 */     this.cursor = cursor;
/*    */   }
/*    */   
/*    */   public TabCompleteRequest(String cursor, boolean assumeCommand, boolean hasPosition, long position)
/*    */   {
/* 31 */     this.cursor = cursor;
/* 32 */     this.assumeCommand = assumeCommand;
/* 33 */     this.hasPositon = hasPosition;
/* 34 */     this.position = position;
/*    */   }
/*    */   
/*    */ 
/*    */   public void read(ByteBuf buf, net.md_5.bungee.protocol.ProtocolConstants.Direction direction, int protocolVersion)
/*    */   {
/* 40 */     if (protocolVersion >= 393)
/*    */     {
/* 42 */       this.transactionId = readVarInt(buf);
/*    */     }
/* 44 */     this.cursor = readString(buf);
/*    */     
/* 46 */     if (protocolVersion < 393)
/*    */     {
/* 48 */       if (protocolVersion >= 107)
/*    */       {
/* 50 */         this.assumeCommand = buf.readBoolean();
/*    */       }
/*    */       
/* 53 */       if ((this.hasPositon = buf.readBoolean()))
/*    */       {
/* 55 */         this.position = buf.readLong();
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public void write(ByteBuf buf, net.md_5.bungee.protocol.ProtocolConstants.Direction direction, int protocolVersion)
/*    */   {
/* 63 */     if (protocolVersion >= 393)
/*    */     {
/* 65 */       writeVarInt(this.transactionId, buf);
/*    */     }
/* 67 */     writeString(this.cursor, buf);
/*    */     
/* 69 */     if (protocolVersion < 393)
/*    */     {
/* 71 */       if (protocolVersion >= 107)
/*    */       {
/* 73 */         buf.writeBoolean(this.assumeCommand);
/*    */       }
/*    */       
/* 76 */       buf.writeBoolean(this.hasPositon);
/* 77 */       if (this.hasPositon)
/*    */       {
/* 79 */         buf.writeLong(this.position);
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */   public void handle(net.md_5.bungee.protocol.AbstractPacketHandler handler)
/*    */     throws Exception
/*    */   {
/* 87 */     handler.handle(this);
/*    */   }
/*    */   
/*    */   public TabCompleteRequest() {}
/*    */ }


