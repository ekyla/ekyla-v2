/*    */ package net.md_5.bungee.protocol.packet;
/*    */ 
/*    */ import com.mojang.brigadier.context.StringRange;
/*    */ import com.mojang.brigadier.suggestion.Suggestion;
/*    */ import com.mojang.brigadier.suggestion.Suggestions;
/*    */ import io.netty.buffer.ByteBuf;
/*    */ import java.util.List;
/*    */ 
/*    */ 
/*    */ public class TabCompleteResponse
/*    */   extends net.md_5.bungee.protocol.DefinedPacket
/*    */ {
/*    */   private int transactionId;
/*    */   private Suggestions suggestions;
/*    */   private List<String> commands;
/*    */   
/* 17 */   public void setTransactionId(int transactionId) { this.transactionId = transactionId; } public void setSuggestions(Suggestions suggestions) { this.suggestions = suggestions; } public void setCommands(List<String> commands) { this.commands = commands; } public String toString() { return "TabCompleteResponse(transactionId=" + getTransactionId() + ", suggestions=" + getSuggestions() + ", commands=" + getCommands() + ")"; }
/*    */   
/* 19 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof TabCompleteResponse)) return false; TabCompleteResponse other = (TabCompleteResponse)o; if (!other.canEqual(this)) return false; if (getTransactionId() != other.getTransactionId()) return false; Object this$suggestions = getSuggestions();Object other$suggestions = other.getSuggestions(); if (this$suggestions == null ? other$suggestions != null : !this$suggestions.equals(other$suggestions)) return false; Object this$commands = getCommands();Object other$commands = other.getCommands();return this$commands == null ? other$commands == null : this$commands.equals(other$commands); } protected boolean canEqual(Object other) { return other instanceof TabCompleteResponse; } public int hashCode() { int PRIME = 59;int result = 1;result = result * 59 + getTransactionId();Object $suggestions = getSuggestions();result = result * 59 + ($suggestions == null ? 43 : $suggestions.hashCode());Object $commands = getCommands();result = result * 59 + ($commands == null ? 43 : $commands.hashCode());return result;
/*    */   }
/*    */   
/*    */ 
/* 23 */   public int getTransactionId() { return this.transactionId; }
/* 24 */   public Suggestions getSuggestions() { return this.suggestions; }
/*    */   
/* 26 */   public List<String> getCommands() { return this.commands; }
/*    */   
/*    */   public TabCompleteResponse(int transactionId, Suggestions suggestions)
/*    */   {
/* 30 */     this.transactionId = transactionId;
/* 31 */     this.suggestions = suggestions;
/*    */   }
/*    */   
/*    */   public TabCompleteResponse(List<String> commands)
/*    */   {
/* 36 */     this.commands = commands;
/*    */   }
/*    */   
/*    */ 
/*    */   public void read(ByteBuf buf, net.md_5.bungee.protocol.ProtocolConstants.Direction direction, int protocolVersion)
/*    */   {
/* 42 */     if (protocolVersion >= 393)
/*    */     {
/* 44 */       this.transactionId = readVarInt(buf);
/* 45 */       int start = readVarInt(buf);
/* 46 */       int length = readVarInt(buf);
/* 47 */       StringRange range = StringRange.between(start, start + length);
/*    */       
/* 49 */       int cnt = readVarInt(buf);
/* 50 */       List<Suggestion> matches = new java.util.LinkedList();
/* 51 */       for (int i = 0; i < cnt; i++)
/*    */       {
/* 53 */         String match = readString(buf);
/* 54 */         String tooltip = buf.readBoolean() ? readString(buf) : null;
/*    */         
/* 56 */         matches.add(new Suggestion(range, match, new com.mojang.brigadier.LiteralMessage(tooltip)));
/*    */       }
/*    */       
/* 59 */       this.suggestions = new Suggestions(range, matches);
/*    */     }
/*    */     
/* 62 */     if (protocolVersion < 393)
/*    */     {
/* 64 */       this.commands = readStringArray(buf);
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */   public void write(ByteBuf buf, net.md_5.bungee.protocol.ProtocolConstants.Direction direction, int protocolVersion)
/*    */   {
/* 71 */     if (protocolVersion >= 393)
/*    */     {
/* 73 */       writeVarInt(this.transactionId, buf);
/* 74 */       writeVarInt(this.suggestions.getRange().getStart(), buf);
/* 75 */       writeVarInt(this.suggestions.getRange().getLength(), buf);
/*    */       
/* 77 */       writeVarInt(this.suggestions.getList().size(), buf);
/* 78 */       for (Suggestion suggestion : this.suggestions.getList())
/*    */       {
/* 80 */         writeString(suggestion.getText(), buf);
/* 81 */         buf.writeBoolean((suggestion.getTooltip() != null) && (suggestion.getTooltip().getString() != null));
/* 82 */         if ((suggestion.getTooltip() != null) && (suggestion.getTooltip().getString() != null))
/*    */         {
/* 84 */           writeString(suggestion.getTooltip().getString(), buf);
/*    */         }
/*    */       }
/*    */     }
/*    */     
/* 89 */     if (protocolVersion < 393)
/*    */     {
/* 91 */       writeStringArray(this.commands, buf);
/*    */     }
/*    */   }
/*    */   
/*    */   public void handle(net.md_5.bungee.protocol.AbstractPacketHandler handler)
/*    */     throws Exception
/*    */   {
/* 98 */     handler.handle(this);
/*    */   }
/*    */   
/*    */   public TabCompleteResponse() {}
/*    */ }


