package net.md_5.bungee.protocol.packet;

import io.netty.buffer.ByteBuf;
import net.md_5.bungee.protocol.ProtocolConstants;


public class KeepAlive
        extends net.md_5.bungee.protocol.DefinedPacket {
    private long randomId;

    public KeepAlive(long randomId) {
        this.randomId = randomId;
    }

    public KeepAlive() {
    }

    public String toString() {
        return "KeepAlive(randomId=" + getRandomId() + ")";
    }

    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof KeepAlive)) return false;
        KeepAlive other = (KeepAlive) o;
        if (!other.canEqual(this)) return false;
        return getRandomId() == other.getRandomId();
    }

    protected boolean canEqual(Object other) {
        return other instanceof KeepAlive;
    }

    public int hashCode() {
        int PRIME = 59;
        int result = 1;
        long $randomId = getRandomId();
        result = result * 59 + (int) ($randomId >>> 32 ^ $randomId);
        return result;
    }

    public long getRandomId() {
        return this.randomId;
    }

    public void setRandomId(long randomId) {
        this.randomId = randomId;
    }

    public void read(ByteBuf buf, ProtocolConstants.Direction direction, int protocolVersion) {
        this.randomId = (protocolVersion >= 340 ? buf.readLong() : readVarInt(buf));
    }

    public void write(ByteBuf buf, ProtocolConstants.Direction direction, int protocolVersion) {
        if (protocolVersion >= 340) {
            buf.writeLong(this.randomId);
        } else {
            writeVarInt((int) this.randomId, buf);
        }
    }

    public void handle(net.md_5.bungee.protocol.AbstractPacketHandler handler)
            throws Exception {
        handler.handle(this);
    }
}


