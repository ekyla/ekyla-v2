/*    */ package net.md_5.bungee.protocol.packet;
/*    */ 
/*    */ import io.netty.buffer.ByteBuf;
/*    */ 
/*    */ public class Title extends net.md_5.bungee.protocol.DefinedPacket { private Action action;
/*    */   private String text;
/*    */   private int fadeIn;
/*    */   private int stay;
/*    */   private int fadeOut;
/*    */   
/* 11 */   public void setAction(Action action) { this.action = action; } public void setText(String text) { this.text = text; } public void setFadeIn(int fadeIn) { this.fadeIn = fadeIn; } public void setStay(int stay) { this.stay = stay; } public void setFadeOut(int fadeOut) { this.fadeOut = fadeOut; } public String toString() { return "Title(action=" + getAction() + ", text=" + getText() + ", fadeIn=" + getFadeIn() + ", stay=" + getStay() + ", fadeOut=" + getFadeOut() + ")"; }
/*    */   
/* 13 */   public boolean equals(Object o) { if (o == this) return true; if (!(o instanceof Title)) return false; Title other = (Title)o; if (!other.canEqual(this)) return false; Object this$action = getAction();Object other$action = other.getAction(); if (this$action == null ? other$action != null : !this$action.equals(other$action)) return false; Object this$text = getText();Object other$text = other.getText(); if (this$text == null ? other$text != null : !this$text.equals(other$text)) return false; if (getFadeIn() != other.getFadeIn()) return false; if (getStay() != other.getStay()) return false; return getFadeOut() == other.getFadeOut(); } protected boolean canEqual(Object other) { return other instanceof Title; } public int hashCode() { int PRIME = 59;int result = 1;Object $action = getAction();result = result * 59 + ($action == null ? 43 : $action.hashCode());Object $text = getText();result = result * 59 + ($text == null ? 43 : $text.hashCode());result = result * 59 + getFadeIn();result = result * 59 + getStay();result = result * 59 + getFadeOut();return result;
/*    */   }
/*    */   
/*    */   public Action getAction() {
/* 17 */     return this.action;
/*    */   }
/*    */   
/* 20 */   public String getText() { return this.text; }
/*    */   
/*    */ 
/* 23 */   public int getFadeIn() { return this.fadeIn; }
/* 24 */   public int getStay() { return this.stay; }
/* 25 */   public int getFadeOut() { return this.fadeOut; }
/*    */   
/*    */ 
/*    */   public void read(ByteBuf buf, net.md_5.bungee.protocol.ProtocolConstants.Direction direction, int protocolVersion)
/*    */   {
/* 30 */     int index = readVarInt(buf);
/*    */     
/*    */ 
/* 33 */     if ((protocolVersion <= 210) && (index >= 2))
/*    */     {
/* 35 */       index++;
/*    */     }
/*    */     
/* 38 */     this.action = Action.values()[index];
/* 39 */     switch (this.action)
/*    */     {
/*    */     case TITLE: 
/*    */     case SUBTITLE: 
/*    */     case ACTIONBAR: 
/* 44 */       this.text = readString(buf);
/* 45 */       break;
/*    */     case TIMES: 
/* 47 */       this.fadeIn = buf.readInt();
/* 48 */       this.stay = buf.readInt();
/* 49 */       this.fadeOut = buf.readInt();
/*    */     }
/*    */     
/*    */   }
/*    */   
/*    */ 
/*    */   public void write(ByteBuf buf, net.md_5.bungee.protocol.ProtocolConstants.Direction direction, int protocolVersion)
/*    */   {
/* 57 */     int index = this.action.ordinal();
/*    */     
/*    */ 
/* 60 */     if ((protocolVersion <= 210) && (index >= 2))
/*    */     {
/* 62 */       index--;
/*    */     }
/*    */     
/* 65 */     writeVarInt(index, buf);
/* 66 */     switch (this.action)
/*    */     {
/*    */     case TITLE: 
/*    */     case SUBTITLE: 
/*    */     case ACTIONBAR: 
/* 71 */       writeString(this.text, buf);
/* 72 */       break;
/*    */     case TIMES: 
/* 74 */       buf.writeInt(this.fadeIn);
/* 75 */       buf.writeInt(this.stay);
/* 76 */       buf.writeInt(this.fadeOut);
/*    */     }
/*    */     
/*    */   }
/*    */   
/*    */   public void handle(net.md_5.bungee.protocol.AbstractPacketHandler handler)
/*    */     throws Exception
/*    */   {
/* 84 */     handler.handle(this);
/*    */   }
/*    */   
/*    */ 
/*    */   public static enum Action
/*    */   {
/* 90 */     TITLE, 
/* 91 */     SUBTITLE, 
/* 92 */     ACTIONBAR, 
/* 93 */     TIMES, 
/* 94 */     CLEAR, 
/* 95 */     RESET;
/*    */     
/*    */     private Action() {}
/*    */   }
/*    */ }


