package net.md_5.bungee.entitymap;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import io.netty.buffer.ByteBuf;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.UserConnection;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.protocol.DefinedPacket;
import net.md_5.bungee.protocol.ProtocolConstants;

import java.util.UUID;

class EntityMap_1_8 extends EntityMap {
    static final EntityMap_1_8 INSTANCE = new EntityMap_1_8();

    EntityMap_1_8() {
        addRewrite(4, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(10, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(11, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(12, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(13, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(14, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(15, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(16, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(17, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(18, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(20, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(21, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(22, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(23, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(24, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(25, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(26, ProtocolConstants.Direction.TO_CLIENT, false);
        addRewrite(27, ProtocolConstants.Direction.TO_CLIENT, false);
        addRewrite(28, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(29, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(30, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(32, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(37, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(44, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(67, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(73, ProtocolConstants.Direction.TO_CLIENT, true);

        addRewrite(2, ProtocolConstants.Direction.TO_SERVER, true);
        addRewrite(11, ProtocolConstants.Direction.TO_SERVER, true);
    }


    @SuppressFBWarnings({"DLS_DEAD_LOCAL_STORE"})
    public void rewriteClientbound(ByteBuf packet, int oldId, int newId) {
        super.rewriteClientbound(packet, oldId, newId);


        int readerIndex = packet.readerIndex();
        int packetId = DefinedPacket.readVarInt(packet);
        int packetIdLength = packet.readerIndex() - readerIndex;
        if (packetId == 13) {
            DefinedPacket.readVarInt(packet);
            rewriteVarInt(packet, oldId, newId, packet.readerIndex());
        } else if (packetId == 27) {
            rewriteInt(packet, oldId, newId, readerIndex + packetIdLength + 4);
        } else if (packetId == 19) {
            int count = DefinedPacket.readVarInt(packet);
            int[] ids = new int[count];
            for (int i = 0; i < count; i++) {
                ids[i] = DefinedPacket.readVarInt(packet);
            }
            packet.readerIndex(readerIndex + packetIdLength);
            packet.writerIndex(readerIndex + packetIdLength);
            DefinedPacket.writeVarInt(count, packet);
            for (int id : ids) {
                if (id == oldId) {
                    id = newId;
                } else if (id == newId) {
                    id = oldId;
                }
                DefinedPacket.writeVarInt(id, packet);
            }
        } else if (packetId == 14) {

            DefinedPacket.readVarInt(packet);
            int type = packet.readUnsignedByte();

            if ((type == 60) || (type == 90)) {
                packet.skipBytes(14);
                int position = packet.readerIndex();
                int readId = packet.readInt();
                int changedId = readId;

                if (readId == oldId) {
                    packet.setInt(position, changedId = newId);
                } else if (readId == newId) {
                    packet.setInt(position, changedId = oldId);
                }

                if ((readId > 0) && (changedId <= 0)) {
                    packet.writerIndex(packet.writerIndex() - 6);
                } else if ((changedId > 0) && (readId <= 0)) {
                    packet.ensureWritable(6);
                    packet.writerIndex(packet.writerIndex() + 6);
                }
            }
        } else if (packetId == 12) {
            DefinedPacket.readVarInt(packet);
            int idLength = packet.readerIndex() - readerIndex - packetIdLength;
            UUID uuid = DefinedPacket.readUUID(packet);
            ProxiedPlayer player;
            if ((player = BungeeCord.getInstance().getPlayerByOfflineUUID(uuid)) != null) {
                int previous = packet.writerIndex();
                packet.readerIndex(readerIndex);
                packet.writerIndex(readerIndex + packetIdLength + idLength);
                DefinedPacket.writeUUID(player.getUniqueId(), packet);
                packet.writerIndex(previous);
            }
        } else if (packetId == 66) {
            int event = packet.readUnsignedByte();
            if (event == 1) {
                DefinedPacket.readVarInt(packet);
                rewriteInt(packet, oldId, newId, packet.readerIndex());
            } else if (event == 2) {
                int position = packet.readerIndex();
                rewriteVarInt(packet, oldId, newId, packet.readerIndex());
                packet.readerIndex(position);
                DefinedPacket.readVarInt(packet);
                rewriteInt(packet, oldId, newId, packet.readerIndex());
            }
        }
        packet.readerIndex(readerIndex);
    }


    public void rewriteServerbound(ByteBuf packet, int oldId, int newId) {
        super.rewriteServerbound(packet, oldId, newId);

        int readerIndex = packet.readerIndex();
        int packetId = DefinedPacket.readVarInt(packet);
        int packetIdLength = packet.readerIndex() - readerIndex;

        if ((packetId == 24) && (!BungeeCord.getInstance().getConfig().isIpForward())) {
            UUID uuid = DefinedPacket.readUUID(packet);
            ProxiedPlayer player;
            if ((player = BungeeCord.getInstance().getPlayer(uuid)) != null) {
                int previous = packet.writerIndex();
                packet.readerIndex(readerIndex);
                packet.writerIndex(readerIndex + packetIdLength);
                DefinedPacket.writeUUID(((UserConnection) player).getPendingConnection().getOfflineId(), packet);
                packet.writerIndex(previous);
            }
        }
        packet.readerIndex(readerIndex);
    }
}


