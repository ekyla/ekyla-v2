package net.md_5.bungee.entitymap;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import io.netty.buffer.ByteBuf;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.UserConnection;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.protocol.DefinedPacket;
import net.md_5.bungee.protocol.ProtocolConstants;

import java.util.UUID;

class EntityMap_1_14 extends EntityMap {
    static final EntityMap_1_14 INSTANCE = new EntityMap_1_14();

    EntityMap_1_14() {
        addRewrite(0, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(1, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(3, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(4, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(5, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(6, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(8, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(27, ProtocolConstants.Direction.TO_CLIENT, false);
        addRewrite(40, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(41, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(42, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(43, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(56, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(59, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(62, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(67, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(68, ProtocolConstants.Direction.TO_CLIENT, false);
        addRewrite(69, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(70, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(74, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(85, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(86, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(88, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(89, ProtocolConstants.Direction.TO_CLIENT, true);

        addRewrite(14, ProtocolConstants.Direction.TO_SERVER, true);
        addRewrite(27, ProtocolConstants.Direction.TO_SERVER, true);
    }


    @SuppressFBWarnings({"DLS_DEAD_LOCAL_STORE"})
    public void rewriteClientbound(ByteBuf packet, int oldId, int newId, int protocolVersion) {
        super.rewriteClientbound(packet, oldId, newId);


        int readerIndex = packet.readerIndex();
        int packetId = DefinedPacket.readVarInt(packet);
        int packetIdLength = packet.readerIndex() - readerIndex;
        int jumpIndex = packet.readerIndex();
        switch (packetId) {
            case 68:
                rewriteInt(packet, oldId, newId, readerIndex + packetIdLength + 4);
                break;
            case 85:
                DefinedPacket.readVarInt(packet);
                rewriteVarInt(packet, oldId, newId, packet.readerIndex());
                break;
            case 74:
                DefinedPacket.readVarInt(packet);
                jumpIndex = packet.readerIndex();

            case 55:
                int count = DefinedPacket.readVarInt(packet);
                int[] ids = new int[count];
                for (int i = 0; i < count; i++) {
                    ids[i] = DefinedPacket.readVarInt(packet);
                }
                packet.readerIndex(jumpIndex);
                packet.writerIndex(jumpIndex);
                DefinedPacket.writeVarInt(count, packet);
                for (int id : ids) {
                    if (id == oldId) {
                        id = newId;
                    } else if (id == newId) {
                        id = oldId;
                    }
                    DefinedPacket.writeVarInt(id, packet);
                }
                break;
            case 0:
                DefinedPacket.readVarInt(packet);
                DefinedPacket.readUUID(packet);
                int type = DefinedPacket.readVarInt(packet);

                if ((type == 2) || (type == 101) || (type == 71)) {
                    if ((type == 2) || (type == 71)) {
                        oldId += 1;
                        newId += 1;
                    }

                    packet.skipBytes(26);
                    int position = packet.readerIndex();
                    int readId = packet.readInt();
                    if (readId == oldId) {
                        packet.setInt(position, newId);
                    } else if (readId == newId) {
                        packet.setInt(position, oldId);
                    }
                }
                break;

            case 5:
                DefinedPacket.readVarInt(packet);
                int idLength = packet.readerIndex() - readerIndex - packetIdLength;
                UUID uuid = DefinedPacket.readUUID(packet);
                ProxiedPlayer player;
                if ((player = BungeeCord.getInstance().getPlayerByOfflineUUID(uuid)) != null) {
                    int previous = packet.writerIndex();
                    packet.readerIndex(readerIndex);
                    packet.writerIndex(readerIndex + packetIdLength + idLength);
                    DefinedPacket.writeUUID(player.getUniqueId(), packet);
                    packet.writerIndex(previous);
                }
                break;

            case 50:
                int event = packet.readUnsignedByte();
                if (event == 1) {
                    DefinedPacket.readVarInt(packet);
                    rewriteInt(packet, oldId, newId, packet.readerIndex());
                } else if (event == 2) {
                    int position = packet.readerIndex();
                    rewriteVarInt(packet, oldId, newId, packet.readerIndex());
                    packet.readerIndex(position);
                    DefinedPacket.readVarInt(packet);
                    rewriteInt(packet, oldId, newId, packet.readerIndex());
                }
                break;

            case 67:
                DefinedPacket.readVarInt(packet);
                rewriteMetaVarInt(packet, oldId + 1, newId + 1, 7, protocolVersion);
                rewriteMetaVarInt(packet, oldId, newId, 8, protocolVersion);
                break;
            case 80:
                DefinedPacket.readVarInt(packet);
                DefinedPacket.readVarInt(packet);
                rewriteVarInt(packet, oldId, newId, packet.readerIndex());
        }

        packet.readerIndex(readerIndex);
    }


    public void rewriteServerbound(ByteBuf packet, int oldId, int newId) {
        super.rewriteServerbound(packet, oldId, newId);

        int readerIndex = packet.readerIndex();
        int packetId = DefinedPacket.readVarInt(packet);
        int packetIdLength = packet.readerIndex() - readerIndex;

        if ((packetId == 43) && (!BungeeCord.getInstance().getConfig().isIpForward())) {
            UUID uuid = DefinedPacket.readUUID(packet);
            ProxiedPlayer player;
            if ((player = BungeeCord.getInstance().getPlayer(uuid)) != null) {
                int previous = packet.writerIndex();
                packet.readerIndex(readerIndex);
                packet.writerIndex(readerIndex + packetIdLength);
                DefinedPacket.writeUUID(((UserConnection) player).getPendingConnection().getOfflineId(), packet);
                packet.writerIndex(previous);
            }
        }
        packet.readerIndex(readerIndex);
    }
}


