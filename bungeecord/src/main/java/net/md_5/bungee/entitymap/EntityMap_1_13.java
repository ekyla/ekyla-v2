package net.md_5.bungee.entitymap;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import io.netty.buffer.ByteBuf;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.UserConnection;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.protocol.DefinedPacket;
import net.md_5.bungee.protocol.ProtocolConstants;

import java.util.UUID;

class EntityMap_1_13 extends EntityMap {
    static final EntityMap_1_13 INSTANCE = new EntityMap_1_13();

    EntityMap_1_13() {
        addRewrite(0, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(1, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(3, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(4, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(5, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(6, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(8, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(28, ProtocolConstants.Direction.TO_CLIENT, false);
        addRewrite(39, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(40, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(41, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(42, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(51, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(54, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(57, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(60, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(63, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(64, ProtocolConstants.Direction.TO_CLIENT, false);
        addRewrite(65, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(66, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(70, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(79, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(80, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(82, ProtocolConstants.Direction.TO_CLIENT, true);
        addRewrite(83, ProtocolConstants.Direction.TO_CLIENT, true);

        addRewrite(13, ProtocolConstants.Direction.TO_SERVER, true);
        addRewrite(25, ProtocolConstants.Direction.TO_SERVER, true);
    }


    @SuppressFBWarnings({"DLS_DEAD_LOCAL_STORE"})
    public void rewriteClientbound(ByteBuf packet, int oldId, int newId, int protocolVersion) {
        super.rewriteClientbound(packet, oldId, newId);


        int readerIndex = packet.readerIndex();
        int packetId = DefinedPacket.readVarInt(packet);
        int packetIdLength = packet.readerIndex() - readerIndex;
        int jumpIndex = packet.readerIndex();
        switch (packetId) {
            case 64:
                rewriteInt(packet, oldId, newId, readerIndex + packetIdLength + 4);
                break;
            case 79:
                DefinedPacket.readVarInt(packet);
                rewriteVarInt(packet, oldId, newId, packet.readerIndex());
                break;
            case 70:
                DefinedPacket.readVarInt(packet);
                jumpIndex = packet.readerIndex();

            case 53:
                int count = DefinedPacket.readVarInt(packet);
                int[] ids = new int[count];
                for (int i = 0; i < count; i++) {
                    ids[i] = DefinedPacket.readVarInt(packet);
                }
                packet.readerIndex(jumpIndex);
                packet.writerIndex(jumpIndex);
                DefinedPacket.writeVarInt(count, packet);
                for (int id : ids) {
                    if (id == oldId) {
                        id = newId;
                    } else if (id == newId) {
                        id = oldId;
                    }
                    DefinedPacket.writeVarInt(id, packet);
                }
                break;
            case 0:
                DefinedPacket.readVarInt(packet);
                DefinedPacket.readUUID(packet);
                int type = packet.readUnsignedByte();

                if ((type == 60) || (type == 90) || (type == 91)) {
                    if ((type == 60) || (type == 91)) {
                        oldId += 1;
                        newId += 1;
                    }

                    packet.skipBytes(26);
                    int position = packet.readerIndex();
                    int readId = packet.readInt();
                    if (readId == oldId) {
                        packet.setInt(position, newId);
                    } else if (readId == newId) {
                        packet.setInt(position, oldId);
                    }
                }
                break;

            case 5:
                DefinedPacket.readVarInt(packet);
                int idLength = packet.readerIndex() - readerIndex - packetIdLength;
                UUID uuid = DefinedPacket.readUUID(packet);
                ProxiedPlayer player;
                if ((player = BungeeCord.getInstance().getPlayerByOfflineUUID(uuid)) != null) {
                    int previous = packet.writerIndex();
                    packet.readerIndex(readerIndex);
                    packet.writerIndex(readerIndex + packetIdLength + idLength);
                    DefinedPacket.writeUUID(player.getUniqueId(), packet);
                    packet.writerIndex(previous);
                }
                break;

            case 47:
                int event = packet.readUnsignedByte();
                if (event == 1) {
                    DefinedPacket.readVarInt(packet);
                    rewriteInt(packet, oldId, newId, packet.readerIndex());
                } else if (event == 2) {
                    int position = packet.readerIndex();
                    rewriteVarInt(packet, oldId, newId, packet.readerIndex());
                    packet.readerIndex(position);
                    DefinedPacket.readVarInt(packet);
                    rewriteInt(packet, oldId, newId, packet.readerIndex());
                }
                break;

            case 63:
                DefinedPacket.readVarInt(packet);
                rewriteMetaVarInt(packet, oldId + 1, newId + 1, 6, protocolVersion);
                rewriteMetaVarInt(packet, oldId, newId, 7, protocolVersion);
        }

        packet.readerIndex(readerIndex);
    }


    public void rewriteServerbound(ByteBuf packet, int oldId, int newId) {
        super.rewriteServerbound(packet, oldId, newId);

        int readerIndex = packet.readerIndex();
        int packetId = DefinedPacket.readVarInt(packet);
        int packetIdLength = packet.readerIndex() - readerIndex;

        if ((packetId == 40) && (!BungeeCord.getInstance().getConfig().isIpForward())) {
            UUID uuid = DefinedPacket.readUUID(packet);
            ProxiedPlayer player;
            if ((player = BungeeCord.getInstance().getPlayer(uuid)) != null) {
                int previous = packet.writerIndex();
                packet.readerIndex(readerIndex);
                packet.writerIndex(readerIndex + packetIdLength);
                DefinedPacket.writeUUID(((UserConnection) player).getPendingConnection().getOfflineId(), packet);
                packet.writerIndex(previous);
            }
        }
        packet.readerIndex(readerIndex);
    }
}


