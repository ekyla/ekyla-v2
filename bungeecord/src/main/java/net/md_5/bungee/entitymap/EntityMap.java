package net.md_5.bungee.entitymap;

import com.flowpowered.nbt.stream.NBTInputStream;
import com.google.common.base.Throwables;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;
import net.md_5.bungee.protocol.DefinedPacket;
import net.md_5.bungee.protocol.ProtocolConstants;

import java.io.IOException;


public abstract class EntityMap {
    private final boolean[] clientboundInts = new boolean['Ā'];
    private final boolean[] clientboundVarInts = new boolean['Ā'];

    private final boolean[] serverboundInts = new boolean['Ā'];
    private final boolean[] serverboundVarInts = new boolean['Ā'];


    public static EntityMap getEntityMap(int version) {
        switch (version) {
            case 47:
                return EntityMap_1_8.INSTANCE;
            case 107:
            case 108:
            case 109:
                return EntityMap_1_9.INSTANCE;
            case 110:
                return EntityMap_1_9_4.INSTANCE;
            case 210:
                return EntityMap_1_10.INSTANCE;
            case 315:
            case 316:
                return EntityMap_1_11.INSTANCE;
            case 335:
                return EntityMap_1_12.INSTANCE;
            case 338:
            case 340:
                return EntityMap_1_12_1.INSTANCE;
            case 393:
            case 401:
            case 404:
                return EntityMap_1_13.INSTANCE;
            case 477:
            case 480:
            case 485:
            case 490:
            case 498:
                return EntityMap_1_14.INSTANCE;
        }
        throw new RuntimeException("Version " + version + " has no entity map");
    }

    protected static void rewriteInt(ByteBuf packet, int oldId, int newId, int offset) {
        int readId = packet.getInt(offset);
        if (readId == oldId) {
            packet.setInt(offset, newId);
        } else if (readId == newId) {
            packet.setInt(offset, oldId);
        }
    }

    @SuppressFBWarnings({"DLS_DEAD_LOCAL_STORE"})
    protected static void rewriteVarInt(ByteBuf packet, int oldId, int newId, int offset) {
        int readId = DefinedPacket.readVarInt(packet);
        int readIdLength = packet.readerIndex() - offset;
        if ((readId == oldId) || (readId == newId)) {
            ByteBuf data = packet.copy();
            packet.readerIndex(offset);
            packet.writerIndex(offset);
            DefinedPacket.writeVarInt(readId == oldId ? newId : oldId, packet);
            packet.writeBytes(data);
            data.release();
        }
    }

    protected static void rewriteMetaVarInt(ByteBuf packet, int oldId, int newId, int metaIndex) {
        rewriteMetaVarInt(packet, oldId, newId, metaIndex, -1);
    }

    protected static void rewriteMetaVarInt(ByteBuf packet, int oldId, int newId, int metaIndex, int protocolVersion) {
        int readerIndex = packet.readerIndex();

        short index;
        while ((index = packet.readUnsignedByte()) != 0xFF) {
            int type = DefinedPacket.readVarInt(packet);
            if (protocolVersion >= ProtocolConstants.MINECRAFT_1_13) {
                switch (type) {
                    case 5: // optional chat
                        if (packet.readBoolean()) {
                            DefinedPacket.readString(packet);
                        }
                        continue;
                    case 15: // particle
                        int particleId = DefinedPacket.readVarInt(packet);

                        if (protocolVersion >= ProtocolConstants.MINECRAFT_1_14) {
                            switch (particleId) {
                                case 3: // minecraft:block
                                case 23: // minecraft:falling_dust
                                    DefinedPacket.readVarInt(packet); // block state
                                    break;
                                case 14: // minecraft:dust
                                    packet.skipBytes(16); // float, float, float, flat
                                    break;
                                case 32: // minecraft:item
                                    readSkipSlot(packet, protocolVersion);
                                    break;
                            }
                        } else {
                            switch (particleId) {
                                case 3: // minecraft:block
                                case 20: // minecraft:falling_dust
                                    DefinedPacket.readVarInt(packet); // block state
                                    break;
                                case 11: // minecraft:dust
                                    packet.skipBytes(16); // float, float, float, flat
                                    break;
                                case 27: // minecraft:item
                                    readSkipSlot(packet, protocolVersion);
                                    break;
                            }
                        }
                        continue;
                    default:
                        if (type >= 6) {
                            type--;
                        }
                        break;
                }
            }

            switch (type) {
                case 0:
                case 6:
                    packet.skipBytes(1); // byte
                    break;
                case 1:
                    if (index == metaIndex) {
                        int position = packet.readerIndex();
                        rewriteVarInt(packet, oldId, newId, position);
                        packet.readerIndex(position);
                    }
                    DefinedPacket.readVarInt(packet);
                    break;
                case 2:
                    packet.skipBytes(4); // float
                    break;
                case 3:
                case 4:
                    DefinedPacket.readString(packet);
                    break;
                case 5:
                    readSkipSlot(packet, protocolVersion);
                    break;
                // boolean
                case 7:
                    packet.skipBytes(12); // float, float, float
                    break;
                case 8:
                    packet.readLong();
                    break;
                case 9:
                    if (packet.readBoolean()) {
                        packet.skipBytes(8); // long
                    }
                    break;
                case 10:
                case 17:
                case 12:
                    DefinedPacket.readVarInt(packet);
                    break;
                case 11:
                    if (packet.readBoolean()) {
                        packet.skipBytes(16); // long, long
                    }
                    break;
                case 13:
                    try {
                        new NBTInputStream(new ByteBufInputStream(packet), false).readTag();
                    } catch (IOException ex) {
                        throw Throwables.propagate(ex);
                    }
                    break;
                case 15:
                    DefinedPacket.readVarInt(packet);
                    DefinedPacket.readVarInt(packet);
                    DefinedPacket.readVarInt(packet);
                    break;
                case 16:
                    if (index == metaIndex) {
                        int position = packet.readerIndex();
                        rewriteVarInt(packet, oldId + 1, newId + 1, position);
                        packet.readerIndex(position);
                    }
                    DefinedPacket.readVarInt(packet);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown meta type " + type);
            }
        }

        packet.readerIndex(readerIndex);
    }

    private static void readSkipSlot(ByteBuf packet, int protocolVersion) {
        if (protocolVersion >= 404 ? packet.readBoolean() : packet.readShort() != -1) {
            if (protocolVersion >= 404) {
                DefinedPacket.readVarInt(packet);
            }
            packet.skipBytes(protocolVersion >= 393 ? 1 : 3);

            int position = packet.readerIndex();
            if (packet.readByte() != 0) {
                packet.readerIndex(position);

                try {
                    new NBTInputStream(new ByteBufInputStream(packet), false).readTag();
                } catch (IOException ex) {
                    throw Throwables.propagate(ex);
                }
            }
        }
    }

    private static void rewrite(ByteBuf packet, int oldId, int newId, boolean[] ints, boolean[] varints) {
        int readerIndex = packet.readerIndex();
        int packetId = DefinedPacket.readVarInt(packet);
        int packetIdLength = packet.readerIndex() - readerIndex;

        if (ints[packetId]) {
            rewriteInt(packet, oldId, newId, readerIndex + packetIdLength);
        } else if (varints[packetId]) {
            rewriteVarInt(packet, oldId, newId, readerIndex + packetIdLength);
        }
        packet.readerIndex(readerIndex);
    }

    protected void addRewrite(int id, ProtocolConstants.Direction direction, boolean varint) {
        if (direction == ProtocolConstants.Direction.TO_CLIENT) {
            if (varint) {
                this.clientboundVarInts[id] = true;
            } else {
                this.clientboundInts[id] = true;
            }
        } else if (varint) {
            this.serverboundVarInts[id] = true;
        } else {
            this.serverboundInts[id] = true;
        }
    }

    public void rewriteServerbound(ByteBuf packet, int oldId, int newId) {
        rewrite(packet, oldId, newId, this.serverboundInts, this.serverboundVarInts);
    }

    public void rewriteServerbound(ByteBuf packet, int oldId, int newId, int protocolVersion) {
        rewriteServerbound(packet, oldId, newId);
    }

    public void rewriteClientbound(ByteBuf packet, int oldId, int newId) {
        rewrite(packet, oldId, newId, this.clientboundInts, this.clientboundVarInts);
    }

    public void rewriteClientbound(ByteBuf packet, int oldId, int newId, int protocolVersion) {
        rewriteClientbound(packet, oldId, newId);
    }
}


