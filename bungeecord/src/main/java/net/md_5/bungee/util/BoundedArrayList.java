/*    */ package net.md_5.bungee.util;
/*    */ 
/*    */ import com.google.common.base.Preconditions;
/*    */ import java.util.ArrayList;
/*    */ import java.util.Collection;
/*    */ 
/*    */ public class BoundedArrayList<E>
/*    */   extends ArrayList<E>
/*    */ {
/*    */   private final int maxSize;
/*    */   
/*    */   public BoundedArrayList(int maxSize)
/*    */   {
/* 14 */     this.maxSize = maxSize;
/*    */   }
/*    */   
/*    */   private void checkSize(int increment)
/*    */   {
/* 19 */     Preconditions.checkState(size() + increment <= this.maxSize, "Adding %s elements would exceed capacity of %s", increment, this.maxSize);
/*    */   }
/*    */   
/*    */ 
/*    */   public boolean add(E e)
/*    */   {
/* 25 */     checkSize(1);
/* 26 */     return super.add(e);
/*    */   }
/*    */   
/*    */ 
/*    */   public void add(int index, E element)
/*    */   {
/* 32 */     checkSize(1);
/* 33 */     super.add(index, element);
/*    */   }
/*    */   
/*    */ 
/*    */   public boolean addAll(Collection<? extends E> c)
/*    */   {
/* 39 */     checkSize(c.size());
/* 40 */     return super.addAll(c);
/*    */   }
/*    */   
/*    */ 
/*    */   public boolean addAll(int index, Collection<? extends E> c)
/*    */   {
/* 46 */     checkSize(c.size());
/* 47 */     return super.addAll(index, c);
/*    */   }
/*    */ }


