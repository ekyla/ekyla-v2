/*    */ package net.md_5.bungee.util;
/*    */ 
/*    */ import gnu.trove.map.hash.TCustomHashMap;
/*    */ import java.util.Map;
/*    */ 
/*    */ public class CaseInsensitiveMap<V>
/*    */   extends TCustomHashMap<String, V>
/*    */ {
/*    */   public CaseInsensitiveMap()
/*    */   {
/* 11 */     super(CaseInsensitiveHashingStrategy.INSTANCE);
/*    */   }
/*    */   
/*    */   public CaseInsensitiveMap(Map<? extends String, ? extends V> map)
/*    */   {
/* 16 */     super(CaseInsensitiveHashingStrategy.INSTANCE, map);
/*    */   }
/*    */ }


