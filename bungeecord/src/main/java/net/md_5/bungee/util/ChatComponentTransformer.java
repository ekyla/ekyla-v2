/*     */ package net.md_5.bungee.util;
/*     */ 
/*     */ import com.google.common.base.Preconditions;
/*     */ import com.google.common.collect.Lists;
/*     */ import java.util.List;
/*     */ import java.util.regex.Matcher;
/*     */ import java.util.regex.Pattern;
/*     */ import net.md_5.bungee.api.chat.BaseComponent;
/*     */ import net.md_5.bungee.api.chat.ScoreComponent;
/*     */ import net.md_5.bungee.api.chat.TextComponent;
/*     */ import net.md_5.bungee.api.connection.ProxiedPlayer;
/*     */ import net.md_5.bungee.api.score.Score;
/*     */ import net.md_5.bungee.api.score.Scoreboard;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public final class ChatComponentTransformer
/*     */ {
/*  29 */   private static final ChatComponentTransformer INSTANCE = new ChatComponentTransformer();
/*     */   
/*     */ 
/*     */ 
/*  33 */   private static final Pattern SELECTOR_PATTERN = Pattern.compile("^@([pares])(?:\\[([^ ]*)\\])?$");
/*     */   
/*     */   public static ChatComponentTransformer getInstance()
/*     */   {
/*  37 */     return INSTANCE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public BaseComponent[] transform(ProxiedPlayer player, BaseComponent... component)
/*     */   {
/*  54 */     if ((component == null) || (component.length < 1) || ((component.length == 1) && (component[0] == null)))
/*     */     {
/*  56 */       return new BaseComponent[] { new TextComponent("") };
/*     */     }
/*     */     
/*     */ 
/*     */ 
/*     */ 
/*  62 */     for (BaseComponent root : component)
/*     */     {
/*  64 */       if ((root.getExtra() != null) && (!root.getExtra().isEmpty()))
/*     */       {
/*  66 */         List<BaseComponent> list = Lists.newArrayList(transform(player, (BaseComponent[])root.getExtra().toArray(new BaseComponent[root.getExtra().size()])));
/*  67 */         root.setExtra(list);
/*     */       }
/*     */       
/*  70 */       if ((root instanceof ScoreComponent))
/*     */       {
/*  72 */         transformScoreComponent(player, (ScoreComponent)root);
/*     */       }
/*     */     }
/*  75 */     return component;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   private void transformScoreComponent(ProxiedPlayer player, ScoreComponent component)
/*     */   {
/*  87 */     Preconditions.checkArgument(!isSelectorPattern(component.getName()), "Cannot transform entity selector patterns");
/*     */     
/*  89 */     if ((component.getValue() != null) && (!component.getValue().isEmpty()))
/*     */     {
/*  91 */       return;
/*     */     }
/*     */     
/*     */ 
/*  95 */     if (component.getName().equals("*"))
/*     */     {
/*  97 */       component.setName(player.getName());
/*     */     }
/*     */     
/* 100 */     if (player.getScoreboard().getObjective(component.getObjective()) != null)
/*     */     {
/* 102 */       Score score = player.getScoreboard().getScore(component.getName());
/* 103 */       if (score != null)
/*     */       {
/* 105 */         component.setValue(Integer.toString(score.getValue()));
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean isSelectorPattern(String pattern)
/*     */   {
/* 118 */     return SELECTOR_PATTERN.matcher(pattern).matches();
/*     */   }
/*     */ }


