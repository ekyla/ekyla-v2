/*    */ package net.md_5.bungee.util;
/*    */ 
/*    */ import gnu.trove.set.hash.TCustomHashSet;
/*    */ import java.util.Collection;
/*    */ 
/*    */ public class CaseInsensitiveSet
/*    */   extends TCustomHashSet<String>
/*    */ {
/*    */   public CaseInsensitiveSet()
/*    */   {
/* 11 */     super(CaseInsensitiveHashingStrategy.INSTANCE);
/*    */   }
/*    */   
/*    */   public CaseInsensitiveSet(Collection<? extends String> collection)
/*    */   {
/* 16 */     super(CaseInsensitiveHashingStrategy.INSTANCE, collection);
/*    */   }
/*    */ }


