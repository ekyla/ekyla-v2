/*    */ package net.md_5.bungee.forge;
/*    */ 
/*    */ import java.util.ArrayDeque;
/*    */ import net.md_5.bungee.UserConnection;
/*    */ import net.md_5.bungee.netty.ChannelWrapper;
/*    */ import net.md_5.bungee.protocol.packet.PluginMessage;
/*    */ 
/*    */ public class ForgeServerHandler
/*    */ {
/*    */   private final UserConnection con;
/*    */   private final ChannelWrapper ch;
/*    */   private final net.md_5.bungee.api.config.ServerInfo serverInfo;
/*    */   
/*    */   public ForgeServerHandler(UserConnection con, ChannelWrapper ch, net.md_5.bungee.api.config.ServerInfo serverInfo)
/*    */   {
/* 16 */     this.con = con;this.ch = ch;this.serverInfo = serverInfo;
/*    */   }
/*    */   
/*    */   public ChannelWrapper getCh()
/*    */   {
/* 21 */     return this.ch;
/*    */   }
/*    */   
/* 24 */   net.md_5.bungee.api.config.ServerInfo getServerInfo() { return this.serverInfo; }
/*    */   
/*    */ 
/* 27 */   public ForgeServerHandshakeState getState() { return this.state; } private ForgeServerHandshakeState state = ForgeServerHandshakeState.START;
/*    */   
/*    */ 
/* 30 */   public boolean isServerForge() { return this.serverForge; } private boolean serverForge = false;
/*    */   
/*    */ 
/* 33 */   private final ArrayDeque<PluginMessage> packetQueue = new ArrayDeque();
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void handle(PluginMessage message)
/*    */     throws IllegalArgumentException
/*    */   {
/* 44 */     if ((!message.getTag().equalsIgnoreCase("FML|HS")) && (!message.getTag().equalsIgnoreCase("FORGE")))
/*    */     {
/* 46 */       throw new IllegalArgumentException("Expecting a Forge REGISTER or FML Handshake packet.");
/*    */     }
/*    */     
/* 49 */     message.setAllowExtendedPacket(true);
/* 50 */     ForgeServerHandshakeState prevState = this.state;
/* 51 */     this.packetQueue.add(message);
/* 52 */     this.state = ((ForgeServerHandshakeState)this.state.send(message, this.con));
/* 53 */     if (this.state != prevState)
/*    */     {
/* 55 */       synchronized (this.packetQueue)
/*    */       {
/* 57 */         while (!this.packetQueue.isEmpty())
/*    */         {
/* 59 */           ForgeLogger.logServer(ForgeLogger.LogDirection.SENDING, prevState.name(), (PluginMessage)this.packetQueue.getFirst());
/* 60 */           this.con.getForgeClientHandler().receive((PluginMessage)this.packetQueue.removeFirst());
/*    */         }
/*    */       }
/*    */     }
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void receive(PluginMessage message)
/*    */     throws IllegalArgumentException
/*    */   {
/* 73 */     this.state = ((ForgeServerHandshakeState)this.state.handle(message, this.ch));
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void setServerAsForgeServer()
/*    */   {
/* 82 */     this.serverForge = true;
/*    */   }
/*    */ }


