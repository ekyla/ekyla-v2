/*    */ package net.md_5.bungee.forge;
/*    */ 
/*    */ import java.util.logging.Level;
/*    */ import java.util.logging.Logger;
/*    */ import net.md_5.bungee.BungeeCord;
/*    */ import net.md_5.bungee.protocol.packet.PluginMessage;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ final class ForgeLogger
/*    */ {
/*    */   static void logServer(LogDirection direction, String stateName, PluginMessage message)
/*    */   {
/* 15 */     String dir = direction == LogDirection.SENDING ? "Server -> Bungee" : "Server <- Bungee";
/* 16 */     String log = "[" + stateName + " " + dir + "][" + direction.name() + ": " + getNameFromDiscriminator(message.getTag(), message) + "]";
/* 17 */     BungeeCord.getInstance().getLogger().log(Level.FINE, log);
/*    */   }
/*    */   
/*    */   static void logClient(LogDirection direction, String stateName, PluginMessage message)
/*    */   {
/* 22 */     String dir = direction == LogDirection.SENDING ? "Client -> Bungee" : "Client <- Bungee";
/* 23 */     String log = "[" + stateName + " " + dir + "][" + direction.name() + ": " + getNameFromDiscriminator(message.getTag(), message) + "]";
/* 24 */     BungeeCord.getInstance().getLogger().log(Level.FINE, log);
/*    */   }
/*    */   
/*    */   private static String getNameFromDiscriminator(String channel, PluginMessage message)
/*    */   {
/* 29 */     byte discrim = message.getData()[0];
/* 30 */     if (channel.equals("FML|HS"))
/*    */     {
/* 32 */       switch (discrim)
/*    */       {
/*    */       case -2: 
/* 35 */         return "Reset";
/*    */       case -1: 
/* 37 */         return "HandshakeAck";
/*    */       case 0: 
/* 39 */         return "ServerHello";
/*    */       case 1: 
/* 41 */         return "ClientHello";
/*    */       case 2: 
/* 43 */         return "ModList";
/*    */       case 3: 
/* 45 */         return "ModIdData";
/*    */       }
/* 47 */       return "Unknown";
/*    */     }
/* 49 */     if (channel.equals("FORGE"))
/*    */     {
/* 51 */       switch (discrim)
/*    */       {
/*    */       case 1: 
/* 54 */         return "DimensionRegister";
/*    */       case 2: 
/* 56 */         return "FluidIdMap";
/*    */       }
/* 58 */       return "Unknown";
/*    */     }
/*    */     
/* 61 */     return "UnknownChannel";
/*    */   }
/*    */   
/*    */ 
/*    */   public static enum LogDirection
/*    */   {
/* 67 */     SENDING, 
/* 68 */     RECEIVED;
/*    */     
/*    */     private LogDirection() {}
/*    */   }
/*    */ }


