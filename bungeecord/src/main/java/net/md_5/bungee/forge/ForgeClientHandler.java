/*     */ package net.md_5.bungee.forge;
/*     */ 
/*     */ import com.google.common.base.Preconditions;
/*     */ import java.util.ArrayDeque;
/*     */ import java.util.Map;
/*     */ import lombok.NonNull;
/*     */ import net.md_5.bungee.UserConnection;
/*     */ import net.md_5.bungee.protocol.packet.PluginMessage;
/*     */ 
/*     */ public class ForgeClientHandler
/*     */ {
/*     */   @NonNull
/*     */   private final UserConnection con;
/*     */   
/*     */   public ForgeClientHandler(@NonNull UserConnection con)
/*     */   {
/*  17 */     if (con == null) throw new NullPointerException("con is marked @NonNull but is null"); this.con = con;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  27 */   public Map<String, String> getClientModList() { return this.clientModList; } private Map<String, String> clientModList = null;
/*  28 */   void setClientModList(Map<String, String> clientModList) { this.clientModList = clientModList; }
/*     */   
/*     */ 
/*  31 */   private final ArrayDeque<PluginMessage> packetQueue = new ArrayDeque();
/*     */   @NonNull
/*  33 */   private ForgeClientHandshakeState state = ForgeClientHandshakeState.HELLO;
/*  34 */   void setState(@NonNull ForgeClientHandshakeState state) { if (state == null) throw new NullPointerException("state is marked @NonNull but is null"); this.state = state;
/*     */   }
/*     */   
/*  37 */   private PluginMessage serverModList = null;
/*  38 */   private PluginMessage serverIdList = null;
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  44 */   public boolean isFmlTokenInHandshake() { return this.fmlTokenInHandshake; } private boolean fmlTokenInHandshake = false;
/*  45 */   public void setFmlTokenInHandshake(boolean fmlTokenInHandshake) { this.fmlTokenInHandshake = fmlTokenInHandshake; }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void handle(PluginMessage message)
/*     */     throws IllegalArgumentException
/*     */   {
/*  55 */     if (!message.getTag().equalsIgnoreCase("FML|HS"))
/*     */     {
/*  57 */       throw new IllegalArgumentException("Expecting a Forge Handshake packet.");
/*     */     }
/*     */     
/*  60 */     message.setAllowExtendedPacket(true);
/*  61 */     ForgeClientHandshakeState prevState = this.state;
/*  62 */     Preconditions.checkState(this.packetQueue.size() < 128, "Forge packet queue too big!");
/*  63 */     this.packetQueue.add(message);
/*  64 */     this.state = ((ForgeClientHandshakeState)this.state.send(message, this.con));
/*  65 */     if (this.state != prevState)
/*     */     {
/*  67 */       synchronized (this.packetQueue)
/*     */       {
/*  69 */         while (!this.packetQueue.isEmpty())
/*     */         {
/*  71 */           ForgeLogger.logClient(ForgeLogger.LogDirection.SENDING, prevState.name(), (PluginMessage)this.packetQueue.getFirst());
/*  72 */           this.con.getForgeServerHandler().receive((PluginMessage)this.packetQueue.removeFirst());
/*     */         }
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void receive(PluginMessage message)
/*     */     throws IllegalArgumentException
/*     */   {
/*  85 */     this.state = ((ForgeClientHandshakeState)this.state.handle(message, this.con));
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void resetHandshake()
/*     */   {
/*  94 */     this.state = ForgeClientHandshakeState.HELLO;
/*  95 */     this.con.unsafe().sendPacket(ForgeConstants.FML_RESET_HANDSHAKE);
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setServerModList(PluginMessage modList)
/*     */     throws IllegalArgumentException
/*     */   {
/* 108 */     if ((!modList.getTag().equalsIgnoreCase("FML|HS")) || (modList.getData()[0] != 2))
/*     */     {
/* 110 */       throw new IllegalArgumentException("modList");
/*     */     }
/*     */     
/* 113 */     this.serverModList = modList;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public void setServerIdList(PluginMessage idList)
/*     */     throws IllegalArgumentException
/*     */   {
/* 126 */     if ((!idList.getTag().equalsIgnoreCase("FML|HS")) || (idList.getData()[0] != 3))
/*     */     {
/* 128 */       throw new IllegalArgumentException("idList");
/*     */     }
/*     */     
/* 131 */     this.serverIdList = idList;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean isHandshakeComplete()
/*     */   {
/* 141 */     return this.state == ForgeClientHandshakeState.DONE;
/*     */   }
/*     */   
/*     */   public void setHandshakeComplete()
/*     */   {
/* 146 */     this.state = ForgeClientHandshakeState.DONE;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   public boolean isForgeUser()
/*     */   {
/* 158 */     return (this.fmlTokenInHandshake) || (this.clientModList != null);
/*     */   }
/*     */ }


