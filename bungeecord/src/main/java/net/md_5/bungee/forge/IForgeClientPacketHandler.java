package net.md_5.bungee.forge;

import net.md_5.bungee.UserConnection;
import net.md_5.bungee.protocol.packet.PluginMessage;

public abstract interface IForgeClientPacketHandler<S>
{
  public abstract S handle(PluginMessage paramPluginMessage, UserConnection paramUserConnection);
  
  public abstract S send(PluginMessage paramPluginMessage, UserConnection paramUserConnection);
}


