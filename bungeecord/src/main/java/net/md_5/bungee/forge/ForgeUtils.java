/*    */ package net.md_5.bungee.forge;
/*    */ 
/*    */ import com.google.common.base.Charsets;
/*    */ import com.google.common.collect.ImmutableSet;
/*    */ import io.netty.buffer.ByteBuf;
/*    */ import io.netty.buffer.Unpooled;
/*    */ import java.util.HashMap;
/*    */ import java.util.Map;
/*    */ import java.util.Set;
/*    */ import java.util.regex.Matcher;
/*    */ import java.util.regex.Pattern;
/*    */ import net.md_5.bungee.protocol.DefinedPacket;
/*    */ import net.md_5.bungee.protocol.packet.PluginMessage;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ForgeUtils
/*    */ {
/*    */   public static Set<String> readRegisteredChannels(PluginMessage pluginMessage)
/*    */   {
/* 25 */     String channels = new String(pluginMessage.getData(), Charsets.UTF_8);
/* 26 */     String[] split = channels.split("\000");
/* 27 */     Set<String> channelSet = ImmutableSet.copyOf(split);
/* 28 */     return channelSet;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public static Map<String, String> readModList(PluginMessage pluginMessage)
/*    */   {
/* 39 */     Map<String, String> modTags = new HashMap();
/* 40 */     ByteBuf payload = Unpooled.wrappedBuffer(pluginMessage.getData());
/* 41 */     byte discriminator = payload.readByte();
/* 42 */     if (discriminator == 2)
/*    */     {
/* 44 */       ByteBuf buffer = payload.slice();
/* 45 */       int modCount = DefinedPacket.readVarInt(buffer, 2);
/* 46 */       for (int i = 0; i < modCount; i++)
/*    */       {
/* 48 */         modTags.put(DefinedPacket.readString(buffer), DefinedPacket.readString(buffer));
/*    */       }
/*    */     }
/* 51 */     return modTags;
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public static int getFmlBuildNumber(Map<String, String> modList)
/*    */   {
/* 62 */     if (modList.containsKey("FML"))
/*    */     {
/* 64 */       String fmlVersion = (String)modList.get("FML");
/*    */       
/*    */ 
/* 67 */       if (fmlVersion.equals("7.10.99.99"))
/*    */       {
/* 69 */         Matcher matcher = ForgeConstants.FML_HANDSHAKE_VERSION_REGEX.matcher((CharSequence)modList.get("Forge"));
/* 70 */         if (matcher.find())
/*    */         {
/*    */ 
/* 73 */           return Integer.parseInt(matcher.group(4));
/*    */         }
/*    */       }
/*    */       else {
/* 77 */         Matcher matcher = ForgeConstants.FML_HANDSHAKE_VERSION_REGEX.matcher(fmlVersion);
/* 78 */         if (matcher.find())
/*    */         {
/*    */ 
/* 81 */           return Integer.parseInt(matcher.group(4));
/*    */         }
/*    */       }
/*    */     }
/*    */     
/* 86 */     return 0;
/*    */   }
/*    */ }


