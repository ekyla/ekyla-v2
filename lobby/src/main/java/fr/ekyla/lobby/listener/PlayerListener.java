package fr.ekyla.lobby.listener;

import fr.ekyla.api.EkylaApi;
import fr.ekyla.api.events.player.PlayerJoinEvent;
import fr.ekyla.api.i18n.TranslateMessage;
import fr.ekyla.api.listener.AutoListener;
import fr.ekyla.api.player.EkylaPlayer;
import fr.ekyla.api.storage.Storage;
import fr.ekyla.api.storage.StorageManager;
import org.bukkit.event.EventHandler;

/**
 * File <b>PlayerListener</b> located on fr.ekyla.lobby.listener
 * PlayerListener is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 17/08/2019 at 21:07
 */
public class PlayerListener extends AutoListener {

    public PlayerListener() {
        super(EkylaApi.getInstance());
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        EkylaPlayer player = event.getPlayer();
        if (player.hasPermission("player.server.join.message")) {
            api.getAnnouncer().broadcastMessage(new TranslateMessage("player.server.join.message", player.getName()));
        }

    }

}
