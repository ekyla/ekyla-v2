package fr.ekyla.lobby;

import fr.ekyla.lobby.listener.PlayerListener;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * File <b>Lobby</b> located on fr.ekyla.lobby
 * Lobby is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 17/08/2019 at 21:03
 */
public class Lobby extends JavaPlugin {

    @Override
    public void onEnable() {
        new PlayerListener();
    }
}
