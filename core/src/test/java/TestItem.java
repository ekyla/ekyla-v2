import fr.ekyla.api.EkylaApi;
import fr.ekyla.api.storage.items.Item;
import fr.ekyla.api.storage.items.ItemBuilder;
import org.bukkit.Material;

/**
 * File <b>TestItem</b> located on PACKAGE_NAME
 * TestItem is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 18/08/2019 at 13:48
 */
public class TestItem {

    public static void main(String... args) {
        Item item = EkylaApi.getInstance().getStorageManager().createItem(new ItemBuilder(Material.ARROW)).withProperties(properties -> {
            properties.setName("Une fleche");
            properties.setAmount(15);
        });
    }

}
