package fr.ekyla.core.commands;

import fr.ekyla.api.EkylaApi;
import fr.ekyla.api.exception.PlayerNotOnlineException;
import fr.ekyla.api.exception.RankNotFoundException;
import fr.ekyla.api.i18n.TranslateMessage;
import fr.ekyla.api.player.EkylaPlayer;
import fr.ekyla.api.player.PlayerInfo;
import fr.ekyla.api.player.Rank;

import java.util.List;
import java.util.stream.Collectors;

public class RankCommand {

    public RankCommand(EkylaApi api) {
        api.registerCommand("rank")
                .setUsage("<set|add|remove> (player) <rank>")
                .setPermission("admin.ranks.manage")
                .addBehavior(0, (player, argument) -> player.sendMessage(new TranslateMessage("player.command.rank.get", player.getRanks().stream().map(rank -> rank.getColor() + rank.getName(player.getLang())).collect(Collectors.joining("§7, ")))))
                .addBehavior(1, (player, argument) -> {
                    api.getPlayer(argument.getString(0)).ifPresentOrElse(target -> {
                        player.sendMessage(new TranslateMessage("player.command.rank.get.target", target.getName(), target.getRanks().stream().map(rank -> rank.getColor() + rank.getName(player.getLang())).collect(Collectors.joining("§7, "))));
                    }, () -> {
                        PlayerInfo info = api.getPlayerInfo(argument.getString(0));
                        if (info.isOnline()) {
                            List<Rank> ranks = info.getRanks();
                            player.sendMessage(String.valueOf(ranks));
                        } else {
                            player.sendMessage(new TranslateMessage("player.command.target.not-connected"));
                        }
                    });
                })

                .addArgument("set")
                .addBehavior(2, (player, argument) -> {
                    EkylaPlayer target = api.getPlayer(argument.getString(0)).orElseThrow(() -> new PlayerNotOnlineException(argument.getString(0)));
                    try {
                        Rank rank = Rank.valueOf(argument.getString(1).toUpperCase());
                        if (target.getRanks().contains(rank) && rank != Rank.PLAYER) {
                            player.sendMessage(new TranslateMessage("player.command.rank.set.already", rank.getName(player.getLang())));
                        } else {
                            target.getRanks().clear();
                            target.addRank(rank);
                            target.sendMessage(new TranslateMessage("player.command.rank.set.target", rank.getName(target.getLang())));
                            player.sendMessage(new TranslateMessage("player.command.rank.set.sender", target.getName(), rank.getName(target.getLang())));
                        }
                    } catch (IllegalArgumentException e) {
                        throw new RankNotFoundException(argument.getString(1));
                    }
                })

                .parent().addArgument("add")
                .addBehavior(2, (player, argument) -> {
                    EkylaPlayer target = api.getPlayer(argument.getString(0)).orElseThrow(() -> new PlayerNotOnlineException(argument.getString(0)));
                    try {
                        Rank rank = Rank.valueOf(argument.getString(1).toUpperCase());
                        if (target.getRanks().contains(rank) || rank == Rank.PLAYER) {
                            player.sendMessage(new TranslateMessage("player.command.rank.set.already", rank.getName(player.getLang())));
                        } else {
                            target.addRank(rank);
                            target.sendMessage(new TranslateMessage("player.command.rank.add.target", rank.getName(target.getLang())));
                            player.sendMessage(new TranslateMessage("player.command.rank.add.sender", rank.getName(target.getLang()), target.getName()));
                        }
                    } catch (IllegalArgumentException e) {
                        throw new RankNotFoundException(argument.getString(1));
                    }
                })

                .parent().addArgument("remove")
                .addBehavior(2, (player, argument) -> {
                    EkylaPlayer target = api.getPlayer(argument.getString(0)).orElseThrow(() -> new PlayerNotOnlineException(argument.getString(0)));
                    try {
                        Rank rank = Rank.valueOf(argument.getString(1).toUpperCase());
                        if (!target.getRanks().contains(rank) || rank == Rank.PLAYER) {
                            player.sendMessage(new TranslateMessage("player.command.rank.remove.impossible", rank.getName(player.getLang()), target.getName()));
                        } else {
                            target.removeRank(rank);
                            target.sendMessage(new TranslateMessage("player.command.rank.remove.target", rank.getName(target.getLang())));
                            player.sendMessage(new TranslateMessage("player.command.rank.remove.sender", rank.getName(target.getLang()), target.getName()));
                        }
                    } catch (IllegalArgumentException e) {
                        throw new RankNotFoundException(argument.getString(1));
                    }
                })

        ;
    }
}
