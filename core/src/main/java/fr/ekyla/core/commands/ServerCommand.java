package fr.ekyla.core.commands;

import fr.ekyla.api.bungeecord.BungeeApi;
import fr.ekyla.api.bungeecord.server.Server;

import java.util.stream.Collectors;

public class ServerCommand {

    public ServerCommand(BungeeApi api) {

        api.registerCommand("servers").addBehavior(0, (player, argument) -> {
            player.sendMessage("Servers: " + api.getServers().stream().map(Server::getName).collect(Collectors.joining(", ")));
        }).addArgument("send").addBehavior(1, (player, argument) -> {
            String serverName = argument.getString(0);
            api.getServer(serverName).ifPresentOrElse(player::connect, () -> {
                player.sendMessage("Unknown server");
            });
        });

    }
}
