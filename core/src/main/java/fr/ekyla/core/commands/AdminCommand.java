package fr.ekyla.core.commands;

import fr.ekyla.api.EkylaApi;
import fr.ekyla.api.exception.EkylaException;
import fr.ekyla.api.i18n.TranslateMessage;
import fr.ekyla.api.positions.Area;
import fr.ekyla.core.portals.Portal;
import fr.ekyla.core.portals.PortalAction;

public class AdminCommand {

    public AdminCommand(EkylaApi api) {
        api.registerCommand("admin")
                .setPermission("player.admin.toggle")
                .addBehavior(0, (player, argument) -> player.getAdminMode().toggle())

                .addArgument("portal")
                .setUsage("<add|remove|list> <args...>")

                .addArgument("add").addBehavior(1, (player, argument) -> {
            String id = argument.getString(0);
            Area selection = player.getAdminMode().getSelection();
            if (selection == null) {
                throw new EkylaException("player.admin.invalid-selection");
            } else {
                if (selection.getSizeX() >= 1 && selection.getSizeZ() >= 1)
                    throw new EkylaException("player.admin.invalid-selection");

                Portal portal = new Portal(selection, id);
                player.sendMessage(new TranslateMessage("player.admin.portals.add", id));
            }
        }).parent()

                .addArgument("remove").addBehavior(1, (player, argument) -> {

        }).parent()

                .addArgument("list").addBehavior(0, (player, argument) -> {

        }).parent()
                .addArgument("set").addBehavior(3, (player, argument) -> {
            try {
                Portal portal = Portal.getPortal(argument.getString(0)).orElseThrow(() -> new EkylaException("player.admin.portals.not-found"));

                portal.setAction(PortalAction.valueOf(argument.getString(1).toUpperCase()));
                portal.setActionParam(portal.getAction() == PortalAction.SERVER ? argument.getString(2) : player.getPosition());
                player.sendMessage(new TranslateMessage("player.admin.portals.set", argument.getString(1).toLowerCase(), argument.getString(2)));
            } catch (IllegalArgumentException e) {
                throw new EkylaException("player.command.bad-usage", "/admin", " portal set <id> <server|teleport> <value>");
            }
        })

        ;
    }
}
