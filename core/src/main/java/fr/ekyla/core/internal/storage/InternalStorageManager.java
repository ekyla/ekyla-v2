package fr.ekyla.core.internal.storage;

import com.google.common.base.Preconditions;
import fr.ekyla.api.storage.Menu;
import fr.ekyla.api.storage.Storage;
import fr.ekyla.api.storage.StorageManager;
import fr.ekyla.api.storage.items.Item;
import fr.ekyla.api.storage.items.ItemBuilder;
import fr.ekyla.core.internal.storage.items.InternalItem;
import net.minecraft.server.v1_12_R1.ItemStack;
import org.bukkit.Material;

import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * File <b>InternalStorageManager</b> located on fr.ekyla.core.internal.storage
 * InternalStorageManager is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 01/08/2019 at 22:04
 */
public class InternalStorageManager implements StorageManager {

    private final ConcurrentHashMap<Integer, Item> itemsCache;
    private final AtomicInteger itemCount;
    private final Item empty;

    public InternalStorageManager() {
        itemsCache = new ConcurrentHashMap<>();
        itemCount = new AtomicInteger(0);
        empty = new InternalItem(new ItemBuilder(Material.AIR), itemCount.getAndIncrement());
    }

    @Override
    public Optional<Item> getItem(ItemStack itemStack) {
        if (itemStack.getTag() == null) return Optional.empty();
        return itemsCache.values().stream().filter(item -> item.getId() == itemStack.getTag().getInt("id")).findFirst();
    }

    @Override
    public Optional<Item> getItem(int id) {
        return Optional.ofNullable(itemsCache.get(id));
    }

    @Override
    public Item createItem(ItemBuilder itemBuilder) {
        Preconditions.checkNotNull(itemBuilder);
        InternalItem item = new InternalItem(itemBuilder, itemCount.getAndIncrement());
        itemsCache.put(item.getId(), item);
        return item;
    }

    @Override
    public Menu createMenu(int lines, String name) {

        InternalMenu menu = new InternalMenu(lines, name);

        return null;
    }

    @Override
    public Item emptyItem() {
        return empty;
    }
}
