package fr.ekyla.core.internal.player.chat;

import fr.ekyla.api.player.chat.DialogMessage;
import fr.ekyla.core.EkylaCore;
import fr.ekyla.core.internal.player.InternalPlayer;
import org.bukkit.Bukkit;

import java.util.Map;

public class DialogChat {

    private boolean listeningForPlayer;
    private DialogMessage currentMessage;
    private DialogMessage nextMessage;
    private Map<String, DialogMessage> messages;

    public DialogChat(InternalPlayer player, Map<String, DialogMessage> messages, String firstMessageKey) {
        this.messages = messages;
        currentMessage = messages.get(firstMessageKey);
        listeningForPlayer = false;
        Bukkit.getScheduler().scheduleSyncDelayedTask(EkylaCore.getInstance(), () -> {
            player.sendMessage(currentMessage.getMessage());
            if (currentMessage.waitForAnswer()) {
                listeningForPlayer = true;
            } else {
                goToNextMessage(player, null);
            }
        }, currentMessage.getDelay());
    }

    public void setNextMessage(String messageKey) {
        this.nextMessage = messages.get(messageKey);
    }

    public void goToNextMessage(InternalPlayer player, String message) {
        listeningForPlayer = false;
        if (currentMessage.getAction() != null) {
            currentMessage.getAction().done(player, message);
        }
        if (nextMessage == null) {
            player.endDialog();
        } else {
            currentMessage = nextMessage;
            nextMessage = null;
            Bukkit.getScheduler().scheduleSyncDelayedTask(EkylaCore.getInstance(), () -> {
                player.sendMessage(currentMessage.getMessage());
                if (currentMessage.waitForAnswer()) {
                    listeningForPlayer = true;
                } else {
                    goToNextMessage(player, null);
                }
            }, currentMessage.getDelay());
        }
    }

    public boolean isListeningForPlayer() {
        return listeningForPlayer;
    }
}
