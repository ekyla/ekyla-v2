package fr.ekyla.core.internal.player;

import fr.ekyla.api.EkylaApi;
import fr.ekyla.api.i18n.Lang;
import fr.ekyla.api.i18n.TranslateMessage;
import fr.ekyla.api.net.packet.play.server.PlayerPositionPacket;
import fr.ekyla.api.player.AdminMode;
import fr.ekyla.api.player.EkylaPlayer;
import fr.ekyla.api.player.Rank;
import fr.ekyla.api.player.chat.DialogMessage;
import fr.ekyla.api.positions.Position;
import fr.ekyla.api.storage.PlayerStorage;
import fr.ekyla.core.EkylaCore;
import fr.ekyla.core.internal.player.chat.DialogChat;
import fr.ekyla.core.internal.storage.InternalPlayerStorage;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;

import java.net.InetSocketAddress;
import java.util.*;

/**
 * File <b>InternalPlayer1</b> located on fr.ekyla.core.internal.Player
 * InternalPlayer1 is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 31/07/2019 at 18:46
 */
public class InternalPlayer implements EkylaPlayer {

    private final CraftPlayer craftPlayer;
    private final int redisId;
    private final int id;
    private final List<String> permissions;
    private final List<Rank> ranks;
    private final InetSocketAddress address;
    private final String username;
    private final InternalPlayerStorage playerStorage;
    private final Lang lang;
    private final AdminMode adminMode;
    public Position position;
    private DialogChat dialogChat;

    public InternalPlayer(int redisId, int id, UUID uuid, String username, List<Rank> ranks, List<String> permissions, Lang lang, InetSocketAddress address) {
        this.craftPlayer = (CraftPlayer) Bukkit.getPlayer(uuid);
        this.username = username;
        this.id = id;
        this.redisId = redisId;
        this.address = address;

        this.playerStorage = new InternalPlayerStorage(this);
        this.ranks = new ArrayList<>(ranks);
        if (!this.ranks.contains(Rank.PLAYER)) this.ranks.add(Rank.PLAYER);
        this.permissions = new ArrayList<>(permissions);
        this.lang = lang == null ? Lang.FRENCH : lang;
        this.dialogChat = null;
        this.adminMode = new InternalAdminMode(this);
        this.position = new Position(craftPlayer.getLocation());

        ((EkylaCore) EkylaApi.getInstance()).addPlayer(this);
    }

    @Override
    public InetSocketAddress getAddress() {
        return address;
    }

    public int getRedisId() {
        return redisId;
    }

    @Override
    public int getPlayerId() {
        return id;
    }

    @Override
    public String getName() {
        return username;
    }

    @Override
    public List<String> getPermissions() {
        return new ArrayList<>(permissions);
    }

    @Override
    public void disconnect() {
        ((EkylaCore) EkylaApi.getInstance()).removePlayer(this);
    }

    @Override
    public PlayerStorage getStorage() {
        return playerStorage;
    }

    @Override
    public UUID getUuid() {
        return craftPlayer.getUniqueId();
    }

    @Override
    public String getUsername() {
        return craftPlayer.getName();
    }

    @Override
    public CraftPlayer getCraftPlayer() {
        return craftPlayer;
    }

    @Override
    public Lang getLang() {
        return lang;
    }

    @Override
    public EkylaPlayer sendMessage(TranslateMessage message) {
        craftPlayer.sendMessage(lang.format(message.getKey(), message.getParameterss().toArray()));
        return this;
    }

    @Override
    public List<Rank> getRanks() {
        return ranks;
    }

    @Override
    public Rank getRank() {
        return ranks.stream().min(Comparator.comparingInt(Rank::getId)).orElse(Rank.PLAYER);
    }

    @Override
    public EkylaPlayer addRank(Rank rank) {
        if (!ranks.contains(rank)) ranks.add(rank);
        return this;
    }

    @Override
    public EkylaPlayer removeRank(Rank rank) {
        ranks.remove(rank);
        return this;
    }

    @Override
    public boolean hasPermission(String permission) {
        return permissions.stream().anyMatch(permission::equalsIgnoreCase) || getRank().getPermissions().stream().anyMatch(permission::equalsIgnoreCase) || getRank().equals(Rank.ADMIN);
    }

    @Override
    public boolean addPersmmsion(String permission) {
        return permissions.add(permission);
    }

    @Override
    public boolean removePermission(String permission) {
        return permissions.remove(permission);
    }


    @Override
    public void startDialog(Map<String, DialogMessage> messages, String firstMessageKey) {
        dialogChat = new DialogChat(this, messages, firstMessageKey);
    }

    @Override
    public void setNextDialogMessage(String messsageKey) {
        if (dialogChat != null) {
            dialogChat.setNextMessage(messsageKey);
        }
    }

    @Override
    public void endDialog() {
        dialogChat = null;
    }

    public DialogChat getDialogChat() {
        return dialogChat;
    }

    @Override
    public AdminMode getAdminMode() {
        return adminMode;
    }

    public Position getPosition() {
        return position;
    }

    @Override
    public EkylaPlayer teleport(Position position) {
        new PlayerPositionPacket(position, 0).setId(this).send(this);
        this.position = position;
        return this;
    }
}
