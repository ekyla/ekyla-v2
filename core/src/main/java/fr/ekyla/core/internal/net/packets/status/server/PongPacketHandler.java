package fr.ekyla.core.internal.net.packets.status.server;

import fr.ekyla.api.net.PacketEvent;
import fr.ekyla.api.net.PacketHandler;
import fr.ekyla.api.net.PacketType;
import fr.ekyla.api.net.packet.status.server.PongPacket;

/**
 * File <b>PongPacketHandler</b> located on fr.ekyla.core.internal.net.packets.status.server
 * PongPacketHandler is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 01/08/2019 at 09:25
 */
public class PongPacketHandler extends PacketHandler<PongPacket> {

    public PongPacketHandler() {
        super(PacketType.Status.Server.PONG);
    }

    @Override
    public void handlePacket(PacketEvent<PongPacket> event) {
        System.out.println(event.getPlayer().getUsername() + " sent a pong packet with " + event.getPacket().getPayloadResponse());
    }
}
