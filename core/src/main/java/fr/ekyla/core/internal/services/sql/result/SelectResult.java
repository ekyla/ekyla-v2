package fr.ekyla.core.internal.services.sql.result;

import fr.ekyla.api.services.sql.SqlResult;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * File <b>SelectResult</b> located on fr.ekyla.core.internal.services.sql.result
 * SelectResult is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 18/08/2019 at 22:21
 */
public class SelectResult implements SqlResult {

    private final List<LinkedHashMap<String, Object>> result = new ArrayList<>();
    private AtomicInteger count = new AtomicInteger(-1);

    public SelectResult(ResultSet result, Connection connection) {

        try {
            ResultSetMetaData metaData = result.getMetaData();

            while (result.next()) {

                LinkedHashMap<String, Object> map = new LinkedHashMap<>();

                for (int i = 1; i <= metaData.getColumnCount(); i++) {
                    map.put(metaData.getColumnName(i), result.getObject(i));
                }
                this.result.add(map);
            }

            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "SelectResult{" +
                "result=" + result +
                ", count=" + count +
                '}';
    }

    @Override
    public boolean next() {
        if (result.size() == 0) return false;
        return count.getAndIncrement() < result.size() - 1;
    }

    public int getCount() {
        return count.get() == -1 ? 0 : count.get();
    }

    @Override
    public Object get(String key) {
        return result.get(getCount()).get(key);
    }

    @Override
    public String getString(String key) {
        return String.valueOf(result.get(getCount()).get(key));
    }

    @Override
    public Integer getInt(String key) {
        return (Integer) result.get(getCount()).get(key);
    }

    @Override
    public Boolean getBoolean(String key) {
        return (Boolean) result.get(getCount()).get(key);
    }

    @Override
    public Object get(int columnid) {
        return ((ArrayList<Object>) result.get(getCount()).values()).get(columnid);
    }

    @Override
    public String getString(int columnid) {
        return String.valueOf(((ArrayList<Object>) result.get(getCount()).values()).get(columnid));
    }

    @Override
    public Integer getInt(int columnid) {
        return (Integer) ((ArrayList<Object>) result.get(getCount()).values()).get(columnid);
    }

    @Override
    public Boolean getBoolean(int columnid) {
        return (Boolean) ((ArrayList<Object>) result.get(getCount()).values()).get(columnid);
    }

}
