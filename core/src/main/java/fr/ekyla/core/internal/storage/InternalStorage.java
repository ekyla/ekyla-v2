package fr.ekyla.core.internal.storage;

import com.google.common.base.Preconditions;
import fr.ekyla.api.storage.items.Item;
import fr.ekyla.api.storage.Storage;
import org.bukkit.inventory.Inventory;

/**
 * File <b>InternalStorage</b> located on fr.ekyla.core.internal.storage
 * InternalStorage is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 02/08/2019 at 19:14
 */
public abstract class InternalStorage implements Storage {

    private final int size;
    private final String name;

    protected InternalStorage(int size, String name) {
        this.size = size;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public Storage addItem(Item item) {
        Preconditions.checkNotNull(item);
        return set(nextFreeSlot(), item);
    }

    @Override
    public Storage setItem(int position, Item item) {
        Preconditions.checkNotNull(item);
        return set(position, item);
    }

    @Override
    public Storage setItem(int row, int column, Item item) {
        Preconditions.checkNotNull(item);
        Preconditions.checkState(row >= 0 && row <= size % 9);
        Preconditions.checkState(column >= 1 && column <= 9);
        return set(row * 9 + column - 1, item);
    }

    @Override
    public Storage removeItem(int position) {
        return remove(position);
    }

    @Override
    public Storage removeItem(int row, int column) {
        Preconditions.checkState(row >= 0 && row <= size % 9);
        Preconditions.checkState(column >= 1 && column <= 9);

        return remove(row * 9 + column - 1);
    }

    protected abstract Storage set(int position, Item item);

    protected abstract Storage remove(int position);

    protected abstract int nextFreeSlot();

    protected abstract Inventory getHandle();

}
