package fr.ekyla.core.internal.net.packets.status.client;

import fr.ekyla.api.net.PacketEvent;
import fr.ekyla.api.net.PacketHandler;
import fr.ekyla.api.net.PacketType;
import fr.ekyla.api.net.packet.status.client.PingPacket;

/**
 * File <b>PingPacketHandler</b> located on fr.ekyla.core.internal.net.packets.status.client
 * PingPacketHandler is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 01/08/2019 at 09:24
 */
public class PingPacketHandler extends PacketHandler<PingPacket> {

    public PingPacketHandler() {
        super(PacketType.Status.Client.PING);
    }

    @Override
    public void handlePacket(PacketEvent<PingPacket> event) {
        System.out.println("ping packet " + event.getPacket().getPayload() + " from " + event.getPlayer());
    }
}
