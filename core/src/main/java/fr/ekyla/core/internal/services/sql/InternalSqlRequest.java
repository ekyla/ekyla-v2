package fr.ekyla.core.internal.services.sql;

import fr.ekyla.api.services.sql.SqlRequest;
import fr.ekyla.api.services.sql.SqlResult;
import fr.ekyla.core.internal.services.sql.result.InsertResult;
import fr.ekyla.core.internal.services.sql.result.SelectResult;
import fr.ekyla.core.internal.services.sql.result.UpdateResult;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * File <b>InternalSqlRequest</b> located on fr.ekyla.core.internal.services.sql
 * InternalSqlRequest is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 18/08/2019 at 14:50
 */
public class InternalSqlRequest implements SqlRequest {

    private final Connection connection;

    private final RequestType requestType;
    private final String table;

    private List<String> wheres = new ArrayList<>();

    private LinkedHashMap<String, String> values = new LinkedHashMap<>();
    private List<String> columns;

    public InternalSqlRequest(Connection connection, RequestType type, String table) {
        this.connection = connection;
        this.requestType = type;
        this.table = table;
    }

    @Override
    public SqlRequest where(String key, Object obj, Check check) {
        if (requestType != RequestType.SELECT && requestType != RequestType.UPDATE) return this;
        wheres.add(key + " " + check.getSymbol() + " " + (obj instanceof Number ? obj.toString() : "'" + obj.toString() + "'"));
        return this;
    }

    @Override
    public SqlResult result() {
        try {
            PreparedStatement statement = build();
            if (requestType != RequestType.SELECT) statement.executeUpdate();
            ResultSet set = requestType.equals(RequestType.SELECT) ? statement.executeQuery() : statement.getGeneratedKeys();

            switch (requestType) {
                case UPDATE:
                    return new UpdateResult(set, connection);
                case SELECT:
                    return new SelectResult(set, connection);
                case INSERT:
                    return new InsertResult(set, connection);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private PreparedStatement build() throws SQLException {
        String type = requestType.name();
        StringBuilder request = new StringBuilder(type).append(" ");

        switch (requestType) {
            case SELECT:
                String cols = String.join(", ", columns);
                request.append(cols).append(" FROM ").append(table);
                if (!wheres.isEmpty()) {
                    request.append(" WHERE ");
                    wheres.forEach(where -> {
                        request.append(where).append(" AND ");
                    });
                    request.replace(request.length() - 5, request.length(), "");
                }
                request.append(";");
                return connection.prepareStatement(request.toString());

            case INSERT:
                request.append("INTO ").append(table).append("(");
                values.keySet().forEach(key -> {
                    request.append(key).append(", ");
                });
                request.replace(request.length() - 2, request.length(), "").append(") VALUES(");

                for (int i = 0; i < values.size(); i++) {
                    request.append("?, ");
                }
                request.replace(request.length() - 2, request.length(), "").append(");");

                PreparedStatement statement = connection.prepareStatement(request.toString(), Statement.RETURN_GENERATED_KEYS);

                for (int i = 1; i <= values.values().size(); i++) {
                    statement.setObject(i, values.values().toArray()[i - 1]);
                }

                return statement;

            case UPDATE:
                request.append(table).append(" SET ");
                values.forEach((key, value) -> {
                    request.append(key).append("=?").append(", ");
                });
                request.replace(request.length() - 2, request.length(), "");
                if (!wheres.isEmpty()) {
                    request.append(" WHERE ");
                    wheres.forEach(where -> request.append(where).append(" AND "));
                    request.replace(request.length() - 5, request.length(), "");
                }

                PreparedStatement update = connection.prepareStatement(request.toString(), Statement.RETURN_GENERATED_KEYS);

                for (int i = 1; i <= values.values().size(); i++) {
                    update.setObject(i, values.values().toArray()[i - 1]);
                }

                return update;
        }
        return null;
    }

    @Override
    public SqlRequest keys(String... keys) {
        for (String key : keys)
            values.put(key, "");

        return this;
    }

    @Override
    public SqlRequest values(Object... values) {
        final AtomicInteger i = new AtomicInteger(0);
        this.values.forEach((s, s2) -> this.values.put(s, values[i.getAndIncrement()].toString()));
        return this;
    }

    public SqlRequest columns(String[] columns) {
        this.columns = Arrays.asList(columns);
        return this;
    }

    public enum RequestType {

        SELECT,
        UPDATE,
        INSERT

    }
}
