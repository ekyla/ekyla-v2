package fr.ekyla.core.internal.player;

import fr.ekyla.api.EkylaApi;
import fr.ekyla.api.events.player.items.ItemClickEvent;
import fr.ekyla.api.i18n.TranslateMessage;
import fr.ekyla.api.player.AdminMode;
import fr.ekyla.api.positions.Area;
import fr.ekyla.api.positions.Position;
import fr.ekyla.api.storage.blocks.Block;
import fr.ekyla.api.storage.items.Item;
import fr.ekyla.api.storage.items.ItemBuilder;
import net.minecraft.server.v1_12_R1.ItemStack;
import net.minecraft.server.v1_12_R1.NonNullList;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftInventoryPlayer;

public class InternalAdminMode implements AdminMode {

    private final InternalPlayer player;

    private Position selectionFirst, selectionSecond;
    private boolean isAdminMode = false;

    public InternalAdminMode(InternalPlayer player) {
        this.player = player;
    }

    @Override
    public void toggle() {
        if (player.hasPermission("player.admin.toggle")) {
            isAdminMode = !isAdminMode;
            player.sendMessage(new TranslateMessage("player.admin.mode." + isAdminMode()));

            if (isAdminMode) {
                Item stick = EkylaApi.getInstance().getStorageManager().createItem(new ItemBuilder(Material.STICK));

                stick.getProperties().setAction(itemAction -> {
                    if (itemAction.getTarget() instanceof Block) {
                        Block target = (Block) itemAction.getTarget();
                        if (itemAction.getClick() == ItemClickEvent.ClickType.LEFT_CLICK) {
                            selectionFirst = target.getPosition();
                            player.sendMessage("Pos 1: " + selectionFirst.loc2str());
                        } else if (itemAction.getClick() == ItemClickEvent.ClickType.RIGHT_CLICK) {
                            selectionSecond = target.getPosition();
                            player.sendMessage("Pos 2: " + selectionSecond.loc2str());
                        }
                        itemAction.setCancelled(true);
                    }
                });

                CraftPlayer bkPlayer = player.getCraftPlayer();
                NonNullList<ItemStack> items = ((CraftInventoryPlayer) bkPlayer.getInventory()).getInventory().items;
                items.clear();
                items.set(0, stick.toItemStack());
            } else {
                CraftPlayer bkPlayer = player.getCraftPlayer();
                NonNullList<ItemStack> items = ((CraftInventoryPlayer) bkPlayer.getInventory()).getInventory().items;
                items.clear();
                for (int i = 0; i < player.getStorage().getItems().size(); i++) {
                    Item it = player.getStorage().getItems().get(i);
                    items.set(i, it.toItemStack());
                }
            }
        }
    }

    @Override
    public boolean isAdminMode() {
        return isAdminMode;
    }

    @Override
    public Position getSelect1() {
        return selectionFirst;
    }

    @Override
    public Position getSelect2() {
        return selectionSecond;
    }

    @Override
    public Area getSelection() {
        return selectionFirst != null && selectionSecond != null ? new Area(selectionFirst, selectionSecond) : null;
    }
}
