package fr.ekyla.core.internal.announce;

import fr.ekyla.api.EkylaApi;
import fr.ekyla.api.announce.Announcer;
import fr.ekyla.api.i18n.TranslateMessage;

/**
 * File <b>InternalAnnouncer</b> located on fr.ekyla.core.announce
 * InternalAnnouncer is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 17/08/2019 at 21:30
 */
public class InternalAnnouncer implements Announcer {

    @Override
    public Announcer broadcastMessage(TranslateMessage message) {
        EkylaApi.getInstance().getPlayers().forEach(player -> player.sendMessage(message));
        return this;
    }

    @Override
    public Announcer broadcastBarMessage(TranslateMessage message) {
        return this;
    }
}
