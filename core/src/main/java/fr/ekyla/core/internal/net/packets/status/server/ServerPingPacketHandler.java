package fr.ekyla.core.internal.net.packets.status.server;

import fr.ekyla.api.net.PacketEvent;
import fr.ekyla.api.net.PacketHandler;
import fr.ekyla.api.net.PacketType;
import fr.ekyla.api.net.packet.status.server.ServerPingPacket;

/**
 * File <b>ServerPingPacketHandler</b> located on fr.ekyla.core.internal.net.packets.status.server
 * ServerPingPacketHandler is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 01/08/2019 at 11:13
 */
public class ServerPingPacketHandler extends PacketHandler<ServerPingPacket> {

    public ServerPingPacketHandler() {
        super(PacketType.Status.Server.SERVER_INFO);
    }

    @Override
    public void handlePacket(PacketEvent<ServerPingPacket> event) {
        System.out.println("onlineplayers: " + event.getPacket().getResponse().getOnlinePlayer());
        event.getPacket().getResponse().setMaxPlayer(100);
    }
}
