package fr.ekyla.core.internal.net.packets.play.client;

import fr.ekyla.api.EkylaApi;
import fr.ekyla.api.commands.Command;
import fr.ekyla.api.exception.EkylaException;
import fr.ekyla.api.exception.NoPermissionException;
import fr.ekyla.api.i18n.TranslateMessage;
import fr.ekyla.api.net.PacketEvent;
import fr.ekyla.api.net.PacketHandler;
import fr.ekyla.api.net.PacketType;
import fr.ekyla.api.net.packet.play.client.ChatInPacket;
import fr.ekyla.api.player.EkylaPlayer;
import fr.ekyla.core.internal.commands.InternalArgument;
import fr.ekyla.core.internal.commands.InternalCommand;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.plugin.SimplePluginManager;

import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * File <b>ChatInPacketHandler</b> located on fr.ekyla.core.internal.net.packet.play.client
 * ChatInPacketHandler is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 01/08/2019 at 11:34
 */
public class ChatInPacketHandler extends PacketHandler<ChatInPacket> {

    public ChatInPacketHandler() {
        super(PacketType.Play.Client.CHAT);
    }

    @Override
    public void handlePacket(PacketEvent<ChatInPacket> event) {
        String message = event.getPacket().getMessage();
        EkylaPlayer player = event.getPlayer();
        if (message.startsWith("/")) {
            message = message.replaceAll(" +", " ");
            String[] splitted = message.substring(1).split(" ");
            if (splitted.length == 0) return;
            String commandName = splitted[0];
            Optional<InternalCommand> optional = EkylaApi.getInstance().getCommands().stream().filter(cmd -> ((InternalCommand) cmd).getName().equalsIgnoreCase(commandName)).map(cmd -> (InternalCommand) cmd).findFirst();
            optional.ifPresentOrElse((command) -> {
                if (splitted.length > 1) {
                    List<String> args = new ArrayList<>(Arrays.asList(splitted).subList(1, splitted.length));
                    AtomicReference<Command> lastCmd = new AtomicReference<>(command);
                    List<Object> toPass = new ArrayList<>(args);
                    args.forEach(arg -> {
                        if (event.isCancelled()) return;
                        Optional<InternalCommand> opt = ((InternalCommand) lastCmd.get()).getArgument(arg);
                        opt.ifPresentOrElse(subCommand -> {
                            lastCmd.set(subCommand);
                            toPass.remove(arg);
                            if (toPass.isEmpty()) {
                                try {
                                    Command ccmd = lastCmd.get();
                                    if (player.hasPermission(ccmd.getPermission()))
                                        ccmd.execute(player, new InternalArgument(Collections.emptyList()));
                                    else throw new NoPermissionException();
                                } catch (EkylaException e) {
                                    player.sendMessage(new TranslateMessage(e.getMessage(), e.getParams()));
                                } finally {
                                    event.setCancelled(true);
                                }
                            }
                        }, () -> {
                            Command last = lastCmd.get();
                            try {
                                if (player.hasPermission(last.getPermission())) {
                                    if (!last.execute(player, new InternalArgument(toPass))) {
                                        String fullCmd = "/" + commandName + " " + args.stream().filter(argu -> !toPass.contains(argu)).collect(Collectors.joining(" "));
                                        player.sendMessage(new TranslateMessage("player.command.bad-usage", fullCmd, last.getUsage()));
                                    }
                                } else
                                    throw new NoPermissionException();
                            } catch (EkylaException e) {
                                player.sendMessage(new TranslateMessage(e.getMessage(), e.getParams()));
                            } finally {
                                event.setCancelled(true);
                            }
                        });
                    });
                } else {
                    try {
                        if (player.hasPermission(command.getPermission())) {
                            command.execute(player, new InternalArgument(Collections.emptyList()));
                        } else throw new NoPermissionException();
                    } catch (EkylaException e) {
                        player.sendMessage(new TranslateMessage(e.getMessage(), e.getParams()));
                    } finally {
                        event.setCancelled(true);
                    }
                }
            }, () -> {
                SimplePluginManager pm = (SimplePluginManager) EkylaApi.getInstance().getServer().getPluginManager();
                try {
                    Class<?> clzz = Class.forName("org.bukkit.plugin.SimplePluginManager");
                    Field field = clzz.getDeclaredField("commandMap");
                    field.setAccessible(true);
                    SimpleCommandMap map = (SimpleCommandMap) field.get(pm);
                    map.getCommands().stream().filter(command -> command.getLabel().equalsIgnoreCase(commandName)).findFirst().ifPresentOrElse((command) -> {
                        //Let Spigot handle the command
                    }, () -> {
                        player.sendMessage(new TranslateMessage("player.command.not-found", commandName));
                        event.setCancelled(true);
                    });
                } catch (ClassNotFoundException | NoSuchFieldException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            });
        }
    }
}
