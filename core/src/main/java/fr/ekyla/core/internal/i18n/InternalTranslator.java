package fr.ekyla.core.internal.i18n;

import fr.ekyla.api.i18n.Lang;
import fr.ekyla.api.i18n.Translator;
import org.bukkit.ChatColor;

/**
 * File <b>InternalTranslator</b> located on fr.ekyla.core.internal.i18n
 * InternalTranslator is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 02/08/2019 at 19:37
 */
public class InternalTranslator implements Translator {

    private final InternalLangLoader loader;

    public InternalTranslator() {
        this.loader = new InternalLangLoader();
    }

    @Override
    public String format(Lang lang, String key, Object... params) {
        return ChatColor.translateAlternateColorCodes('&', String.format(loader.values.get(lang).getOrDefault(key, key), params));
    }

    public InternalLangLoader getLoader() {
        return loader;
    }
}
