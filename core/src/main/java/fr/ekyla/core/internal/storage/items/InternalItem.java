package fr.ekyla.core.internal.storage.items;

import fr.ekyla.api.storage.items.Item;
import fr.ekyla.api.storage.items.ItemBuilder;
import fr.ekyla.api.storage.items.ItemProperties;
import net.minecraft.server.v1_12_R1.ItemStack;
import net.minecraft.server.v1_12_R1.NBTTagCompound;

import java.util.function.Consumer;

/**
 * File <b>InternalItem</b> located on fr.ekyla.core.internal.storage
 * InternalItem is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 01/08/2019 at 22:21
 */
public class InternalItem implements Item {

    private final ItemBuilder builder;
    private final int id;

    private ItemStack nmsItem;
    private ItemProperties properties;

    public InternalItem(ItemBuilder builder, int id) {
        this.builder = builder;
        this.id = id;
        this.properties = new InternalItemProperties(builder);

        builder.getCompound().set("tag", new NBTTagCompound());
        builder.getCompound().getCompound("tag").setInt("id", id);
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public ItemStack toItemStack() {
        if (nmsItem == null) {
            this.nmsItem = new ItemStack(builder.getCompound());
        }
        return nmsItem;
    }

    @Override
    public ItemProperties getProperties() {
        return properties;
    }

    @Override
    public Item withProperties(Consumer<ItemProperties> properties) {
        properties.accept(this.properties);
        return this;
    }

    @Override
    public String toString() {
        return "InternalItem{" +
                builder.getMaterial() +
                "}";
    }
}
