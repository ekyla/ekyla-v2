package fr.ekyla.core.internal.storage.items;

import fr.ekyla.api.callbacks.Callback;
import fr.ekyla.api.storage.items.ItemBuilder;
import fr.ekyla.api.storage.items.ItemProperties;
import fr.ekyla.api.storage.items.events.ItemAction;
import net.minecraft.server.v1_12_R1.NBTBase;
import net.minecraft.server.v1_12_R1.NBTTagCompound;
import net.minecraft.server.v1_12_R1.NBTTagList;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * File <b>InternalItemProperties</b> located on fr.ekyla.core.internal.storage.items
 * InternalItemProperties is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 02/08/2019 at 22:24
 */
public class InternalItemProperties implements ItemProperties {

    private final ItemBuilder builder;
    private final boolean playerSkull = false;
    private Callback<ItemAction> action;

    public InternalItemProperties(ItemBuilder builder) {
        this.builder = builder;
        getTag();
    }

    private NBTTagCompound getTag() {
        if (!builder.getCompound().hasKey("tag"))
            builder.getCompound().set("tag", new NBTTagCompound());
        return builder.getCompound().getCompound("tag");
    }

    @Override
    public ItemProperties addEnchant(Enchantment enchantment, int level) {
        NBTTagCompound tag = getTag();
        if (!tag.hasKeyOfType("ench", 9)) tag.set("ench", new NBTTagList());

        NBTTagList list = tag.getList("ench", 10);
        NBTTagCompound nbttagcompound = new NBTTagCompound();
        nbttagcompound.setShort("id", (short) enchantment.getId());
        nbttagcompound.setShort("lvl", (short) level);
        list.add(nbttagcompound);

        return this;
    }

    @Override
    public Map<Enchantment, Integer> getEnchants() {
        return null;
    }

    @Override
    public ItemProperties addItemFlag(ItemFlag... flags) {
        NBTTagCompound tag = getTag();

        if (!tag.hasKeyOfType("HideFlags", 3))
            tag.setInt("HideFlags", 0);

        int hideFlag = tag.getInt("HideFlags");
        for (var flag : flags) {
            hideFlag |= this.getBitModifier(flag);
            builder.getCompound().setInt("HideFlags", hideFlag);
        }

        return this;
    }

    private byte getBitModifier(ItemFlag hideFlag) {
        return (byte) (1 << hideFlag.ordinal());
    }

    @Override
    public List<ItemFlag> getItemFlags() {
        return new ArrayList<>();
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public ItemProperties setName(String name) {
        NBTTagCompound tag = getTag();

        if (!tag.hasKey("display")) tag.set("display", new NBTTagCompound());

        NBTTagCompound display = tag.getCompound("display");
        display.setString("Name", name);
        return this;
    }

    @Override
    public ItemProperties addNBTTag(String key, NBTBase nbtTag) {
        return null;
    }

    @Override
    public Material getMaterial() {
        return builder.getMaterial();
    }

    @Override
    public int getAmount() {
        return builder.getAmount();
    }

    @Override
    public ItemProperties setAmount(int amount) {
        return null;
    }

    @Override
    public short getDurability() {
        return 0;
    }

    @Override
    public ItemProperties setDurability(short durability) {
        return null;
    }

    @Override
    public byte getData() {
        return builder.getData();
    }

    @Override
    public ItemProperties setData(byte data) {
        builder.setData(data);
        return this;
    }

    @Override
    public boolean isGlowing() {
        return false;
    }

    @Override
    public ItemProperties setGlowing(boolean glowing) {
        return null;
    }

    @Override
    public boolean isUnbreakable() {
        return false;
    }

    @Override
    public ItemProperties setUnbreakable(boolean unbreakable) {
        return null;
    }

    @Override
    public ItemProperties usePlayerHead() {
        return null;
    }

    @Override
    public ItemProperties setAction(Callback<ItemAction> callback) {
        action = callback;
        return this;
    }

    @Override
    public Callback<ItemAction> getAction() {
        return action;
    }
}
