package fr.ekyla.core.internal.commands;

import fr.ekyla.api.callbacks.DualCallback;
import fr.ekyla.api.commands.Argument;
import fr.ekyla.api.commands.Command;
import fr.ekyla.api.exception.EkylaException;
import fr.ekyla.api.i18n.TranslateMessage;
import fr.ekyla.api.player.EkylaPlayer;

import java.util.*;

/**
 * File <b>InternalCommand</b> located on fr.ekyla.core.internal.commands
 * InternalCommand is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 25/08/2019 at 15:39
 */
public class InternalCommand implements Command {

    private final String name;
    private final Map<Integer, DualCallback<EkylaPlayer, Argument>> behaviors;
    private final Command parent;
    List<InternalCommand> arguments;
    private String usage = "";
    private String permission = "";

    public InternalCommand(Command parent, String name) {
        this.parent = parent;
        this.name = name;
        this.arguments = new ArrayList<>();
        this.behaviors = new HashMap<>();
    }

    public InternalCommand(String name) {
        this(null, name);
    }

    @Override
    public Command parent() {
        return parent;
    }

    @Override
    public Command setUsage(String usage) {
        this.usage = usage;
        return this;
    }

    public String getUsage() {
        return usage;
    }

    public Optional<InternalCommand> getArgument(String name) {
        return arguments.stream().filter(subCommand -> subCommand.getName().equalsIgnoreCase(name) || subCommand.getName().equals("")).findFirst();
    }

    public String getName() {
        return name;
    }

    @Override
    public Command addBehavior(int argumentNumber, DualCallback<EkylaPlayer, Argument> behavior) {
        this.behaviors.put(argumentNumber, behavior);
        return this;
    }

    @Override
    public Command addArgument(String name) {
        InternalCommand subcommand = new InternalCommand(this, name);
        this.arguments.add(subcommand);
        return subcommand;
    }

    public Map<Integer, DualCallback<EkylaPlayer, Argument>> getBehaviors() {
        return behaviors;
    }

    @Override
    public boolean execute(EkylaPlayer player, Argument argument) throws EkylaException {
        Optional<Integer> opt = behaviors.keySet().stream().filter(args -> args == argument.number()).findFirst();
        if (opt.isPresent()) {
            int args = opt.get();
            DualCallback<EkylaPlayer, Argument> behavior = behaviors.get(args);
            try {
                behavior.done(player, argument);
            } catch (EkylaException e) {
                player.sendMessage(new TranslateMessage(e.getMessage(), e.getParams()));
            }
            return true;
        }
        return false;
    }

    @Override
    public String getPermission() {
        return permission;
    }

    @Override
    public InternalCommand setPermission(String permission) {
        this.permission = permission;
        return this;
    }
}
