package fr.ekyla.core.internal.net;

import com.google.common.base.Preconditions;
import fr.ekyla.api.net.*;
import fr.ekyla.api.player.EkylaPlayer;
import fr.ekyla.api.utils.NMSReflection;
import io.netty.channel.Channel;
import net.minecraft.server.v1_12_R1.Packet;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * File <b>PacketHandlerManager</b> located on fr.ekyla.core.internal.net
 * PacketHandlerManager is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 31/07/2019 at 18:46
 */
public class PacketHandlerManager implements ProtocolManager {

    private final Map<String, List<PacketHandler>> handlers;

    public PacketHandlerManager() {
        new PacketInterceptor(this);
        this.handlers = new HashMap<>();
    }

    @Override
    public <T extends GamePacket> void sendPacket(EkylaPlayer player, T packet) {
        Preconditions.checkNotNull(player);
        Preconditions.checkNotNull(packet);

        CraftPlayer cPlayer = player.getCraftPlayer();
        if (packet.getPacketType().getBound() == PacketType.Bound.OUT)
            cPlayer.getHandle().playerConnection.sendPacket(packet.getPacket());
        else
            throw new UnsupportedOperationException("Can't send an input packet (" + packet.getPacketType().getID() + ") to a player");
    }

    @Override
    public void subscribeHandler(PacketHandler<? extends GamePacket> packetHandler) {
        Preconditions.checkNotNull(packetHandler);

        var packetHandlers = handlers.computeIfAbsent(packetHandler.getPacketType().getID(), k -> new ArrayList<>());
        packetHandlers.add(packetHandler);
    }

    @Override
    public void unsubscribeHandler(PacketHandler<? extends GamePacket> packetHandler) {
        Preconditions.checkNotNull(packetHandler);

        if (handlers.containsKey(packetHandler.getPacketType().getID()))
            handlers.get(packetHandler.getPacketType().getID()).remove(packetHandler);
    }

    @SuppressWarnings("unchecked")
    private <T extends GamePacket> boolean post(PacketEvent<T> event, List<PacketHandler> handlers) {
        Preconditions.checkNotNull(event);
        Preconditions.checkNotNull(handlers);

        handlers.forEach(packetHandler -> packetHandler.handlePacket(event));

        return event.isCancelled();
    }

    boolean handlePacket(Packet packet, Channel channel) {
        if (handlers.containsKey(packet.getClass().getSimpleName())) {
            var reflectPacket = PacketType.getPacketList().get(packet.getClass());
            if (reflectPacket != null) {
                Player player = Bukkit.getOnlinePlayers().stream().map(CraftPlayer.class::cast).filter(craftPlayer -> craftPlayer.getHandle().playerConnection.networkManager.channel.equals(channel)).findFirst().orElseThrow(() -> new NullPointerException("Received a packet for a non registered player"));

                var event = new PacketEvent<GamePacket>(player, NMSReflection.getConstructorAccessor(reflectPacket, packet.getClass()).newInstance(packet));
                return post(event, handlers.get(packet.getClass().getSimpleName()));
            }
        }
        return false;
    }

    public Map<String, List<PacketHandler>> getHandlers() {
        return handlers;
    }
}
