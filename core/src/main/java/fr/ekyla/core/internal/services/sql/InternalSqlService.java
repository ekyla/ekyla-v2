package fr.ekyla.core.internal.services.sql;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import com.zaxxer.hikari.pool.HikariPool;
import fr.ekyla.api.services.sql.SqlRequest;
import fr.ekyla.api.services.sql.SqlService;

import java.sql.SQLException;

/**
 * File <b>InternalSqlService</b> located on fr.ekyla.core.services.sql
 * InternalSqlService is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 18/08/2019 at 14:46
 */
public class InternalSqlService implements SqlService {

    public final String host, username, password, database;
    public final int port;

    private HikariDataSource connectionPool;

    public InternalSqlService(String host, int port, String username, String password, String database) {
        this.host = host;
        this.username = username;
        this.password = password;
        this.database = database;
        this.port = port;
    }

    public boolean init() {
        try {
            final HikariConfig hikariConfig = new HikariConfig();

            hikariConfig.setMaximumPoolSize(10);
            hikariConfig.setJdbcUrl("jdbc:mysql://" + host + ":" + port + "/" + database + "?useSSL=false");
            hikariConfig.setUsername(username);
            hikariConfig.setPassword(password);
            hikariConfig.setMaxLifetime(600000L);
            hikariConfig.setIdleTimeout(300000L);
            hikariConfig.setLeakDetectionThreshold(300000L);
            hikariConfig.setConnectionTimeout(10000L);

            this.connectionPool = new HikariDataSource(hikariConfig);
            return true;
        } catch (HikariPool.PoolInitializationException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public SqlRequest select(String table, String... columns) {
        try {
            return new InternalSqlRequest(connectionPool.getConnection(), InternalSqlRequest.RequestType.SELECT, table).columns((columns.length == 0 ? new String[]{"*"} : columns));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public SqlRequest insert(String table) {
        try {
            return new InternalSqlRequest(connectionPool.getConnection(), InternalSqlRequest.RequestType.INSERT, table);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public SqlRequest update(String table) {
        try {
            return new InternalSqlRequest(connectionPool.getConnection(), InternalSqlRequest.RequestType.UPDATE, table);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void close() {
        if (this.connectionPool != null) this.connectionPool.close();
    }
}
