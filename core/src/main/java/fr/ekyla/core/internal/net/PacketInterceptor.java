package fr.ekyla.core.internal.net;

import fr.ekyla.api.EkylaApi;
import fr.ekyla.api.services.jedis.JedisService;
import fr.ekyla.api.utils.NMSReflection;
import fr.ekyla.core.EkylaCore;
import fr.ekyla.core.internal.player.InternalPlayer;
import io.netty.channel.*;
import net.minecraft.server.v1_12_R1.Packet;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * File <b>PacketInterceptor</b> located on fr.ekyla.core.internal.net
 * PacketInterceptor is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 31/07/2019 at 19:00
 */
public class PacketInterceptor {

    private static final Map<SocketAddress, PacketHandler> HANDLERS = new ConcurrentHashMap<>();
    private static List<ChannelFuture> channelFutures;
    private static PacketHandlerManager manager;


    PacketInterceptor(PacketHandlerManager packetHandlerManager) {
        manager = packetHandlerManager;
    }

    public static void clean() {
        if (channelFutures == null)
            return;

        for (ChannelFuture channelFuture : channelFutures) {
            ChannelPipeline pipeline = channelFuture.channel().pipeline();
            if (pipeline.get(ChannelFutureHandler.ID) != null)
                pipeline.remove(ChannelFutureHandler.ID);
        }

        for (PacketHandler value : HANDLERS.values()) {
            value.channel.pipeline().remove(value);
        }

    }

    public static void inject() {
        var server = NMSReflection.getValue(Bukkit.getServer(), "console");
        var connection = NMSReflection.getFirstValueOfType(server, "{nms}.ServerConnection");
        channelFutures = NMSReflection.getFirstValueOfType(connection, List.class);

        for (ChannelFuture channelFuture : channelFutures)
            channelFuture.channel().pipeline().addFirst(ChannelFutureHandler.ID, ChannelFutureHandler.INSTANCE);

        for (Player player : Bukkit.getOnlinePlayers())
            injectPlayer(player);

        System.out.println("Injector enabled");
    }

    private static void injectPlayer(Player player) {
        Channel channel = ((CraftPlayer) player).getHandle().playerConnection.networkManager.channel;
        PacketHandler handler = new PacketHandler(channel);
        channel.pipeline().addBefore("packet_handler", PacketHandler.ID, handler);
        HANDLERS.put(channel.remoteAddress(), handler);
    }

    public static final class ChannelFutureHandler extends ChannelDuplexHandler {

        private static final ChannelFutureHandler INSTANCE = new ChannelFutureHandler();
        private static final String ID = "NMSProtocol-ChannelFuture";

        private ChannelFutureHandler() {
        }

        @Override
        public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
            Channel channel = ((Channel) msg);
            channel.pipeline().addFirst(ChannelInitHandler.ID, new ChannelInitHandler(channel));

            super.channelRead(ctx, msg);
        }

    }

    @ChannelHandler.Sharable
    public static final class ChannelInitHandler extends ChannelDuplexHandler {

        private static final String ID = "NMSProtocol-Init";
        private final Channel channel;

        ChannelInitHandler(Channel channel) {
            this.channel = channel;
        }

        @Override
        public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
            PacketHandler handler = new PacketHandler(channel);
            HANDLERS.put(channel.remoteAddress(), handler);

            ChannelPipeline pipeline = ctx.channel().pipeline();
            pipeline.addBefore("packet_handler", PacketHandler.ID, handler);
            pipeline.remove(this);

            super.channelRead(ctx, msg);
        }
    }

    static final class PacketHandler extends ChannelDuplexHandler {

        private static final String ID = "NMSProtocol-PacketHandler";
        private final Channel channel;

        PacketHandler(Channel channel) {
            this.channel = channel;
        }

        @Override
        public void channelUnregistered(ChannelHandlerContext ctx) {
            HANDLERS.remove(channel.remoteAddress());
            System.out.println("unregister " + channel.localAddress() + " - " + channel.remoteAddress());
            InternalPlayer player = ((EkylaCore) EkylaApi.getInstance()).getPlayer((InetSocketAddress) channel.remoteAddress()).orElseThrow(() -> new NullPointerException("Player not found"));
            System.out.println("player disconnect: " + player.getName());

            JedisService jedis = EkylaApi.getInstance().getJedis();
            jedis.lset("players", player.getRedisId(), EkylaApi.getInstance().getJson().toJson(player, InternalPlayer.class));
            EkylaApi.getInstance().getPlayers().remove(player);
            ctx.fireChannelUnregistered();
        }

        @Override
        public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
            if (!manager.handlePacket((Packet) msg, channel))
                super.write(ctx, msg, promise);

        }

        @Override
        public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
            if (!manager.handlePacket((Packet) msg, channel))
                super.channelRead(ctx, msg);
        }
    }
}
