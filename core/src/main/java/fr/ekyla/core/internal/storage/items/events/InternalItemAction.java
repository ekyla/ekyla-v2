package fr.ekyla.core.internal.storage.items.events;

import fr.ekyla.api.events.player.items.ItemClickEvent;
import fr.ekyla.api.player.EkylaPlayer;
import fr.ekyla.api.storage.items.Item;
import fr.ekyla.api.storage.items.events.ItemAction;

public class InternalItemAction implements ItemAction {

    private final EkylaPlayer user;
    private final ItemClickEvent.ClickType click;
    private final Item item;
    private final Object target;
    private boolean cancelled = false;

    public InternalItemAction(EkylaPlayer user, ItemClickEvent.ClickType click, Item item, Object target) {
        this.user = user;
        this.click = click;
        this.item = item;
        this.target = target;
    }

    @Override
    public EkylaPlayer getUser() {
        return user;
    }

    @Override
    public ItemClickEvent.ClickType getClick() {
        return click;
    }

    @Override
    public Item getItem() {
        return item;
    }

    @Override
    public Object getTarget() {
        return target;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public ItemAction setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
        return this;
    }
}
