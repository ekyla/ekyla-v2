package fr.ekyla.core.internal.net.packets.play.client;

import fr.ekyla.api.EkylaApi;
import fr.ekyla.api.events.player.items.ItemClickEvent;
import fr.ekyla.api.net.PacketEvent;
import fr.ekyla.api.net.PacketHandler;
import fr.ekyla.api.net.PacketType;
import fr.ekyla.api.net.packet.play.client.UseItemPacket;
import fr.ekyla.api.player.EkylaPlayer;
import fr.ekyla.api.storage.items.Item;
import fr.ekyla.core.internal.storage.items.events.InternalItemAction;
import net.minecraft.server.v1_12_R1.ItemStack;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftInventoryPlayer;

public class UseItemPacketHandler extends PacketHandler<UseItemPacket> {

    private long lastPacket = 0;

    public UseItemPacketHandler() {
        super(PacketType.Play.Client.USE_ITEM);
    }

    @Override
    public void handlePacket(PacketEvent<UseItemPacket> event) {
        if (!check(System.currentTimeMillis())) return; //Eviter le double packet

        UseItemPacket packet = event.getPacket();
        EkylaPlayer player = event.getPlayer();
        Item item;
        if (player.getAdminMode().isAdminMode()) {
            ItemStack stack = ((CraftInventoryPlayer) player.getCraftPlayer().getInventory()).getInventory().getItemInHand();
            item = EkylaApi.getInstance().getStorageManager().getItem(stack).orElse(EkylaApi.getInstance().getStorageManager().emptyItem());
        } else {
            item = player.getStorage().getItems().get(0);
        }
        if (item != null) {
            if (item.getProperties().getAction() != null) {
                item.getProperties().getAction().done(new InternalItemAction(player, ItemClickEvent.ClickType.RIGHT_CLICK, item, packet.getBlock()));
                event.setCancelled(true);
            } else {
                Bukkit.getPluginManager().callEvent(new ItemClickEvent(player, item, packet.getBlock(), ItemClickEvent.ClickType.RIGHT_CLICK));
            }
        }

    }

    private boolean check(long time) {
        boolean ret = true;
        if (time - lastPacket < 30) {
            ret = false;
        }
        lastPacket = time;
        return ret;
    }

}
