package fr.ekyla.core.internal.net.packets.play.client;

import fr.ekyla.api.net.PacketEvent;
import fr.ekyla.api.net.PacketHandler;
import fr.ekyla.api.net.packet.play.client.PlayerLookPacket;
import fr.ekyla.core.internal.player.InternalPlayer;

import static fr.ekyla.api.net.PacketType.Play.Client.PLAYER_LOOK;

public class PlayerLookPacketHandler extends PacketHandler<PlayerLookPacket> {

    public PlayerLookPacketHandler() {
        super(PLAYER_LOOK);
    }

    @Override
    public void handlePacket(PacketEvent<PlayerLookPacket> event) {
        PlayerLookPacket packet = event.getPacket();
        if (event.isCancelled())return;
        InternalPlayer player = ((InternalPlayer) event.getPlayer());
        player.position = packet.getPosition(player.position);
    }
}
