package fr.ekyla.core.internal.net.packets.play.client;

import fr.ekyla.api.EkylaApi;
import fr.ekyla.api.net.PacketEvent;
import fr.ekyla.api.net.PacketHandler;
import fr.ekyla.api.net.packet.play.client.WindowClickPacket;
import fr.ekyla.api.player.EkylaPlayer;
import fr.ekyla.api.storage.items.Item;
import net.minecraft.server.v1_12_R1.ItemStack;

import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static fr.ekyla.api.net.PacketType.Play.Client.INVENTORY_CLICK;

/**
 * File <b>WindowClickPacketHandler</b> located on fr.ekyla.core.internal.net.packets.play.client
 * WindowClickPacketHandler is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 01/08/2019 at 22:30
 */
public class WindowClickPacketHandler extends PacketHandler<WindowClickPacket> {

    private static final ConcurrentMap<Integer, ItemStack> actions = new ConcurrentHashMap<>();
    private static final ConcurrentMap<EkylaPlayer, Item> playerActions = new ConcurrentHashMap<>();

    public WindowClickPacketHandler() {
        super(INVENTORY_CLICK);
    }

    @Override
    public void handlePacket(PacketEvent<WindowClickPacket> event) {
        WindowClickPacket packet = event.getPacket();
        ItemStack stack = event.getPacket().getItem();
        Optional<Item> item = EkylaApi.getInstance().getStorageManager().getItem(stack);
        System.out.println("w id: " + event.getPacket().getInventoryId() + " - action: " + event.getPacket().getAction());
        if (packet.getInventoryId() == 0) {
            //Inventaire du joueur
            item.ifPresentOrElse(it -> {
                switch (packet.getMode()) {
                    case THROW:
                        playerActions.remove(event.getPlayer());
                        break;
                    case CLONE:
                        break;
                    case SWAP:
                        break;
                    case PICKUP:
                        break;
                    default:
                        playerActions.put(event.getPlayer(), it);
                        break;
                }
            }, () -> {
            });
        } else {

            //On click
        }
    }
}
