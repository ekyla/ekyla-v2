package fr.ekyla.core.internal.services.rabbitmq;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import fr.ekyla.api.services.rabbitmq.RabbitMQService;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeoutException;

/**
 * File <b>InternalRabbitMQ</b> located on fr.ekyla.core.services.rabbitmq
 * InternalRabbitMQ is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 15/08/2019 at 20:32
 */
public class InternalRabbitMQ implements RabbitMQService {

    private Channel channel;
    private Connection connection;

    public InternalRabbitMQ(String host, String username, String password) {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(host);
        factory.setUsername(username);
        factory.setPassword(password);

        try {
            this.connection = factory.newConnection();
            this.channel = connection.createChannel();
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }

    }

    public Channel getChannel() {
        return channel;
    }

    public Connection getConnection() {
        return connection;
    }

    @Override
    public void closeConnection() {
        try {
            this.connection.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void publishToQueue(String queue, String buffer) {
        try {
            channel.basicPublish("", queue, null, buffer.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String publishAndResponse(String queue, String buffer) {
        final String corrId = UUID.randomUUID().toString();
        try {
            String replyQueueName = channel.queueDeclare().getQueue();
            AMQP.BasicProperties props = new AMQP.BasicProperties
                    .Builder()
                    .correlationId(corrId)
                    .replyTo(replyQueueName)
                    .build();
            channel.basicPublish("", "players", props, buffer.getBytes(StandardCharsets.UTF_8));

            final BlockingQueue<String> response = new ArrayBlockingQueue<>(1);

            String cTag = channel.basicConsume(replyQueueName, true, (tag, delivery) -> {
                if (delivery.getProperties().getCorrelationId().equals(corrId))
                    response.offer(new String(delivery.getBody(), StandardCharsets.UTF_8));
            }, tag -> {
            });

            String result = response.take();
            channel.basicCancel(cTag);
            return result;
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return "";
    }
}
