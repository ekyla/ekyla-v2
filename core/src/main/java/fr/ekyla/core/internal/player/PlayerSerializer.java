package fr.ekyla.core.internal.player;

import com.google.gson.*;
import fr.ekyla.api.i18n.Lang;
import fr.ekyla.api.player.Rank;

import java.lang.reflect.Type;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * File <b>PlayerSerializer</b> located on fr.ekyla.core.internal.player
 * PlayerSerializer is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 22/08/2019 at 22:05
 */
public class PlayerSerializer implements JsonDeserializer<InternalPlayer>, JsonSerializer<InternalPlayer> {

    @Override
    public InternalPlayer deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        JsonObject object = (JsonObject) jsonElement;

        int redisId = object.get("redisId").getAsInt();
        int id = object.get("id").getAsInt();
        UUID uuid = UUID.fromString(object.get("uuid").getAsString());
        String username = object.get("username").getAsString();
        Lang lang = Lang.valueOf(object.get("lang").getAsString());

        List<Rank> ranks = new ArrayList<>();
        JsonArray ranksArray = object.get("ranks").getAsJsonArray();
        ranksArray.forEach(rankNumber -> Rank.getById(rankNumber.getAsInt()).ifPresent(ranks::add));

        List<String> perms = new ArrayList<>();
        JsonArray permsArray = object.get("permissions").getAsJsonArray();
        permsArray.forEach(permission -> perms.add(permission.getAsString()));

        String[] addressJson = object.get("address").getAsString().split(":");
        InetSocketAddress address = new InetSocketAddress(addressJson[0], Integer.parseInt(addressJson[1]));

        return new InternalPlayer(redisId, id, uuid, username, ranks, perms, lang, address);
    }

    @Override
    public JsonElement serialize(InternalPlayer player, Type type, JsonSerializationContext jsonSerializationContext) {
        JsonObject object = new JsonObject();
        object.addProperty("id", player.getPlayerId());
        object.addProperty("uuid", player.getUuid().toString());
        object.addProperty("username", player.getUsername());
        object.addProperty("lang", player.getLang().name());

        JsonArray ranks = new JsonArray();
        player.getRanks().forEach(rank -> ranks.add(rank.getId()));

        JsonArray perms = new JsonArray();
        player.getPermissions().forEach(perms::add);

        object.add("ranks", ranks);
        object.add("permissions", perms);
        return object;
    }
}
