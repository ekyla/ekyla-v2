package fr.ekyla.core.internal.net.packets.play.server;

/**
 * File <b>ChatOutPacketHandler</b> located on fr.ekyla.core.internal.net.packets.play.server
 * ChatOutPacketHandler is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 01/08/2019 at 11:34
 */
public class ChatOutPacketHandler {
}
