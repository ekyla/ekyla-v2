package fr.ekyla.core.internal.player;

import com.rabbitmq.client.AMQP;
import fr.ekyla.api.EkylaApi;
import fr.ekyla.api.player.PlayerInfo;
import fr.ekyla.api.player.Rank;
import fr.ekyla.core.internal.services.rabbitmq.InternalRabbitMQ;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * File <b>PlayerRpc</b> located on fr.ekyla.core.internal.player
 * PlayerRpc is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 30/08/2019 at 12:05
 */
public class InternalPlayerInfo implements PlayerInfo {

    private final String username;

    public InternalPlayerInfo(String username) {
        this.username = username;
    }


    @Override
    public boolean isOnline() {
        return Boolean.parseBoolean(EkylaApi.getInstance().getRabbitMQ().publishAndResponse("players", "online|" + username));
    }

    @Override
    public List<Rank> getRanks() {
        List<Rank> ranks = new ArrayList<>();

        String response = EkylaApi.getInstance().getRabbitMQ().publishAndResponse("players", "online|" + username);
        for (String s : response.split(",")) {
            int id = Integer.parseInt(s);
            Rank rank = Rank.getById(id).orElse(Rank.PLAYER);
            ranks.add(rank);
        }
        if (ranks.isEmpty()) ranks.add(Rank.PLAYER);
        return ranks;
    }
}
