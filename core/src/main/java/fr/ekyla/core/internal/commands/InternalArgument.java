package fr.ekyla.core.internal.commands;

import fr.ekyla.api.commands.Argument;

import java.util.List;

/**
 * File <b>InternalCommandArgument</b> located on fr.ekyla.core.internal.commands
 * InternalCommandArgument is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 26/08/2019 at 21:46
 */
public class InternalArgument implements Argument {

    private final List<Object> arguments;

    public InternalArgument(List<Object> arguments) {
        this.arguments = arguments;
    }

    public List<Object> getArguments() {
        return arguments;
    }

    @Override
    public Object get(int position) {
        return arguments.get(position);
    }

    @Override
    public String getString(int position) {
        return String.valueOf(arguments.get(position));
    }

    @Override
    public int number() {
        return arguments.size();
    }
}
