package fr.ekyla.core.internal.net.packets.play.client;

import fr.ekyla.api.net.PacketEvent;
import fr.ekyla.api.net.PacketHandler;
import fr.ekyla.api.net.packet.play.client.PlayerMovePacket;
import fr.ekyla.api.positions.Position;
import fr.ekyla.core.internal.player.InternalPlayer;
import fr.ekyla.core.portals.Portal;

import java.util.Optional;

import static fr.ekyla.api.net.PacketType.Play.Client.PLAYER_MOVE;

public class PlayerMovePacketHandler extends PacketHandler<PlayerMovePacket> {

    public PlayerMovePacketHandler() {
        super(PLAYER_MOVE);
    }

    @Override
    public void handlePacket(PacketEvent<PlayerMovePacket> event) {
        PlayerMovePacket packet = event.getPacket();
        InternalPlayer player = ((InternalPlayer) event.getPlayer());
        if (event.isCancelled()) return;
        player.position = packet.getPosition(player.position);
        Optional<Portal> opt = Portal.getPortals().stream().filter(portal -> portal.getArea().isInArea(player.getPosition())).findFirst();
        if (opt.isPresent()) {
            System.out.println("player in portal");
            Portal portal = opt.get();
            switch (portal.getAction()) {
                case SERVER:
                    //Send to server

                    break;
                case TELEPORT:
                    Position tp = (Position) portal.getActionParam();
                    player.teleport(tp);
                    break;
                default:
                    break;
            }
        }
    }
}
