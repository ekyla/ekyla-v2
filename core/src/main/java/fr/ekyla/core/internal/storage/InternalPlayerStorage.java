package fr.ekyla.core.internal.storage;

import fr.ekyla.api.EkylaApi;
import fr.ekyla.api.storage.PlayerStorage;
import fr.ekyla.api.storage.Storage;
import fr.ekyla.api.storage.items.Item;
import fr.ekyla.core.internal.player.InternalPlayer;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftInventoryPlayer;
import org.bukkit.inventory.Inventory;

import java.util.ArrayList;
import java.util.List;

/**
 * File <b>PlayerStorage</b> located on fr.ekyla.core.internal.player
 * PlayerStorage is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 02/08/2019 at 21:49
 */
public class InternalPlayerStorage extends InternalStorage implements PlayerStorage {

    private final InternalPlayer player;
    private Item mainHand;
    private Item offHand;

    public InternalPlayerStorage(InternalPlayer player) {
        super(36, "");
        this.player = player;
    }

    public InternalPlayer getPlayer() {
        return player;
    }

    @Override
    protected Storage set(int position, Item item) {
        getHandle().getInventory().setItem(position, item.toItemStack());
        return this;
    }

    @Override
    protected Storage remove(int position) {
        getHandle().setItem(position, null);
        return this;
    }

    @Override
    protected int nextFreeSlot() {
        Inventory inventory = getHandle();
        for (int i = 0; i < getSize(); i++) {
            if (inventory.getItem(i) == null)
                return i;
        }
        return -1;
    }

    @Override
    protected CraftInventoryPlayer getHandle() {
        return (CraftInventoryPlayer) player.getCraftPlayer().getInventory();
    }

    @Override
    public List<Item> getItems() {
        List<Item> tmp = new ArrayList<>();
        getHandle().getInventory().getContents().forEach(itemStack -> EkylaApi.getInstance().getStorageManager().getItem(itemStack).ifPresent(tmp::add));
        return tmp;
    }

    @Override
    public Item getItemInHand() {
        return mainHand;
    }

    @Override
    public Item getItemInOffHand() {
        return offHand;
    }

    public PlayerStorage setItemInMainHand(Item mainHand) {
        this.mainHand = mainHand;
        return this;
    }

    public PlayerStorage setItemInOffHand(Item offHand) {
        this.offHand = offHand;
        return this;
    }
}
