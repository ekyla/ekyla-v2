package fr.ekyla.core.internal.services.sql.result;

import fr.ekyla.api.services.sql.SqlResult;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * File <b>InsertResult</b> located on fr.ekyla.core.internal.services.sql.result
 * InsertResult is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 18/08/2019 at 22:22
 */
public class InsertResult implements SqlResult {

    private List<Object> returnedKeys = new ArrayList<>();

    public InsertResult(ResultSet result, Connection connection) {

        try {
            ResultSetMetaData metaData = result.getMetaData();

            if (result.next()) {
                for (int i = 1; i <= metaData.getColumnCount(); i++) {
                    this.returnedKeys.add(result.getObject(i));
                }
            }

            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean next() {
        return returnedKeys.size() != 0;
    }

    @Override
    public Object get(String key) {
        return null;
    }

    @Override
    public String getString(String key) {
        return null;
    }

    @Override
    public Integer getInt(String key) {
        return null;
    }

    @Override
    public Boolean getBoolean(String key) {
        return null;
    }

    @Override
    public Object get(int columnid) {
        return returnedKeys.get(columnid);
    }

    @Override
    public String getString(int columnid) {
        return String.valueOf(returnedKeys.get(columnid));
    }

    @Override
    public Integer getInt(int columnid) {
        return Integer.parseInt(getString(columnid));
    }

    @Override
    public Boolean getBoolean(int columnid) {
        return Boolean.parseBoolean(getString(columnid));
    }
}
