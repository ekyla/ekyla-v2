package fr.ekyla.core.internal.services.sql.result;

import fr.ekyla.api.services.sql.SqlResult;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * File <b>UpdateResult</b> located on fr.ekyla.core.internal.services.sql.result
 * UpdateResult is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 18/08/2019 at 22:28
 */
public class UpdateResult implements SqlResult {

    private boolean updated = false;

    public UpdateResult(ResultSet set, Connection connection) {
        try {
            this.updated = set.next();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean next() {
        return updated;
    }

    @Override
    public Object get(String key) {
        return null;
    }

    @Override
    public String getString(String key) {
        return null;
    }

    @Override
    public Integer getInt(String key) {
        return null;
    }

    @Override
    public Boolean getBoolean(String key) {
        return null;
    }

    @Override
    public Object get(int columnid) {
        return null;
    }

    @Override
    public String getString(int columnid) {
        return null;
    }

    @Override
    public Integer getInt(int columnid) {
        return null;
    }

    @Override
    public Boolean getBoolean(int columnid) {
        return null;
    }
}
