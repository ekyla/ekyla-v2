package fr.ekyla.core.internal.net.packets.play.client;

import fr.ekyla.api.EkylaApi;
import fr.ekyla.api.events.player.items.ItemClickEvent;
import fr.ekyla.api.net.PacketEvent;
import fr.ekyla.api.net.PacketHandler;
import fr.ekyla.api.net.PacketType;
import fr.ekyla.api.net.packet.play.client.PlayerDigPacket;
import fr.ekyla.api.player.EkylaPlayer;
import fr.ekyla.api.storage.items.Item;
import fr.ekyla.api.storage.items.events.ItemAction;
import fr.ekyla.core.internal.storage.items.events.InternalItemAction;
import net.minecraft.server.v1_12_R1.ItemStack;
import net.minecraft.server.v1_12_R1.PacketPlayInBlockDig;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftInventoryPlayer;

import java.util.ArrayList;
import java.util.List;

public class PlayerDigPacketHandler extends PacketHandler<PlayerDigPacket> {

    private final List<ItemAction> actions = new ArrayList<>();

    public PlayerDigPacketHandler() {
        super(PacketType.Play.Client.PLAYER_DIG);
    }

    @Override
    public void handlePacket(PacketEvent<PlayerDigPacket> event) {
        PlayerDigPacket packet = event.getPacket();
        EkylaPlayer player = event.getPlayer();

        if (packet.getDigState() == PacketPlayInBlockDig.EnumPlayerDigType.START_DESTROY_BLOCK) {
            Item item;
            if (player.getAdminMode().isAdminMode()) {
                ItemStack stack = ((CraftInventoryPlayer) player.getCraftPlayer().getInventory()).getInventory().getItemInHand();
                item = EkylaApi.getInstance().getStorageManager().getItem(stack).orElse(EkylaApi.getInstance().getStorageManager().emptyItem());
            } else {
                item = player.getStorage().getItems().get(0);
            }
            if (item != null) {
                if (item.getProperties().getAction() != null) {
                    ItemAction action = new InternalItemAction(player, ItemClickEvent.ClickType.LEFT_CLICK, item, packet.getBlock());
                    item.getProperties().getAction().done(action);
                    event.setCancelled(action.isCancelled());
                    actions.add(action);
                } else {
                    Bukkit.getPluginManager().callEvent(new ItemClickEvent(player, item, packet.getBlock(), ItemClickEvent.ClickType.LEFT_CLICK));
                }
            }
        } else if (packet.getDigState() == PacketPlayInBlockDig.EnumPlayerDigType.STOP_DESTROY_BLOCK) {
            if (player.getAdminMode().isAdminMode()) {
                ItemStack stack = ((CraftInventoryPlayer) player.getCraftPlayer().getInventory()).getInventory().getItemInHand();
                Item item = EkylaApi.getInstance().getStorageManager().getItem(stack).orElse(EkylaApi.getInstance().getStorageManager().emptyItem());
                actions.forEach(action -> {
                    if (action.getItem().equals(item)) {
                        event.setCancelled(action.isCancelled());
                        actions.remove(action);
                    }
                });
            }
        } else
            System.out.println(packet.getBlock() + " - " + packet.getBlockFace() + " - " + packet.getDigState());
    }
}
