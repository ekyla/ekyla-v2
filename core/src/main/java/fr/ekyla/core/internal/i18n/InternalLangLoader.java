package fr.ekyla.core.internal.i18n;

import com.google.common.base.Preconditions;
import fr.ekyla.api.i18n.Lang;
import fr.ekyla.api.i18n.i18nLoader;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * File <b>InternalLangLoader</b> located on fr.ekyla.core.internal.i18n
 * InternalLangLoader is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 02/08/2019 at 19:36
 */
public class InternalLangLoader implements i18nLoader {

    public ConcurrentMap<Lang, ConcurrentMap<String, String>> values;

    public InternalLangLoader() {
        values = new ConcurrentHashMap<>();
        for (Lang lang : Lang.values())
            values.put(lang, new ConcurrentHashMap<>());
    }

    @Override
    public void loadLangs(Plugin plugin) {
        Preconditions.checkNotNull(plugin);
        File langFolder = new File(plugin.getDataFolder(), "lang/");
        if (!langFolder.exists()) langFolder.mkdirs();
        for (Lang lang : Lang.values()) {
            File langFile = new File(langFolder, lang.getName() + ".lang");
            if (!langFile.exists()) {
                try {
                    URI uri = plugin.getClass().getProtectionDomain().getCodeSource().getLocation().toURI();
                    JarFile jar = new JarFile(uri.getPath());
                    JarEntry entry = (JarEntry) jar.getEntry("lang/" + lang.getName() + ".lang");
                    if (entry != null)
                        plugin.saveResource(entry.getName(), true);
                    else {
                        throw new Exception("Locale " + lang.name() + " is registred but no lang file were found.");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            var properties = new Properties();
            FileInputStream stream = null;
            InputStreamReader reader = null;
            try {
                stream = new FileInputStream(langFile);
                reader = new InputStreamReader(stream);
                properties.load(reader);
                properties.forEach((langKey, langValue) -> values.computeIfPresent(lang, (loc, map) -> {
                    map.putIfAbsent(langKey.toString(), langValue.toString());
                    return map;
                }));
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (stream != null)
                        stream.close();
                    if (reader != null)
                        reader.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void addProperty(Lang lang, String key, String value) {

    }
}
