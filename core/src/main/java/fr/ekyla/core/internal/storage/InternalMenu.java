package fr.ekyla.core.internal.storage;

import fr.ekyla.api.EkylaApi;
import fr.ekyla.api.storage.Menu;
import fr.ekyla.api.storage.Storage;
import fr.ekyla.api.storage.items.Item;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;

import java.util.ArrayList;
import java.util.List;

public class InternalMenu extends InternalStorage implements Menu {

    private final Inventory handle;
    private final List<Item> items;

    protected InternalMenu(int size, String name) {
        super(size, name);
        this.handle = Bukkit.createInventory(null, size * 9, name);
        this.items = new ArrayList<>(size * 9);
        for (int i = 0; i < size*9; i++) {
            items.set(i, EkylaApi.getInstance().getStorageManager().emptyItem());
        }
    }

    @Override
    protected Storage set(int position, Item item) {
        return null;
    }

    @Override
    protected Storage remove(int position) {
        return null;
    }

    @Override
    protected int nextFreeSlot() {
        int slot = 0;
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getProperties().getMaterial() == Material.AIR) {
                slot = i;
                break;
            }
        }
        return slot;
    }

    @Override
    protected Inventory getHandle() {
        return handle;
    }

    @Override
    public List<Item> getItems() {
        return items;
    }
}
