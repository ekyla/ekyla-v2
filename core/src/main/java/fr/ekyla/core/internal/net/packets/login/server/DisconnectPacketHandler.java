package fr.ekyla.core.internal.net.packets.login.server;

import fr.ekyla.api.net.PacketEvent;
import fr.ekyla.api.net.PacketHandler;
import fr.ekyla.api.net.PacketType;
import fr.ekyla.api.net.packet.login.server.DisconnectPacket;

/**
 * File <b>DisconnectPacketHandler</b> located on fr.ekyla.core.internal.net.packets.login.server
 * DisconnectPacketHandler is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 27/08/2019 at 12:37
 */
public class DisconnectPacketHandler extends PacketHandler<DisconnectPacket> {

    public DisconnectPacketHandler() {
        super(PacketType.Login.Server.DISCONNECT);
    }

    @Override
    public void handlePacket(PacketEvent<DisconnectPacket> event) {
        System.out.println(event.getPlayer().getUsername() + " disconnect because: " + event.getPacket().getReason().getText());
    }
}
