package fr.ekyla.core.internal.services.jedis;

import fr.ekyla.api.services.jedis.JedisService;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.time.Duration;
import java.util.List;
import java.util.Optional;

/**
 * File <b>InternalJedis</b> located on fr.ekyla.core.internal.services.jedis
 * InternalJedis is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 15/08/2019 at 19:06
 */
public class InternalJedis implements JedisService {

    private final String host;
    private final int port;
    private final String password;
    private JedisPool pool;

    public InternalJedis(String host, int port, String password) {
        this.host = host;
        this.port = port;
        this.password = password;
    }

    @Override
    public void initConnection() {
        final JedisPoolConfig poolConfig = new JedisPoolConfig();
        poolConfig.setMaxTotal(128);
        poolConfig.setMaxIdle(128);
        poolConfig.setMinIdle(16);
        poolConfig.setTestOnBorrow(true);
        poolConfig.setTestOnReturn(true);
        poolConfig.setTestWhileIdle(true);
        poolConfig.setMinEvictableIdleTimeMillis(Duration.ofSeconds(60).toMillis());
        poolConfig.setTimeBetweenEvictionRunsMillis(Duration.ofSeconds(30).toMillis());
        poolConfig.setNumTestsPerEvictionRun(3);
        poolConfig.setBlockWhenExhausted(true);
        this.pool = new JedisPool(poolConfig, this.host, this.port, 10000, this.password);
    }

    @Override
    public long lrem(String key, int count, String value) {
        Jedis jedis = pool.getResource();
        long ret = jedis.lrem(key, count, value);
        jedis.close();
        return ret;
    }

    @Override
    public long ldel(String key, long id) {
        Jedis jedis = pool.getResource();
        List<String> list = lrange(key, id - 1, id).orElseThrow(() -> new NullPointerException("resource not found (key=" + key + ",id=" + id + ")"));
        if (list.isEmpty()) return 0;
        String toDel = list.get(0);
        jedis.close();
        return lrem(key, 0, toDel);
    }

    @Override
    public void closeConnection() {
        this.pool.close();
    }

    @Override
    public Optional<String> get(String key) {
        Jedis jedis = pool.getResource();
        Optional<String> ret = Optional.ofNullable(jedis.get(key));
        jedis.close();
        return ret;
    }

    @Override
    public String set(String key, String value) {
        Jedis jedis = pool.getResource();
        String set = jedis.set(key, value);
        jedis.close();
        return set;
    }

    @Override
    public long rpush(String key, String... values) {
        Jedis jedis = pool.getResource();
        long count = jedis.rpush(key, values);
        jedis.close();
        return count;
    }

    @Override
    public String lset(String key, long id, String value) {
        Jedis jedis = pool.getResource();
        String count = jedis.lset(key, id, value);
        jedis.close();
        return count;
    }

    @Override
    public long lsize(String key) {
        Jedis jedis = pool.getResource();
        long size = jedis.llen(key);
        jedis.close();
        return size;
    }

    @Override
    public Optional<List<String>> lrange(String key, long start, long stop) {
        Jedis jedis = pool.getResource();
        Optional<List<String>> list = Optional.ofNullable(jedis.lrange(key, start, stop));
        jedis.close();
        return list;
    }

    @Override
    public Optional<String> lget(String key, long id) {

        Jedis jedis = pool.getResource();
        List<String> list = jedis.lrange(key, id, id);
        Optional<String> ret = list.isEmpty() ? Optional.empty() : Optional.of(list.get(0));
        jedis.close();
        return ret;
    }

    @Override
    public boolean delete(String key) {
        Jedis jedis = pool.getResource();
        long count = jedis.del(key);
        jedis.close();
        return count == 1;
    }
}
