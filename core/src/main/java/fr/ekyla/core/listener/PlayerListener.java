package fr.ekyla.core.listener;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import fr.ekyla.api.EkylaApi;
import fr.ekyla.api.listener.AutoListener;
import fr.ekyla.api.player.EkylaPlayer;
import fr.ekyla.api.storage.items.ItemBuilder;
import fr.ekyla.core.internal.player.InternalPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.Random;

/**
 * File <b>PlayerListener</b> located on fr.ekyla.core.listener
 * PlayerListener is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 01/08/2019 at 20:22
 */
public class PlayerListener extends AutoListener {

    public PlayerListener(EkylaApi api) {
        super(api);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        EkylaApi.getInstance().getJedis().lrange("players", 0, -1).ifPresentOrElse(list -> {
            for (int i = 0; i < list.size(); i++) {
                JsonParser parser = new JsonParser();
                JsonObject object = (JsonObject) parser.parse(list.get(i));
                object.addProperty("redisId", i + 1);
                System.out.println("addresse du joueur: " + event.getPlayer().getAddress());
                object.addProperty("address", event.getPlayer().getAddress().getHostName() + ":" + event.getPlayer().getAddress().getPort());
                if (object.get("uuid").getAsString().equalsIgnoreCase(event.getPlayer().getUniqueId().toString())) {
                    InternalPlayer player = EkylaApi.getInstance().getJson().fromJson(object, InternalPlayer.class);
                }
            }
        }, () -> event.getPlayer().kickPlayer("An error occured while connecting you.\nIf the issue keep poping, please contact an administrator"));
        EkylaPlayer player = api.getPlayer(event.getPlayer().getUniqueId()).orElseThrow(() -> new NullPointerException("Player not found"));
        player.getCraftPlayer().getHandle().inventory.setItem(new Random().nextInt(20), api.getStorageManager().createItem(new ItemBuilder(Material.APPLE)).withProperties(properties -> properties.setName("§6Une Pomme")).toItemStack());

        fr.ekyla.api.events.player.PlayerJoinEvent playerJoinEvent = new fr.ekyla.api.events.player.PlayerJoinEvent(player);

        Bukkit.getPluginManager().callEvent(playerJoinEvent);
        event.setJoinMessage(playerJoinEvent.getMessage());
        if (playerJoinEvent.isCancelled()) {
            player.getCraftPlayer().kickPlayer("");
        }

        //c'est temporaire ca gros ca va pas rester
        player.getStorage().setItem(0, api.getStorageManager().createItem(new ItemBuilder(Material.ACACIA_DOOR)).withProperties(properties -> {
            properties.setAmount(10);
        }));

    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        EkylaPlayer player = getEkylaPlayer(event.getPlayer());
        player.disconnect();
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        InternalPlayer player = (InternalPlayer) getEkylaPlayer(event.getPlayer());
        if (player.getDialogChat() != null) {
            if (player.getDialogChat().isListeningForPlayer()) {
                String message = event.getMessage();
                event.setCancelled(true);
                player.sendMessage(message);
                player.getDialogChat().goToNextMessage(player, message);
            } else {
                event.setCancelled(true);
            }
        }
    }

}
