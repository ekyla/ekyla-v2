package fr.ekyla.core.portals;

import fr.ekyla.api.positions.Area;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Portal {

    private final static List<Portal> portals = new ArrayList<>();

    private final Area area;
    private final String id;
    private PortalAction action;
    private Object actionParam;

    public Portal(Area area, String id) {
        this.area = area;
        this.id = id;
        portals.add(this);
    }

    public static List<Portal> getPortals() {
        return portals;
    }

    public static Optional<Portal> getPortal(String id) {
        return portals.stream().filter(portal -> portal.id.equals(id)).findFirst();
    }

    public String getId() {
        return id;
    }

    public Area getArea() {
        return area;
    }

    public PortalAction getAction() {
        return action;
    }

    public Portal setAction(PortalAction action) {
        this.action = action;
        return this;
    }

    public Object getActionParam() {
        return actionParam;
    }

    public Portal setActionParam(Object actionParam) {
        this.actionParam = actionParam;
        return this;
    }

    @Override
    public String toString() {
        return "Portal{" +
                "area=" + area +
                ", id='" + id + '\'' +
                ", action=" + action +
                ", actionParam=" + actionParam +
                '}';
    }
}
