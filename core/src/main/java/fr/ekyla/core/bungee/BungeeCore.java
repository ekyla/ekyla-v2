package fr.ekyla.core.bungee;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import fr.ekyla.api.bungeecord.BungeeApi;
import fr.ekyla.api.bungeecord.commands.Command;
import fr.ekyla.api.bungeecord.player.BungeePlayer;
import fr.ekyla.api.bungeecord.server.Server;
import fr.ekyla.api.module.Module;
import fr.ekyla.api.server.ServerType;
import fr.ekyla.api.services.jedis.JedisService;
import fr.ekyla.api.services.rabbitmq.RabbitMQService;
import fr.ekyla.api.utils.NMSReflection;
import fr.ekyla.core.bungee.commands.InternalCommand;
import fr.ekyla.core.bungee.listener.PlayerListener;
import fr.ekyla.core.bungee.player.InternalBungeePlayer;
import fr.ekyla.core.bungee.server.InternalServer;
import fr.ekyla.core.bungee.server.PlayerSerializer;
import fr.ekyla.core.bungee.server.ServerSerializer;
import fr.ekyla.core.bungee.services.RabbitConsumer;
import fr.ekyla.core.commands.ServerCommand;
import fr.ekyla.core.internal.services.jedis.InternalJedis;
import fr.ekyla.core.internal.services.rabbitmq.InternalRabbitMQ;
import fr.ekyla.core.internal.services.sql.InternalSqlService;
import net.md_5.bungee.api.ChatColor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

/**
 * File <b>BungeeCore</b> located on fr.ekyla.core.bungee
 * BungeeCore is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 14/08/2019 at 13:32
 */
public class BungeeCore extends BungeeApi {

    private final List<Module> modules;
    private final CopyOnWriteArrayList<Server> servers;
    private final JedisService jedis;
    private final RabbitMQService rabbitmq;
    private final List<BungeePlayer> players;
    private final Gson gson;
    private final InternalSqlService sql;
    private final List<Command> commands;
    private boolean enabled;

    public BungeeCore() {
        enabled = false;
        modules = new ArrayList<>();
        servers = new CopyOnWriteArrayList<>();
        this.jedis = new InternalJedis("127.0.1.1", 6379, null);
        this.rabbitmq = new InternalRabbitMQ("127.0.0.1", "guest", "guest");
        this.sql = new InternalSqlService("127.0.0.1", 3306, "ekyla-server", "password", "ekyla-v2");
        this.players = new CopyOnWriteArrayList<>();
        this.commands = new ArrayList<>();

        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(InternalBungeePlayer.class, new PlayerSerializer());
        builder.registerTypeAdapter(InternalServer.class, new ServerSerializer());
        this.gson = builder.create();

    }

    @Override
    public void onEnable() {
        this.jedis.initConnection();
        InternalRabbitMQ rabbit = (InternalRabbitMQ) rabbitmq;
        try {
            rabbit.getChannel().queueDeclare("servers", false, false, false, null);
            rabbit.getChannel().queueDeclare("players", false, false, false, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        new RabbitConsumer(rabbit.getChannel());
        this.sql.init();

        //getProxy().getPluginManager().registerCommand(this, new TestCommand());
        new ServerCommand(this);
        getProxy().getPluginManager().registerListener(this, new PlayerListener());


        Optional<List<String>> srv = jedis.lrange("servers", 0, -1);

        srv.ifPresent(list -> list.forEach(serverStr -> {
            InternalServer server = gson.fromJson(serverStr, InternalServer.class);
            System.out.println("Registered server " + ChatColor.GREEN + server.name);
        }));

        enabled = true;
    }

    @Override
    public void onDisable() {
        //this.jedis.delete("servers");

        this.rabbitmq.closeConnection();
        this.jedis.closeConnection();

        enabled = false;
    }

    @Override
    public InternalSqlService getSql() {
        return sql;
    }

    @Override
    public Gson getGson() {
        return gson;
    }

    @Override
    public List<BungeePlayer> getPlayers() {
        return players;
    }

    @Override
    public Optional<BungeePlayer> getPlayer(UUID playerId) {
        return players.stream().filter(player -> player.getProxiedPlayer().getUniqueId().equals(playerId)).findFirst();
    }

    @Override
    public Optional<BungeePlayer> getPlayer(String username) {
        return players.stream().filter(player -> player.getProxiedPlayer().getName().equals(username)).findFirst();
    }

    @Override
    public RabbitMQService getRabbitMQ() {
        return rabbitmq;
    }

    @Override
    public List<Command> getCommands() {
        return commands;
    }

    @Override
    public JedisService getJedis() {
        return jedis;
    }

    @Override
    public Optional<Server> getServer(String name) {
        return servers.stream().filter(server -> server.getName().equalsIgnoreCase(name)).findFirst();
    }

    @Override
    public Optional<Server> getServer(int port) {
        return servers.stream().filter(server -> server.getPort() == port).findFirst();
    }

    @Override
    public List<Server> getServers(ServerType type) {
        return servers.stream().filter(server -> server.getType() == type).collect(Collectors.toList());
    }

    @Override
    public List<Server> getServers() {
        return servers;
    }

    public <T extends Module> Optional<Module> getModule(Class<T> module) {
        return modules.stream().filter(m -> m.getClass().equals(module)).findFirst();
    }

    public <T extends Module> T registerModule(Class<T> module) {
        try {
            T mod = NMSReflection.getConstructorAccessor(module, BungeeApi.class).newInstance(BungeeApi.getInstance());
            if (isEnabled()) {
                mod.onLoad();
                mod.onEnable();
            }
            modules.add(mod);
            return mod;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private boolean isEnabled() {
        return enabled;
    }

    public <T extends Module> void unregisterModule(Class<T> module) {
        Optional<? extends Module> optional = modules.stream().filter(mod -> mod.getClass().equals(module)).findFirst();
        optional.ifPresent(mod -> {
            mod.onDisable();
            modules.remove(mod);
        });
    }

    @Override
    public Command registerCommand(String name) {
        InternalCommand command = new InternalCommand(name);
        commands.add(command);
        return command;
    }

    /*
    private class TestCommand extends Command {

        public TestCommand() {
            super("srv");
        }

        @Override
        public void execute(CommandSender commandSender, String[] args) {
            if (commandSender instanceof ProxiedPlayer) {
                BungeePlayer player = getPlayer(((ProxiedPlayer) commandSender).getUniqueId()).orElseThrow(() -> new NullPointerException("Player not found."));
                if (args.length == 1) {
                    String serverName = args[0];
                    Optional<Server> serverOptional = getServer(serverName);

                    serverOptional.ifPresentOrElse(server -> {
                        commandSender.sendMessage("Connecting you to " + server.getName());
                        server.connect(player);
                    }, () -> commandSender.sendMessage("Invalid server."));
                } else {
                    commandSender.sendMessage("Available servers: " + getServers().stream().map(server -> server.getName() + " (" + server.getPlayerNumber() + ")").collect(Collectors.joining(", ")));
                }
            }
        }
    }
    */
}
