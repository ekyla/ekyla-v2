package fr.ekyla.core.bungee.server;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import fr.ekyla.api.bungeecord.BungeeApi;
import fr.ekyla.api.bungeecord.player.BungeePlayer;
import fr.ekyla.api.bungeecord.server.Server;
import fr.ekyla.api.server.ServerType;
import fr.ekyla.api.services.jedis.JedisService;
import fr.ekyla.core.bungee.player.InternalBungeePlayer;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.util.internal.PlatformDependent;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.BungeeServerInfo;
import net.md_5.bungee.ServerConnector;
import net.md_5.bungee.UserConnection;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.netty.ChannelWrapper;
import net.md_5.bungee.netty.HandlerBoss;
import net.md_5.bungee.netty.PipelineUtils;
import net.md_5.bungee.protocol.MinecraftDecoder;
import net.md_5.bungee.protocol.MinecraftEncoder;
import net.md_5.bungee.protocol.Protocol;

import java.lang.reflect.Field;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

/**
 * File <b>InternalServer</b> located on fr.ekyla.core.bungee.server
 * InternalServer is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 15/08/2019 at 21:07
 */
public class InternalServer implements Server {

    public long id;
    public int port;
    public String name;
    public ServerType type;
    public int maxPlayer;
    public List<InternalBungeePlayer> players;
    public String motd;

    public InternalServer(int port, String name, ServerType type, int maxPlayer, String motd) {
        this.port = port;
        this.type = type;
        this.maxPlayer = maxPlayer;
        this.players = new ArrayList<>();
        this.motd = motd;
        this.name = name;

        String serialized = BungeeApi.getInstance().getGson().toJson(this, InternalServer.class);
        JedisService jedis = BungeeApi.getInstance().getJedis();
        this.id = jedis.rpush("servers", serialized) - 1;

        BungeeApi.getInstance().getServers().add(this);
    }

    public InternalServer setPlayers(List<InternalBungeePlayer> players) {
        this.players = players;
        return this;
    }

    public long getId() {
        return id;
    }

    @Override
    public int getPlayerNumber() {
        refresh();
        return players.size();
    }

    private void refresh() {
        String serialized = BungeeApi.getInstance().getJedis().lrange("servers", id, id).orElseThrow(() -> new NullPointerException("Server not found exception")).get(0);
        JsonParser parser = new JsonParser();
        JsonObject object = parser.parse(serialized).getAsJsonObject();
        this.motd = object.get("motd").getAsString();
        this.maxPlayer = object.get("maxPlayer").getAsInt();
        this.name = object.get("name").getAsString();
        this.type = ServerType.valueOf(object.get("type").getAsString());
    }

    @Override
    public String toString() {
        return "InternalServer{" +
                "id=" + id +
                ", port=" + port +
                ", name='" + name + '\'' +
                ", type=" + type +
                ", maxPlayer=" + maxPlayer +
                ", playerNumber=" + players.size() +
                ", players=" + players +
                ", motd='" + motd + '\'' +
                '}';
    }

    @Override
    public List<BungeePlayer> getConnectedPlayers() {
        refresh();
        return new ArrayList<>(players);
    }

    @Override
    public Server connect(BungeePlayer player) {
        if (player.getServer() == this) return this;
        InternalServer oldServer = (InternalServer) player.getServer();
        BungeeServerInfo target = new BungeeServerInfo(getName(), new InetSocketAddress("127.0.0.1", port), getMotd(), false);
        ChannelInitializer initializer = new ChannelInitializer() {
            protected void initChannel(Channel ch) {
                PipelineUtils.BASE.initChannel(ch);
                ch.pipeline().addAfter("frame-decoder", "packet-decoder", new MinecraftDecoder(Protocol.HANDSHAKE, false, player.getProxiedPlayer().getPendingConnection().getVersion()));
                ch.pipeline().addAfter("frame-prepender", "packet-encoder", new MinecraftEncoder(Protocol.HANDSHAKE, false, player.getProxiedPlayer().getPendingConnection().getVersion()));
                ch.pipeline().get(HandlerBoss.class).setHandler(new ServerConnector(BungeeCord.getInstance(), player.getProxiedPlayer(), target));
            }
        };
        ChannelFutureListener listener = future -> {

            if (!future.isSuccess()) {
                future.channel().close();
                player.getProxiedPlayer().getPendingConnects().remove(target);
                ServerInfo def = player.getProxiedPlayer().updateAndGetNextServer(target);
                if (def != null && (player.getProxiedPlayer().getServer() == null || def != player.getProxiedPlayer().getServer().getInfo())) {
                    Optional<Server> srv = BungeeApi.getInstance().getServers(ServerType.HUB).stream().min(Comparator.comparingInt(Server::getPlayerNumber));
                    if (srv.isPresent()) {
                        Server hub = srv.get();
                        player.getProxiedPlayer().sendMessage(new TextComponent("Could not connect to the server. Fallin backto a hub."));
                        hub.connect(player);
                    } else {
                        player.getProxiedPlayer().disconnect(new TextComponent("No server were found."));
                    }
                } else if (player.getProxiedPlayer().isDimensionChange()) {
                    //player.getProxiedPlayer().disconnect(player.getProxiedPlayer().bungee.getTranslation("fallback_kick", new Object[]{future.cause().getClass().getName()}));
                    System.out.println("changement de dimension");
                } else {
                    //player.getProxiedPlayer().sendMessage(player.getProxiedPlayer().bungee.getTranslation("fallback_kick", new Object[]{future.cause().getClass().getName()}));
                    System.out.println("erreur lors du tp");
                }
            } else {
                player.setServer(this);
                this.addPlayer((InternalBungeePlayer) player);
                if (oldServer != null)
                    oldServer.removePlayer((InternalBungeePlayer) player);
            }

        };
        UserConnection connection = player.getProxiedPlayer();
        Class clazz = connection.getClass();
        try {
            Field channel = clazz.getDeclaredField("ch");
            channel.setAccessible(true);
            ChannelWrapper ch = (ChannelWrapper) channel.get(connection);
            Bootstrap b = new Bootstrap().channel(PipelineUtils.getChannel()).group(ch.getHandle().eventLoop()).handler(initializer).option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 5000).remoteAddress(target.getAddress());
            if (player.getProxiedPlayer().getPendingConnection().getListener().isSetLocalAddress() && !PlatformDependent.isWindows()) {
                b.localAddress(player.getProxiedPlayer().getPendingConnection().getListener().getHost().getHostString(), 0);
            }

            b.connect().addListener(listener);
        } catch (IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
        }
        return this;
    }

    public InternalServer addPlayer(InternalBungeePlayer player) {
        this.players.add(player);
        return publish();
    }

    public InternalServer removePlayer(InternalBungeePlayer player) {
        this.players.remove(player);
        return publish();
    }

    @Override
    public String getMotd() {
        refresh();
        return motd;
    }

    @Override
    public Server setMotd(String motd) {
        this.motd = motd;
        return publish();
    }

    @Override
    public String getName() {
        refresh();
        return name;
    }

    @Override
    public Server setName(String name) {
        this.name = name;
        return publish();
    }

    @Override
    public int getMaxPlayer() {
        refresh();
        return maxPlayer;
    }

    @Override
    public Server setMaxPlayer(int maxPlayer) {
        this.maxPlayer = maxPlayer;
        return publish();
    }

    @Override
    public ServerType getType() {
        refresh();
        return this.type;
    }

    @Override
    public Server setType(ServerType type) {
        this.type = type;
        return publish();
    }

    @Override
    public int getPort() {
        return this.port;
    }

    private InternalServer publish() {
        Gson gson = BungeeApi.getInstance().getGson();
        String serialized = gson.toJson(this, InternalServer.class);
        JedisService jedis = BungeeApi.getInstance().getJedis();
        jedis.lset("servers", id, serialized);
        return this;
    }
}
