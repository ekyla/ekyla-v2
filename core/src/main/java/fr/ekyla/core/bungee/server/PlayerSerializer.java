package fr.ekyla.core.bungee.server;

import com.google.gson.*;
import fr.ekyla.api.bungeecord.player.BungeePlayer;
import fr.ekyla.core.bungee.BungeeCore;

import java.lang.reflect.Type;
import java.util.UUID;

/**
 * File <b>PlayerSerializer</b> located on fr.ekyla.core.bungee.server
 * PlayerSerializer is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 16/08/2019 at 20:42
 */
public class PlayerSerializer implements JsonSerializer<BungeePlayer>, JsonDeserializer<BungeePlayer> {

    @Override
    public BungeePlayer deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
        JsonObject obj = (JsonObject) jsonElement;
        String uuid = obj.get("uuid").getAsString();
        System.out.println("deserialized player: " + uuid);
        return BungeeCore.getInstance().getPlayer(UUID.fromString(uuid)).orElseThrow(() -> new NullPointerException("Player not found"));
    }

    @Override
    public JsonElement serialize(BungeePlayer player, Type type, JsonSerializationContext context) {
        JsonObject object = new JsonObject();
        System.out.println("serializing: " + player);
        object.addProperty("uuid", player.getUuid().toString());
        return object;
    }
}
