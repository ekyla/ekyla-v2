package fr.ekyla.core.bungee.commands;

import fr.ekyla.api.bungeecord.commands.Command;
import fr.ekyla.api.bungeecord.player.BungeePlayer;
import fr.ekyla.api.callbacks.DualCallback;
import fr.ekyla.api.commands.Argument;
import fr.ekyla.api.exception.EkylaException;

import java.util.*;

/**
 * File <b>InternalCommand</b> located on fr.ekyla.core.internal.commands
 * InternalCommand is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 25/08/2019 at 15:39
 */
public class InternalCommand implements Command {

    private final String name;
    List<InternalCommand> arguments;
    private final Map<Integer, DualCallback<BungeePlayer, Argument>> behaviors;
    private String usage = "";
    private final Command parent;

    public InternalCommand(Command parent, String name) {
        this.parent = parent;
        this.name = name;
        this.arguments = new ArrayList<>();
        this.behaviors = new HashMap<>();
        this.behaviors.put(0, (player, argument) -> {
        });
    }

    public InternalCommand(String name) {
        this(null, name);
    }

    @Override
    public Command setUsage(String usage) {
        this.usage = usage;
        return this;
    }

    public String getUsage() {
        return usage;
    }

    public Optional<InternalCommand> getArgument(String name) {
        return arguments.stream().filter(subCommand -> subCommand.getName().equalsIgnoreCase(name)).findFirst();
    }

    public String getName() {
        return name;
    }

    @Override
    public Command addBehavior(int argumentNumber, DualCallback<BungeePlayer, Argument> behavior) {
        this.behaviors.put(argumentNumber, behavior);
        return this;
    }

    @Override
    public Command addArgument(String name) {
        InternalCommand subcommand = new InternalCommand(this, name);
        this.arguments.add(subcommand);
        return subcommand;
    }

    public Map<Integer, DualCallback<BungeePlayer, Argument>> getBehaviors() {
        return behaviors;
    }

    @Override
    public boolean execute(BungeePlayer player, Argument argument) throws EkylaException {
        Optional<Integer> opt = behaviors.keySet().stream().filter(args -> args == argument.number()).findFirst();
        if (opt.isPresent()) {
            int args = opt.get();
            DualCallback<BungeePlayer, Argument> behavior = behaviors.get(args);
            behavior.done(player, argument);
            return true;
        }
        return false;
    }
}
