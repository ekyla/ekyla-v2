package fr.ekyla.core.bungee.server;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import fr.ekyla.api.server.ServerType;
import fr.ekyla.core.bungee.player.InternalBungeePlayer;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * File <b>ServerSerializer</b> located on fr.ekyla.core.bungee.server
 * ServerSerializer is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 22/08/2019 at 16:50
 */
public class ServerSerializer implements JsonSerializer<InternalServer>, JsonDeserializer<InternalServer> {

    @Override
    public JsonElement serialize(InternalServer server, Type type, JsonSerializationContext context) {
        JsonObject object = new JsonObject();
        object.addProperty("id", server.id);
        object.addProperty("name", server.name);
        object.addProperty("port", server.port);
        object.addProperty("maxPlayer", server.maxPlayer);
        object.addProperty("motd", server.motd);
        object.addProperty("type", server.type.name());
        object.add("players", context.serialize(server.players));

        return object;
    }

    @Override
    public InternalServer deserialize(JsonElement jsonElement, Type classType, JsonDeserializationContext context) throws JsonParseException {
        JsonObject object = (JsonObject) jsonElement;
        int port = object.get("port").getAsInt();
        String name = object.get("name").getAsString();
        ServerType type = ServerType.valueOf(object.get("type").getAsString());
        int maxPlayer = object.get("maxPlayer").getAsInt();
        String motd = object.get("motd").getAsString();

        Type lType = new TypeToken<ArrayList<InternalBungeePlayer>>() {
        }.getType();

        List<InternalBungeePlayer> players = new ArrayList<>();
        try {
            players = context.deserialize(object.get("players"), lType);
        } catch (NullPointerException ignored) {
        }

        return new InternalServer(port, name, type, maxPlayer, motd).setPlayers(players);
    }
}
