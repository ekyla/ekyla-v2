package fr.ekyla.core.bungee.listener;

import fr.ekyla.api.bungeecord.BungeeApi;
import fr.ekyla.api.bungeecord.commands.Command;
import fr.ekyla.api.bungeecord.player.BungeePlayer;
import fr.ekyla.api.bungeecord.server.Server;
import fr.ekyla.api.exception.EkylaException;
import fr.ekyla.api.exception.PlayerNotOnlineException;
import fr.ekyla.api.i18n.TranslateMessage;
import fr.ekyla.api.server.ServerType;
import fr.ekyla.api.services.jedis.JedisService;
import fr.ekyla.core.bungee.commands.InternalCommand;
import fr.ekyla.core.bungee.player.InternalBungeePlayer;
import fr.ekyla.core.bungee.server.InternalServer;
import fr.ekyla.core.internal.commands.InternalArgument;
import net.md_5.bungee.UserConnection;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.Connection;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * File <b>PlayerListener</b> located on fr.ekyla.core.bungee.listener
 * PlayerListener is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 16/08/2019 at 14:14
 */
public class PlayerListener implements Listener {

    @EventHandler
    public void onJoin(PostLoginEvent event) {
        ProxiedPlayer player = event.getPlayer();
        InternalBungeePlayer bungeePlayer = new InternalBungeePlayer(player.getUniqueId());

        if (!bungeePlayer.prepareEkylaPlayer()) return;


        Optional<Server> srv = BungeeApi.getInstance().getServers(ServerType.HUB).stream().min(Comparator.comparingInt(Server::getPlayerNumber));
        if (srv.isPresent()) {
            event.setCancelTeleportation(true);
            InternalServer hub = (InternalServer) srv.get();
            System.out.println("Sending to hub: " + hub.getName());
            hub.connect(bungeePlayer);
            bungeePlayer.getProxiedPlayer().sendMessage("Il y a " + hub.getPlayerNumber() + " connectés.");
        } else {
            bungeePlayer.getProxiedPlayer().disconnect(new TextComponent("No hub server were found."));
        }
    }

    @EventHandler
    public void onQuit(PlayerDisconnectEvent event) {
        InternalBungeePlayer player = (InternalBungeePlayer) BungeeApi.getInstance().getPlayer(event.getPlayer().getUniqueId()).orElseThrow(() -> new NullPointerException("Player not found"));
        //TODO: Save Player

        JedisService jedis = BungeeApi.getInstance().getJedis();

        jedis.ldel("players", player.getJedisId());
        ((InternalServer) player.getServer()).removePlayer(player);

        BungeeApi.getInstance().getPlayers().remove(player);
    }

    @EventHandler
    public void onChat(ChatEvent event) {
        String message = event.getMessage();
        Connection sender = event.getSender();
        if (!(sender instanceof UserConnection)) return;
        BungeePlayer player = BungeeApi.getInstance().getPlayer(((UserConnection) sender).getUniqueId()).orElseThrow(() -> new PlayerNotOnlineException(""));
        if (message.startsWith("/")) {
            message = message.replaceAll(" +", " ");
            String[] splitted = message.substring(1).split(" ");
            if (splitted.length == 0) return;
            String commandName = splitted[0];
            Optional<InternalCommand> optional = BungeeApi.getInstance().getCommands().stream().filter(cmd -> ((InternalCommand) cmd).getName().equalsIgnoreCase(commandName)).map(cmd -> (InternalCommand) cmd).findFirst();
            optional.ifPresent((command) -> {
                if (splitted.length > 1) {
                    List<String> args = new ArrayList<>(Arrays.asList(splitted).subList(1, splitted.length));
                    AtomicReference<Command> lastCmd = new AtomicReference<>(command);
                    List<Object> toPass = new ArrayList<>(args);
                    args.forEach(arg -> {
                        if (event.isCancelled()) return;
                        Optional<InternalCommand> opt = command.getArgument(arg);
                        opt.ifPresentOrElse(subCommand -> {
                            lastCmd.set(subCommand);
                            toPass.remove(arg);
                            if (toPass.isEmpty()) {
                                try {
                                    lastCmd.get().execute(player, new InternalArgument(Collections.emptyList()));
                                } catch (EkylaException e) {
                                    player.sendMessage(new TranslateMessage(e.getMessage(), e.getParams()));
                                }
                            }
                        }, () -> {
                            Command last = lastCmd.get();
                            try {
                                if (!last.execute(player, new InternalArgument(toPass))) {
                                    String fullCmd = "/" + commandName + " " + args.stream().filter(argu -> !toPass.contains(argu)).collect(Collectors.joining(" "));
                                    player.sendMessage(new TranslateMessage("player.command.bad-usage", fullCmd, last.getUsage()));
                                }
                            } catch (EkylaException e) {
                                player.sendMessage(new TranslateMessage(e.getMessage(), e.getParams()));
                            }
                            event.setCancelled(true);
                        });
                    });
                } else {
                    try {
                        command.execute(player, new InternalArgument(Collections.emptyList()));
                    } catch (EkylaException e) {
                        player.sendMessage(new TranslateMessage(e.getMessage(), e.getParams()));
                    }
                    event.setCancelled(true);
                }
            });
        }
    }

}
