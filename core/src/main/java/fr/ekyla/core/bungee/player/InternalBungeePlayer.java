package fr.ekyla.core.bungee.player;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import fr.ekyla.api.bungeecord.BungeeApi;
import fr.ekyla.api.bungeecord.player.BungeePlayer;
import fr.ekyla.api.bungeecord.server.Server;
import fr.ekyla.api.i18n.Lang;
import fr.ekyla.api.i18n.TranslateMessage;
import fr.ekyla.api.player.Rank;
import fr.ekyla.api.services.sql.SqlRequest;
import fr.ekyla.api.services.sql.SqlResult;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.UserConnection;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * File <b>InternalBungeePlayer</b> located on fr.ekyla.core.bungee.player
 * InternalBungeePlayer is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 16/08/2019 at 14:16
 */
public class InternalBungeePlayer implements BungeePlayer {

    private final UserConnection proxiedPlayer;
    private final List<Rank> ranks;
    private Server server;
    @Expose
    private UUID uuid;
    private long jedisId;
    private int id;

    public InternalBungeePlayer(UUID uuid) {
        this.proxiedPlayer = (UserConnection) BungeeCord.getInstance().getPlayer(uuid);
        this.uuid = uuid;
        this.ranks = new ArrayList<>();

        BungeeApi.getInstance().getPlayers().add(this);
    }

    public boolean prepareEkylaPlayer() {

        //get SQL
        UUID uuid = proxiedPlayer.getUniqueId();
        String username = proxiedPlayer.getName();

        SqlResult result = BungeeApi.getInstance().getSql().select("players").where("uuid", uuid, SqlRequest.Check.EQUALS).result();

        JsonObject player = new JsonObject();

        if (result.next()) {

            this.id = result.getInt("id");
            Lang lang = Lang.valueOf(result.getString("lang"));

            SqlResult ranksql = BungeeApi.getInstance().getSql().select("ranks").where("player_id", id, SqlRequest.Check.EQUALS).result();
            JsonArray ranks = new JsonArray();

            while (ranksql.next()) {
                Rank.getById(ranksql.getInt("rank")).ifPresent(rank -> {
                    ranks.add(rank.getId());
                    this.ranks.add(rank);
                });
            }

            SqlResult permsql = BungeeApi.getInstance().getSql().select("permissions").where("player_id", id, SqlRequest.Check.EQUALS).result();
            JsonArray perms = new JsonArray();

            while (permsql.next()) {
                perms.add(permsql.getString("permission"));
            }

            player.addProperty("id", id);
            player.addProperty("uuid", uuid.toString());
            player.addProperty("username", username);
            player.addProperty("lang", lang.name());
            player.add("ranks", ranks);
            player.add("permissions", perms);


        } else {
            Lang lang = Lang.FRENCH;

            SqlResult insertPlayer = BungeeApi.getInstance().getSql().insert("players").keys("uuid", "username", "lang").values(uuid, username, lang.name()).result();

            if (insertPlayer.next()) {
                int id = insertPlayer.getInt(0);

                player.addProperty("id", id);
                player.addProperty("uuid", uuid.toString());
                player.addProperty("username", username);
                player.addProperty("lang", lang.name());
                player.add("ranks", new JsonArray());
                player.add("permissions", new JsonArray());
            } else {
                proxiedPlayer.disconnect("An error occured while creating your player. Please contact an administrator");
                return false;
            }

        }

        this.jedisId = BungeeApi.getInstance().getJedis().rpush("players", player.toString());

        return true;
    }

    public int getId() {
        return id;
    }

    @Override
    public List<Rank> getRanks() {
        return ranks;
    }

    public long getJedisId() {
        return jedisId;
    }

    @Override
    public UserConnection getProxiedPlayer() {
        return proxiedPlayer;
    }

    @Override
    public UUID getUuid() {
        return uuid;
    }

    @Override
    public BungeePlayer setServer(Server server) {
        this.server = server;
        return this;
    }

    @Override
    public Server getServer() {
        return server;
    }

    @Override
    public BungeePlayer sendMessage(TranslateMessage message) {
        proxiedPlayer.sendMessage(message.format(getLang()));
        return this;
    }

    @Override
    public Lang getLang() {
        return Lang.FRENCH;
    }

    @Override
    public BungeePlayer sendMessage(String message) {
        proxiedPlayer.sendMessage(message);
        return this;
    }
}
