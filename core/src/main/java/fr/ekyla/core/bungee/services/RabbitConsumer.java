package fr.ekyla.core.bungee.services;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;
import fr.ekyla.api.bungeecord.BungeeApi;
import fr.ekyla.api.server.ServerType;
import fr.ekyla.api.services.jedis.JedisService;
import fr.ekyla.core.bungee.BungeeCore;
import fr.ekyla.core.bungee.server.InternalServer;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

/**
 * File <b>RabbitConsumer</b> located on fr.ekyla.core.bungee.services
 * RabbitConsumer is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 15/08/2019 at 20:53
 */
public class RabbitConsumer {

    public RabbitConsumer(Channel channel) {
        DeliverCallback serversConsumer = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
            //System.out.println(" [x] Received '" + message + "'");

            String[] split = message.split("\\|");
            String type = split[0];

            if (type.equalsIgnoreCase("enabled")) {
                int port = Integer.parseInt(split[1]);
                String name = split[2];
                ServerType serverType = ServerType.valueOf(split[3]);
                int maxPlayer = Integer.parseInt(split[4]);
                String motd = split[5];

                InternalServer server = new InternalServer(port, name, serverType, maxPlayer, motd);
                System.out.println("Registered server \"" + server.getName() + "\"");

            } else if (type.equalsIgnoreCase("disabled")) {

                int port = Integer.parseInt(split[1]);

                BungeeApi.getInstance().getServer(port).ifPresent(internalServer -> {
                    System.out.println("Disabling server \"" + internalServer.getName() + "\"");
                    JedisService jedis = BungeeApi.getInstance().getJedis();
                    jedis.ldel("servers", internalServer.getId());
                    BungeeCore.getInstance().getServers().remove(internalServer);
                });

            }

        };

        DeliverCallback playerConsumer = (tag, delivery) -> {
            AMQP.BasicProperties replyProps = new AMQP.BasicProperties.Builder().correlationId(delivery.getProperties().getCorrelationId()).build();
            StringBuilder response = new StringBuilder();

            String message = new String(delivery.getBody(), StandardCharsets.UTF_8);

            String[] splitted = message.split("\\|");
            String type = splitted[0];
            if (type.equalsIgnoreCase("online")) {
                String username = splitted[1];
                response.append(BungeeApi.getInstance().getPlayer(username).isPresent());
            } else if (type.equalsIgnoreCase("ranks")) {
                String username = splitted[1];
                BungeeApi.getInstance().getPlayer(username).ifPresentOrElse(player -> response.append(player.getRanks().stream().map(rank -> String.valueOf(rank.getId())).collect(Collectors.joining(","))), () -> response.append(false));
            }

            channel.basicPublish("", delivery.getProperties().getReplyTo(), replyProps, response.toString().getBytes(StandardCharsets.UTF_8));
            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);

        };

        try {
            channel.basicConsume("servers", true, serversConsumer, consumerTag -> {
            });
            channel.basicConsume("players", true, playerConsumer, consumerTag -> {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
