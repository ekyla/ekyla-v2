package fr.ekyla.core;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import fr.ekyla.api.EkylaApi;
import fr.ekyla.api.announce.Announcer;
import fr.ekyla.api.commands.Command;
import fr.ekyla.api.i18n.Translator;
import fr.ekyla.api.i18n.i18nLoader;
import fr.ekyla.api.module.Module;
import fr.ekyla.api.net.ProtocolManager;
import fr.ekyla.api.player.EkylaPlayer;
import fr.ekyla.api.player.PlayerInfo;
import fr.ekyla.api.server.ServerType;
import fr.ekyla.api.services.jedis.JedisService;
import fr.ekyla.api.services.rabbitmq.RabbitMQService;
import fr.ekyla.api.storage.StorageManager;
import fr.ekyla.api.utils.NMSReflection;
import fr.ekyla.core.commands.AdminCommand;
import fr.ekyla.core.commands.RankCommand;
import fr.ekyla.core.internal.announce.InternalAnnouncer;
import fr.ekyla.core.internal.commands.InternalCommand;
import fr.ekyla.core.internal.i18n.InternalTranslator;
import fr.ekyla.core.internal.net.PacketHandlerManager;
import fr.ekyla.core.internal.net.PacketInterceptor;
import fr.ekyla.core.internal.net.packets.login.server.DisconnectPacketHandler;
import fr.ekyla.core.internal.net.packets.play.client.*;
import fr.ekyla.core.internal.player.InternalPlayer;
import fr.ekyla.core.internal.player.InternalPlayerInfo;
import fr.ekyla.core.internal.player.PlayerSerializer;
import fr.ekyla.core.internal.services.jedis.InternalJedis;
import fr.ekyla.core.internal.services.rabbitmq.InternalRabbitMQ;
import fr.ekyla.core.internal.services.sql.InternalSqlService;
import fr.ekyla.core.internal.storage.InternalStorageManager;
import fr.ekyla.core.listener.PlayerListener;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * File <b>EkylaCore</b> located on fr.ekyla.core
 * EkylaCore is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 31/07/2019 at 17:38
 */
public class EkylaCore extends EkylaApi {

    private final List<Module> modules;
    private final CopyOnWriteArrayList<EkylaPlayer> players;
    private final ProtocolManager protocolManager;
    private final StorageManager storageManager;
    private final InternalTranslator translator;
    private final JedisService jedis;
    private final RabbitMQService rabbitmq;
    private final InternalAnnouncer announcer;
    private final InternalSqlService sql;
    private final Gson gson;
    private final List<Command> commands;

    public EkylaCore() {
        this.modules = new ArrayList<>();
        this.players = new CopyOnWriteArrayList<>();
        this.protocolManager = new PacketHandlerManager();
        this.storageManager = new InternalStorageManager();
        this.translator = new InternalTranslator();
        this.jedis = new InternalJedis("127.0.1.1", 6379, null);
        this.rabbitmq = new InternalRabbitMQ("127.0.0.1", "guest", "guest");
        this.sql = new InternalSqlService("127.0.0.1", 3306, "ekyla-server", "password", "ekyla-v2");
        this.announcer = new InternalAnnouncer();
        this.commands = new ArrayList<>();

        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(InternalPlayer.class, new PlayerSerializer());

        this.gson = builder.create();

    }

    @Override
    public void onLoad() {
        this.jedis.initConnection();
        YamlConfiguration configuration = new YamlConfiguration();
        try {
            File conf = new File(getDataFolder(), "../../options.yml");
            if (conf.exists()) {
                configuration.load(conf);
                String type = configuration.getString("type");
                try {
                    this.serverType = ServerType.valueOf(type);
                } catch (IllegalArgumentException e) {
                    this.serverType = ServerType.HUB;
                }
                this.maxPlayer = configuration.getInt("maxplayer");
                this.motd = configuration.getString("motd");
                this.serverId = configuration.getInt("serverid");

            } else {
                this.serverType = ServerType.HUB;
                this.maxPlayer = 25;
                this.motd = "server.default-motd";
                this.serverId = (int) jedis.lsize("servers");
            }
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onEnable() {
        System.out.println("-------------------------");
        System.out.println("-                       -");
        System.out.println("-      Ekyla - Api      -");
        System.out.println("-                       -");
        System.out.println("-------------------------");

        PacketInterceptor.inject();
        protocolManager.subscribeHandler(new ChatInPacketHandler());
        protocolManager.subscribeHandler(new WindowClickPacketHandler());
        protocolManager.subscribeHandler(new DisconnectPacketHandler());
        protocolManager.subscribeHandler(new UseItemPacketHandler());
        protocolManager.subscribeHandler(new PlayerDigPacketHandler());
        protocolManager.subscribeHandler(new PlayerMovePacketHandler());
        protocolManager.subscribeHandler(new PlayerLookPacketHandler());

        System.out.println("Registering listeners:");
        new PlayerListener(this);

        System.out.println("Registering languages...");
        geti18nLoader().loadLangs(this);
        System.out.println("Registered languages: " + translator.getLoader().values.keySet());

        this.sql.init();

        this.rabbitmq.publishToQueue("servers", "enabled|" + this.getServer().getPort() + "|" + getServerName() + "|" + serverType.name() + "|" + getMaxPlayer() + "|" + getMotd());
        System.out.println("Registered server in bungee");


        new RankCommand(this);
        new AdminCommand(this);
    }

    @Override
    public void onDisable() {
        PacketInterceptor.clean();

        this.rabbitmq.publishToQueue("servers", "disabled|" + this.getServer().getPort());
        System.out.println("Unregistered server in bungee");

        this.jedis.closeConnection();
        this.sql.close();
        this.rabbitmq.closeConnection();
    }

    @Override
    public PlayerInfo getPlayerInfo(String username) {
        return new InternalPlayerInfo(username);
    }

    public Optional<InternalPlayer> getPlayer(InetSocketAddress address) {
        return players.stream().filter(player -> player.getAddress().getPort() == address.getPort()).map(player -> (InternalPlayer) player).findFirst();
    }

    public Optional<EkylaPlayer> getPlayer(String username) {
        return players.stream().filter(player -> player.getUsername().equalsIgnoreCase(username)).findFirst();
    }

    @Override
    public List<Command> getCommands() {
        return commands;
    }

    @Override
    public Gson getJson() {
        return gson;
    }

    @Override
    public InternalSqlService getSql() {
        return sql;
    }

    @Override
    public Translator getTranslator() {
        return translator;
    }

    @Override
    public i18nLoader geti18nLoader() {
        return translator.getLoader();
    }

    public void addPlayer(EkylaPlayer player) {
        if (players.contains(player)) return;
        players.add(player);
    }

    @Override
    public Announcer getAnnouncer() {
        return announcer;
    }

    public String getServerName() {
        return serverType.name().toLowerCase() + "-" + getServerId();
    }

    @Override
    public ProtocolManager getProtocolManager() {
        return protocolManager;
    }

    @Override
    public StorageManager getStorageManager() {
        return storageManager;
    }

    @Override
    public Optional<EkylaPlayer> getPlayer(UUID playerId) {
        return players.stream().filter(player -> player.getUuid().equals(playerId)).findFirst();
    }

    @Override
    public CopyOnWriteArrayList<EkylaPlayer> getPlayers() {
        return players;
    }

    @Override
    public RabbitMQService getRabbitMQ() {
        return rabbitmq;
    }

    @Override
    public JedisService getJedis() {
        return jedis;
    }

    public <T extends Module> Optional<Module> getModule(Class<T> module) {
        return modules.stream().filter(m -> m.getClass().equals(module)).findFirst();
    }

    public <T extends Module> T registerModule(Class<T> module) {
        try {
            T mod = NMSReflection.getConstructorAccessor(module, EkylaApi.class).newInstance(EkylaApi.getInstance());
            if (isEnabled()) {
                mod.onLoad();
                mod.onEnable();
            }
            modules.add(mod);
            return mod;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public <T extends Module> void unregisterModule(Class<T> module) {
        Optional<? extends Module> optional = modules.stream().filter(mod -> mod.getClass().equals(module)).findFirst();
        optional.ifPresent(mod -> {
            mod.onDisable();
            modules.remove(mod);
        });
    }

    public void removePlayer(EkylaPlayer player) {
        players.remove(player);
    }

    @Override
    public Command registerCommand(String name) {
        InternalCommand command = new InternalCommand(name);
        commands.add(command);
        return command;
    }
}
