package fr.ekyla.api.callbacks;

public abstract class Condition<T> {

    public abstract boolean test(T t);

}
