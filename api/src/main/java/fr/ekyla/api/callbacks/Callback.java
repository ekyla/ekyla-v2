package fr.ekyla.api.callbacks;

public interface Callback<T> {

    void done(T t);

}
