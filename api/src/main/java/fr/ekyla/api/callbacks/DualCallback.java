package fr.ekyla.api.callbacks;

public interface DualCallback<T, U> {

    void done(T t, U u);

}
