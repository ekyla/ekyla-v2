package fr.ekyla.api.bungeecord.player;

import fr.ekyla.api.bungeecord.server.Server;
import fr.ekyla.api.i18n.Lang;
import fr.ekyla.api.i18n.TranslateMessage;
import fr.ekyla.api.player.Rank;
import net.md_5.bungee.UserConnection;

import java.util.List;
import java.util.UUID;

/**
 * File <b>BungeePlayer</b> located on fr.ekyla.api.bungeecord.player
 * BungeePlayer is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 14/08/2019 at 13:19
 */
public interface BungeePlayer {

    UserConnection getProxiedPlayer();

    default BungeePlayer connect(Server server) {
        server.connect(this);
        return this;
    }

    UUID getUuid();

    Server getServer();

    BungeePlayer setServer(Server server);

    List<Rank> getRanks();

    default BungeePlayer sendMessage(TranslateMessage message) {
        sendMessage(message.format(getLang()));
        return this;
    }

    Lang getLang();

    default BungeePlayer sendMessage(String message) {
        getProxiedPlayer().sendMessage(message);
        return this;
    }

}
