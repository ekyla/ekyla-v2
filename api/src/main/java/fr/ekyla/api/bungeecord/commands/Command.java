package fr.ekyla.api.bungeecord.commands;

import fr.ekyla.api.bungeecord.player.BungeePlayer;
import fr.ekyla.api.callbacks.DualCallback;
import fr.ekyla.api.commands.Argument;
import fr.ekyla.api.exception.EkylaException;

/**
 * File <b>Command</b> located on fr.ekyla.api.commands
 * Command is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 23/08/2019 at 13:53
 */
public interface Command {

    Command addBehavior(int argumentNumber, DualCallback<BungeePlayer, Argument> behavior);

    Command addArgument(String name);

    boolean execute(BungeePlayer player, Argument argument) throws EkylaException;

    String getUsage();

    Command setUsage(String usage);
}
