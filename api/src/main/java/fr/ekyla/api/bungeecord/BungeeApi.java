package fr.ekyla.api.bungeecord;

import com.google.gson.Gson;
import fr.ekyla.api.bungeecord.commands.Command;
import fr.ekyla.api.bungeecord.player.BungeePlayer;
import fr.ekyla.api.bungeecord.server.Server;
import fr.ekyla.api.module.ModuleManager;
import fr.ekyla.api.server.ServerType;
import fr.ekyla.api.services.jedis.JedisService;
import fr.ekyla.api.services.rabbitmq.RabbitMQService;
import fr.ekyla.api.services.sql.SqlService;
import net.md_5.bungee.api.plugin.Plugin;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * File <b>BungeeApi</b> located on fr.ekyla.api.bungeecord
 * BungeeApi is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 14/08/2019 at 13:16
 */
public abstract class BungeeApi extends Plugin implements ModuleManager {

    private static BungeeApi instance;

    protected BungeeApi() {
        if (instance != null) throw new UnsupportedOperationException("Instance already exist");
        instance = this;
    }

    public static BungeeApi getInstance() {
        return instance;
    }

    public abstract Gson getGson();

    public abstract SqlService getSql();

    public abstract Optional<Server> getServer(String name);

    public abstract Optional<Server> getServer(int port);

    public abstract List<Server> getServers(ServerType type);

    public abstract List<Server> getServers();

    public abstract RabbitMQService getRabbitMQ();

    public abstract JedisService getJedis();

    public abstract Optional<BungeePlayer> getPlayer(UUID playerId);
    public abstract Optional<BungeePlayer> getPlayer(String username);

    public abstract List<BungeePlayer> getPlayers();

    public abstract Command registerCommand(String name);

    public abstract List<Command> getCommands();
}
