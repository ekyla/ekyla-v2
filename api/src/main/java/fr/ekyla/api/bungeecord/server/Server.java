package fr.ekyla.api.bungeecord.server;

import fr.ekyla.api.bungeecord.player.BungeePlayer;
import fr.ekyla.api.server.ServerType;

import java.util.List;

/**
 * File <b>Server</b> located on fr.ekyla.api.bungeecord.server
 * Server is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 14/08/2019 at 13:26
 */
public interface Server {

    long getId();

    int getPlayerNumber();

    List<BungeePlayer> getConnectedPlayers();

    Server connect(BungeePlayer player);

    String getMotd();

    Server setMotd(String motd);

    String getName();

    Server setName(String name);

    int getMaxPlayer();

    Server setMaxPlayer(int maxPlayer);

    ServerType getType();

    Server setType(ServerType type);

    int getPort();
}
