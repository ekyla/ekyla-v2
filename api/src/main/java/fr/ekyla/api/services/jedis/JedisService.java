package fr.ekyla.api.services.jedis;

import java.util.List;
import java.util.Optional;

/**
 * File <b>JedisService</b> located on fr.ekyla.api.services.jedis
 * JedisService is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 15/08/2019 at 12:35
 */
public interface JedisService {

    Optional<String> get(String key);
    String set(String key, String value);

    long rpush(String key, String... values);
    String lset(String key, long id, String value);
    long lsize(String key);
    Optional<List<String>> lrange(String key, long start, long stop);
    Optional<String> lget(String key, long id);
    long lrem(String key, int count, String value);
    long ldel(String key, long id);

    boolean delete(String key);

    void initConnection();
    void closeConnection();
}
