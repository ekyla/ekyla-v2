package fr.ekyla.api.services.sql;

/**
 * File <b>SqlResult</b> located on fr.ekyla.api.services.sql
 * SqlResult is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 18/08/2019 at 14:42
 */
public interface SqlResult {

    boolean next();

    Object get(String key);

    String getString(String key);

    Integer getInt(String key);

    Boolean getBoolean(String key);

    Object get(int columnid);

    String getString(int columnid);

    Integer getInt(int columnid);

    Boolean getBoolean(int columnid);
}
