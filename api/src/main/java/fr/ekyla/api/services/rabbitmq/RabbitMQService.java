package fr.ekyla.api.services.rabbitmq;

/**
 * File <b>RabbitMQService</b> located on fr.ekyla.api.services.rabbitmq
 * RabbitMQService is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 15/08/2019 at 18:55
 */
public interface RabbitMQService {

    void closeConnection();

    void publishToQueue(String queue, String buffer);

    String publishAndResponse(String queue, String buffer);

}
