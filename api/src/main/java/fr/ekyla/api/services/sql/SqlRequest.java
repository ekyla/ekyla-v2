package fr.ekyla.api.services.sql;

/**
 * File <b>SqlRequest</b> located on fr.ekyla.api.services.sql
 * SqlRequest is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 18/08/2019 at 14:33
 */
public interface SqlRequest {

    SqlRequest where(String key, Object obj, Check check);

    SqlResult result();

    SqlRequest keys(String... keys);
    SqlRequest values(Object... values);

    enum Check {

        EQUALS('='),
        HIGHER('>'),
        LOWER('<');

        private final char symbol;

        Check(char symbol) {
            this.symbol = symbol;
        }

        public char getSymbol() {
            return symbol;
        }
    }

}
