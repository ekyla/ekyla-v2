package fr.ekyla.api.services.sql;

/**
 * File <b>SqlService</b> located on fr.ekyla.api.services
 * SqlService is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 18/08/2019 at 14:33
 */
public interface SqlService {

    SqlRequest select(String table, String... columns);

    SqlRequest insert(String table);

    SqlRequest update(String table);
}
