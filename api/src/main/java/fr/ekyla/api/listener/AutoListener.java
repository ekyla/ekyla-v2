package fr.ekyla.api.listener;

import fr.ekyla.api.EkylaApi;
import fr.ekyla.api.player.EkylaPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

/**
 * File <b>AutoListener</b> located on fr.ekyla.api.listener
 * AutoListener is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 01/08/2019 at 11:37
 */
public abstract class AutoListener implements Listener {

    protected final EkylaApi api;

    public AutoListener(EkylaApi api) {
        this.api = api;
        api.registerEvent(this);
    }

    protected final EkylaPlayer getEkylaPlayer(Player player) {
        return api.getPlayer(player.getUniqueId()).orElseThrow(NullPointerException::new);
    }

}
