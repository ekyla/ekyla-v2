package fr.ekyla.api.commands;

import fr.ekyla.api.callbacks.DualCallback;
import fr.ekyla.api.exception.EkylaException;
import fr.ekyla.api.player.EkylaPlayer;

import javax.annotation.Nullable;

/**
 * File <b>Command</b> located on fr.ekyla.api.commands
 * Command is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 23/08/2019 at 13:53
 */
public interface Command {

    /**
     * Get the parent command
     * @return  command or null
     */
    Command parent();

    /**
     * Set the behavior of the default command (/<command>)
     * @param argumentNumber the number of arguments it requires
     * @param behavior the method to execute
     * @return this
     */
    Command addBehavior(int argumentNumber, DualCallback<EkylaPlayer, Argument> behavior);

    /**
     * Add a subcommand
     * @param name the argument name
     * @return the subcommand
     */
    Command addArgument(String name);

    /**
     * Execute the command or the subcommand
     * @param player the player who execute the command
     * @param argument the arguments
     * @return true if the command was succefully executed
     * @throws EkylaException if there was an error while executing the command
     */
    boolean execute(EkylaPlayer player, Argument argument) throws EkylaException;

    /**
     * Get the correct usage of the command
     * @return usage
     */
    String getUsage();

    /**
     * Set the info to be displayed when the command is incorrect
     * @param usage the message
     * @return this
     */
    Command setUsage(String usage);

    /**
     * The permission required to execute the command
     * Otherwise, send the player a "player.command.not-permission"
     * @param permission the permission
     * @return this
     */
    Command setPermission(String permission);

    /**
     * Get the required permission
     * @return the permssion
     */
    String getPermission();
}
