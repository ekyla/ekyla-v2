package fr.ekyla.api.commands;

import java.net.InetSocketAddress;

/**
 * File <b>CommandArgument</b> located on fr.ekyla.api.commands
 * CommandArgument is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 23/08/2019 at 14:20
 */
public interface Argument {

    Object get(int postion);

    String getString(int position);

    int number();
}
