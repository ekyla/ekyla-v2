package fr.ekyla.api.server;

import fr.ekyla.api.player.EkylaPlayer;

import java.util.List;

/**
 * File <b>Server</b> located on fr.ekyla.api.bungeecord.server
 * Server is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 14/08/2019 at 13:26
 */
public interface Server {

    long getId();

    int getPlayerNumber();

    List<EkylaPlayer> getConnectedPlayers();

    Server connect(EkylaPlayer player);

    String getMotd();

    Server setMotd(String motd);

    String getName();

    Server setName(String name);

    int getMaxPlayer();

    Server setMaxPlayer(int maxPlayer);

    ServerType getType();

    Server setType(ServerType type);

    int getPort();
}
