package fr.ekyla.api.server;

/**
 * File <b>ServerType</b> located on fr.ekyla.api.server
 * ServerType is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 31/07/2019 at 17:36
 */
public enum ServerType {

    HUB(),
    ROLEPLAY()
    ;



}
