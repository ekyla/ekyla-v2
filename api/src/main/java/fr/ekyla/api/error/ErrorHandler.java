package fr.ekyla.api.error;

/**
 * File <b>ErrorHandler</b> located on fr.ekyla.api.error
 * ErrorHandler is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 01/08/2019 at 21:33
 */
@FunctionalInterface
public interface ErrorHandler {

    void handle(Throwable throwable);

}
