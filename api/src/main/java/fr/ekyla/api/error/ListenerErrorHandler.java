package fr.ekyla.api.error;

import org.bukkit.event.Event;
import org.bukkit.event.EventException;
import org.bukkit.event.EventHandler;
import org.bukkit.plugin.RegisteredListener;

/**
 * File <b>ListenerErrorHandler</b> located on fr.ekyla.api.error
 * ListenerErrorHandler is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 01/08/2019 at 21:34
 */
public class ListenerErrorHandler extends RegisteredListener {
    private final RegisteredListener listener;
    private final ErrorHandler handler;

    public ListenerErrorHandler(RegisteredListener listener, ErrorHandler handler) {
        super(listener.getListener(), null, listener.getPriority(), listener.getPlugin(), listener.isIgnoringCancelled());
        this.handler = handler;
        this.listener = listener;
    }

    @EventHandler
    public void callEvent(Event event) {
        try {
            listener.callEvent(event);
        } catch (EventException e) {
            handler.handle(e.getCause());
        } catch (Throwable throwable) {
            handler.handle(throwable);
        }
    }

}
