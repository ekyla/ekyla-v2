package fr.ekyla.api.exception;

/**
 * File <b>EkylaException</b> located on fr.ekyla.api.exception
 * EkylaException is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 27/08/2019 at 16:45
 */
public class EkylaException extends RuntimeException {

    private final String message;
    private final Object[] params;

    public EkylaException(String message, Object... params) {
        this.message = message;
        this.params = params;
    }

    public Object[] getParams() {
        return params;
    }

    @Override
    public String getMessage() {
        return message;
    }

}
