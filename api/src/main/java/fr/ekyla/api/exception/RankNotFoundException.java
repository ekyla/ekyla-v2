package fr.ekyla.api.exception;

/**
 * File <b>RankNotFoundException</b> located on fr.ekyla.api.exception
 * RankNotFoundException is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 27/08/2019 at 16:52
 */
public class RankNotFoundException extends EkylaException {
    public RankNotFoundException(String rankName) {
        super("player.command.rank.not-found", rankName);
    }
}
