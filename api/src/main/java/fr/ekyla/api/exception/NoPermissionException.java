package fr.ekyla.api.exception;

public class NoPermissionException extends EkylaException {

    public NoPermissionException() {
        super("player.command.no-permission");
    }
}
