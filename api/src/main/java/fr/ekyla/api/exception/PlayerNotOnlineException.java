package fr.ekyla.api.exception;

/**
 * File <b>PlayerNotOnlineException</b> located on fr.ekyla.api.exception
 * PlayerNotOnlineException is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 27/08/2019 at 16:39
 */
public class PlayerNotOnlineException extends EkylaException {

    public PlayerNotOnlineException(String player) {
        super("player.command.target.not-connected", player);
    }

}
