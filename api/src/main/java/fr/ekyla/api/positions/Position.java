package fr.ekyla.api.positions;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.util.Vector;

public class Position {

    private World world = Bukkit.getWorld("world");
    private double x;
    private double y;
    private double z;
    private float yaw = 0;
    private float pitch = 0;

    /**
     * Create a position with 3 coordinates
     *
     * @param x the x coordinate
     * @param y the y coordinate
     * @param z the z coordinate
     */
    public Position(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Create a position with 3 coordinates and the rotation
     *
     * @param x     the x coordinate
     * @param y     the y coordinate
     * @param z     the z coordinate
     * @param yaw   the left/right rotation
     * @param pitch the top/bottom rotation
     */
    public Position(double x, double y, double z, float yaw, float pitch) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.yaw = yaw;
        this.pitch = pitch;
    }

    /**
     * Create a position with 3 coordinates and a world
     *
     * @param world the world
     * @param x     the x coordinate
     * @param y     the y coordinate
     * @param z     the z coordinate
     */
    public Position(World world, double x, double y, double z) {
        this.world = world;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Create a position with 3 coordinates, a world and
     *
     * @param world the world
     * @param x     the x coordinate
     * @param y     the y coordinate
     * @param z     the z coordinate
     * @param yaw   the right/left rotation
     * @param pitch the top/bottom rotation
     */
    public Position(World world, double x, double y, double z, float yaw, float pitch) {
        this.world = world;
        this.x = x;
        this.y = y;
        this.z = z;
        this.yaw = yaw;
        this.pitch = pitch;
    }

    public Position(Location location) {
        this.world = location.getWorld();
        this.x = location.getX();
        this.y = location.getY();
        this.z = location.getZ();
        this.yaw = location.getYaw();
        this.pitch = location.getPitch();
    }

    public static Position str2pos(String pos) {
        if (pos.equals("") || (pos.split(";").length != 3 && pos.split(";").length != 5))
            return new Position(0, 0, 0, 0, 0);
        String[] split = pos.split(";");
        double[] val = new double[]{0, 0, 0, 0, 0};
        for (int i = 0; i < split.length; i++) val[i] = Double.parseDouble(split[i]);
        return new Position(val[0], val[1], val[2], (float) val[3], (float) val[4]);
    }

    public String loc2str() {
        return x + ";" + y + ";" + z + ";" + yaw + ";" + pitch;
    }

    public Vector toVector() {
        return new Vector(x, y, z);
    }

    /**
     * World getter
     *
     * @return world
     */
    public World getWorld() {
        return world;
    }

    /**
     * World setter
     *
     * @param world the new world
     * @return the current Position
     */
    public Position setWorld(World world) {
        this.world = world;
        return this;
    }

    /**
     * X getter
     *
     * @return x
     */
    public double getX() {
        return x;
    }

    /**
     * X setter
     *
     * @param x the new x value
     * @return the current position
     */
    public Position setX(double x) {
        this.x = x;
        return this;
    }

    /**
     * Floor the x coord
     *
     * @return block x
     */
    public int getBlockX() {
        return (int) Math.floor(x);
    }

    /**
     * Floor y coord
     *
     * @return block y
     */
    public int getBlockY() {
        return (int) Math.floor(y);
    }

    /**
     * Floor z coord
     *
     * @return block z
     */
    public int getBlockZ() {
        return (int) Math.floor(z);
    }

    /**
     * Y getter
     *
     * @return y
     */
    public double getY() {
        return y;
    }

    /**
     * Y setter
     *
     * @param y the new y
     * @return the current position
     */
    public Position setY(double y) {
        this.y = y;
        return this;
    }

    /**
     * Z getter
     *
     * @return z
     */
    public double getZ() {
        return z;
    }

    /**
     * Z setter
     *
     * @param z the new z
     * @return the current position
     */
    public Position setZ(double z) {
        this.z = z;
        return this;
    }

    /**
     * Yaw getter
     *
     * @return yaw
     */
    public float getYaw() {
        return yaw;
    }

    /**
     * Yaw setter
     *
     * @param yaw the new yaw
     * @return the current position
     */
    public Position setYaw(float yaw) {
        this.yaw = yaw;
        return this;
    }

    /**
     * Pitch getter
     *
     * @return pitch
     */
    public float getPitch() {
        return pitch;
    }

    /**
     * Pitch setter
     *
     * @param pitch the new pitch
     * @return the current position
     */
    public Position setPitch(float pitch) {
        this.pitch = pitch;
        return this;
    }

    /**
     * Transform this position into a bukkit location
     *
     * @return a bukkit location
     */
    public Location toLocation() {
        return new Location(world, x, y, z, yaw, pitch);
    }

    /**
     * Increment Z value
     *
     * @param i the increment value
     * @return the current position
     */
    public Position addZ(double i) {
        this.z += i;
        return this;
    }

    /**
     * Increment X value
     *
     * @param i the increment value
     * @return the current position
     */
    public Position addX(double i) {
        this.x += i;
        return this;
    }

    /**
     * Increment Y value
     *
     * @param i the increment value
     * @return the current position
     */
    public Position addY(double i) {
        this.y += i;
        return this;
    }

    public Position round() {
        return new Position(Math.floor(x), Math.floor(y), Math.floor(z), yaw, pitch);
    }


    @Override
    public String toString() {
        return "Position{" +
                "world=" + world +
                ", x=" + x +
                ", y=" + y +
                ", z=" + z +
                ", yaw=" + yaw +
                ", pitch=" + pitch +
                '}';
    }

}
