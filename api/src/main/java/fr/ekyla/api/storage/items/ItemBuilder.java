package fr.ekyla.api.storage.items;

import com.google.common.base.Preconditions;
import net.minecraft.server.v1_12_R1.NBTTagCompound;
import org.bukkit.Material;

/**
 * File <b>ItemBuilder</b> located on fr.ekyla.api.storage
 * ItemBuilder is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 01/08/2019 at 22:01
 */
public class ItemBuilder {

    private Material material;
    private int amount;
    private byte data;

    private NBTTagCompound tag;

    public ItemBuilder(Material material) {
        this.tag = new NBTTagCompound();
        setMaterial(material);
        setAmount(1);
    }

    public ItemBuilder(Material material, int amount) {
        this.tag = new NBTTagCompound();
        setMaterial(material);
        setAmount(amount);
    }

    public ItemBuilder(Material material, byte data) {
        this.tag = new NBTTagCompound();
        setMaterial(material);
        setData(data);
    }

    public ItemBuilder(Material material, int amount, byte data) {
        this.tag = new NBTTagCompound();
        setMaterial(material);
        setData(data);
        setAmount(amount);
    }

    public int getAmount() {
        return amount;
    }

    private ItemBuilder setAmount(int amount) {
        Preconditions.checkState(amount > 0 && amount <= 128);
        this.amount = amount;
        tag.setByte("Count", (byte) amount);
        return this;
    }

    public Material getMaterial() {
        return material;
    }

    public ItemBuilder setMaterial(Material material) {
        Preconditions.checkNotNull(material);
        this.material = material;
        tag.setString("id", String.valueOf(material.getId()));
        return this;
    }

    public byte getData() {
        return data;
    }

    public ItemBuilder setData(byte data) {
        Preconditions.checkState(data > 0);
        this.data = data;
        tag.setShort("Damage", data);
        return this;
    }

    public NBTTagCompound getCompound() {
        return tag;
    }
}
