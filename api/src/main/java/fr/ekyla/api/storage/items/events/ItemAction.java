package fr.ekyla.api.storage.items.events;

import fr.ekyla.api.events.player.items.ItemClickEvent;
import fr.ekyla.api.player.EkylaPlayer;
import fr.ekyla.api.storage.items.Item;

public interface ItemAction {

    EkylaPlayer getUser();
    ItemClickEvent.ClickType getClick();
    Item getItem();

    /**
     * Either a player or a block
     * @return the clicked object
     * @see EkylaPlayer
     * @see net.minecraft.server.v1_12_R1.Block
     */
    Object getTarget();

    boolean isCancelled();
    ItemAction setCancelled(boolean cancelled);

}
