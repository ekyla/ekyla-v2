package fr.ekyla.api.storage;

import fr.ekyla.api.storage.items.Item;
import fr.ekyla.api.storage.items.ItemBuilder;
import net.minecraft.server.v1_12_R1.ItemStack;

import java.util.Optional;

/**
 * File <b>StorageManager</b> located on fr.ekyla.api.storage
 * StorageManager is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 01/08/2019 at 21:59
 */
public interface StorageManager {

    Optional<Item> getItem(ItemStack itemStack);

    Optional<Item> getItem(int id);

    Item createItem(ItemBuilder itemBuilder);

    /**
     * Create a storage
     * @return the storage
     */
    Menu createMenu(int lines, String name);

    Item emptyItem();
}
