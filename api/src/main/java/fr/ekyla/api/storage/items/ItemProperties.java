package fr.ekyla.api.storage.items;

import fr.ekyla.api.callbacks.Callback;
import fr.ekyla.api.storage.items.events.ItemAction;
import net.minecraft.server.v1_12_R1.NBTBase;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;

import java.util.List;
import java.util.Map;

/**
 * File <b>ItemProperties</b> located on fr.ekyla.api.storage.items
 * ItemProperties is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 02/08/2019 at 22:16
 */
public interface ItemProperties {

    ItemProperties addEnchant(Enchantment enchantment, int level);

    Map<Enchantment, Integer> getEnchants();

    ItemProperties addItemFlag(ItemFlag... flags);

    List<ItemFlag> getItemFlags();

    String getName();

    ItemProperties setName(String name);

    ItemProperties addNBTTag(String key, NBTBase nbtTag);

    Material getMaterial();

    int getAmount();

    ItemProperties setAmount(int amount);

    short getDurability();

    ItemProperties setDurability(short durability);

    byte getData();

    ItemProperties setData(byte data);

    boolean isGlowing();

    ItemProperties setGlowing(boolean glowing);

    boolean isUnbreakable();

    ItemProperties setUnbreakable(boolean unbreakable);

    ItemProperties usePlayerHead();

    Callback<ItemAction> getAction();

    ItemProperties setAction(Callback<ItemAction> callback);


}
