package fr.ekyla.api.storage;

import fr.ekyla.api.storage.items.Item;

public interface PlayerStorage extends Storage {

    /**
     * Item in the main hand
     * @return item
     * @see Item
     */
    Item getItemInHand();

    /**
     * Item in the second hand
     * @return item
     * @see Item
     */
    Item getItemInOffHand();

    /**
     * Set the item in the main hand
     * @param item the new item
     * @return this
     */
    PlayerStorage setItemInMainHand(Item item);

    /**
     * Set the item in the off hand
     * @param item the new item
     * @return this
     */
    PlayerStorage setItemInOffHand(Item item);

}
