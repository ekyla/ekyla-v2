package fr.ekyla.api.storage;

import fr.ekyla.api.storage.items.Item;

import java.util.List;

/**
 * File <b>Storage</b> located on fr.ekyla.api.storage
 * Storage is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 02/08/2019 at 19:06
 */
public interface Storage {

    default int getRow(int position) {
        return position / 9;
    }

    default int getColumn(int position) {
        return position - getRow(position) * 9 + 1;
    }

    int getSize();

    Storage addItem(Item item);

    default Storage setItem(int position, Item item) {
        return setItem(getRow(position), getColumn(position), item);
    }

    Storage setItem(int row, int column, Item item);

    Storage removeItem(int row, int column);

    default Storage removeItem(int position) {
        return removeItem(getRow(position), getColumn(position));
    }

    List<Item> getItems();

}
