package fr.ekyla.api.storage.items;

import net.minecraft.server.v1_12_R1.ItemStack;

import java.util.function.Consumer;

/**
 * File <b>Item</b> located on fr.ekyla.api.storage
 * Item is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 01/08/2019 at 21:57
 */
public interface Item {

    int getId();

    ItemStack toItemStack();

    ItemProperties getProperties();

    Item withProperties(Consumer<ItemProperties> properties);

}
