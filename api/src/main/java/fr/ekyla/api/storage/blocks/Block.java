package fr.ekyla.api.storage.blocks;

import fr.ekyla.api.positions.Position;
import net.minecraft.server.v1_12_R1.BlockPosition;
import org.bukkit.Material;

public class Block {

    private final Position position;
    private final Material material;

    public Block(BlockPosition blPos, net.minecraft.server.v1_12_R1.Block bl) {
        this.position = new Position(blPos.getX(), blPos.getY(), blPos.getZ());
        this.material = Material.getMaterial(bl.getName().toUpperCase());
    }

    public Block(Position position, Material material) {
        this.position = position;
        this.material = material;
    }

    public Position getPosition() {
        return position;
    }

    public Material getMaterial() {
        return material;
    }

    @Override
    public String toString() {
        return "Block{" +
                "position=" + position +
                ", material=" + material +
                '}';
    }
}
