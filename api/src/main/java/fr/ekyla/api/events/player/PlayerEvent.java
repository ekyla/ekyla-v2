package fr.ekyla.api.events.player;

import fr.ekyla.api.player.EkylaPlayer;
import org.bukkit.event.Event;

/**
 * File <b>PlayerEvent</b> located on fr.ekyla.api.events.player
 * PlayerEvent is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 02/08/2019 at 10:17
 */
public abstract class PlayerEvent extends Event {

    private final EkylaPlayer player;


    protected PlayerEvent(EkylaPlayer player) {
        this.player = player;
    }

    public EkylaPlayer getPlayer() {
        return player;
    }
    
}
