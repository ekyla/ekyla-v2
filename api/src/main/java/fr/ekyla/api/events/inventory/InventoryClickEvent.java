package fr.ekyla.api.events.inventory;

import fr.ekyla.api.player.EkylaPlayer;
import fr.ekyla.api.storage.items.Item;
import net.minecraft.server.v1_12_R1.IInventory;
import org.bukkit.event.HandlerList;

/**
 * File <b>InventoryClickEvent</b> located on fr.ekyla.api.events
 * InventoryClickEvent is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 02/08/2019 at 10:13
 */
public class InventoryClickEvent extends InventoryEvent {

    private static final HandlerList handlers = new HandlerList();
    private final EkylaPlayer player;
    private final Item item;

    public InventoryClickEvent(IInventory inventory, EkylaPlayer player, Item item) {
        super(inventory);
        this.player = player;
        this.item = item;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public Item getItem() {
        return item;
    }

    public EkylaPlayer getPlayer() {
        return player;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

}
