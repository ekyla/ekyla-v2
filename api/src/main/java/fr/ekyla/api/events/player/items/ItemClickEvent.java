package fr.ekyla.api.events.player.items;

import fr.ekyla.api.player.EkylaPlayer;
import fr.ekyla.api.storage.blocks.Block;
import fr.ekyla.api.storage.items.Item;
import org.bukkit.event.HandlerList;

public class ItemClickEvent extends ItemEvent {

    private static final HandlerList handlers = new HandlerList();
    private final Block block;
    private final ClickType click;

    public ItemClickEvent(EkylaPlayer player, Item item, Block block, ClickType click) {
        super(player, item);
        this.block = block;
        this.click = click;
    }

    public Block getBlock() {
        return block;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public ClickType getClick() {
        return click;
    }

    public enum ClickType {
        LEFT_CLICK,
        RIGHT_CLICK,
    }

}
