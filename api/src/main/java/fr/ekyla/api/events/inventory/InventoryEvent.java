package fr.ekyla.api.events.inventory;

import net.minecraft.server.v1_12_R1.IInventory;
import org.bukkit.event.Event;

/**
 * File <b>InventoryEvent</b> located on fr.ekyla.api.events.inventory
 * InventoryEvent is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 02/08/2019 at 10:16
 */
public abstract class InventoryEvent extends Event {

    private IInventory inventory;

    public InventoryEvent(IInventory inventory) {
        this.inventory = inventory;
    }

    public IInventory getInventory() {
        return inventory;
    }
    
}
