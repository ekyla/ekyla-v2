package fr.ekyla.api.events.player.items;

import fr.ekyla.api.events.player.PlayerEvent;
import fr.ekyla.api.player.EkylaPlayer;
import fr.ekyla.api.storage.items.Item;
import org.bukkit.event.Event;

public abstract class ItemEvent extends PlayerEvent {

    private final Item item;

    public ItemEvent(EkylaPlayer player, Item item) {
        super(player);
        this.item = item;
    }

    public Item getItem() {
        return item;
    }
}
