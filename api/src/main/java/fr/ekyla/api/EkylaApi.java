package fr.ekyla.api;

import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import fr.ekyla.api.announce.Announcer;
import fr.ekyla.api.commands.Command;
import fr.ekyla.api.error.ErrorHandler;
import fr.ekyla.api.error.ListenerErrorHandler;
import fr.ekyla.api.i18n.Translator;
import fr.ekyla.api.i18n.i18nLoader;
import fr.ekyla.api.module.ModuleManager;
import fr.ekyla.api.net.ProtocolManager;
import fr.ekyla.api.player.EkylaPlayer;
import fr.ekyla.api.player.PlayerInfo;
import fr.ekyla.api.server.ServerType;
import fr.ekyla.api.services.jedis.JedisService;
import fr.ekyla.api.services.rabbitmq.RabbitMQService;
import fr.ekyla.api.services.sql.SqlService;
import fr.ekyla.api.storage.StorageManager;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * File <b>Api</b> located on fr.ekyla.api
 * Api is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 31/07/2019 at 17:24
 */
public abstract class EkylaApi extends JavaPlugin implements ModuleManager {

    private static EkylaApi instance;
    protected int serverId;
    protected String motd;
    protected ServerType serverType;
    protected int maxPlayer;
    private ErrorHandler errorHandler;

    protected EkylaApi() {
        if (instance != null) throw new UnsupportedOperationException("Instance can't be redefined");
        instance = this;
        errorHandler = Throwable::printStackTrace;
    }

    public abstract PlayerInfo getPlayerInfo(String username);

    public static EkylaApi getInstance() {
        return instance;
    }

    public abstract Announcer getAnnouncer();

    public abstract SqlService getSql();

    public int getServerId() {
        return serverId;
    }

    public void setServerId(int serverId) {
        this.serverId = serverId;
    }

    public String getMotd() {
        return motd;
    }

    public void setMotd(String motd) {
        this.motd = motd;
    }

    public int getMaxPlayer() {
        return maxPlayer;
    }

    public void setMaxPlayer(int maxPlayer) {
        this.maxPlayer = maxPlayer;
    }

    public ServerType getServerType() {
        return serverType;
    }

    public void setServerType(ServerType serverType) {
        this.serverType = serverType;
    }

    public abstract StorageManager getStorageManager();

    public abstract ProtocolManager getProtocolManager();

    public abstract Optional<EkylaPlayer> getPlayer(UUID playerId);

    public abstract Optional<EkylaPlayer> getPlayer(String username);

    public abstract List<EkylaPlayer> getPlayers();

    public abstract RabbitMQService getRabbitMQ();

    public abstract JedisService getJedis();

    public void registerEvent(Listener listener) {
        Preconditions.checkNotNull(listener);
        getPluginLoader().createRegisteredListeners(listener, this).forEach((event, listeners) -> getHandlerList(event).registerAll(listeners.stream().map(l -> new ListenerErrorHandler(l, errorHandler)).collect(Collectors.toList())));
    }

    private HandlerList getHandlerList(Class<? extends Event> event) {
        while (event.getSuperclass() != null && Event.class.isAssignableFrom(event.getSuperclass())) {
            try {
                Method method = event.getDeclaredMethod("getHandlerList");
                method.setAccessible(true);
                return (HandlerList) method.invoke(null);
            } catch (ReflectiveOperationException e) {
                event = event.getSuperclass().asSubclass(Event.class);
            }
        }
        throw new IllegalStateException("No HandlerList for " + event.getName());
    }

    public abstract Translator getTranslator();

    public abstract i18nLoader geti18nLoader();

    public abstract Gson getJson();

    public abstract Command registerCommand(String name);

    public abstract List<Command> getCommands();
}
