package fr.ekyla.api.i18n;

import fr.ekyla.api.EkylaApi;

/**
 * File <b>Locale</b> located on fr.ekyla.api.i18n
 * Locale is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 02/08/2019 at 19:20
 */
public enum Lang {

    FRENCH("fr-FR"),
    ENGLISH("en-GB")
    ;

    private final String name;

    Lang(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String format(String key, Object... params) {
        return EkylaApi.getInstance().getTranslator().format(this, key, params);
    }

}
