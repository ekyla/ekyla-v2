package fr.ekyla.api.i18n;

import java.util.Arrays;
import java.util.List;

/**
 * File <b>TranslateMessage</b> located on fr.ekyla.api.i18n
 * TranslateMessage is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 17/08/2019 at 21:31
 */
public class TranslateMessage {

    private String key;
    private List<Object> parameters;

    public TranslateMessage(String key, Object... parameters) {
        this.key = key;
        this.parameters = Arrays.asList(parameters);
    }

    public String format(Lang lang) {
        return lang.format(key, parameters.toArray());
    }

    public TranslateMessage addParameter(Object parameter) {
        this.parameters.add(parameter);
        return this;
    }

    public TranslateMessage removeParameter(Object parameter) {
        this.parameters.remove(parameter);
        return this;
    }

    public String getKey() {
        return key;
    }

    public List<Object> getParameterss() {
        return parameters;
    }

}
