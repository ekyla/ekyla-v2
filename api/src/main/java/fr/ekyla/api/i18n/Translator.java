package fr.ekyla.api.i18n;

import javax.annotation.concurrent.ThreadSafe;

/**
 * File <b>Translator</b> located on fr.ekyla.api.i18n
 * Translator is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 02/08/2019 at 19:23
 */

@ThreadSafe
public interface Translator {

    String format(Lang lang, String key, Object... params);

}
