package fr.ekyla.api.i18n;

import org.bukkit.plugin.Plugin;

/**
 * File <b>i18nLoader</b> located on fr.ekyla.api.i18n
 * i18nLoader is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 02/08/2019 at 19:25
 */
public interface i18nLoader {

    void loadLangs(Plugin plugin);

    void addProperty(Lang lang, String key, String value);

}
