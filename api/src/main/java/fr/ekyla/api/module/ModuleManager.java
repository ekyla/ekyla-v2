package fr.ekyla.api.module;

import java.util.Optional;

/**
 * File <b>ModuleManager1</b> located on fr.ekyla.api.module
 * ModuleManager1 is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 31/07/2019 at 17:26
 */
public interface ModuleManager {

    <T extends Module> Optional<Module> getModule(Class<T> module);

    <T extends Module> T registerModule(Class<T> module);
    <T extends Module> void unregisterModule(Class<T> module);
}
