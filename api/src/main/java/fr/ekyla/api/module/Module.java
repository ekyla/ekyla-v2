package fr.ekyla.api.module;

import fr.ekyla.api.EkylaApi;

/**
 * File <b>Module</b> located on fr.ekyla.api.module
 * Module is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 31/07/2019 at 17:30
 */
public abstract class Module {

    protected final EkylaApi api;
    protected boolean enabled;


    protected Module(EkylaApi api) {
        this.api = api;
        this.enabled = false;
    }

    public abstract void onLoad();
    public abstract void onEnable();
    public abstract void onDisable();

}
