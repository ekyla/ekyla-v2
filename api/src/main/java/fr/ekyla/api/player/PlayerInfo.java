package fr.ekyla.api.player;

import java.util.List;

/**
 * File <b>PlayerInfo</b> located on fr.ekyla.api.player
 * PlayerInfo is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 30/08/2019 at 12:27
 */
public interface PlayerInfo {

    boolean isOnline();

    List<Rank> getRanks();
}
