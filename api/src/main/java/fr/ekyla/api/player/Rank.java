package fr.ekyla.api.player;

import fr.ekyla.api.EkylaApi;
import fr.ekyla.api.i18n.Lang;
import net.md_5.bungee.api.ChatColor;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * File <b>Rank</b> located on fr.ekyla.api.player
 * Rank is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 17/08/2019 at 21:13
 */
public enum Rank {

    PLAYER(100, "player", ChatColor.GRAY, null),

    ADMIN(1, "administrator", ChatColor.RED, PLAYER, "player.server.join.message", "player.server.quit.message", "admin.ranks.manage"),
    ;

    private final String key;
    private final ChatColor color;
    private final Rank inherited;
    private final String[] permissions;
    private final int id;

    Rank(int id, String key, ChatColor color, Rank inherited, String... permissions) {
        this.id = id;
        this.key = key;
        this.color = color;
        this.inherited = inherited;
        this.permissions = permissions;
    }

    public static Optional<Rank> getById(Integer id) {
        return Arrays.stream(values()).filter(rank -> rank.id == id).findFirst();
    }

    public int getId() {
        return id;
    }

    public List<String> getPermissions() {
        List<String> perms = Arrays.asList(permissions);
        getInherited().ifPresent(rank -> perms.addAll(rank.getPermissions()));
        return perms;
    }

    public Optional<Rank> getInherited() {
        return Optional.ofNullable(inherited);
    }

    public ChatColor getColor() {
        return color;
    }

    public String getKey() {
        return "ranks." + key;
    }

    public String getName() {
        return getKey() + ".name";
    }

    public String getName(Lang lang) {
        return EkylaApi.getInstance().getTranslator().format(lang, getName());
    }

}
