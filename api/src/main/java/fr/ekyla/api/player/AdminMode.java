package fr.ekyla.api.player;

import fr.ekyla.api.positions.Area;
import fr.ekyla.api.positions.Position;

public interface AdminMode {

    void toggle();
    boolean isAdminMode();

    Area getSelection();
    Position getSelect1();
    Position getSelect2();

}
