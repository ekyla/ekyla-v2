package fr.ekyla.api.player;

import fr.ekyla.api.i18n.Lang;
import fr.ekyla.api.i18n.TranslateMessage;
import fr.ekyla.api.player.chat.DialogMessage;
import fr.ekyla.api.positions.Position;
import fr.ekyla.api.server.Server;
import fr.ekyla.api.storage.PlayerStorage;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;

import java.net.InetSocketAddress;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * File <b>IPlayer</b> located on fr.ekyla.api.player
 * IPlayer is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 31/07/2019 at 17:47
 */
public interface EkylaPlayer {

    UUID getUuid();

    int getPlayerId();

    String getUsername();

    CraftPlayer getCraftPlayer();

    void disconnect();

    PlayerStorage getStorage();

    Lang getLang();

    default EkylaPlayer sendMessage(String message) {
        sendMessage(new TranslateMessage(message));
        return this;
    }

    EkylaPlayer sendMessage(TranslateMessage message);

    List<Rank> getRanks();

    Rank getRank();

    boolean hasPermission(String permssion);

    boolean addPersmmsion(String permission);

    boolean removePermission(String permission);

    default String getName() {
        return getCraftPlayer().getName();
    }

    void startDialog(Map<String, DialogMessage> messages, String firstMessageKey);

    void setNextDialogMessage(String messsageKey);

    void endDialog();

    EkylaPlayer addRank(Rank rank);

    InetSocketAddress getAddress();

    List<String> getPermissions();

    EkylaPlayer removeRank(Rank rank);

    AdminMode getAdminMode();

    Position getPosition();

    EkylaPlayer teleport(Position position);
    
    void send(Server server);

}
