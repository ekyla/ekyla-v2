package fr.ekyla.api.player.chat;

import fr.ekyla.api.callbacks.DualCallback;
import fr.ekyla.api.i18n.TranslateMessage;
import fr.ekyla.api.player.EkylaPlayer;

public class DialogMessage {

    private TranslateMessage message;
    private long delay;
    private boolean waitForAnswer;
    private DualCallback<EkylaPlayer, String> action;

    public DialogMessage(TranslateMessage message, long delay, boolean waitForAnswer) {
        this.message = message;
        this.delay = delay;
        this.waitForAnswer = waitForAnswer;
        this.action = null;
    }

    public DialogMessage(TranslateMessage message, long delay, boolean waitForAnswer, DualCallback<EkylaPlayer, String> action) {
        this.message = message;
        this.delay = delay;
        this.waitForAnswer = waitForAnswer;
        this.action = action;
    }

    public TranslateMessage getMessage() {
        return message;
    }

    public long getDelay() {
        return delay;
    }

    public boolean waitForAnswer() {
        return waitForAnswer;
    }

    public DualCallback<EkylaPlayer, String> getAction() {
        return action;
    }
}

