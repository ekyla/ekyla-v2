package fr.ekyla.api.net.packet.status.server;

import fr.ekyla.api.net.GamePacket;
import fr.ekyla.api.net.PacketType;
import fr.ekyla.api.net.wrapper.ServerPingWrapper;
import net.minecraft.server.v1_12_R1.PacketStatusOutServerInfo;
import net.minecraft.server.v1_12_R1.ServerPing;

/**
 * File <b>ServerPingPacket</b> located on fr.ekyla.api.net.packet.status.server
 * ServerPingPacket is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 01/08/2019 at 11:10
 */
public class ServerPingPacket extends GamePacket<PacketStatusOutServerInfo> {

    private ServerPingWrapper wrapper;

    protected ServerPingPacket(PacketStatusOutServerInfo handle) {
        super(handle);
    }

    public ServerPingWrapper getResponse() {
        if (wrapper == null) wrapper = new ServerPingWrapper((ServerPing) getHandleValue("b"));
        return wrapper;
    }

    @Override
    public PacketType getPacketType() {
        return PacketType.Status.Server.SERVER_INFO;
    }
}
