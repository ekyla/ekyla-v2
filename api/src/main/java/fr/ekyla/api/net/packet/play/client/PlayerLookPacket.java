package fr.ekyla.api.net.packet.play.client;

import fr.ekyla.api.net.GamePacket;
import fr.ekyla.api.net.PacketType;
import fr.ekyla.api.positions.Position;
import net.minecraft.server.v1_12_R1.PacketPlayInFlying;

import static fr.ekyla.api.net.PacketType.Play.Client.PLAYER_LOOK;

public class PlayerLookPacket extends GamePacket<PacketPlayInFlying.PacketPlayInLook> {

    protected PlayerLookPacket(PacketPlayInFlying.PacketPlayInLook handle) {
        super(handle);
    }

    public Position getPosition(Position position) {
        return new Position(handle.a(position.getX()), handle.b(position.getY()), handle.c(position.getZ()), handle.a(position.getYaw()), handle.b(position.getPitch()));
    }

    @Override
    public PacketType getPacketType() {
        return PLAYER_LOOK;
    }
}
