package fr.ekyla.api.net.packet.play.client;

import fr.ekyla.api.net.GamePacket;
import fr.ekyla.api.net.PacketType;
import fr.ekyla.api.storage.blocks.Block;
import net.minecraft.server.v1_12_R1.BlockPosition;
import net.minecraft.server.v1_12_R1.EnumDirection;
import net.minecraft.server.v1_12_R1.PacketPlayInBlockDig;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_12_R1.CraftServer;

public class PlayerDigPacket extends GamePacket<PacketPlayInBlockDig> {

    protected PlayerDigPacket(PacketPlayInBlockDig handle) {
        super(handle);
    }

    public Block getBlock() {
        BlockPosition position = handle.a();
        net.minecraft.server.v1_12_R1.Block block = ((CraftServer) Bukkit.getServer()).getServer().getWorld().c(position).getBlock();
        return new Block(position, block);
    }

    @Override
    public PacketType getPacketType() {
        return PacketType.Play.Client.PLAYER_DIG;
    }

    public PacketPlayInBlockDig.EnumPlayerDigType getDigState() {
        return handle.c();
    }

    public EnumDirection getBlockFace() {
        return handle.b();
    }

}
