package fr.ekyla.api.net.packet.play.server;

import fr.ekyla.api.net.GamePacket;
import fr.ekyla.api.net.PacketType;
import fr.ekyla.api.player.EkylaPlayer;
import fr.ekyla.api.positions.Position;
import net.minecraft.server.v1_12_R1.PacketPlayOutPosition;
import net.minecraft.server.v1_12_R1.PlayerConnection;

import java.lang.reflect.Field;
import java.util.HashSet;

import static fr.ekyla.api.net.PacketType.Play.Server.PLAYER_POSITION;

public class PlayerPositionPacket extends GamePacket<PacketPlayOutPosition> {


    public PlayerPositionPacket(PacketPlayOutPosition handle) {
        super(handle);
    }

    public PlayerPositionPacket(double x, double y, double z, float yaw, float pitch, int id) {
        super(new PacketPlayOutPosition(x, y, z, yaw, pitch, new HashSet<>(), id));
    }

    public PlayerPositionPacket(Position position, int id) {
        super(new PacketPlayOutPosition(position.getX(), position.getY(), position.getZ(), position.getYaw(), position.getPitch(), new HashSet<>(), id));
    }

    public PlayerPositionPacket setX(double x) {
        setHandleValue("a", x);
        return this;
    }

    public PlayerPositionPacket setY(double y) {
        setHandleValue("b", y);
        return this;
    }

    public PlayerPositionPacket setz(double z) {
        setHandleValue("c", z);
        return this;
    }

    public PlayerPositionPacket setYaw(float yaw) {
        setHandleValue("d", yaw);
        return this;
    }

    public PlayerPositionPacket setPitch(float pitch) {
        setHandleValue("e", pitch);
        return this;
    }

    public PlayerPositionPacket setId(EkylaPlayer player) {
        try {
            PlayerConnection connection = player.getCraftPlayer().getHandle().playerConnection;
            Class<? extends PlayerConnection> clazz = connection.getClass();
            Field tpAwait = clazz.getDeclaredField("teleportAwait");
            tpAwait.setAccessible(true);
            int id = tpAwait.getInt(connection);
            if (id + 1 == Integer.MAX_VALUE) tpAwait.setInt(connection, 0);
            else tpAwait.setInt(connection, id + 1);
            setHandleValue("g", id + 1);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return this;
    }

    @Override
    public PacketType getPacketType() {
        return PLAYER_POSITION;
    }
}
