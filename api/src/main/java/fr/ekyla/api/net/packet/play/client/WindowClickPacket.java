package fr.ekyla.api.net.packet.play.client;

import fr.ekyla.api.net.GamePacket;
import fr.ekyla.api.net.PacketType;
import net.minecraft.server.v1_12_R1.InventoryClickType;
import net.minecraft.server.v1_12_R1.ItemStack;
import net.minecraft.server.v1_12_R1.PacketPlayInWindowClick;

/**
 * File <b>WindowClickPacket</b> located on fr.ekyla.api.net.packet.play.client
 * WindowClickPacket is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 01/08/2019 at 21:46
 */
public class WindowClickPacket extends GamePacket<PacketPlayInWindowClick> {

    protected WindowClickPacket(PacketPlayInWindowClick handle) {
        super(handle);
    }

    public int getInventoryId() {
        return handle.a();
    }

    public int getSlot() {
        return handle.b();
    }

    public int getButton() {
        return handle.c();
    }

    public short getAction() {
        return handle.d();
    }

    public InventoryClickType getMode() {
        return handle.f();
    }

    public ItemStack getItem() {
        return handle.e();
    }

    @Override
    public PacketType<WindowClickPacket> getPacketType() {
        return PacketType.Play.Client.INVENTORY_CLICK;
    }
}
