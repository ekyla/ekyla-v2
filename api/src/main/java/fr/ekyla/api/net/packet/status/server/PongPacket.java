package fr.ekyla.api.net.packet.status.server;

import fr.ekyla.api.net.GamePacket;
import fr.ekyla.api.net.PacketType;
import net.minecraft.server.v1_12_R1.PacketStatusOutPong;

/**
 * File <b>PongPacket</b> located on fr.ekyla.api.net.packet.status.server
 * PongPacket is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 31/07/2019 at 18:37
 */
public class PongPacket extends GamePacket<PacketStatusOutPong> {

    protected PongPacket(PacketStatusOutPong handle) {
        super(handle);
    }

    public long getPayloadResponse() {
        return (long) getHandleValue("a");
    }

    @Override
    public PacketType getPacketType() {
        return PacketType.Status.Server.PONG;
    }
}
