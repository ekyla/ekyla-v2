package fr.ekyla.api.net.wrapper;

import fr.ekyla.api.utils.NMSReflection;

import java.lang.reflect.Field;

/**
 * File <b>Wrapper</b> located on fr.ekyla.api.net
 * Wrapper is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 31/07/2019 at 17:48
 */
public abstract class Wrapper<T> {

    protected final T handle;

    public Wrapper(T handle) {
        this.handle = handle;
    }

    /**
     * Return the label of the given fieldname
     *
     * @param field a label to get from this field
     * @return an object, if founded
     * @see NMSReflection#getFieldAccessor(Field)
     */
    protected final Object getHandleValue(String field) {
        return NMSReflection.getFieldAccessor(handle.getClass(), field).get(handle);
    }

    /**
     * Get the label from a given field
     *
     * @param field    the field name to get label from
     * @param instance the object to get field
     * @return the label if found
     * @see #getHandleValue(String, Object)
     */
    protected final Object getHandleValue(String field, Object instance) {
        return NMSReflection.getFieldAccessor(instance.getClass(), field).get(instance);
    }

    /**
     * Set a new label to the given field
     *
     * @param field the field name to update
     * @param value the new label
     * @see NMSReflection#getFieldAccessor(Field)
     */
    protected final void setHandleValue(String field, Object value) {
        NMSReflection.getFieldAccessor(handle.getClass(), field).set(handle, value);
    }

    /**
     * Set a new label to the given field in the given instance
     *
     * @param field    the field name to update
     * @param instance an instance to set new label
     * @param value    the new label
     * @see NMSReflection#getFieldAccessor(Field)
     */
    protected final void setHandleValue(String field, Object instance, Object value) {
        NMSReflection.getFieldAccessor(instance.getClass(), field).set(instance, value);
    }

}
