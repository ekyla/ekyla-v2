package fr.ekyla.api.net.packet.play.client;

import fr.ekyla.api.net.GamePacket;
import fr.ekyla.api.net.PacketType;
import fr.ekyla.api.storage.blocks.Block;
import net.minecraft.server.v1_12_R1.BlockPosition;
import net.minecraft.server.v1_12_R1.EnumDirection;
import net.minecraft.server.v1_12_R1.EnumHand;
import net.minecraft.server.v1_12_R1.PacketPlayInUseItem;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_12_R1.CraftServer;

public class UseItemPacket extends GamePacket<PacketPlayInUseItem> {

    protected UseItemPacket(PacketPlayInUseItem handle) {
        super(handle);
    }

    public Block getBlock() {
        BlockPosition position = handle.a();
        net.minecraft.server.v1_12_R1.Block block = ((CraftServer) Bukkit.getServer()).getServer().getWorld().c(position).getBlock();
        return new Block(position, block);
    }

    public EnumDirection getFace() {
        return handle.b();
    }

    public EnumHand getHand() {
        return handle.c();
    }

    @Override
    public PacketType<UseItemPacket> getPacketType() {
        return PacketType.Play.Client.USE_ITEM;
    }
}
