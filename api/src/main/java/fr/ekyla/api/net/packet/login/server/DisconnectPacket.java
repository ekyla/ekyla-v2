package fr.ekyla.api.net.packet.login.server;

import fr.ekyla.api.net.GamePacket;
import fr.ekyla.api.net.PacketType;
import net.minecraft.server.v1_12_R1.IChatBaseComponent;
import net.minecraft.server.v1_12_R1.PacketLoginOutDisconnect;

/**
 * File <b>DisconnectInPacket</b> located on fr.ekyla.api.net.packet.login.play
 * DisconnectInPacket is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 27/08/2019 at 12:31
 */
public class DisconnectPacket extends GamePacket<PacketLoginOutDisconnect> {

    protected DisconnectPacket(PacketLoginOutDisconnect handle) {
        super(handle);
    }

    public IChatBaseComponent getReason() {
        return (IChatBaseComponent) getHandleValue("a");
    }

    @Override
    public PacketType getPacketType() {
        return PacketType.Login.Server.DISCONNECT;
    }
}
