package fr.ekyla.api.net;

import fr.ekyla.api.EkylaApi;
import fr.ekyla.api.player.EkylaPlayer;
import org.bukkit.entity.Player;

/**
 * File <b>PacketEvent</b> located on fr.ekyla.api.net
 * PacketEvent is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 31/07/2019 at 18:31
 */
public class PacketEvent<T extends GamePacket> {

    private final EkylaPlayer player;
    private final T packet;
    private boolean cancelled = false;

    public PacketEvent(Player player, T packet) {
        this.player = EkylaApi.getInstance().getPlayer(player.getUniqueId()).orElseThrow(() -> new NullPointerException("Unknown Player"));
        this.packet = packet;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public T getPacket() {
        return packet;
    }

    public EkylaPlayer getPlayer() {
        return player;
    }
}
