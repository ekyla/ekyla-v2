package fr.ekyla.api.net.packet.status.client;

import fr.ekyla.api.net.GamePacket;
import fr.ekyla.api.net.PacketType;
import net.minecraft.server.v1_12_R1.PacketStatusInPing;

/**
 * File <b>PacketCLientPing</b> located on fr.ekyla.api.net.packet.status.client
 * PacketCLientPing is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 31/07/2019 at 18:37
 */
public class PingPacket extends GamePacket<PacketStatusInPing> {

    protected PingPacket(PacketStatusInPing handle) {
        super(handle);
    }

    public long getPayload() {
        return handle.a();
    }

    @Override
    public PacketType getPacketType() {
        return PacketType.Status.Client.PING;
    }
}
