package fr.ekyla.api.net;

import fr.ekyla.api.player.EkylaPlayer;

/**
 * File <b>ProtocolManager</b> located on fr.ekyla.api.net
 * ProtocolManager is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 31/07/2019 at 17:43
 */
public interface ProtocolManager {

    <T extends GamePacket> void sendPacket(EkylaPlayer player, T packet);

    void subscribeHandler(PacketHandler<? extends GamePacket> packetHandler);
    void unsubscribeHandler(PacketHandler<? extends GamePacket> packetHandler);

}
