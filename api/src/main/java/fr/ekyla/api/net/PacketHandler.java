package fr.ekyla.api.net;

/**
 * File <b>PacketHandler</b> located on fr.ekyla.api.net
 * PacketHandler is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 31/07/2019 at 18:28
 */
public abstract class PacketHandler<T extends GamePacket> {


    private final PacketType packetType;


    protected PacketHandler(PacketType packetType) {
        this.packetType = packetType;
    }

    public PacketType getPacketType() {
        return packetType;
    }

    public abstract void handlePacket(PacketEvent<T> event);

}
