package fr.ekyla.api.net;

import fr.ekyla.api.EkylaApi;
import fr.ekyla.api.net.wrapper.Wrapper;
import fr.ekyla.api.player.EkylaPlayer;
import net.minecraft.server.v1_12_R1.Packet;

import java.util.stream.Stream;

/**
 * File <b>GamePacket</b> located on fr.ekyla.api.net
 * GamePacket is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 31/07/2019 at 17:47
 */
public abstract class GamePacket<T extends Packet> extends Wrapper<T> {

    protected T handle;

    protected GamePacket(T handle) {
        super(handle);
        this.handle = handle;
    }

    public T getPacket() {
        return handle;
    }

    public abstract PacketType getPacketType();

    public void send(EkylaPlayer player) {
        EkylaApi.getInstance().getProtocolManager().sendPacket(player, this);
    }

    public void send(Stream<EkylaPlayer> players) {
        players.forEach(this::send);
    }

}
