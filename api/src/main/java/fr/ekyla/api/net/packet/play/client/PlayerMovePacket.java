package fr.ekyla.api.net.packet.play.client;

import fr.ekyla.api.net.GamePacket;
import fr.ekyla.api.net.PacketType;
import fr.ekyla.api.positions.Position;
import net.minecraft.server.v1_12_R1.PacketPlayInFlying;

import static fr.ekyla.api.net.PacketType.Play.Client.PLAYER_MOVE;

public class PlayerMovePacket extends GamePacket<PacketPlayInFlying.PacketPlayInPosition> {

    public PlayerMovePacket(PacketPlayInFlying.PacketPlayInPosition handle) {
        super(handle);
    }

    public Position getPosition(Position position) {
        return new Position(handle.a(position.getX()), handle.b(position.getY()), handle.c(position.getZ()), handle.a(position.getYaw()), handle.b(position.getPitch()));
    }

    @Override
    public PacketType getPacketType() {
        return PLAYER_MOVE;
    }
}
