package fr.ekyla.api.net.wrapper;

import com.mojang.authlib.GameProfile;
import net.minecraft.server.v1_12_R1.IChatBaseComponent;
import net.minecraft.server.v1_12_R1.ServerPing;

/**
 * File <b>ServerPingWrapper</b> located on fr.ekyla.api.net.wrapper
 * ServerPingWrapper is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 01/08/2019 at 11:12
 */
public class ServerPingWrapper extends Wrapper<ServerPing> {

    public ServerPingWrapper(ServerPing handle) {
        super(handle);
    }

    public String getDescription() {
        return getHandleValue("a").toString();
    }

    public void setDescription(IChatBaseComponent component) {
        handle.setMOTD(component);
    }

    public String getFaviconPath() {
        return (String) getHandleValue("d");
    }

    public void setFaviconPath(String faviconPath) {
        handle.setFavicon(faviconPath);
    }

    public int getMaxPlayer() {
        return handle.b().a();
    }

    public void setMaxPlayer(int maxPlayer) {
        setHandleValue("a", handle.b(), maxPlayer);
    }

    public ServerPing getNMS() {
        return handle;
    }

    public int getOnlinePlayer() {
        return handle.b().b();
    }

    public void setOnlinePlayer(int onlinePlayer) {
        setHandleValue("b", handle.b(), onlinePlayer);
    }

    public int getProtocolversion() {
        return handle.getServerData().getProtocolVersion();
    }

    public String getProtocolversionName() {
        return handle.getServerData().a();
    }

    public GameProfile[] getSamples() {
        return handle.b().c();
    }

    public void setProtocolVersion(int protocolVersion) {
        setHandleValue("b", handle.getServerData(), protocolVersion);
    }

    public void setProtocolVersionName(String protocolVersionName) {
        setHandleValue("a", handle.getServerData(), protocolVersionName);
    }

}
