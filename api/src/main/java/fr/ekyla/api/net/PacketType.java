package fr.ekyla.api.net;

import fr.ekyla.api.net.packet.login.server.DisconnectPacket;
import fr.ekyla.api.net.packet.play.client.*;
import fr.ekyla.api.net.packet.play.server.PlayerPositionPacket;
import fr.ekyla.api.net.packet.server.HandShakePacket;
import fr.ekyla.api.net.packet.status.client.PingPacket;
import fr.ekyla.api.net.packet.status.server.PongPacket;
import fr.ekyla.api.net.packet.status.server.ServerPingPacket;
import net.minecraft.server.v1_12_R1.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static fr.ekyla.api.net.PacketType.Bound.IN;
import static fr.ekyla.api.net.PacketType.Bound.OUT;
import static fr.ekyla.api.net.PacketType.Protocol.*;

/**
 * File <b>PacketType</b> located on fr.ekyla.api.net
 * PacketType is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 31/07/2019 at 17:58
 */


public class PacketType<T extends GamePacket> {

    private static final Map<Class<? extends Packet>, Class<? extends GamePacket>> packetList = new HashMap<>();
    private static Map<Class<? extends Packet>, Class<? extends GamePacket>> unmodifiablePacketList;
    private final Class<T> packet;
    private final Class<? extends Packet> nmsPacket;
    private final Protocol protocol;
    private final Bound bound;

    public PacketType(Class<T> packet, Class<? extends Packet> nmsPacket, Protocol protocol, Bound bound) {
        this.protocol = protocol;
        this.nmsPacket = nmsPacket;
        this.packet = packet;
        this.bound = bound;
        packetList.put(nmsPacket, packet);
    }

    /**
     * @return an immutable Map access (read only)
     * @see Collections#unmodifiableMap(Map)
     */
    public static Map<Class<? extends Packet>, Class<? extends GamePacket>> getPacketList() {
        return unmodifiablePacketList == null ? unmodifiablePacketList = Collections.unmodifiableMap(packetList) : unmodifiablePacketList;
    }

    /**
     * @return the packet's bound
     */
    public Bound getBound() {
        return bound;
    }

    /**
     * @return this packet name
     */
    public String getID() {
        return nmsPacket.getSimpleName();
    }

    /**
     * @return the packet wrapper's class
     */
    public Class<T> getPacket() {
        return packet;
    }

    /**
     * @return the packet wrapper's protocol
     */
    public Protocol getProtocol() {
        return protocol;
    }

    public enum Protocol {
        /**
         * Id: -1
         */
        HANDSHAKE,
        /**
         * Id: 0
         */
        PLAY,
        /**
         * Id: 1
         */
        STATUS,
        /**
         * Id: 2
         */
        LOGIN
    }

    public enum Bound {
        /**
         * Incomming buffer
         */
        IN,

        /**
         * Outcomming buffer
         */
        OUT
    }

    public static final class Status {

        public static final class Client {

            public static final PacketType<PingPacket> PING = new PacketType<>(PingPacket.class, PacketStatusInPing.class, STATUS, IN);
            /*
            public static final PacketType<StartPacket> START = new PacketType<>(StartPacket.class, PacketStatusInStart.class, STATUS, IN);

             */
        }

        public static final class Server {

            public static final PacketType<PongPacket> PONG = new PacketType<>(PongPacket.class, PacketStatusOutPong.class, STATUS, OUT);
            public static final PacketType<ServerPingPacket> SERVER_INFO = new PacketType<>(ServerPingPacket.class, PacketStatusOutServerInfo.class, STATUS, OUT);

        }

    }

    public static final class Login {

        public static final class Client {
            /*
            public static final PacketType<EncryptionResponsePacket> ENCRYPION_RESPONSE = new PacketType<>(EncryptionResponsePacket.class, PacketLoginInEncryptionBegin.class, LOGIN, IN);
            public static final PacketType<LoginStartPacket>         LOGIN_START        = new PacketType<>(LoginStartPacket.class, PacketLoginInStart.class, LOGIN, IN);

             */
        }

        public static final class Server {

            public static final PacketType<DisconnectPacket> DISCONNECT = new PacketType<>(DisconnectPacket.class, PacketLoginOutDisconnect.class, LOGIN, OUT);
            /*
            public static final PacketType<EncryptionRequestPacket> ENCRYPTION_REQUEST = new PacketType<>(EncryptionRequestPacket.class, PacketLoginOutEncryptionBegin.class, LOGIN, OUT);
            public static final PacketType<SetCompressionPacket>    COMPRESSION        = new PacketType<>(SetCompressionPacket.class, PacketLoginOutSetCompression.class, LOGIN, OUT);
            public static final PacketType<SuccessPacket>           SUCCESS            = new PacketType<>(SuccessPacket.class, PacketLoginOutSuccess.class, LOGIN, OUT);

             */
        }
    }

    public static final class Handshake {

        public static final class Server {

            public static final PacketType<HandShakePacket> HANDSHAKE = new PacketType<>(HandShakePacket.class, PacketHandshakingInSetProtocol.class, Protocol.HANDSHAKE, OUT);

        }

    }

    public static final class Play {

        public static final class Client {

            public static final PacketType<ChatInPacket> CHAT = new PacketType<>(ChatInPacket.class, PacketPlayInChat.class, PLAY, IN);
            public static final PacketType<WindowClickPacket> INVENTORY_CLICK = new PacketType<>(WindowClickPacket.class, PacketPlayInWindowClick.class, PLAY, IN);
            public static final PacketType<UseItemPacket> USE_ITEM = new PacketType<>(UseItemPacket.class, PacketPlayInUseItem.class, PLAY, IN);
            public static final PacketType<PlayerDigPacket> PLAYER_DIG = new PacketType<>(PlayerDigPacket.class, PacketPlayInBlockDig.class, PLAY, IN);
            public static final PacketType<PlayerMovePacket> PLAYER_MOVE = new PacketType<>(PlayerMovePacket.class, PacketPlayInFlying.PacketPlayInPosition.class, PLAY, IN);
            public static final PacketType<PlayerLookPacket> PLAYER_LOOK = new PacketType<>(PlayerLookPacket.class, PacketPlayInFlying.PacketPlayInLook.class, PLAY, IN);

            /*
            public static final PacketType<FlyingInPacket>             FLYING               = new PacketType<>(FlyingInPacket.class, PacketPlayInFlying.class, PLAY, IN);
            public static final PacketType<AbilitiesInPacket>          ABILITIES            = new PacketType<>(AbilitiesInPacket.class, PacketPlayInAbilities.class, PLAY, IN);
            public static final PacketType<WindowInPacket>             CLOSE_WINDOW         = new PacketType<>(WindowInPacket.class, PacketPlayInCloseWindow.class, PLAY, IN);
            public static final PacketType<EnchantItemInPacket>        ENCHANT_ITEM         = new PacketType<>(EnchantItemInPacket.class, PacketPlayInEnchantItem.class, PLAY, IN);
            public static final PacketType<ArmAnimationInPacket>       ARM_ANIMATION        = new PacketType<>(ArmAnimationInPacket.class, PacketPlayInArmAnimation.class, PLAY, IN);
            public static final PacketType<EntityActionInPacket>       ENTITY_ACTION        = new PacketType<>(EntityActionInPacket.class, PacketPlayInEntityAction.class, PLAY, IN);
            public static final PacketType<HeldItemSlotInPacket>       HELD_ITEM_SLOT       = new PacketType<>(HeldItemSlotInPacket.class, PacketPlayInHeldItemSlot.class, PLAY, IN);
            public static final PacketType<KeepAliveInPacket>          KEEP_ALIVE           = new PacketType<>(KeepAliveInPacket.class, PacketPlayInKeepAlive.class, PLAY, IN);
            public static final PacketType<ResourcePackStatusInPacket> RESOURCE_PACK_STATUS = new PacketType<>(ResourcePackStatusInPacket.class, PacketPlayInResourcePackStatus.class, PLAY, IN);
            public static final PacketType<SetCreativeSlotInPacket>    SET_CREATIVE_SLOT    = new PacketType<>(SetCreativeSlotInPacket.class, PacketPlayInSetCreativeSlot.class, PLAY, IN);
            public static final PacketType<TabCompleteInPacket>        TAB_COMPLETE         = new PacketType<>(TabCompleteInPacket.class, PacketPlayInTabComplete.class, PLAY, IN);

             */
        }

        public static final class Server {
            public static final PacketType<PlayerPositionPacket> PLAYER_POSITION = new PacketType<>(PlayerPositionPacket.class, PacketPlayOutPosition.class, PLAY, OUT);

            /*
            public static final PacketType<WindowItemsPacket>                WINDOW_ITEMS      = new PacketType<>(WindowItemsPacket.class, PacketPlayOutWindowItems.class, PLAY, OUT);
            public static final PacketType<SetSlotPacket>                    SET_SLOT          = new PacketType<>(SetSlotPacket.class, PacketPlayOutSetSlot.class, PLAY, OUT);
            public static final PacketType<ChatOutPacket>                    CHAT_OUT          = new PacketType<>(ChatOutPacket.class, PacketPlayOutChat.class, PLAY, OUT);
            public static final PacketType<BossPacket>                       BOSS              = new PacketType<>(BossPacket.class, PacketPlayOutBoss.class, PLAY, OUT);
            public static final PacketType<ScoreboardTeamPacket>             TEAM              = new PacketType<>(ScoreboardTeamPacket.class, PacketPlayOutScoreboardTeam.class, PLAY, OUT);
            public static final PacketType<ScoreboardObjectivePacket>        OBJECTIVE         = new PacketType<>(ScoreboardObjectivePacket.class, PacketPlayOutScoreboardObjective.class, PLAY, OUT);
            public static final PacketType<ScoreboardDisplayObjectivePacket> DISPLAY_OBJECTIVE = new PacketType<>(ScoreboardDisplayObjectivePacket.class, PacketPlayOutScoreboardDisplayObjective.class, PLAY, OUT);
            public static final PacketType<ScoreboardScorePacket>            SCORE             = new PacketType<>(ScoreboardScorePacket.class, PacketPlayOutScoreboardDisplayObjective.class, PLAY, OUT);
            public static final PacketType<TabCompleteOutPacket>             TAB_COMPLETE      = new PacketType<>(TabCompleteOutPacket.class, PacketPlayOutTabComplete.class, PLAY, OUT);
            public static final PacketType<WorldParticlesPacket>             WORLD_PARTICLES   = new PacketType<>(WorldParticlesPacket.class, PacketPlayOutWorldParticles.class, PLAY, OUT);
            public static final PacketType<TitlePacket>                      TITLE             = new PacketType<>(TitlePacket.class, PacketPlayOutTitle.class, PLAY, OUT);
            public static final PacketType<OpenWindowPacket>                 OPEN_WINDOW       = new PacketType<>(OpenWindowPacket.class, PacketPlayOutOpenWindow.class, PLAY, OUT);

             */
        }
    }
}

