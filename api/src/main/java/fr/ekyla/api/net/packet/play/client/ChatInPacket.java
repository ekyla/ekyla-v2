package fr.ekyla.api.net.packet.play.client;

import fr.ekyla.api.net.GamePacket;
import fr.ekyla.api.net.PacketType;
import net.minecraft.server.v1_12_R1.PacketPlayInChat;

/**
 * File <b>ChatInPacket</b> located on fr.ekyla.api.net.packet.play.client
 * ChatInPacket is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 01/08/2019 at 11:30
 */
public class ChatInPacket extends GamePacket<PacketPlayInChat> {

    protected ChatInPacket(PacketPlayInChat handle) {
        super(handle);
    }

    public String getMessage() {
        return handle.a();
    }

    public void setMessage(String message) {
        setHandleValue("a", message);
    }

    @Override
    public PacketType getPacketType() {
        return PacketType.Play.Client.CHAT;
    }
}
