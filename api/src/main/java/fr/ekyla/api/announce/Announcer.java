package fr.ekyla.api.announce;

import fr.ekyla.api.i18n.TranslateMessage;

/**
 * File <b>Announcer</b> located on fr.ekyla.api.announce
 * Announcer is a part of parent.
 * <p>
 * Copyright (c) 2018 MrFeedthecookie.
 *
 * @author MrFeedthecookie, {@literal <mrfeedthecookie@gmail.com>}
 * Created the 17/08/2019 at 21:26
 */
public interface Announcer {

    Announcer broadcastMessage(TranslateMessage message);
    Announcer broadcastBarMessage(TranslateMessage message);

}
